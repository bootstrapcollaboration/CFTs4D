# CFTs4D

## Description

This Mathematica package provides functionality for conformal bootstrap calculations
in 4D Conformal Field Theories. 

## Installation

  1. Download the [CFTs4D.zip](https://gitlab.com/bootstrapcollaboration/CFTs4D/raw/master/CFTs4D.zip)
 file containing the latest build of the package.
  2. Install the package
    * (Automatic) Open Mathematica and choose the menu item `File>Install..`. Specify CFTs4D.zip as Source. Adjust options
as needed and press OK. 
    * (Manual) To install manually, extract the contents of CFTs4D.zip into the folder `CFTs4D` and
put the folder `CFTs4D` under `$UserBaseDirectory/Applications` or `$BaseDirectory/Applications`. Note that
these directories may be hidden.
You can find the values of `$UserBaseDirectory` or `$BaseDirectory` by evaluating these expressions
in any Mathematica notebook.
  3. You may need to restart Mathematica to get access to the package documentation.

After the installation one can access the `CFTs4D` package and use its functionality from any Mathematica
notebook by typing ``<<CFTs4D` `` or ``Needs["CFTs4D`"]``. 

## Documentation

The documentation is included in the package and is automatically available after
installation via Mathematica's help system. The main page for the documentation
is `CFTs4D`.

## Development

The main code of the package is a plain text `.m` file, which can be edited in any
text editor (including Mathematica). The documentation is developed using [Wolfram Workbench](https://www.wolfram.com/workbench/).
Because of this, the repository is essentially a Workbench project.

## Attribution

This package is based on

  - Gabriel Francisco Cuomo, Denis Karateev and Petr Kravchuk, 'General Bootstrap Equations in 4D CFTs,' [arXiv:1705.05401](https://arxiv.org/abs/1705.05401)
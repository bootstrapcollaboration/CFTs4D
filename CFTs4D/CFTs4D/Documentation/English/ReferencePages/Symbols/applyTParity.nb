(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     19452,        760]
NotebookOptionsPosition[     13816,        556]
NotebookOutlinePosition[     14445,        581]
CellTagsIndexPosition[     14366,        576]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["1.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["CFTs4D", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["CFTs4D`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["CFTs4D/ref/applyTParity", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["applyTParity", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"applyTParity", "[", "expr", "]"}]], "InlineFormula"],
 " \[LineSeparator]applies time reversal transformation to tensor structures \
in ",
 Cell[BoxData[
  StyleBox["expr", "TI"]], "InlineFormula"],
 "."
}], "Usage",
 CellChangeTimes->{{3.703020849846116*^9, 3.703020862237774*^9}},
 CellID->982511436],

Cell[TextData[{
 "To use ",
 Cell[BoxData[
  ButtonBox[
   ButtonBox[
    ButtonBox["applyTParity",
     BaseStyle->"Link",
     ButtonData->"paclet:CFTs4D/ref/applyTParity"],
    BaseStyle->"Link",
    ButtonData->"paclet:CFTs4D/ref/applyPParity"],
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/"]], "InlineFormula"],
 ", you first need to load the ",
 ButtonBox["CFTs4D",
  BaseStyle->"Link",
  ButtonData->"paclet:CFTs4D/guide/CFTs4D"],
 " package using ",
 Cell[BoxData[
  RowBox[{
   ButtonBox["Needs",
    BaseStyle->"Link"], "[", "\"\<CFTs4D`\>\"", "]"}]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{{3.703020870306616*^9, 3.703020884572076*^9}, {
  3.703264583465942*^9, 3.703264583752033*^9}},
 CellID->1067943069],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["applyTParity",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/applyTParity"]], "InlineFormula"],
 " works both with EF expressions, CF expressions in terms of \[Xi] and \
\[Eta] or ",
 Cell[BoxData[
  ButtonBox["CF4pt",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/CF4pt"]], "InlineFormula"],
 " structures."
}], "Notes",
 CellChangeTimes->{{3.7030209076495237`*^9, 3.703020934383678*^9}},
 CellID->1610504475],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["applyTParity",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/applyTParity"]], "InlineFormula"],
 " does not apply the complex conjugation to the coefficients of the tensor \
structures (which comes from anti-unitarity of time reversal)."
}], "Notes",
 CellChangeTimes->{{3.703019984791773*^9, 3.703020055329459*^9}, {
   3.703020940590219*^9, 3.70302101508853*^9}, {3.703663602783595*^9, 
   3.703663611887189*^9}, 3.703726807255126*^9},
 CellID->831531457]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell[TextData[ButtonBox["Conformal Partial Waves",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConformalPartialWaves"]], "Tutorials",
 CellChangeTimes->{{3.703866584347806*^9, 3.703866596787722*^9}},
 CellID->1542474150],

Cell[TextData[ButtonBox["Conserved Operators",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConservedOperators"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866522555599*^9}, {
  3.703866599411406*^9, 3.703866599610306*^9}},
 CellID->1375158345],

Cell[TextData[ButtonBox["Differential Basis",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/DifferentialBasis"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866525150303*^9}, {
  3.7038666017019033`*^9, 3.7038666018652353`*^9}},
 CellID->1009189986]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["applyPParity",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/applyPParity"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["applyConjugation",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/applyConjugation"]], "InlineFormula"]
}], "SeeAlso",
 CellChangeTimes->{{3.703021029104735*^9, 3.703021087704274*^9}},
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell["XXXX", "MoreAbout",
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<CFTs4D`\>\"", "]"}]], "Input",
 CellID->1613821206],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["\t", "ExampleDelimiter"],
  $Line = 0; Null]], "ExampleDelimiter",
 CellID->563614333],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  SuperscriptBox["I", 
   RowBox[{"{", 
    RowBox[{"1", ",", "2"}], "}"}]], "//", "applyTParity"}]], "Input",
 CellChangeTimes->{{3.703658912062241*^9, 3.70365891240422*^9}},
 CellLabel->"In[65]:=",
 CellID->1914406751],

Cell[BoxData[
 SuperscriptBox["I", 
  RowBox[{"{", 
   RowBox[{"1", ",", "2"}], "}"}]]], "Output",
 CellChangeTimes->{3.7036586514427023`*^9, 3.703658912863379*^9},
 CellLabel->"Out[65]=",
 CellID->189681386]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"{", 
   RowBox[{
    SubscriptBox["\[Xi]", "1"], ",", 
    SubscriptBox["\[Eta]", "1"], ",", 
    SubscriptBox[
     OverscriptBox["\[Xi]", "\[HorizontalLine]"], "1"], ",", 
    SubscriptBox[
     OverscriptBox["\[Eta]", "\[HorizontalLine]"], "1"]}], "}"}], "//", 
  "applyTParity"}]], "Input",
 CellChangeTimes->{{3.703020149465457*^9, 3.703020180039081*^9}, 
   3.703658915301374*^9},
 CellLabel->"In[66]:=",
 CellID->924005664],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"\[ImaginaryI]", " ", 
    SubscriptBox["\[Xi]", "1"]}], ",", 
   RowBox[{"\[ImaginaryI]", " ", 
    SubscriptBox["\[Eta]", "1"]}], ",", 
   RowBox[{
    RowBox[{"-", "\[ImaginaryI]"}], " ", 
    SubscriptBox[
     OverscriptBox["\[Xi]", "\[HorizontalLine]"], "1"]}], ",", 
   RowBox[{
    RowBox[{"-", "\[ImaginaryI]"}], " ", 
    SubscriptBox[
     OverscriptBox["\[Eta]", "\[HorizontalLine]"], "1"]}]}], "}"}]], "Output",\

 CellChangeTimes->{3.703020182523199*^9, 3.703020424152224*^9, 
  3.703658502292156*^9, 3.703658915847032*^9},
 CellLabel->"Out[66]=",
 CellID->979711969]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["\t", "ExampleDelimiter"],
  $Line = 0; Null]], "ExampleDelimiter",
 CellID->2019753090],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"testA", "=", 
  RowBox[{
   SubsuperscriptBox["K", 
    RowBox[{"{", "3", "}"}], 
    RowBox[{"{", 
     RowBox[{"1", ",", "2"}], "}"}]], "//", "applyTParity"}]}]], "Input",
 CellChangeTimes->{{3.703021094331211*^9, 3.703021117189365*^9}, 
   3.703658922332817*^9},
 CellLabel->"In[67]:=",
 CellID->1536943857],

Cell[BoxData[
 RowBox[{"-", 
  SubsuperscriptBox["K", 
   RowBox[{"{", "3", "}"}], 
   RowBox[{"{", 
    RowBox[{"1", ",", "2"}], "}"}]]}]], "Output",
 CellChangeTimes->{3.703021117437482*^9, 3.7036589235213842`*^9},
 CellLabel->"Out[67]=",
 CellID->506138938]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"testB", "=", 
  RowBox[{
   RowBox[{
    SubsuperscriptBox["K", 
     RowBox[{"{", "3", "}"}], 
     RowBox[{"{", 
      RowBox[{"1", ",", "2"}], "}"}]], "//", "toConformalFrame"}], "//", 
   "applyTParity"}]}]], "Input",
 CellChangeTimes->{{3.7036589265012283`*^9, 3.703658930714559*^9}},
 CellLabel->"In[68]:=",
 CellID->861745886],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "+", 
     OverscriptBox["z", "\[HorizontalLine]"]}], ")"}], " ", 
   SubscriptBox["\[Eta]", "2"], " ", 
   SubscriptBox["\[Xi]", "1"]}], "-", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "+", "z"}], ")"}], " ", 
   SubscriptBox["\[Eta]", "1"], " ", 
   SubscriptBox["\[Xi]", "2"]}]}]], "Output",
 CellChangeTimes->{3.703658931381287*^9},
 CellLabel->"Out[68]=",
 CellID->290306717]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{"testA", "//", "toConformalFrame"}], ")"}], "-", 
  "testB"}]], "Input",
 CellLabel->"In[69]:=",
 CellID->507439533],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{3.703658939295538*^9},
 CellLabel->"Out[69]=",
 CellID->921713869]
}, Open  ]],

Cell["\<\
We see that the definition of time conjugation is consistent in EF and CF \
\>", "ExampleText",
 CellChangeTimes->{{3.7030207939605618`*^9, 3.703020823417478*^9}, {
  3.703658750616255*^9, 3.7036587976890717`*^9}, {3.703658957058395*^9, 
  3.703658966852282*^9}},
 CellID->2057347705]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["applyTParity",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/applyTParity"]], "InlineFormula"],
 " does not complex-conjugate the coefficients"
}], "ExampleText",
 CellChangeTimes->{{3.703726836251136*^9, 3.7037268524544907`*^9}},
 CellID->454131831],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"{", 
   RowBox[{
    RowBox[{"\[ImaginaryI]", " ", 
     SubscriptBox["\[Xi]", "1"]}], ",", 
    SubscriptBox["\[Xi]", "1"]}], "}"}], "//", "applyTParity"}]], "Input",
 CellChangeTimes->{{3.703726856625139*^9, 3.703726873950185*^9}},
 CellLabel->"In[11]:=",
 CellID->2051041925],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"-", 
    SubscriptBox["\[Xi]", "1"]}], ",", 
   RowBox[{"\[ImaginaryI]", " ", 
    SubscriptBox["\[Xi]", "1"]}]}], "}"}]], "Output",
 CellChangeTimes->{{3.703726857926217*^9, 3.703726874198885*^9}},
 CellLabel->"Out[11]=",
 CellID->488435221]
}, Open  ]]
}, Open  ]],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{4, Automatic}, {Automatic, 56}},
CellContext->"Global`",
FrontEndVersion->"11.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (July 28, \
2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[11308, 460, 100, 2, 55, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 14227, 569}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 325, 14, 24, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[908, 38, 68, 1, 29, "CategorizationSection",
 CellID->1122911449],
Cell[979, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1061, 45, 79, 2, 70, "Categorization",
 CellID->605800465],
Cell[1143, 49, 76, 2, 70, "Categorization",
 CellID->468444828],
Cell[1222, 53, 68, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1327, 59, 55, 1, 19, "KeywordsSection",
 CellID->477174294],
Cell[1385, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1467, 68, 65, 1, 19, "TemplatesSection",
 CellID->1872225408],
Cell[1535, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1632, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1717, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1801, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1920, 90, 53, 1, 19, "DetailsSection",
 CellID->307771771],
Cell[1976, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2042, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2114, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2181, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2253, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2317, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2381, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2447, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2528, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2632, 132, 55, 1, 63, "ObjectName",
 CellID->1224892054],
Cell[2690, 135, 369, 11, 74, "Usage",
 CellID->982511436],
Cell[3062, 148, 745, 25, 28, "Notes",
 CellID->1067943069],
Cell[3810, 175, 474, 14, 28, "Notes",
 CellID->1610504475],
Cell[4287, 191, 511, 11, 44, "Notes",
 CellID->831531457]
}, Open  ]],
Cell[CellGroupData[{
Cell[4835, 207, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[4895, 210, 233, 4, 16, "Tutorials",
 CellID->1542474150],
Cell[5131, 216, 277, 5, 16, "Tutorials",
 CellID->1375158345],
Cell[5411, 223, 279, 5, 16, "Tutorials",
 CellID->1009189986]
}, Open  ]],
Cell[CellGroupData[{
Cell[5727, 233, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[5813, 236, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[5908, 242, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[5976, 245, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[6062, 251, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[6120, 254, 422, 12, 22, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[6579, 271, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[6639, 274, 46, 1, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[6722, 280, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[7081, 293, 93, 2, 27, "Input",
 CellID->1613821206],
Cell[CellGroupData[{
Cell[7199, 299, 125, 3, 17, "ExampleDelimiter",
 CellID->563614333],
Cell[CellGroupData[{
Cell[7349, 306, 244, 7, 28, "Input",
 CellID->1914406751],
Cell[7596, 315, 208, 6, 27, "Output",
 CellID->189681386]
}, Open  ]],
Cell[CellGroupData[{
Cell[7841, 326, 465, 14, 35, "Input",
 CellID->924005664],
Cell[8309, 342, 632, 19, 33, "Output",
 CellID->979711969]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[8990, 367, 126, 3, 17, "ExampleDelimiter",
 CellID->2019753090],
Cell[CellGroupData[{
Cell[9141, 374, 334, 10, 31, "Input",
 CellID->1536943857],
Cell[9478, 386, 260, 8, 30, "Output",
 CellID->506138938]
}, Open  ]],
Cell[CellGroupData[{
Cell[9775, 399, 357, 11, 31, "Input",
 CellID->861745886],
Cell[10135, 412, 494, 17, 29, "Output",
 CellID->290306717]
}, Open  ]],
Cell[CellGroupData[{
Cell[10666, 434, 167, 6, 27, "Input",
 CellID->507439533],
Cell[10836, 442, 114, 3, 26, "Output",
 CellID->921713869]
}, Open  ]],
Cell[10965, 448, 294, 6, 22, "ExampleText",
 CellID->2057347705]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[11308, 460, 100, 2, 55, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[11411, 464, 125, 3, 33, "ExampleSection",
 CellID->1293636265],
Cell[11539, 469, 148, 3, 21, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[11712, 476, 127, 3, 21, "ExampleSection",
 CellID->2061341341],
Cell[11842, 481, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[11975, 486, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[12120, 492, 131, 3, 21, "ExampleSection",
 CellID->258228157],
Cell[12254, 497, 142, 3, 21, "ExampleSection",
 CellID->2123667759],
Cell[CellGroupData[{
Cell[12421, 504, 135, 3, 21, "ExampleSection",
 CellID->1305812373],
Cell[12559, 509, 304, 8, 26, "ExampleText",
 CellID->454131831],
Cell[CellGroupData[{
Cell[12888, 521, 313, 9, 27, "Input",
 CellID->2051041925],
Cell[13204, 532, 294, 9, 26, "Output",
 CellID->488435221]
}, Open  ]]
}, Open  ]],
Cell[13525, 545, 140, 3, 33, "ExampleSection",
 CellID->1653164318],
Cell[13668, 550, 132, 3, 21, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)


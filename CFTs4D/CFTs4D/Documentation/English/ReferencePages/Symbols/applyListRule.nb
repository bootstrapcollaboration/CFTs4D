(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     19202,        709]
NotebookOptionsPosition[     14351,        537]
NotebookOutlinePosition[     14980,        562]
CellTagsIndexPosition[     14901,        557]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["1.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["CFTs4D", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["CFTs4D`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["CFTs4D/ref/applyListRule", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["applyListRule", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{
   RowBox[{"applyListRule", "[", "rule", "]"}], "[", "expr", "]"}]], 
  "InlineFormula"],
 " \[LineSeparator]applies the list rule ",
 Cell[BoxData[
  StyleBox["rule", "TI"]], "InlineFormula"],
 " produced by ",
 Cell[BoxData[
  ButtonBox["operatorListRule",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/operatorListRule"]], "InlineFormula"],
 " to ",
 Cell[BoxData[
  StyleBox["expr", "TI"]], "InlineFormula"]
}], "Usage",
 CellChangeTimes->{{3.7030095242691917`*^9, 3.7030095973387003`*^9}, {
  3.703663461478441*^9, 3.703663468463644*^9}},
 CellID->982511436],

Cell[TextData[{
 "To use ",
 Cell[BoxData[
  ButtonBox["applyListRule",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/applyListRule"]], "InlineFormula"],
 ", you first need to load the ",
 ButtonBox["CFTs4D",
  BaseStyle->"Link",
  ButtonData->"paclet:CFTs4D/guide/CFTs4D"],
 " package using ",
 Cell[BoxData[
  RowBox[{
   ButtonBox["Needs",
    BaseStyle->"Link"], "[", "\"\<CFTs4D`\>\"", "]"}]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{{3.7030096108615026`*^9, 3.7030096231390657`*^9}, {
  3.703264575011723*^9, 3.7032645753021297`*^9}},
 CellID->1067943069],

Cell[TextData[{
 Cell[BoxData[
  StyleBox["expr", "TI"]], "InlineFormula"],
 " should be a ",
 Cell[BoxData[
  ButtonBox["List",
   BaseStyle->"Link"]], "InlineFormula"],
 " of ",
 Cell[BoxData[
  ButtonBox["CF4pt",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/CF4pt"]], "InlineFormula"],
 " structures or a single ",
 Cell[BoxData[
  ButtonBox["CF4pt",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/CF4pt"]], "InlineFormula"],
 " structure."
}], "Notes",
 CellChangeTimes->{{3.703009628893187*^9, 3.703009683221874*^9}, {
  3.7037267193696737`*^9, 3.703726740318088*^9}},
 CellID->1136994973],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["applyListRule",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/applyListRule"]], "InlineFormula"],
 " is useful when working with lots of tensor structures. In that case the \
automatic addition of ",
 Cell[BoxData[
  ButtonBox["CF4pt",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/CF4pt"]], "InlineFormula"],
 " structures can be extremely slow. See an example of this in the Possible \
Issues section of examples to ",
 Cell[BoxData[
  ButtonBox["CF4pt",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/CF4pt"]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{{3.7036153257121487`*^9, 3.7036154128021317`*^9}, {
  3.703615595278235*^9, 3.7036156257967463`*^9}},
 CellID->512267862]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell[TextData[ButtonBox["Conformal Partial Waves",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConformalPartialWaves"]], "Tutorials",
 CellChangeTimes->{{3.703866584347806*^9, 3.703866596787722*^9}},
 CellID->1542474150],

Cell[TextData[ButtonBox["Conserved Operators",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConservedOperators"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866522555599*^9}, {
  3.703866599411406*^9, 3.703866599610306*^9}},
 CellID->1375158345],

Cell[TextData[ButtonBox["Differential Basis",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/DifferentialBasis"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866525150303*^9}, {
  3.7038666017019033`*^9, 3.7038666018652353`*^9}},
 CellID->1009189986]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["operatorListRule",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/operatorListRule"]], "InlineFormula"],
 " \[FilledVerySmallSquare]  ",
 Cell[BoxData[
  ButtonBox["operatorRule",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/operatorRule"]], "InlineFormula"]
}], "SeeAlso",
 CellChangeTimes->{{3.703019737536171*^9, 3.703019758233774*^9}},
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell[TextData[ButtonBox["CFTs4D",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/guide/CFTs4D"]], "MoreAbout",
 CellChangeTimes->{{3.703819196363316*^9, 3.70381920279813*^9}},
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<CFTs4D`\>\"", "]"}]], "Input",
 CellLabel->"In[1]:=",
 CellID->91646140],

Cell[BoxData[
 RowBox[{
  RowBox[{"I12ListRule", "=", 
   RowBox[{
    RowBox[{"operatorListRule", "[", "opI4D", "]"}], "[", 
    RowBox[{"1", ",", "2"}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.703009706539772*^9, 3.703009763069541*^9}, {
  3.703009802752323*^9, 3.703009812460247*^9}, {3.7030193987604637`*^9, 
  3.7030194014663486`*^9}, {3.703019442502529*^9, 3.703019463798739*^9}},
 CellLabel->"In[1]:=",
 CellID->550900081],

Cell[TextData[{
 "The input should be a flat list of ",
 Cell[BoxData[
  ButtonBox["CF4pt",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/CF4pt"]], "InlineFormula"],
 " structures"
}], "ExampleText",
 CellChangeTimes->{{3.703019481271237*^9, 3.703019481712256*^9}, {
  3.7030196340001097`*^9, 3.703019704876565*^9}},
 CellID->149814617],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"scalar4pt", "=", 
  RowBox[{"{", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"g", "[", 
      RowBox[{"z", ",", "zb"}], "]"}], "//", 
     RowBox[{"collapseCFStructs", "[", 
      RowBox[{"{", 
       RowBox[{
       "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3", 
        ",", "\[CapitalDelta]4"}], "}"}], "]"}]}], ")"}], "}"}]}]], "Input",
 CellChangeTimes->{{3.703009765069957*^9, 3.70300978935913*^9}, {
  3.703019410441267*^9, 3.7030194315564957`*^9}},
 CellLabel->"In[2]:=",
 CellID->187732253],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"CF4pt", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
     "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3", 
      ",", "\[CapitalDelta]4"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
    RowBox[{"g", "[", 
     RowBox[{"z", ",", 
      OverscriptBox["z", "\[HorizontalLine]"]}], "]"}]}], "]"}], 
  "}"}]], "Output",
 CellChangeTimes->{{3.703019403779614*^9, 3.703019465685994*^9}, 
   3.7030195006059*^9, 3.703019686211686*^9},
 CellLabel->"Out[2]=",
 CellID->1163204724]
}, Open  ]],

Cell[TextData[{
 "The output is again a flat list of ",
 Cell[BoxData[
  ButtonBox["CF4pt",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/CF4pt"]], "InlineFormula"],
 " structures."
}], "ExampleText",
 CellChangeTimes->{{3.703019695305883*^9, 3.703019715771937*^9}},
 CellID->569874416],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"applyListRule", "[", "I12ListRule", "]"}], "[", "scalar4pt", 
  "]"}]], "Input",
 CellChangeTimes->{{3.703009792716728*^9, 3.703009795653947*^9}, {
  3.703019435554644*^9, 3.703019457242262*^9}},
 CellLabel->"In[3]:=",
 CellID->1681710099],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"CF4pt", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{
        RowBox[{"-", 
         FractionBox["1", "2"]}], "+", "\[CapitalDelta]1"}], ",", 
       RowBox[{
        RowBox[{"-", 
         FractionBox["1", "2"]}], "+", "\[CapitalDelta]2"}], ",", 
       "\[CapitalDelta]3", ",", "\[CapitalDelta]4"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "1", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", 
       RowBox[{"-", 
        FractionBox["1", "2"]}], ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"-", 
        FractionBox["1", "2"]}], ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"z", " ", 
      RowBox[{"g", "[", 
       RowBox[{"z", ",", 
        OverscriptBox["z", "\[HorizontalLine]"]}], "]"}]}]}], "]"}], ",", 
   RowBox[{"CF4pt", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{
        RowBox[{"-", 
         FractionBox["1", "2"]}], "+", "\[CapitalDelta]1"}], ",", 
       RowBox[{
        RowBox[{"-", 
         FractionBox["1", "2"]}], "+", "\[CapitalDelta]2"}], ",", 
       "\[CapitalDelta]3", ",", "\[CapitalDelta]4"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "1", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", 
       FractionBox["1", "2"], ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       FractionBox["1", "2"], ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{
      RowBox[{"-", 
       OverscriptBox["z", "\[HorizontalLine]"]}], " ", 
      RowBox[{"g", "[", 
       RowBox[{"z", ",", 
        OverscriptBox["z", "\[HorizontalLine]"]}], "]"}]}]}], "]"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{{3.703009795876398*^9, 3.703009805434092*^9}, {
   3.703019457638064*^9, 3.7030194662985764`*^9}, 3.703019500690096*^9},
 CellLabel->"Out[3]=",
 CellID->65140347]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{4, Automatic}, {Automatic, 28}},
CellContext->"Global`",
FrontEndVersion->"11.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (July 28, \
2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[12831, 479, 100, 2, 55, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 14762, 550}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 325, 14, 24, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[908, 38, 68, 1, 29, "CategorizationSection",
 CellID->1122911449],
Cell[979, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1061, 45, 79, 2, 70, "Categorization",
 CellID->605800465],
Cell[1143, 49, 76, 2, 70, "Categorization",
 CellID->468444828],
Cell[1222, 53, 69, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1328, 59, 55, 1, 19, "KeywordsSection",
 CellID->477174294],
Cell[1386, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1468, 68, 65, 1, 19, "TemplatesSection",
 CellID->1872225408],
Cell[1536, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1633, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1718, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1802, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1921, 90, 53, 1, 19, "DetailsSection",
 CellID->307771771],
Cell[1977, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2043, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2115, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2182, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2254, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2318, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2382, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2448, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2529, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2633, 132, 56, 1, 63, "ObjectName",
 CellID->1224892054],
Cell[2692, 135, 640, 20, 77, "Usage",
 CellID->982511436],
Cell[3335, 157, 586, 19, 28, "Notes",
 CellID->1067943069],
Cell[3924, 178, 614, 21, 28, "Notes",
 CellID->1136994973],
Cell[4541, 201, 763, 21, 66, "Notes",
 CellID->512267862]
}, Open  ]],
Cell[CellGroupData[{
Cell[5341, 227, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[5401, 230, 233, 4, 16, "Tutorials",
 CellID->1542474150],
Cell[5637, 236, 277, 5, 16, "Tutorials",
 CellID->1375158345],
Cell[5917, 243, 279, 5, 16, "Tutorials",
 CellID->1009189986]
}, Open  ]],
Cell[CellGroupData[{
Cell[6233, 253, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[6319, 256, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[6414, 262, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[6482, 265, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[6568, 271, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[6626, 274, 423, 12, 22, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[7086, 291, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[7146, 294, 197, 4, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[7380, 303, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[7739, 316, 114, 3, 27, "Input",
 CellID->91646140],
Cell[7856, 321, 437, 10, 27, "Input",
 CellID->550900081],
Cell[8296, 333, 346, 10, 26, "ExampleText",
 CellID->149814617],
Cell[CellGroupData[{
Cell[8667, 347, 547, 15, 27, "Input",
 CellID->187732253],
Cell[9217, 364, 832, 23, 51, "Output",
 CellID->1163204724]
}, Open  ]],
Cell[10064, 390, 296, 9, 26, "ExampleText",
 CellID->569874416],
Cell[CellGroupData[{
Cell[10385, 403, 274, 7, 27, "Input",
 CellID->1681710099],
Cell[10662, 412, 2120, 61, 114, "Output",
 CellID->65140347]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[12831, 479, 100, 2, 55, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[12934, 483, 125, 3, 33, "ExampleSection",
 CellID->1293636265],
Cell[13062, 488, 148, 3, 21, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[13235, 495, 127, 3, 21, "ExampleSection",
 CellID->2061341341],
Cell[13365, 500, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[13498, 505, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[13643, 511, 131, 3, 21, "ExampleSection",
 CellID->258228157],
Cell[13777, 516, 142, 3, 21, "ExampleSection",
 CellID->2123667759],
Cell[13922, 521, 135, 3, 21, "ExampleSection",
 CellID->1305812373],
Cell[14060, 526, 140, 3, 21, "ExampleSection",
 CellID->1653164318],
Cell[14203, 531, 132, 3, 21, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)


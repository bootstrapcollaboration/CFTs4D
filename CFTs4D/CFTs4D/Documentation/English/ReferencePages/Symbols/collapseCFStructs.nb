(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     24693,        901]
NotebookOptionsPosition[     19500,        715]
NotebookOutlinePosition[     20129,        740]
CellTagsIndexPosition[     20050,        735]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["1.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["CFTs4D", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["CFTs4D`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["CFTs4D/ref/collapseCFStructs", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["collapseCFStructs", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{
   RowBox[{"collapseCFStructs", "[", 
    RowBox[{"{", 
     RowBox[{
      SubscriptBox["\[CapitalDelta]", "1"], ",", 
      SubscriptBox["\[CapitalDelta]", "2"], ",", 
      SubscriptBox["\[CapitalDelta]", "3"], ",", 
      SubscriptBox["\[CapitalDelta]", "4"]}], "}"}], "]"}], "[", "expr", 
   "]"}]], "InlineFormula"],
 " \[LineSeparator]transforms a conformal frame 4-point function ",
 Cell[BoxData[
  StyleBox["expr", "TI"]], "InlineFormula"],
 " given in terms of ",
 Cell[BoxData[
  StyleBox["\[Xi]", "TI"]], "InlineFormula"],
 " and ",
 Cell[BoxData[
  StyleBox["\[Eta]", "TI"]], "InlineFormula"],
 " polarizations into a conformal frame 4-point function expression given in \
terms of ",
 Cell[BoxData[
  ButtonBox["CF4pt",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/CF4pt"]], "InlineFormula"],
 " structures, assuming the scaling dimensions ",
 Cell[BoxData[
  SubscriptBox["\[CapitalDelta]", "i"]], "InlineFormula"],
 " for the operators."
}], "Usage",
 CellChangeTimes->{{3.703022086637991*^9, 3.7030222406354017`*^9}, {
  3.703664208165771*^9, 3.70366423662963*^9}},
 CellID->982511436],

Cell[TextData[{
 "To use ",
 Cell[BoxData[
  ButtonBox[
   ButtonBox["collapseCFStructs",
    BaseStyle->"Link",
    ButtonData->"paclet:CFTs4D/ref/collapseCFStructs"],
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/cfEvaluateInPlane"]], "InlineFormula"],
 ", you first need to load the ",
 ButtonBox["CFTs4D",
  BaseStyle->"Link",
  ButtonData->"paclet:CFTs4D/guide/CFTs4D"],
 " package using ",
 Cell[BoxData[
  RowBox[{
   ButtonBox["Needs",
    BaseStyle->"Link"], "[", "\"\<CFTs4D`\>\"", "]"}]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{
  3.7030220832553053`*^9, {3.703022251213847*^9, 3.7030222627080603`*^9}, {
   3.703264593377901*^9, 3.7032645936780043`*^9}},
 CellID->1067943069]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell[TextData[ButtonBox["Conformal Partial Waves",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConformalPartialWaves"]], "Tutorials",
 CellChangeTimes->{{3.703866584347806*^9, 3.703866596787722*^9}},
 CellID->1542474150],

Cell[TextData[ButtonBox["Conserved Operators",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConservedOperators"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866522555599*^9}, {
  3.703866599411406*^9, 3.703866599610306*^9}},
 CellID->1375158345],

Cell[TextData[ButtonBox["Differential Basis",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/DifferentialBasis"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866525150303*^9}, {
  3.7038666017019033`*^9, 3.7038666018652353`*^9}},
 CellID->1009189986]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["CF4pt",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/CF4pt"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["expandCFStructs",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/expandCFStructs"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["toConformalFrame",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/toConformalFrame"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["toEmbeddingFormalism",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/toEmbeddingFormalism"]], "InlineFormula"]
}], "SeeAlso",
 CellChangeTimes->{{3.7030221645613737`*^9, 3.703022169886985*^9}, {
  3.7030227304514093`*^9, 3.703022821210731*^9}, {3.703035208827606*^9, 
  3.703035214611536*^9}},
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell[TextData[ButtonBox["CFTs4D",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/guide/CFTs4D"]], "MoreAbout",
 CellChangeTimes->{{3.703819196363316*^9, 3.70381920279813*^9}},
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<CFTs4D`\>\"", "]"}]], "Input",
 CellID->1478215121],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["\t", "ExampleDelimiter"],
  $Line = 0; Null]], "ExampleDelimiter",
 CellID->34840611],

Cell["A simple scalar 4-point function", "ExampleText",
 CellChangeTimes->{{3.703022306148361*^9, 3.7030223097550583`*^9}, {
  3.703022427384585*^9, 3.70302243101468*^9}},
 CellID->123226504],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"efScalar4pt", "=", 
  RowBox[{"n4KinematicFactor", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
     "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3", 
      ",", "\[CapitalDelta]4"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"0", ",", "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0", ",", "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0", ",", "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0", ",", "0"}], "}"}]}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7030222900644608`*^9, 3.7030223564981833`*^9}, {
  3.703022389591733*^9, 3.7030224180714073`*^9}},
 CellLabel->"In[40]:=",
 CellID->1297308064],

Cell[BoxData[
 RowBox[{
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"1", ",", "2"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "\[CapitalDelta]1"}], "-", "\[CapitalDelta]2"}], ")"}]}]], 
  " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"1", ",", "3"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "\[CapitalDelta]3"}], "+", "\[CapitalDelta]4"}], ")"}]}]], 
  " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"1", ",", "4"}], "}"}], 
   RowBox[{
    RowBox[{
     FractionBox["1", "2"], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "\[CapitalDelta]1"}], "+", "\[CapitalDelta]2"}], ")"}]}], 
    "+", 
    FractionBox[
     RowBox[{"\[CapitalDelta]3", "-", "\[CapitalDelta]4"}], "2"]}]], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"2", ",", "4"}], "}"}], 
   FractionBox[
    RowBox[{"\[CapitalDelta]1", "-", "\[CapitalDelta]2"}], "2"]], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"3", ",", "4"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "\[CapitalDelta]3"}], "-", "\[CapitalDelta]4"}], 
     ")"}]}]]}]], "Output",
 CellChangeTimes->{{3.7030223230072603`*^9, 3.703022357056299*^9}, 
   3.703022418578972*^9},
 CellLabel->"Out[40]=",
 CellID->731347335]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"efScalar4pt", "//", "toConformalFrame"}], "//", 
   RowBox[{"collapseCFStructs", "[", 
    RowBox[{"{", 
     RowBox[{
     "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3", 
      ",", "\[CapitalDelta]4"}], "}"}], "]"}]}], "//", 
  "simplifyInCF4pt"}]], "Input",
 CellChangeTimes->{{3.703022359333446*^9, 3.7030224045366592`*^9}},
 CellLabel->"In[41]:=",
 CellID->350347378],

Cell[BoxData[
 RowBox[{"CF4pt", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
    "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3", ",",
      "\[CapitalDelta]4"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"z", " ", 
      OverscriptBox["z", "\[HorizontalLine]"]}], ")"}], 
    RowBox[{
     FractionBox["1", "2"], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "\[CapitalDelta]1"}], "-", "\[CapitalDelta]2"}], 
      ")"}]}]]}], "]"}]], "Output",
 CellChangeTimes->{{3.703022373396353*^9, 3.703022422295391*^9}},
 CellLabel->"Out[41]=",
 CellID->1501421569]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["\t", "ExampleDelimiter"],
  $Line = 0; Null]], "ExampleDelimiter",
 CellID->1051744289],

Cell["A scalar-fermion four-point function", "ExampleText",
 CellChangeTimes->{{3.703022443884701*^9, 3.703022458051887*^9}},
 CellID->728366766],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"efSpinor4pt", "=", 
  RowBox[{
   RowBox[{"n4KinematicFactor", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
      "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3", 
       ",", "\[CapitalDelta]4"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"1", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"1", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0"}], "}"}]}], "}"}]}], "]"}], 
   SubsuperscriptBox[
    OverscriptBox["K", "^"], 
    RowBox[{"{", "3", "}"}], 
    RowBox[{"{", 
     RowBox[{"1", ",", "2"}], "}"}]]}]}]], "Input",
 CellChangeTimes->{{3.703022464752563*^9, 3.703022475347973*^9}, {
  3.7030225063085117`*^9, 3.7030225102285433`*^9}},
 CellLabel->"In[37]:=",
 CellID->280437309],

Cell[BoxData[
 RowBox[{
  SubsuperscriptBox[
   OverscriptBox["K", "^"], 
   RowBox[{"{", "3", "}"}], 
   RowBox[{"{", 
    RowBox[{"1", ",", "2"}], "}"}]], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"1", ",", "2"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "1"}], "-", "\[CapitalDelta]1", "-", "\[CapitalDelta]2"}], 
     ")"}]}]], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"1", ",", "3"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "\[CapitalDelta]3"}], "+", "\[CapitalDelta]4"}], ")"}]}]], 
  " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"1", ",", "4"}], "}"}], 
   RowBox[{
    RowBox[{
     FractionBox["1", "2"], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "\[CapitalDelta]1"}], "+", "\[CapitalDelta]2"}], ")"}]}], 
    "+", 
    FractionBox[
     RowBox[{"\[CapitalDelta]3", "-", "\[CapitalDelta]4"}], "2"]}]], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"2", ",", "4"}], "}"}], 
   FractionBox[
    RowBox[{"\[CapitalDelta]1", "-", "\[CapitalDelta]2"}], "2"]], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"3", ",", "4"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "\[CapitalDelta]3"}], "-", "\[CapitalDelta]4"}], 
     ")"}]}]]}]], "Output",
 CellChangeTimes->{3.703022512526533*^9, 3.703664327603428*^9},
 CellLabel->"Out[37]=",
 CellID->138212251]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"efSpinor4pt", "//", "toConformalFrame"}], "//", 
   RowBox[{"collapseCFStructs", "[", 
    RowBox[{"{", 
     RowBox[{
     "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3", 
      ",", "\[CapitalDelta]4"}], "}"}], "]"}]}], "//", 
  "simplifyInCF4pt"}]], "Input",
 CellChangeTimes->{
  3.7030225296483994`*^9, {3.703022570489954*^9, 3.703022594421917*^9}, {
   3.703664317356739*^9, 3.703664336572681*^9}},
 CellLabel->"In[39]:=",
 CellID->479036222],

Cell[BoxData[
 RowBox[{
  RowBox[{"CF4pt", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
     "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3", 
      ",", "\[CapitalDelta]4"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"1", ",", "1", ",", "0", ",", "0"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"-", 
       FractionBox["1", "2"]}], ",", 
      FractionBox["1", "2"], ",", "0", ",", "0"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
    FractionBox[
     RowBox[{
      RowBox[{"(", 
       RowBox[{"1", "-", 
        OverscriptBox["z", "\[HorizontalLine]"]}], ")"}], " ", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"z", " ", 
         OverscriptBox["z", "\[HorizontalLine]"]}], ")"}], 
       RowBox[{
        FractionBox["1", "2"], " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "1"}], "-", "\[CapitalDelta]1", "-", 
          "\[CapitalDelta]2"}], ")"}]}]]}], 
     SqrtBox[
      FractionBox[
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "1"}], "+", "z"}], ")"}], " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "1"}], "+", 
          OverscriptBox["z", "\[HorizontalLine]"]}], ")"}]}], 
       RowBox[{"z", " ", 
        OverscriptBox["z", "\[HorizontalLine]"]}]]]]}], "]"}], "+", 
  RowBox[{"CF4pt", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
     "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3", 
      ",", "\[CapitalDelta]4"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"1", ",", "1", ",", "0", ",", "0"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{
      FractionBox["1", "2"], ",", 
      RowBox[{"-", 
       FractionBox["1", "2"]}], ",", "0", ",", "0"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
    FractionBox[
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", "1"}], "+", "z"}], ")"}], " ", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"z", " ", 
         OverscriptBox["z", "\[HorizontalLine]"]}], ")"}], 
       RowBox[{
        FractionBox["1", "2"], " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "1"}], "-", "\[CapitalDelta]1", "-", 
          "\[CapitalDelta]2"}], ")"}]}]]}], 
     SqrtBox[
      FractionBox[
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "1"}], "+", "z"}], ")"}], " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "1"}], "+", 
          OverscriptBox["z", "\[HorizontalLine]"]}], ")"}]}], 
       RowBox[{"z", " ", 
        OverscriptBox["z", "\[HorizontalLine]"]}]]]]}], "]"}]}]], "Output",
 CellChangeTimes->{
  3.703022530035396*^9, {3.7030225624364033`*^9, 3.703022604013599*^9}, {
   3.703664324399191*^9, 3.7036643377132607`*^9}},
 CellLabel->"Out[39]=",
 CellID->1268291853]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{4, Automatic}, {Automatic, 28}},
CellContext->"Global`",
FrontEndVersion->"11.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (July 28, \
2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[17980, 657, 100, 2, 55, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 19911, 728}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 325, 14, 24, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[908, 38, 68, 1, 29, "CategorizationSection",
 CellID->1122911449],
Cell[979, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1061, 45, 79, 2, 70, "Categorization",
 CellID->605800465],
Cell[1143, 49, 76, 2, 70, "Categorization",
 CellID->468444828],
Cell[1222, 53, 73, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1332, 59, 55, 1, 19, "KeywordsSection",
 CellID->477174294],
Cell[1390, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1472, 68, 65, 1, 19, "TemplatesSection",
 CellID->1872225408],
Cell[1540, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1637, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1722, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1806, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1925, 90, 53, 1, 19, "DetailsSection",
 CellID->307771771],
Cell[1981, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2047, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2119, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2186, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2258, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2322, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2386, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2452, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2533, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2637, 132, 60, 1, 63, "ObjectName",
 CellID->1224892054],
Cell[2700, 135, 1181, 34, 110, "Usage",
 CellID->982511436],
Cell[3884, 171, 713, 23, 28, "Notes",
 CellID->1067943069]
}, Open  ]],
Cell[CellGroupData[{
Cell[4634, 199, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[4694, 202, 233, 4, 16, "Tutorials",
 CellID->1542474150],
Cell[4930, 208, 277, 5, 16, "Tutorials",
 CellID->1375158345],
Cell[5210, 215, 279, 5, 16, "Tutorials",
 CellID->1009189986]
}, Open  ]],
Cell[CellGroupData[{
Cell[5526, 225, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[5612, 228, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[5707, 234, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[5775, 237, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[5861, 243, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[5919, 246, 862, 24, 22, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[6818, 275, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[6878, 278, 197, 4, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[7112, 287, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[7471, 300, 93, 2, 27, "Input",
 CellID->1478215121],
Cell[CellGroupData[{
Cell[7589, 306, 124, 3, 17, "ExampleDelimiter",
 CellID->34840611],
Cell[7716, 311, 191, 3, 22, "ExampleText",
 CellID->123226504],
Cell[CellGroupData[{
Cell[7932, 318, 728, 21, 47, "Input",
 CellID->1297308064],
Cell[8663, 341, 1425, 49, 39, "Output",
 CellID->731347335]
}, Open  ]],
Cell[CellGroupData[{
Cell[10125, 395, 442, 12, 47, "Input",
 CellID->350347378],
Cell[10570, 409, 930, 27, 65, "Output",
 CellID->1501421569]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[11549, 442, 126, 3, 17, "ExampleDelimiter",
 CellID->1051744289],
Cell[11678, 447, 145, 2, 22, "ExampleText",
 CellID->728366766],
Cell[CellGroupData[{
Cell[11848, 453, 893, 27, 52, "Input",
 CellID->280437309],
Cell[12744, 482, 1547, 53, 39, "Output",
 CellID->138212251]
}, Open  ]],
Cell[CellGroupData[{
Cell[14328, 540, 517, 14, 47, "Input",
 CellID->479036222],
Cell[14848, 556, 3071, 94, 203, "Output",
 CellID->1268291853]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[17980, 657, 100, 2, 55, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[18083, 661, 125, 3, 33, "ExampleSection",
 CellID->1293636265],
Cell[18211, 666, 148, 3, 22, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[18384, 673, 127, 3, 22, "ExampleSection",
 CellID->2061341341],
Cell[18514, 678, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[18647, 683, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[18792, 689, 131, 3, 22, "ExampleSection",
 CellID->258228157],
Cell[18926, 694, 142, 3, 22, "ExampleSection",
 CellID->2123667759],
Cell[19071, 699, 135, 3, 22, "ExampleSection",
 CellID->1305812373],
Cell[19209, 704, 140, 3, 22, "ExampleSection",
 CellID->1653164318],
Cell[19352, 709, 132, 3, 22, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)


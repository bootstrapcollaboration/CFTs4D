(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     15409,        584]
NotebookOptionsPosition[     10911,        425]
NotebookOutlinePosition[     11527,        449]
CellTagsIndexPosition[     11448,        444]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["1.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["CFTs4D", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["CFTs4D`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["CFTs4D/ref/H", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["H", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{
   RowBox[{"H", "[", 
    StyleBox[
     RowBox[{
      RowBox[{"{", 
       RowBox[{"p", ",", "e"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"\[CapitalDelta]", ",", "l"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"\[CapitalDelta]", "[", "1", "]"}], ",", 
        RowBox[{"\[CapitalDelta]", "[", "2", "]"}], ",", 
        RowBox[{"\[CapitalDelta]", "[", "3", "]"}], ",", 
        RowBox[{"\[CapitalDelta]", "[", "4", "]"}]}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"n", ",", "m"}], "}"}]}], "MSG"], "]"}], "[", 
   RowBox[{"x", ",", "y"}], "]"}]], "InlineFormula"],
 " \[LineSeparator]represents the ",
 Cell[BoxData[
  RowBox[{"(", 
   RowBox[{
    StyleBox["n", "TI"], ",", 
    StyleBox["m", "TI"]}], ")"}]], "InlineFormula"],
 " derivative of the seed or the dual seed conformal block in ",
 Cell[BoxData[
  RowBox[{"(", 
   RowBox[{"x", ",", "y"}], ")"}]], "InlineFormula"],
 "-variables"
}], "Usage",
 CellChangeTimes->{{3.703656564274555*^9, 3.703656579579918*^9}, {
  3.7036568951359*^9, 3.703656895390633*^9}, {3.703656963057764*^9, 
  3.7036569891947203`*^9}, {3.7061829325969048`*^9, 3.706182936380131*^9}, {
  3.7084322201755333`*^9, 3.708432228639123*^9}},
 CellID->982511436],

Cell[TextData[{
 "To use ",
 Cell[BoxData[
  ButtonBox["H",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/H"]], "InlineFormula"],
 ", you first need to load the ",
 ButtonBox["CFTs4D",
  BaseStyle->"Link",
  ButtonData->"paclet:CFTs4D/guide/CFTs4D"],
 " package using ",
 Cell[BoxData[
  RowBox[{
   ButtonBox["Needs",
    BaseStyle->"Link"], "[", "\"\<CFTs4D`\>\"", "]"}]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{{3.703264387488255*^9, 3.703264393847538*^9}, {
  3.7036160682142467`*^9, 3.703616070932129*^9}},
 CellID->1067943069],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["H",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/H"]], "InlineFormula"],
 " can be differentiated using the standard ",
 Cell[BoxData[
  ButtonBox["D",
   BaseStyle->"Link"]], "InlineFormula"],
 " operator."
}], "Notes",
 CellChangeTimes->{{3.703725978314926*^9, 3.7037260096314497`*^9}},
 CellID->1740390358]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell[TextData[ButtonBox["Conformal Partial Waves",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConformalPartialWaves"]], "Tutorials",
 CellChangeTimes->{{3.703866584347806*^9, 3.703866596787722*^9}},
 CellID->1542474150],

Cell[TextData[ButtonBox["Conserved Operators",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConservedOperators"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866522555599*^9}, {
  3.703866599411406*^9, 3.703866599610306*^9}},
 CellID->1375158345],

Cell[TextData[ButtonBox["Differential Basis",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/DifferentialBasis"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866525150303*^9}, {
  3.7038666017019033`*^9, 3.7038666018652353`*^9}},
 CellID->1009189986]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["G",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/G"]], "InlineFormula"],
 " \[FilledVerySmallSquare]  ",
 Cell[BoxData[
  ButtonBox["plugSeedBlocks",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/plugSeedBlocks"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["plugDualSeedBlocks",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/plugDualSeedBlocks"]], "InlineFormula"]
}], "SeeAlso",
 CellChangeTimes->{
  3.703657000747303*^9, {3.703657038490191*^9, 3.7036570396221457`*^9}},
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell[TextData[ButtonBox["CFTs4D",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/guide/CFTs4D"]], "MoreAbout",
 CellChangeTimes->{{3.703819196363316*^9, 3.70381920279813*^9}},
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<CFTs4D`\>\"", "]"}]], "Input",
 CellLabel->"In[8]:=",
 CellID->1173817566],

Cell["Derivatives are processed automatically", "ExampleText",
 CellChangeTimes->{{3.703726055893744*^9, 3.7037260632634974`*^9}},
 CellID->1844731300],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"D", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"H", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"p", ",", "e"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"\[CapitalDelta]", ",", "l"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"\[CapitalDelta]", "[", "1", "]"}], ",", 
        RowBox[{"\[CapitalDelta]", "[", "2", "]"}], ",", 
        RowBox[{"\[CapitalDelta]", "[", "3", "]"}], ",", 
        RowBox[{"\[CapitalDelta]", "[", "4", "]"}]}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0", ",", "0"}], "}"}]}], "]"}], "[", 
    RowBox[{"z", ",", "zb"}], "]"}], ",", "z"}], "]"}]], "Input",
 CellChangeTimes->{{3.703726023408942*^9, 3.703726050059483*^9}, {
  3.708432527973989*^9, 3.7084325314540854`*^9}},
 CellLabel->"In[75]:=",
 CellID->1854774353],

Cell[BoxData[
 RowBox[{
  RowBox[{"H", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"p", ",", "e"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"\[CapitalDelta]", ",", "l"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"\[CapitalDelta]", "[", "1", "]"}], ",", 
      RowBox[{"\[CapitalDelta]", "[", "2", "]"}], ",", 
      RowBox[{"\[CapitalDelta]", "[", "3", "]"}], ",", 
      RowBox[{"\[CapitalDelta]", "[", "4", "]"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"1", ",", "0"}], "}"}]}], "]"}], "[", 
  RowBox[{"z", ",", 
   OverscriptBox["z", "\[HorizontalLine]"]}], "]"}]], "Output",
 CellChangeTimes->{3.703726050286697*^9, 3.7084325355725737`*^9},
 CellLabel->"Out[75]=",
 CellID->657507832]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{Automatic, 456}, {Automatic, 112}},
CellContext->"Global`",
FrontEndVersion->"11.0 for Linux x86 (64-bit) (September 21, 2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[9391, 367, 100, 2, 56, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 11310, 437}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 325, 14, 24, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[908, 38, 68, 1, 30, "CategorizationSection",
 CellID->1122911449],
Cell[979, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1061, 45, 79, 2, 70, "Categorization",
 CellID->605800465],
Cell[1143, 49, 76, 2, 70, "Categorization",
 CellID->468444828],
Cell[1222, 53, 57, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1316, 59, 55, 1, 20, "KeywordsSection",
 CellID->477174294],
Cell[1374, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1456, 68, 65, 1, 20, "TemplatesSection",
 CellID->1872225408],
Cell[1524, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1621, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1706, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1790, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1909, 90, 53, 1, 20, "DetailsSection",
 CellID->307771771],
Cell[1965, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2031, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2103, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2170, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2242, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2306, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2370, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2436, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2517, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2621, 132, 44, 1, 64, "ObjectName",
 CellID->1224892054],
Cell[2668, 135, 1297, 36, 79, "Usage",
 CellID->982511436],
Cell[3968, 173, 558, 19, 26, "Notes",
 CellID->1067943069],
Cell[4529, 194, 364, 12, 26, "Notes",
 CellID->1740390358]
}, Open  ]],
Cell[CellGroupData[{
Cell[4930, 211, 57, 1, 45, "TutorialsSection",
 CellID->250839057],
Cell[4990, 214, 233, 4, 16, "Tutorials",
 CellID->1542474150],
Cell[5226, 220, 277, 5, 16, "Tutorials",
 CellID->1375158345],
Cell[5506, 227, 279, 5, 16, "Tutorials",
 CellID->1009189986]
}, Open  ]],
Cell[CellGroupData[{
Cell[5822, 237, 83, 1, 32, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[5908, 240, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[6003, 246, 65, 1, 32, "RelatedLinksSection",
 CellID->1584193535],
Cell[6071, 249, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[6157, 255, 55, 1, 32, "SeeAlsoSection",
 CellID->1255426704],
Cell[6215, 258, 601, 18, 20, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[6853, 281, 57, 1, 32, "MoreAboutSection",
 CellID->38303248],
Cell[6913, 284, 197, 4, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[7147, 293, 356, 11, 70, "PrimaryExamplesSection",
 CellID->880084151],
Cell[7506, 306, 116, 3, 27, "Input",
 CellID->1173817566],
Cell[7625, 311, 151, 2, 22, "ExampleText",
 CellID->1844731300],
Cell[CellGroupData[{
Cell[7801, 317, 816, 22, 50, "Input",
 CellID->1854774353],
Cell[8620, 341, 722, 20, 29, "Output",
 CellID->657507832]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[9391, 367, 100, 2, 56, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[9494, 371, 125, 3, 34, "ExampleSection",
 CellID->1293636265],
Cell[9622, 376, 148, 3, 22, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[9795, 383, 127, 3, 22, "ExampleSection",
 CellID->2061341341],
Cell[9925, 388, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[10058, 393, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[10203, 399, 131, 3, 22, "ExampleSection",
 CellID->258228157],
Cell[10337, 404, 142, 3, 22, "ExampleSection",
 CellID->2123667759],
Cell[10482, 409, 135, 3, 22, "ExampleSection",
 CellID->1305812373],
Cell[10620, 414, 140, 3, 22, "ExampleSection",
 CellID->1653164318],
Cell[10763, 419, 132, 3, 22, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)


(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     16986,        639]
NotebookOptionsPosition[     12192,        469]
NotebookOutlinePosition[     12823,        494]
CellTagsIndexPosition[     12744,        489]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["1.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["CFTs4D", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["CFTs4D`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["CFTs4D/ref/invSbS", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["invSbS", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"invSbS", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"i", ",", "j"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{
      SubscriptBox["k", "1"], ",", 
      RowBox[{
       SubscriptBox["k", 
        RowBox[{"2", ","}]], "..."}]}], "}"}]}], "]"}]], "InlineFormula"],
 " \[LineSeparator]represents the normalized version of the EF invariant ",
 Cell[BoxData[
  RowBox[{"\[LeftAngleBracket]", 
   RowBox[{
    RowBox[{
     RowBox[{
      SubscriptBox[
       OverscriptBox["S", "\[HorizontalLine]"], "i"], 
      SubscriptBox["X", 
       SubscriptBox["k", "1"]], 
      SubscriptBox[
       OverscriptBox["X", "\[HorizontalLine]"], 
       SubscriptBox["k", "2"]]}], "..."}], 
    SubscriptBox["S", "j"]}], "\[RightAngleBracket]"}]], "InlineFormula",
  FormatType->"StandardForm"]
}], "Usage",
 CellChangeTimes->{{3.7036266277137423`*^9, 3.703626631192355*^9}},
 CellID->982511436],

Cell[TextData[{
 "To use ",
 Cell[BoxData[
  ButtonBox["invSbS",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/invSbS"]], "InlineFormula"],
 ", you first need to load the ",
 ButtonBox["CFTs4D",
  BaseStyle->"Link",
  ButtonData->"paclet:CFTs4D/guide/CFTs4D"],
 " package using ",
 Cell[BoxData[
  RowBox[{
   ButtonBox["Needs",
    BaseStyle->"Link"], "[", "\"\<CFTs4D`\>\"", "]"}]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{{3.703039193544949*^9, 3.7030392141372538`*^9}, {
  3.7032639561532373`*^9, 3.703263956616253*^9}, {3.7032642341979723`*^9, 
  3.703264240491478*^9}, {3.703264507945855*^9, 3.703264511299938*^9}},
 CellID->1067943069],

Cell[TextData[{
 "The named non-normalized invariants (such as ",
 Cell[BoxData[
  SubsuperscriptBox[
   OverscriptBox["K", "\[HorizontalLine]"], 
   RowBox[{"{", "3", "}"}], 
   RowBox[{"{", 
    RowBox[{"1", ",", "2"}], "}"}]]], "InlineFormula",
  FormatType->"StandardForm"],
 ") can be typed in by using the key combinations Ctrl+6, Ctrl+5 (after \
Ctrl+6), and Ctrl+7. The bar \[HorizontalLine] is entered as \
\[EscapeKey]hline\[EscapeKey] or \[Backslash][HorizontalLine]. "
}], "Notes",
 CellChangeTimes->{{3.70362611704739*^9, 3.703626238690783*^9}},
 CellID->1847721452],

Cell[TextData[{
 "The non-normalized invariants can be converted to normalized ones by using ",
 Cell[BoxData[
  ButtonBox["normalizeInvariants",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/normalizeInvariants"]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{{3.7036255370942698`*^9, 3.703625571439081*^9}, {
  3.703626254990184*^9, 3.7036262700685177`*^9}},
 CellID->358961813]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell[TextData[ButtonBox["Conformal Partial Waves",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConformalPartialWaves"]], "Tutorials",
 CellChangeTimes->{{3.703866584347806*^9, 3.703866596787722*^9}},
 CellID->1542474150],

Cell[TextData[ButtonBox["Conserved Operators",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConservedOperators"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866522555599*^9}, {
  3.703866599411406*^9, 3.703866599610306*^9}},
 CellID->1375158345],

Cell[TextData[ButtonBox["Differential Basis",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/DifferentialBasis"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866525150303*^9}, {
  3.7038666017019033`*^9, 3.7038666018652353`*^9}},
 CellID->1009189986]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["normalizeInvariants",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/normalizeInvariants"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["invSbSnorm",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/invSbSnorm"]], "InlineFormula"]
}], "SeeAlso",
 CellChangeTimes->{{3.703626654526784*^9, 3.703626666254036*^9}},
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell[TextData[ButtonBox["CFTs4D",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/guide/CFTs4D"]], "MoreAbout",
 CellChangeTimes->{{3.703819196363316*^9, 3.70381920279813*^9}},
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<CFTs4D`\>\"", "]"}]], "Input",
 CellID->1133974552],

Cell["Some invariants have special display names", "ExampleText",
 CellChangeTimes->{{3.703625736923336*^9, 3.703625746649746*^9}},
 CellID->1261876492],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"invSbS", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"1", ",", "2"}], "}"}], ",", 
     RowBox[{"{", "}"}]}], "]"}], ",", 
   RowBox[{"invSbS", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"1", ",", "2"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"3", ",", "4"}], "}"}]}], "]"}], ",", 
   RowBox[{"invSbS", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"1", ",", "1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"2", ",", "3"}], "}"}]}], "]"}]}], "}"}]], "Input",
 CellChangeTimes->{{3.7036256979492207`*^9, 3.703625752054421*^9}, {
  3.7036259358623533`*^9, 3.703625952975412*^9}, {3.7036266858156767`*^9, 
  3.7036266906790733`*^9}},
 CellLabel->"In[120]:=",
 CellID->1788242343],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   SuperscriptBox["I", 
    RowBox[{"{", 
     RowBox[{"1", ",", "2"}], "}"}]], ",", 
   SubsuperscriptBox["I", 
    RowBox[{"{", 
     RowBox[{"3", ",", "4"}], "}"}], 
    RowBox[{"{", 
     RowBox[{"1", ",", "2"}], "}"}]], ",", 
   SubsuperscriptBox["J", 
    RowBox[{"{", 
     RowBox[{"2", ",", "3"}], "}"}], 
    RowBox[{"{", "1", "}"}]]}], "}"}]], "Output",
 CellChangeTimes->{{3.703625712961412*^9, 3.703625715145483*^9}, {
   3.703625747809799*^9, 3.7036257523984337`*^9}, 3.7036259532737427`*^9, 
   3.703626690994635*^9},
 CellLabel->"Out[120]=",
 CellID->1267879165]
}, Open  ]],

Cell["\<\
Subsuperscrits can be entered by using Ctrl+6 and then Ctrl+5.\
\>", "ExampleText",
 CellChangeTimes->{{3.703625761516048*^9, 3.703625791580402*^9}, {
  3.703626054949293*^9, 3.703626071976699*^9}, {3.7036266946478357`*^9, 
  3.703626698872753*^9}},
 CellID->452873148],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"{", 
   RowBox[{
    SuperscriptBox["I", 
     RowBox[{"{", 
      RowBox[{"1", ",", "2"}], "}"}]], ",", 
    SubsuperscriptBox["I", 
     RowBox[{"{", 
      RowBox[{"3", ",", "4"}], "}"}], 
     RowBox[{"{", 
      RowBox[{"1", ",", "2"}], "}"}]], ",", 
    SubsuperscriptBox["J", 
     RowBox[{"{", 
      RowBox[{"2", ",", "3"}], "}"}], 
     RowBox[{"{", "1", "}"}]]}], "}"}], "//", "InputForm"}]], "Input",
 CellChangeTimes->{{3.703625755425992*^9, 3.70362584206253*^9}, {
  3.703625957417407*^9, 3.703625968475127*^9}, {3.703626706578972*^9, 
  3.703626715123209*^9}},
 CellLabel->"In[121]:=",
 CellID->1188480413],

Cell["\<\
{invSbS[{1, 2}, {}], invSbS[{1, 2}, {3, 4}], invSbS[{1, 1}, {2, 3}]}\
\>", "Output",
 CellChangeTimes->{{3.7036258288676043`*^9, 3.703625842260448*^9}, 
   3.703625968765191*^9, 3.703626715603086*^9},
 CellLabel->"Out[121]//InputForm=",
 CellID->1926590001]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{Automatic, 52}, {858, Automatic}},
CellContext->"Global`",
FrontEndVersion->"11.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (July 28, \
2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[10672, 411, 100, 2, 55, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 12605, 482}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 325, 14, 24, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[908, 38, 68, 1, 29, "CategorizationSection",
 CellID->1122911449],
Cell[979, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1061, 45, 79, 2, 70, "Categorization",
 CellID->605800465],
Cell[1143, 49, 76, 2, 70, "Categorization",
 CellID->468444828],
Cell[1222, 53, 62, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1321, 59, 55, 1, 19, "KeywordsSection",
 CellID->477174294],
Cell[1379, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1461, 68, 65, 1, 19, "TemplatesSection",
 CellID->1872225408],
Cell[1529, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1626, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1711, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1795, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1914, 90, 53, 1, 19, "DetailsSection",
 CellID->307771771],
Cell[1970, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2036, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2108, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2175, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2247, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2311, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2375, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2441, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2522, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2626, 132, 49, 1, 63, "ObjectName",
 CellID->1224892054],
Cell[2678, 135, 953, 30, 83, "Usage",
 CellID->982511436],
Cell[3634, 167, 667, 20, 28, "Notes",
 CellID->1067943069],
Cell[4304, 189, 579, 14, 47, "Notes",
 CellID->1847721452],
Cell[4886, 205, 400, 10, 28, "Notes",
 CellID->358961813]
}, Open  ]],
Cell[CellGroupData[{
Cell[5323, 220, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[5383, 223, 233, 4, 16, "Tutorials",
 CellID->1542474150],
Cell[5619, 229, 277, 5, 16, "Tutorials",
 CellID->1375158345],
Cell[5899, 236, 279, 5, 16, "Tutorials",
 CellID->1009189986]
}, Open  ]],
Cell[CellGroupData[{
Cell[6215, 246, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[6301, 249, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[6396, 255, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[6464, 258, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[6550, 264, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[6608, 267, 424, 12, 22, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[7069, 284, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[7129, 287, 197, 4, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[7363, 296, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[7722, 309, 93, 2, 27, "Input",
 CellID->1133974552],
Cell[7818, 313, 152, 2, 22, "ExampleText",
 CellID->1261876492],
Cell[CellGroupData[{
Cell[7995, 319, 763, 24, 27, "Input",
 CellID->1788242343],
Cell[8761, 345, 617, 19, 33, "Output",
 CellID->1267879165]
}, Open  ]],
Cell[9393, 367, 279, 6, 22, "ExampleText",
 CellID->452873148],
Cell[CellGroupData[{
Cell[9697, 377, 656, 20, 34, "Input",
 CellID->1188480413],
Cell[10356, 399, 267, 6, 26, "Output",
 CellID->1926590001]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[10672, 411, 100, 2, 55, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[10775, 415, 125, 3, 33, "ExampleSection",
 CellID->1293636265],
Cell[10903, 420, 148, 3, 21, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[11076, 427, 127, 3, 21, "ExampleSection",
 CellID->2061341341],
Cell[11206, 432, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[11339, 437, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[11484, 443, 131, 3, 21, "ExampleSection",
 CellID->258228157],
Cell[11618, 448, 142, 3, 21, "ExampleSection",
 CellID->2123667759],
Cell[11763, 453, 135, 3, 21, "ExampleSection",
 CellID->1305812373],
Cell[11901, 458, 140, 3, 21, "ExampleSection",
 CellID->1653164318],
Cell[12044, 463, 132, 3, 21, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)


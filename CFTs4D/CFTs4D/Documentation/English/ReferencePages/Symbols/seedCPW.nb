(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     21835,        804]
NotebookOptionsPosition[     16918,        629]
NotebookOutlinePosition[     17532,        653]
CellTagsIndexPosition[     17453,        648]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["1.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["CFTs4D", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["CFTs4D`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["CFTs4D/ref/seedCPW", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["seedCPW", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"seedCPW", "[", "p", "]"}]], "InlineFormula"],
 " \[LineSeparator]gives the (dual) seed conformal partial wave for the \
exchange of an operator in the representation ",
 Cell[BoxData[
  RowBox[{"(", 
   RowBox[{"l", ",", 
    RowBox[{"l", "+", "p"}]}], ")"}]], "InlineFormula",
  FormatType->"StandardForm"],
 " or ",
 Cell[BoxData[
  RowBox[{"(", 
   RowBox[{
    RowBox[{"l", "+", "p"}], ",", "l"}], ")"}]], "InlineFormula",
  FormatType->"StandardForm"],
 " in terms of the functions ",
 ButtonBox["G",
  BaseStyle->"Link",
  ButtonData->"paclet:CFTs4D/ref/G"],
 ". "
}], "Usage",
 CellChangeTimes->{{3.7033949251985083`*^9, 3.703394933727457*^9}, 
   3.703395018690413*^9, {3.7033950844661417`*^9, 3.703395087026267*^9}, {
   3.7036224144028273`*^9, 3.703622525621361*^9}},
 CellID->982511436],

Cell[TextData[{
 "To use ",
 Cell[BoxData[
  ButtonBox["seedCPW",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/seedCPW"]], "InlineFormula"],
 ", you first need to load the ",
 ButtonBox["CFTs4D",
  BaseStyle->"Link",
  ButtonData->"paclet:CFTs4D/guide/CFTs4D"],
 " package using ",
 Cell[BoxData[
  RowBox[{
   ButtonBox["Needs",
    BaseStyle->"Link"], "[", "\"\<CFTs4D`\>\"", "]"}]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{{3.703039193544949*^9, 3.7030392141372538`*^9}, {
  3.7032639561532373`*^9, 3.703263956616253*^9}, {3.703264139201261*^9, 
  3.70326414228759*^9}},
 CellID->1067943069],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["seedCPW",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/seedCPW"]], "InlineFormula"],
 " assumes that a the operators have scaling dimensions ",
 Cell[BoxData[
  RowBox[{"\[CapitalDelta]", "[", "i", "]"}]], "InlineFormula",
  FormatType->"StandardForm"],
 "."
}], "Notes",
 CellChangeTimes->{{3.703622530933853*^9, 3.703622553482781*^9}},
 CellID->431419733],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["seedCPW",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/seedCPW"]], "InlineFormula"],
 " assumes that the exchanged operator has scaling dimension ",
 Cell[BoxData[
  StyleBox["\[CapitalDelta]", "TR"]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{{3.703622563126001*^9, 3.703622590292811*^9}},
 CellID->989607971],

Cell[TextData[{
 "The distinction between seed CPW exchanging ",
 Cell[BoxData[
  RowBox[{"(", 
   RowBox[{"l", ",", 
    RowBox[{"l", "+", "p"}]}], ")"}]], "InlineFormula"],
 " and the dual seed CPW exchanging ",
 Cell[BoxData[
  RowBox[{"(", 
   RowBox[{
    RowBox[{"l", "+", "p"}], ",", "l"}], ")"}]], "InlineFormula"],
 " is made when either ",
 Cell[BoxData[
  ButtonBox["plugSeedBlocks",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/plugSeedBlocks"]], "InlineFormula"],
 " or ",
 Cell[BoxData[
  ButtonBox["plugDualSeedBlocks",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/plugDualSeedBlocks"]], "InlineFormula"],
 " is applied (after ",
 Cell[BoxData[
  ButtonBox["changeVariables",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/changeVariables"]], "InlineFormula"],
 ")."
}], "Notes",
 CellChangeTimes->{{3.703622699002612*^9, 3.703622700386862*^9}, {
  3.7036227766101933`*^9, 3.7036228439951687`*^9}},
 CellID->1317478368]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell[TextData[ButtonBox["Conformal Partial Waves",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConformalPartialWaves"]], "Tutorials",
 CellChangeTimes->{{3.703866584347806*^9, 3.703866596787722*^9}},
 CellID->1542474150],

Cell[TextData[ButtonBox["Conserved Operators",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConservedOperators"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866522555599*^9}, {
  3.703866599411406*^9, 3.703866599610306*^9}},
 CellID->1375158345],

Cell[TextData[ButtonBox["Differential Basis",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/DifferentialBasis"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866525150303*^9}, {
  3.7038666017019033`*^9, 3.7038666018652353`*^9}},
 CellID->1009189986]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["plugSeedBlocks",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/plugSeedBlocks"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["plugDualSeedBlocks",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/plugDualSeedBlocks"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["changeVariables",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/changeVariables"]], "InlineFormula"]
}], "SeeAlso",
 CellChangeTimes->{{3.70362285722764*^9, 3.703622890433505*^9}},
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell["XXXX", "MoreAbout",
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<CFTs4D`\>\"", "]"}]], "Input",
 CellChangeTimes->{{3.703394938663739*^9, 3.7033949431699133`*^9}},
 CellLabel->"In[140]:=",
 CellID->2100089815],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["\t", "ExampleDelimiter"],
  $Line = 0; Null]], "ExampleDelimiter",
 CellContext->Notebook,
 CellID->1951555625],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"seedCPW", "[", "0", "]"}]], "Input",
 CellChangeTimes->{{3.703395103049139*^9, 3.7033951054050293`*^9}},
 CellLabel->"In[141]:=",
 CellID->1704514717],

Cell[BoxData[
 RowBox[{
  RowBox[{"G", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"0", ",", "0"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"\[CapitalDelta]", ",", "l"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"\[CapitalDelta]", "[", "1", "]"}], ",", 
      RowBox[{"\[CapitalDelta]", "[", "2", "]"}], ",", 
      RowBox[{"\[CapitalDelta]", "[", "3", "]"}], ",", 
      RowBox[{"\[CapitalDelta]", "[", "4", "]"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"0", ",", "0"}], "}"}]}], "]"}], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"1", ",", "2"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", 
       RowBox[{"\[CapitalDelta]", "[", "1", "]"}]}], "-", 
      RowBox[{"\[CapitalDelta]", "[", "2", "]"}]}], ")"}]}]], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"1", ",", "3"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", 
       RowBox[{"\[CapitalDelta]", "[", "3", "]"}]}], "+", 
      RowBox[{"\[CapitalDelta]", "[", "4", "]"}]}], ")"}]}]], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"1", ",", "4"}], "}"}], 
   RowBox[{
    RowBox[{
     FractionBox["1", "2"], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", 
        RowBox[{"\[CapitalDelta]", "[", "1", "]"}]}], "+", 
       RowBox[{"\[CapitalDelta]", "[", "2", "]"}]}], ")"}]}], "+", 
    RowBox[{
     FractionBox["1", "2"], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"\[CapitalDelta]", "[", "3", "]"}], "-", 
       RowBox[{"\[CapitalDelta]", "[", "4", "]"}]}], ")"}]}]}]], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"2", ",", "4"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"\[CapitalDelta]", "[", "1", "]"}], "-", 
      RowBox[{"\[CapitalDelta]", "[", "2", "]"}]}], ")"}]}]], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"3", ",", "4"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", 
       RowBox[{"\[CapitalDelta]", "[", "3", "]"}]}], "-", 
      RowBox[{"\[CapitalDelta]", "[", "4", "]"}]}], ")"}]}]]}]], "Output",
 CellChangeTimes->{3.703395106081153*^9, 3.706183132010943*^9},
 CellLabel->"Out[141]=",
 CellID->431293981]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["\t", "ExampleDelimiter"],
  $Line = 0; Null]], "ExampleDelimiter",
 CellContext->Notebook,
 CellID->1625876627],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"seedCPW", "[", "1", "]"}]], "Input",
 CellChangeTimes->{{3.703395110672331*^9, 3.7033951108239594`*^9}},
 CellLabel->"In[142]:=",
 CellID->342493424],

Cell[BoxData[
 RowBox[{
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"1", ",", "2"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", 
       FractionBox["1", "2"]}], "-", 
      RowBox[{"\[CapitalDelta]", "[", "1", "]"}], "-", 
      RowBox[{"\[CapitalDelta]", "[", "2", "]"}]}], ")"}]}]], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{
     RowBox[{"G", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"1", ",", "1"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"\[CapitalDelta]", ",", "l"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"\[CapitalDelta]", "[", "1", "]"}], ",", 
         RowBox[{"\[CapitalDelta]", "[", "2", "]"}], ",", 
         RowBox[{"\[CapitalDelta]", "[", "3", "]"}], ",", 
         RowBox[{"\[CapitalDelta]", "[", "4", "]"}]}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"0", ",", "0"}], "}"}]}], "]"}], " ", 
     SuperscriptBox["I", 
      RowBox[{"{", 
       RowBox[{"4", ",", "2"}], "}"}]]}], "-", 
    FractionBox[
     RowBox[{"2", " ", 
      RowBox[{"G", "[", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"1", ",", "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"\[CapitalDelta]", ",", "l"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"\[CapitalDelta]", "[", "1", "]"}], ",", 
          RowBox[{"\[CapitalDelta]", "[", "2", "]"}], ",", 
          RowBox[{"\[CapitalDelta]", "[", "3", "]"}], ",", 
          RowBox[{"\[CapitalDelta]", "[", "4", "]"}]}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"0", ",", "0"}], "}"}]}], "]"}], " ", 
      SubsuperscriptBox["I", 
       RowBox[{"{", 
        RowBox[{"3", ",", "1"}], "}"}], 
       RowBox[{"{", 
        RowBox[{"4", ",", "2"}], "}"}]]}], 
     SubscriptBox["X", 
      RowBox[{"{", 
       RowBox[{"1", ",", "3"}], "}"}]]]}], ")"}], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"1", ",", "3"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      FractionBox["1", "2"], "-", 
      RowBox[{"\[CapitalDelta]", "[", "3", "]"}], "+", 
      RowBox[{"\[CapitalDelta]", "[", "4", "]"}]}], ")"}]}]], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"1", ",", "4"}], "}"}], 
   RowBox[{
    RowBox[{
     FractionBox["1", "2"], " ", 
     RowBox[{"(", 
      RowBox[{
       FractionBox["1", "2"], "-", 
       RowBox[{"\[CapitalDelta]", "[", "1", "]"}], "+", 
       RowBox[{"\[CapitalDelta]", "[", "2", "]"}]}], ")"}]}], "+", 
    RowBox[{
     FractionBox["1", "2"], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", 
        FractionBox["1", "2"]}], "+", 
       RowBox[{"\[CapitalDelta]", "[", "3", "]"}], "-", 
       RowBox[{"\[CapitalDelta]", "[", "4", "]"}]}], ")"}]}]}]], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"2", ",", "4"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", 
       FractionBox["1", "2"]}], "+", 
      RowBox[{"\[CapitalDelta]", "[", "1", "]"}], "-", 
      RowBox[{"\[CapitalDelta]", "[", "2", "]"}]}], ")"}]}]], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"3", ",", "4"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", 
       FractionBox["1", "2"]}], "-", 
      RowBox[{"\[CapitalDelta]", "[", "3", "]"}], "-", 
      RowBox[{"\[CapitalDelta]", "[", "4", "]"}]}], ")"}]}]]}]], "Output",
 CellChangeTimes->{3.703395111127716*^9, 3.706183134585902*^9},
 CellLabel->"Out[142]=",
 CellID->1837872765]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{Automatic, 0}, {Automatic, 56}},
CellContext->"Global`",
FrontEndVersion->"11.0 for Linux x86 (64-bit) (September 21, 2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[15398, 571, 100, 2, 56, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 17314, 641}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 325, 14, 24, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[908, 38, 68, 1, 30, "CategorizationSection",
 CellID->1122911449],
Cell[979, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1061, 45, 79, 2, 70, "Categorization",
 CellID->605800465],
Cell[1143, 49, 76, 2, 70, "Categorization",
 CellID->468444828],
Cell[1222, 53, 63, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1322, 59, 55, 1, 20, "KeywordsSection",
 CellID->477174294],
Cell[1380, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1462, 68, 65, 1, 20, "TemplatesSection",
 CellID->1872225408],
Cell[1530, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1627, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1712, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1796, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1915, 90, 53, 1, 20, "DetailsSection",
 CellID->307771771],
Cell[1971, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2037, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2109, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2176, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2248, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2312, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2376, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2442, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2523, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2627, 132, 50, 1, 64, "ObjectName",
 CellID->1224892054],
Cell[2680, 135, 864, 26, 93, "Usage",
 CellID->982511436],
Cell[3547, 163, 620, 20, 26, "Notes",
 CellID->1067943069],
Cell[4170, 185, 412, 12, 26, "Notes",
 CellID->431419733],
Cell[4585, 199, 378, 11, 26, "Notes",
 CellID->989607971],
Cell[4966, 212, 965, 30, 64, "Notes",
 CellID->1317478368]
}, Open  ]],
Cell[CellGroupData[{
Cell[5968, 247, 57, 1, 45, "TutorialsSection",
 CellID->250839057],
Cell[6028, 250, 233, 4, 16, "Tutorials",
 CellID->1542474150],
Cell[6264, 256, 277, 5, 16, "Tutorials",
 CellID->1375158345],
Cell[6544, 263, 279, 5, 16, "Tutorials",
 CellID->1009189986]
}, Open  ]],
Cell[CellGroupData[{
Cell[6860, 273, 83, 1, 32, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[6946, 276, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[7041, 282, 65, 1, 32, "RelatedLinksSection",
 CellID->1584193535],
Cell[7109, 285, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[7195, 291, 55, 1, 32, "SeeAlsoSection",
 CellID->1255426704],
Cell[7253, 294, 600, 17, 20, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[7890, 316, 57, 1, 32, "MoreAboutSection",
 CellID->38303248],
Cell[7950, 319, 46, 1, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[8033, 325, 356, 11, 70, "PrimaryExamplesSection",
 CellID->880084151],
Cell[8392, 338, 186, 4, 27, "Input",
 CellID->2100089815],
Cell[CellGroupData[{
Cell[8603, 346, 150, 4, 15, "ExampleDelimiter",
 CellID->1951555625],
Cell[CellGroupData[{
Cell[8778, 354, 174, 4, 27, "Input",
 CellID->1704514717],
Cell[8955, 360, 2367, 74, 104, "Output",
 CellID->431293981]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[11371, 440, 150, 4, 15, "ExampleDelimiter",
 CellID->1625876627],
Cell[CellGroupData[{
Cell[11546, 448, 173, 4, 27, "Input",
 CellID->342493424],
Cell[11722, 454, 3615, 110, 246, "Output",
 CellID->1837872765]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[15398, 571, 100, 2, 56, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[15501, 575, 125, 3, 34, "ExampleSection",
 CellID->1293636265],
Cell[15629, 580, 148, 3, 22, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[15802, 587, 127, 3, 22, "ExampleSection",
 CellID->2061341341],
Cell[15932, 592, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[16065, 597, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[16210, 603, 131, 3, 22, "ExampleSection",
 CellID->258228157],
Cell[16344, 608, 142, 3, 22, "ExampleSection",
 CellID->2123667759],
Cell[16489, 613, 135, 3, 22, "ExampleSection",
 CellID->1305812373],
Cell[16627, 618, 140, 3, 22, "ExampleSection",
 CellID->1653164318],
Cell[16770, 623, 132, 3, 22, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)


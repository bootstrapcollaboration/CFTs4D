(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     19204,        742]
NotebookOptionsPosition[     13887,        552]
NotebookOutlinePosition[     14516,        577]
CellTagsIndexPosition[     14437,        572]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["1.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["CFTs4D", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["CFTs4D`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["CFTs4D/ref/n2CorrelationFunction", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["n2CorrelationFunction", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"n2CorrelationFunction", "[", 
   RowBox[{"\[CapitalDelta]", ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
        SubscriptBox["l", "1"], ",", 
        SubscriptBox["lb", "1"]}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
        SubscriptBox["l", "2"], ",", 
        SubscriptBox["lb", "2"]}], "}"}]}], "}"}]}], "]"}]], 
  "InlineFormula"],
 " \[LineSeparator]returns the 2-point correlation functions with scaling \
dimension ",
 Cell[BoxData[
  StyleBox["\[CapitalDelta]", "TR"]], "InlineFormula"],
 " and spins ",
 Cell[BoxData[
  RowBox[{
   SubscriptBox[
    StyleBox["l", "TI"], 
    StyleBox["1", "TR"]], "=", 
   SubscriptBox[
    StyleBox["lb", "TI"], 
    StyleBox["2", "TR"]]}]], "InlineFormula"],
 " and ",
 Cell[BoxData[
  RowBox[{
   SubscriptBox[
    StyleBox["lb", "TI"], 
    StyleBox["1", "TR"]], "=", 
   SubscriptBox[
    StyleBox["l", "TI"], 
    StyleBox["2", "TR"]]}]], "InlineFormula"],
 ", in the standard normalization."
}], "Usage",
 CellChangeTimes->{{3.703039219009795*^9, 3.703039292176284*^9}, {
  3.703039650771924*^9, 3.703039660325017*^9}},
 CellID->982511436],

Cell[TextData[{
 "To use ",
 Cell[BoxData[
  ButtonBox["n2CorrelationFunction",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n2CorrelationFunction"]], "InlineFormula"],
 ", you first need to load the ",
 ButtonBox["CFTs4D",
  BaseStyle->"Link",
  ButtonData->"paclet:CFTs4D/guide/CFTs4D"],
 " package using ",
 Cell[BoxData[
  RowBox[{
   ButtonBox["Needs",
    BaseStyle->"Link"], "[", "\"\<CFTs4D`\>\"", "]"}]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{{3.703039193544949*^9, 3.7030392141372538`*^9}, {
  3.70326467256245*^9, 3.703264672846766*^9}},
 CellID->1067943069],

Cell[TextData[{
 "If ",
 Cell[BoxData[
  RowBox[{
   SubscriptBox["l", 
    StyleBox["1", "TR"]], "\[NotEqual]", 
   SubscriptBox["lb", 
    StyleBox["2", "TR"]]}]], "InlineFormula"],
 " or ",
 Cell[BoxData[
  RowBox[{
   SubscriptBox["l", "2"], "\[NotEqual]", 
   SubscriptBox["lb", "1"]}]], "InlineFormula"],
 ", then ",
 Cell[BoxData[
  ButtonBox["n2CorrelationFunction",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n2CorrelationFunction"]], "InlineFormula"],
 " returns ",
 Cell[BoxData["0"], "InlineFormula"]
}], "Notes",
 CellChangeTimes->{{3.703039563735073*^9, 3.703039636006892*^9}},
 CellID->625452287],

Cell[TextData[{
 "The 2-point function returned by ",
 Cell[BoxData[
  ButtonBox["n2CorrelationFunction",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n2CorrelationFunction"]], "InlineFormula"],
 " is properly normalized according to unitarity constraints. "
}], "Notes",
 CellChangeTimes->{{3.703039563735073*^9, 3.703039636006892*^9}, {
  3.703665012228436*^9, 3.703665101887556*^9}, {3.7037272460251503`*^9, 
  3.703727246841485*^9}},
 CellID->959609343]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell[TextData[ButtonBox["Conformal Partial Waves",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConformalPartialWaves"]], "Tutorials",
 CellChangeTimes->{{3.703866584347806*^9, 3.703866596787722*^9}},
 CellID->1542474150],

Cell[TextData[ButtonBox["Conserved Operators",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConservedOperators"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866522555599*^9}, {
  3.703866599411406*^9, 3.703866599610306*^9}},
 CellID->1375158345],

Cell[TextData[ButtonBox["Differential Basis",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/DifferentialBasis"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866525150303*^9}, {
  3.7038666017019033`*^9, 3.7038666018652353`*^9}},
 CellID->1009189986]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["n3CorrelationFunction",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n3CorrelationFunction"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["n3ListStructures",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n3ListStructures"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["n3ListStructuresAlternativeTS",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n3ListStructuresAlternativeTS"]], 
  "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["n4ListStructures",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n4ListStructures"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["n3KinematicFactor",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n3KinematicFactor"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["n4KinematicFactor",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n4KinematicFactor"]], "InlineFormula"]
}], "SeeAlso",
 CellChangeTimes->{{3.7030394179532757`*^9, 3.70303954142632*^9}},
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell["XXXX", "MoreAbout",
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<CFTs4D`\>\"", "]"}]], "Input",
 CellID->882720134],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["\t", "ExampleDelimiter"],
  $Line = 0; Null]], "ExampleDelimiter",
 CellID->127150297],

Cell["Scalar 2-point function", "ExampleText",
 CellChangeTimes->{{3.703039333978681*^9, 3.703039339123466*^9}},
 CellID->1626984095],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"n2CorrelationFunction", "[", 
  RowBox[{"\[CapitalDelta]", ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0"}], "}"}]}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.703039315896083*^9, 3.703039328037117*^9}},
 CellLabel->"In[44]:=",
 CellID->1060258662],

Cell[BoxData[
 SubsuperscriptBox["X", 
  RowBox[{"{", 
   RowBox[{"1", ",", "2"}], "}"}], 
  RowBox[{"-", "\[CapitalDelta]"}]]], "Output",
 CellChangeTimes->{3.703039328669773*^9},
 CellLabel->"Out[44]=",
 CellID->1112490011]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["\t", "ExampleDelimiter"],
  $Line = 0; Null]], "ExampleDelimiter",
 CellID->969193563],

Cell["Fermion 2-point function", "ExampleText",
 CellChangeTimes->{{3.703039344432548*^9, 3.703039347700307*^9}},
 CellID->870196030],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"n2CorrelationFunction", "[", 
  RowBox[{"\[CapitalDelta]", ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0", ",", "1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1", ",", "0"}], "}"}]}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.703039349147374*^9, 3.703039359551708*^9}},
 CellLabel->"In[45]:=",
 CellID->1333563293],

Cell[BoxData[
 RowBox[{"\[ImaginaryI]", " ", 
  SuperscriptBox[
   OverscriptBox["I", "^"], 
   RowBox[{"{", 
    RowBox[{"1", ",", "2"}], "}"}]], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"1", ",", "2"}], "}"}], 
   RowBox[{
    RowBox[{"-", 
     FractionBox["1", "2"]}], "-", "\[CapitalDelta]"}]]}]], "Output",
 CellChangeTimes->{3.703039360041114*^9},
 CellLabel->"Out[45]=",
 CellID->446841704]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["\t", "ExampleDelimiter"],
  $Line = 0; Null]], "ExampleDelimiter",
 CellID->459031952],

Cell["General 2-point function", "ExampleText",
 CellChangeTimes->{{3.703039364216819*^9, 3.70303937208573*^9}},
 CellID->318205569],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"n2CorrelationFunction", "[", 
  RowBox[{"\[CapitalDelta]", ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"l", ",", "lb"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"lb", ",", "l"}], "}"}]}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.703039374024869*^9, 3.703039399799857*^9}},
 CellLabel->"In[48]:=",
 CellID->1016419064],

Cell[BoxData[
 RowBox[{
  SuperscriptBox["\[ImaginaryI]", 
   RowBox[{
    RowBox[{"-", "l"}], "+", "lb"}]], " ", 
  SuperscriptBox[
   RowBox[{"(", 
    SuperscriptBox[
     OverscriptBox["I", "^"], 
     RowBox[{"{", 
      RowBox[{"1", ",", "2"}], "}"}]], ")"}], "lb"], " ", 
  SuperscriptBox[
   RowBox[{"(", 
    SuperscriptBox[
     OverscriptBox["I", "^"], 
     RowBox[{"{", 
      RowBox[{"2", ",", "1"}], "}"}]], ")"}], "l"], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"1", ",", "2"}], "}"}], 
   RowBox[{
    RowBox[{
     FractionBox["1", "2"], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "l"}], "-", "lb"}], ")"}]}], "-", 
    "\[CapitalDelta]"}]]}]], "Output",
 CellChangeTimes->{{3.703039385332069*^9, 3.7030394001071577`*^9}},
 CellLabel->"Out[48]=",
 CellID->280307950]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{4, Automatic}, {Automatic, 28}},
CellContext->"Global`",
FrontEndVersion->"11.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (July 28, \
2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[12367, 494, 100, 2, 55, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 14298, 565}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 325, 14, 24, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[908, 38, 68, 1, 29, "CategorizationSection",
 CellID->1122911449],
Cell[979, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1061, 45, 79, 2, 70, "Categorization",
 CellID->605800465],
Cell[1143, 49, 76, 2, 70, "Categorization",
 CellID->468444828],
Cell[1222, 53, 77, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1336, 59, 55, 1, 19, "KeywordsSection",
 CellID->477174294],
Cell[1394, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1476, 68, 65, 1, 19, "TemplatesSection",
 CellID->1872225408],
Cell[1544, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1641, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1726, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1810, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1929, 90, 53, 1, 19, "DetailsSection",
 CellID->307771771],
Cell[1985, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2051, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2123, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2190, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2262, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2326, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2390, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2456, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2537, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2641, 132, 64, 1, 63, "ObjectName",
 CellID->1224892054],
Cell[2708, 135, 1196, 42, 91, "Usage",
 CellID->982511436],
Cell[3907, 179, 597, 19, 47, "Notes",
 CellID->1067943069],
Cell[4507, 200, 624, 22, 28, "Notes",
 CellID->625452287],
Cell[5134, 224, 468, 11, 44, "Notes",
 CellID->959609343]
}, Open  ]],
Cell[CellGroupData[{
Cell[5639, 240, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[5699, 243, 233, 4, 16, "Tutorials",
 CellID->1542474150],
Cell[5935, 249, 277, 5, 16, "Tutorials",
 CellID->1375158345],
Cell[6215, 256, 279, 5, 16, "Tutorials",
 CellID->1009189986]
}, Open  ]],
Cell[CellGroupData[{
Cell[6531, 266, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[6617, 269, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[6712, 275, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[6780, 278, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[6866, 284, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[6924, 287, 1166, 33, 41, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[8127, 325, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[8187, 328, 46, 1, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[8270, 334, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[8629, 347, 92, 2, 27, "Input",
 CellID->882720134],
Cell[CellGroupData[{
Cell[8746, 353, 125, 3, 17, "ExampleDelimiter",
 CellID->127150297],
Cell[8874, 358, 133, 2, 22, "ExampleText",
 CellID->1626984095],
Cell[CellGroupData[{
Cell[9032, 364, 377, 11, 27, "Input",
 CellID->1060258662],
Cell[9412, 377, 225, 7, 30, "Output",
 CellID->1112490011]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[9686, 390, 125, 3, 17, "ExampleDelimiter",
 CellID->969193563],
Cell[9814, 395, 133, 2, 22, "ExampleText",
 CellID->870196030],
Cell[CellGroupData[{
Cell[9972, 401, 377, 11, 27, "Input",
 CellID->1333563293],
Cell[10352, 414, 418, 14, 39, "Output",
 CellID->446841704]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[10819, 434, 125, 3, 17, "ExampleDelimiter",
 CellID->459031952],
Cell[10947, 439, 132, 2, 22, "ExampleText",
 CellID->318205569],
Cell[CellGroupData[{
Cell[11104, 445, 379, 11, 27, "Input",
 CellID->1016419064],
Cell[11486, 458, 820, 29, 39, "Output",
 CellID->280307950]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[12367, 494, 100, 2, 55, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[12470, 498, 125, 3, 33, "ExampleSection",
 CellID->1293636265],
Cell[12598, 503, 148, 3, 21, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[12771, 510, 127, 3, 21, "ExampleSection",
 CellID->2061341341],
Cell[12901, 515, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[13034, 520, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[13179, 526, 131, 3, 21, "ExampleSection",
 CellID->258228157],
Cell[13313, 531, 142, 3, 22, "ExampleSection",
 CellID->2123667759],
Cell[13458, 536, 135, 3, 22, "ExampleSection",
 CellID->1305812373],
Cell[13596, 541, 140, 3, 22, "ExampleSection",
 CellID->1653164318],
Cell[13739, 546, 132, 3, 22, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)


(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     16718,        625]
NotebookOptionsPosition[     12149,        463]
NotebookOutlinePosition[     12781,        488]
CellTagsIndexPosition[     12702,        483]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["1.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["CFTs4D", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["CFTs4D`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["CFTs4D/ref/plugKFunctionsApproximated", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["plugKFunctionsApproximated", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{
   RowBox[{"plugKFunctionsApproximated", "[", "ord", "]"}], "[", "expr", 
   "]"}]], "InlineFormula"],
 " \[LineSeparator]replaces all the k-function ",
 Cell[BoxData[
  StyleBox[
   RowBox[{"k", "[", 
    RowBox[{"z", "=", 
     RowBox[{"1", "/", "2"}]}], "]"}], "TI"]], "InlineFormula"],
 " inside ",
 Cell[BoxData[
  StyleBox["expr", "TI"]], "InlineFormula"],
 " by its rational approximation. Parameter ",
 Cell[BoxData["ord"], "InlineFormula"],
 " is order of the approximation"
}], "Usage",
 CellChangeTimes->{{3.703394320538719*^9, 3.7033943371130457`*^9}, {
  3.703674754182239*^9, 3.7036749037794743`*^9}, {3.7037425415415297`*^9, 
  3.703742592764022*^9}},
 CellID->982511436],

Cell[TextData[{
 "To use ",
 Cell[BoxData[
  ButtonBox["plugKFunctionsApproximated",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/plugKFunctionsApproximated"]], 
  "InlineFormula"],
 ", you first need to load the ",
 ButtonBox["CFTs4D",
  BaseStyle->"Link",
  ButtonData->"paclet:CFTs4D/guide/CFTs4D"],
 " package using ",
 Cell[BoxData[
  RowBox[{
   ButtonBox["Needs",
    BaseStyle->"Link"], "[", "\"\<CFTs4D`\>\"", "]"}]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{{3.703039193544949*^9, 3.7030392141372538`*^9}, {
  3.7032639561532373`*^9, 3.7032639661101093`*^9}, {3.703729981870865*^9, 
  3.703729992796955*^9}},
 CellID->1831512071],

Cell[TextData[{
 "The rational approximation of ",
 Cell[BoxData[
  StyleBox[
   RowBox[{"k", "[", "z", "]"}], "TI"]], "InlineFormula"],
 " at ",
 Cell[BoxData[
  StyleBox[
   RowBox[{"z", "=", 
    RowBox[{"1", "/", "2"}]}], "TI"]], "InlineFormula"],
 " is derived by switching to the Hogervorst-Rychkov  ",
 Cell[BoxData["\[Rho]"], "InlineFormula"],
 "-variable, expanding the expression to the order ",
 Cell[BoxData["ord"], "InlineFormula"],
 " in  ",
 Cell[BoxData["\[Rho]"], "InlineFormula"],
 "-variable and then setting the value of  ",
 Cell[BoxData["\[Rho]"], "InlineFormula"],
 "-variable corresponding to  ",
 Cell[BoxData[
  RowBox[{"z", "=", 
   RowBox[{"1", "/", "2"}]}]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{{3.703742614107588*^9, 3.7037427600185833`*^9}, {
  3.703800849009519*^9, 3.703800884363594*^9}},
 CellID->504952844],

Cell["\<\
This function is intended only for an experimental use and not for high \
precision numerical applications.\
\>", "Notes",
 CellChangeTimes->{{3.7037300016747723`*^9, 3.703730063144548*^9}, {
  3.703730114595196*^9, 3.7037301231152983`*^9}, {3.70374278327192*^9, 
  3.70374281521432*^9}},
 CellID->164647684],

Cell[TextData[{
 "The form of rational approximations are precomputed for  ",
 Cell[BoxData[
  RowBox[{"ord", "\[LessEqual]", "12"}]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{{3.703674915016387*^9, 3.703674942438098*^9}, {
  3.703800899445424*^9, 3.7038009026229277`*^9}},
 CellID->1067943069]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell[TextData[ButtonBox["Conformal Partial Waves",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConformalPartialWaves"]], "Tutorials",
 CellChangeTimes->{{3.703866584347806*^9, 3.703866596787722*^9}},
 CellID->1542474150],

Cell[TextData[ButtonBox["Conserved Operators",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConservedOperators"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866522555599*^9}, {
  3.703866599411406*^9, 3.703866599610306*^9}},
 CellID->1375158345],

Cell[TextData[ButtonBox["Differential Basis",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/DifferentialBasis"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866525150303*^9}, {
  3.7038666017019033`*^9, 3.7038666018652353`*^9}},
 CellID->1009189986]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell["XXXX", "SeeAlso",
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell["XXXX", "MoreAbout",
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<CFTs4D`\>\"", "]"}]], "Input",
 CellChangeTimes->{{3.703394360301114*^9, 3.7033943659438133`*^9}},
 CellLabel->"In[18]:=",
 CellID->2079406267],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"k", "[", 
    RowBox[{"\[CapitalDelta]", ",", "a", ",", "b", ",", "c"}], "]"}], "[", 
   RowBox[{"1", "/", "2"}], "]"}], "//", 
  RowBox[{"plugKFunctionsApproximated", "[", "2", "]"}]}]], "Input",
 CellChangeTimes->{{3.7036749510612698`*^9, 3.70367495712325*^9}, {
  3.7037425058156853`*^9, 3.7037425296790657`*^9}},
 CellLabel->"In[141]:=",
 CellID->1444548162],

Cell[BoxData[
 RowBox[{
  SuperscriptBox[
   RowBox[{"(", 
    RowBox[{"12", "-", 
     RowBox[{"8", " ", 
      SqrtBox["2"]}]}], ")"}], "\[CapitalDelta]"], " ", 
  RowBox[{"(", 
   RowBox[{"1", "+", 
    FractionBox[
     RowBox[{"2", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", "3"}], "+", 
        RowBox[{"2", " ", 
         SqrtBox["2"]}]}], ")"}], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{
            RowBox[{"-", "2"}], " ", "b"}], "+", "c"}], ")"}], " ", 
         "\[CapitalDelta]"}], "-", 
        RowBox[{"2", " ", "a", " ", 
         RowBox[{"(", 
          RowBox[{"b", "+", "\[CapitalDelta]"}], ")"}]}]}], ")"}]}], 
     RowBox[{"c", "+", 
      RowBox[{"2", " ", "\[CapitalDelta]"}]}]], "+", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"17", "-", 
       RowBox[{"12", " ", 
        SqrtBox["2"]}]}], ")"}], " ", 
     RowBox[{"(", 
      RowBox[{"\[CapitalDelta]", "+", 
       RowBox[{"2", " ", 
        SuperscriptBox["\[CapitalDelta]", "2"]}], "-", 
       FractionBox[
        RowBox[{"8", " ", "\[CapitalDelta]", " ", 
         RowBox[{"(", 
          RowBox[{"a", "+", "\[CapitalDelta]"}], ")"}], " ", 
         RowBox[{"(", 
          RowBox[{"b", "+", "\[CapitalDelta]"}], ")"}]}], 
        RowBox[{"c", "+", 
         RowBox[{"2", " ", "\[CapitalDelta]"}]}]], "+", 
       FractionBox[
        RowBox[{"8", " ", 
         RowBox[{"(", 
          RowBox[{"a", "+", "\[CapitalDelta]"}], ")"}], " ", 
         RowBox[{"(", 
          RowBox[{"b", "+", "\[CapitalDelta]"}], ")"}], " ", 
         RowBox[{"(", 
          RowBox[{"b", "-", "c", "+", 
           RowBox[{"b", " ", "\[CapitalDelta]"}], "+", 
           SuperscriptBox["\[CapitalDelta]", "2"], "+", 
           RowBox[{"a", " ", 
            RowBox[{"(", 
             RowBox[{"1", "+", "b", "+", "\[CapitalDelta]"}], ")"}]}]}], 
          ")"}]}], 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"c", "+", 
           RowBox[{"2", " ", "\[CapitalDelta]"}]}], ")"}], " ", 
         RowBox[{"(", 
          RowBox[{"1", "+", "c", "+", 
           RowBox[{"2", " ", "\[CapitalDelta]"}]}], ")"}]}]]}], ")"}]}]}], 
   ")"}]}]], "Output",
 CellChangeTimes->{
  3.7033943743788137`*^9, {3.703674951654735*^9, 3.7036749574005833`*^9}, {
   3.703742508531831*^9, 3.703742529898212*^9}},
 CellLabel->"Out[141]=",
 CellID->1148673561]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{265, Automatic}, {Automatic, 103}},
CellContext->"Global`",
FrontEndVersion->"11.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (July 28, \
2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[10629, 405, 100, 2, 55, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 12563, 476}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 325, 14, 24, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[908, 38, 68, 1, 29, "CategorizationSection",
 CellID->1122911449],
Cell[979, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1061, 45, 79, 2, 70, "Categorization",
 CellID->605800465],
Cell[1143, 49, 76, 2, 70, "Categorization",
 CellID->468444828],
Cell[1222, 53, 82, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1341, 59, 55, 1, 19, "KeywordsSection",
 CellID->477174294],
Cell[1399, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1481, 68, 65, 1, 19, "TemplatesSection",
 CellID->1872225408],
Cell[1549, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1646, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1731, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1815, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1934, 90, 53, 1, 19, "DetailsSection",
 CellID->307771771],
Cell[1990, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2056, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2128, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2195, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2267, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2331, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2395, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2461, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2542, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2646, 132, 69, 1, 63, "ObjectName",
 CellID->1224892054],
Cell[2718, 135, 753, 22, 91, "Usage",
 CellID->982511436],
Cell[3474, 159, 664, 21, 47, "Notes",
 CellID->1831512071],
Cell[4141, 182, 860, 26, 60, "Notes",
 CellID->504952844],
Cell[5004, 210, 318, 7, 25, "Notes",
 CellID->164647684],
Cell[5325, 219, 308, 8, 26, "Notes",
 CellID->1067943069]
}, Open  ]],
Cell[CellGroupData[{
Cell[5670, 232, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[5730, 235, 233, 4, 16, "Tutorials",
 CellID->1542474150],
Cell[5966, 241, 277, 5, 16, "Tutorials",
 CellID->1375158345],
Cell[6246, 248, 279, 5, 16, "Tutorials",
 CellID->1009189986]
}, Open  ]],
Cell[CellGroupData[{
Cell[6562, 258, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[6648, 261, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[6743, 267, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[6811, 270, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[6897, 276, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[6955, 279, 43, 1, 16, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[7035, 285, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[7095, 288, 46, 1, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[7178, 294, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[7537, 307, 185, 4, 27, "Input",
 CellID->2079406267],
Cell[CellGroupData[{
Cell[7747, 315, 409, 10, 27, "Input",
 CellID->1444548162],
Cell[8159, 327, 2421, 72, 100, "Output",
 CellID->1148673561]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[10629, 405, 100, 2, 55, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[10732, 409, 125, 3, 33, "ExampleSection",
 CellID->1293636265],
Cell[10860, 414, 148, 3, 21, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[11033, 421, 127, 3, 21, "ExampleSection",
 CellID->2061341341],
Cell[11163, 426, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[11296, 431, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[11441, 437, 131, 3, 21, "ExampleSection",
 CellID->258228157],
Cell[11575, 442, 142, 3, 21, "ExampleSection",
 CellID->2123667759],
Cell[11720, 447, 135, 3, 21, "ExampleSection",
 CellID->1305812373],
Cell[11858, 452, 140, 3, 21, "ExampleSection",
 CellID->1653164318],
Cell[12001, 457, 132, 3, 21, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)


(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     16038,        602]
NotebookOptionsPosition[     11312,        434]
NotebookOutlinePosition[     11940,        459]
CellTagsIndexPosition[     11861,        454]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["1.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["CFTs4D", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["CFTs4D`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["CFTs4D/ref/opDtEF", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["opDtEF", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{
   RowBox[{"opDtEF", "[", 
    RowBox[{"i", ",", "j"}], "]"}], "[", "expr", "]"}]], "InlineFormula"],
 " \[LineSeparator]applies spinning differential operator ",
 Cell[BoxData[
  SubscriptBox[
   OverscriptBox["D", "~"], "ij"]], "InlineFormula"],
 " to EF expression ",
 Cell[BoxData[
  StyleBox["expr", "TI"]], "InlineFormula"],
 ", written in terms of non-normalized EF invariants."
}], "Usage",
 CellChangeTimes->{{3.703302182004575*^9, 3.703302195327529*^9}, {
  3.7036743579253063`*^9, 3.70367437225511*^9}},
 CellID->982511436],

Cell[TextData[{
 "To use ",
 Cell[BoxData[
  ButtonBox["opDtEF",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/opDtEF"]], "InlineFormula"],
 ", you first need to load the ",
 ButtonBox["CFTs4D",
  BaseStyle->"Link",
  ButtonData->"paclet:CFTs4D/guide/CFTs4D"],
 " package using ",
 Cell[BoxData[
  RowBox[{
   ButtonBox["Needs",
    BaseStyle->"Link"], "[", "\"\<CFTs4D`\>\"", "]"}]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{{3.703039193544949*^9, 3.7030392141372538`*^9}, {
  3.703263647617464*^9, 3.703263650807987*^9}, {3.70326474242828*^9, 
  3.703264742724671*^9}},
 CellID->1067943069],

Cell[TextData[{
 Cell[BoxData[
  StyleBox["expr", "TI"]], "InlineFormula"],
 " has to be expressed in terms of non-normalized invariants. Use ",
 Cell[BoxData[
  ButtonBox["denormalizeInvariants",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/denormalizeInvariants"]], "InlineFormula"],
 " to convert from normalized invariants if needed."
}], "Notes",
 CellChangeTimes->{{3.703301800635041*^9, 3.7033018911265097`*^9}},
 CellID->939429338]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell[TextData[ButtonBox["Conformal Partial Waves",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConformalPartialWaves"]], "Tutorials",
 CellChangeTimes->{{3.703866584347806*^9, 3.703866596787722*^9}},
 CellID->1542474150],

Cell[TextData[ButtonBox["Conserved Operators",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConservedOperators"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866522555599*^9}, {
  3.703866599411406*^9, 3.703866599610306*^9}},
 CellID->1375158345],

Cell[TextData[ButtonBox["Differential Basis",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/DifferentialBasis"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866525150303*^9}, {
  3.7038666017019033`*^9, 3.7038666018652353`*^9}},
 CellID->1009189986]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell[TextData[Cell[BoxData[
 ButtonBox["opDt4D",
  BaseStyle->"Link",
  ButtonData->"paclet:CFTs4D/ref/opDt4D"]], "InlineFormula"]], "SeeAlso",
 CellChangeTimes->{{3.7033022139846077`*^9, 3.703302224624001*^9}},
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell[TextData[ButtonBox["Spinning differential operators",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/guide/SpinningDiffOperators"]], "MoreAbout",
 CellChangeTimes->{3.703302231267529*^9},
 CellID->1665078683]
}, Open  ]],

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell["This function does not work with normalized invariants,", "ExampleText",
 CellChangeTimes->{{3.702964824664667*^9, 3.70296484216897*^9}},
 CellID->2033809041],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   SubsuperscriptBox[
    OverscriptBox["J", "^"], 
    RowBox[{"{", 
     RowBox[{"1", ",", "2"}], "}"}], 
    RowBox[{"{", "3", "}"}]], "//", 
   RowBox[{"opDtEF", "[", 
    RowBox[{"1", ",", "2"}], "]"}]}], "//", "Short"}]], "Input",
 CellChangeTimes->{{3.703672564919076*^9, 3.703672601585235*^9}, 
   3.7037612107579536`*^9, {3.7037618211444283`*^9, 3.7037618237584457`*^9}},
 CellLabel->"In[21]:=",
 CellID->253371773],

Cell[BoxData[
 TagBox[
  RowBox[{"\[LeftSkeleton]", "1", "\[RightSkeleton]"}],
  Short]], "Output",
 CellChangeTimes->{{3.703672567363337*^9, 3.703672602073707*^9}, 
   3.703761211918947*^9, 3.70376182438843*^9},
 CellLabel->"Out[21]//Short=",
 CellID->1816861174]
}, Open  ]],

Cell["Denormalized the invariants before applying the functions", \
"ExampleText",
 CellChangeTimes->{{3.7029648909266567`*^9, 3.702964905450025*^9}, {
  3.70367264886504*^9, 3.7036726534716578`*^9}},
 CellID->226131168],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    SubsuperscriptBox[
     OverscriptBox["J", "^"], 
     RowBox[{"{", 
      RowBox[{"1", ",", "2"}], "}"}], 
     RowBox[{"{", "3", "}"}]], "//", "denormalizeInvariants"}], "//", 
   RowBox[{"opDtEF", "[", 
    RowBox[{"1", ",", "2"}], "]"}]}], "//", "normalizeInvariants"}]], "Input",\

 CellChangeTimes->{{3.703672607523417*^9, 3.703672612067615*^9}, 
   3.703761215334299*^9, {3.7037618281449537`*^9, 3.70376184068981*^9}},
 CellLabel->"In[23]:=",
 CellID->685074261],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "2"}], " ", 
  SubsuperscriptBox[
   OverscriptBox["I", "^"], 
   RowBox[{"{", 
    RowBox[{"2", ",", "1"}], "}"}], 
   RowBox[{"{", 
    RowBox[{"1", ",", "3"}], "}"}]], " ", 
  SuperscriptBox[
   OverscriptBox["I", "^"], 
   RowBox[{"{", 
    RowBox[{"3", ",", "1"}], "}"}]]}]], "Output",
 CellChangeTimes->{{3.703672609382442*^9, 3.703672613081479*^9}, 
   3.703761215769383*^9, {3.703761834356139*^9, 3.703761841177538*^9}},
 CellLabel->"Out[23]=",
 CellID->841614613]
}, Open  ]],

Cell[TextData[{
 "Other issues arise when acting on the expression containing unfamiliar \
symbols to the package. Please always define new symbols (constants and \
functions) through ",
 Cell[BoxData[
  ButtonBox["listConstants",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/listConstants"]], "InlineFormula"],
 " and ",
 Cell[BoxData[
  ButtonBox["listFunctions",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/listFunctions"]], "InlineFormula"],
 ". See these pages also for examples. "
}], "ExampleText",
 CellContext->Notebook,
 CellChangeTimes->{
  3.7036714936681128`*^9, {3.703760554890656*^9, 3.703760653637703*^9}, {
   3.703760824636071*^9, 3.7037608706284027`*^9}, {3.7037609176156807`*^9, 
   3.703760935343403*^9}, {3.7037609673826714`*^9, 3.703761043579358*^9}, {
   3.703761167793392*^9, 3.7037611685429497`*^9}, {3.703761219700605*^9, 
   3.703761222260386*^9}},
 CellID->1902486586]
}, Open  ]],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{4, Automatic}, {Automatic, 84}},
CellContext->"Global`",
FrontEndVersion->"11.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (July 28, \
2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[6611, 274, 100, 2, 55, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 11723, 447}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 325, 14, 24, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[908, 38, 68, 1, 29, "CategorizationSection",
 CellID->1122911449],
Cell[979, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1061, 45, 79, 2, 70, "Categorization",
 CellID->605800465],
Cell[1143, 49, 76, 2, 70, "Categorization",
 CellID->468444828],
Cell[1222, 53, 62, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1321, 59, 55, 1, 19, "KeywordsSection",
 CellID->477174294],
Cell[1379, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1461, 68, 65, 1, 19, "TemplatesSection",
 CellID->1872225408],
Cell[1529, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1626, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1711, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1795, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1914, 90, 53, 1, 19, "DetailsSection",
 CellID->307771771],
Cell[1970, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2036, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2108, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2175, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2247, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2311, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2375, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2441, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2522, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2626, 132, 49, 1, 63, "ObjectName",
 CellID->1224892054],
Cell[2678, 135, 601, 17, 94, "Usage",
 CellID->982511436],
Cell[3282, 154, 616, 20, 28, "Notes",
 CellID->1067943069],
Cell[3901, 176, 450, 11, 44, "Notes",
 CellID->939429338]
}, Open  ]],
Cell[CellGroupData[{
Cell[4388, 192, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[4448, 195, 233, 4, 16, "Tutorials",
 CellID->1542474150],
Cell[4684, 201, 277, 5, 16, "Tutorials",
 CellID->1375158345],
Cell[4964, 208, 279, 5, 16, "Tutorials",
 CellID->1009189986]
}, Open  ]],
Cell[CellGroupData[{
Cell[5280, 218, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[5366, 221, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[5461, 227, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[5529, 230, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[5615, 236, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[5673, 239, 231, 5, 22, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[5941, 249, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[6001, 252, 214, 4, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[6230, 259, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[CellGroupData[{
Cell[6611, 274, 100, 2, 55, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[6714, 278, 125, 3, 33, "ExampleSection",
 CellID->1293636265],
Cell[6842, 283, 148, 3, 21, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[7015, 290, 127, 3, 21, "ExampleSection",
 CellID->2061341341],
Cell[7145, 295, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[7278, 300, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[7423, 306, 131, 3, 21, "ExampleSection",
 CellID->258228157],
Cell[7557, 311, 142, 3, 21, "ExampleSection",
 CellID->2123667759],
Cell[CellGroupData[{
Cell[7724, 318, 135, 3, 21, "ExampleSection",
 CellID->1305812373],
Cell[7862, 323, 164, 2, 22, "ExampleText",
 CellID->2033809041],
Cell[CellGroupData[{
Cell[8051, 329, 459, 13, 33, "Input",
 CellID->253371773],
Cell[8513, 344, 264, 7, 26, "Output",
 CellID->1816861174]
}, Open  ]],
Cell[8792, 354, 220, 4, 22, "ExampleText",
 CellID->226131168],
Cell[CellGroupData[{
Cell[9037, 362, 520, 15, 33, "Input",
 CellID->685074261],
Cell[9560, 379, 511, 16, 32, "Output",
 CellID->841614613]
}, Open  ]],
Cell[10086, 398, 920, 22, 56, "ExampleText",
 CellID->1902486586]
}, Open  ]],
Cell[11021, 423, 140, 3, 33, "ExampleSection",
 CellID->1653164318],
Cell[11164, 428, 132, 3, 21, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)


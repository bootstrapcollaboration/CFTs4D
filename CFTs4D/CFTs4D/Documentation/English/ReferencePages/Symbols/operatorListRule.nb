(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     21225,        759]
NotebookOptionsPosition[     16365,        587]
NotebookOutlinePosition[     16998,        612]
CellTagsIndexPosition[     16919,        607]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["1.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["CFTs4D", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["CFTs4D`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["CFTs4D/ref/operatorListRule", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["operatorListRule", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{
   RowBox[{"operatorListRule", "[", "op", "]"}], "[", "args", "]"}]], 
  "InlineFormula"],
 " \[LineSeparator]is the same as ",
 Cell[BoxData[
  ButtonBox["operatorRule",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/operatorRule"]], "InlineFormula"],
 ", except that it produces a list of ",
 Cell[BoxData[
  ButtonBox["CF4pt",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/CF4pt"]], "InlineFormula"],
 " structures rather than a sum."
}], "Usage",
 CellChangeTimes->{{3.70330518924894*^9, 3.703305259134856*^9}},
 CellID->982511436],

Cell[TextData[{
 "To use ",
 Cell[BoxData[
  ButtonBox["operatorListRule",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/operatorListRule"]], "InlineFormula"],
 ", you first need to load the ",
 ButtonBox["CFTs4D",
  BaseStyle->"Link",
  ButtonData->"paclet:CFTs4D/guide/CFTs4D"],
 " package using ",
 Cell[BoxData[
  RowBox[{
   ButtonBox["Needs",
    BaseStyle->"Link"], "[", "\"\<CFTs4D`\>\"", "]"}]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{{3.703039193544949*^9, 3.7030392141372538`*^9}, {
  3.7032638570218287`*^9, 3.703263862357807*^9}, {3.703264779982359*^9, 
  3.703264780261196*^9}},
 CellID->1067943069],

Cell[TextData[{
 "The rule produced by ",
 Cell[BoxData[
  ButtonBox["operatorListRule",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/operatorListRule"]], "InlineFormula"],
 " is sometimes (much) faster to apply when there are hundreds of tensor \
structures, since the list prevents Mathematica from trying to apply the \
addition rules between the ",
 Cell[BoxData[
  ButtonBox["CF4pt",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/CF4pt"]], "InlineFormula"],
 " structures. See the Possible Issues section of examples for ",
 Cell[BoxData[
  ButtonBox["CF4pt",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/CF4pt"]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{{3.7033052680106564`*^9, 3.703305345272985*^9}, {
  3.7036156403454742`*^9, 3.703615671677473*^9}, {3.703625220497991*^9, 
  3.70362522953946*^9}},
 CellID->1257802987],

Cell[TextData[{
 "The rule produced by ",
 Cell[BoxData[
  ButtonBox["operatorListRule",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/operatorListRule"]], "InlineFormula"],
 " can be applied to expressions using ",
 Cell[BoxData[
  ButtonBox["applyListRule",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/applyListRule"]], "InlineFormula"],
 ", which automatically combines equivalent structures."
}], "Notes",
 CellChangeTimes->{{3.7033053594561863`*^9, 3.703305420271109*^9}, {
  3.7036252064088383`*^9, 3.7036252086756887`*^9}},
 CellID->1129777026]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell[TextData[ButtonBox["Conformal Partial Waves",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConformalPartialWaves"]], "Tutorials",
 CellChangeTimes->{{3.703866584347806*^9, 3.703866596787722*^9}},
 CellID->1542474150],

Cell[TextData[ButtonBox["Conserved Operators",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConservedOperators"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866522555599*^9}, {
  3.703866599411406*^9, 3.703866599610306*^9}},
 CellID->1375158345],

Cell[TextData[ButtonBox["Differential Basis",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/DifferentialBasis"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866525150303*^9}, {
  3.7038666017019033`*^9, 3.7038666018652353`*^9}},
 CellID->1009189986]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell[TextData[Cell[BoxData[
 ButtonBox["operatorRule",
  BaseStyle->"Link",
  ButtonData->"paclet:CFTs4D/ref/operatorRule"]], "InlineFormula"]], "SeeAlso",\

 CellChangeTimes->{{3.703305425355892*^9, 3.703305431426783*^9}},
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell[TextData[ButtonBox["CFTs4D",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/guide/CFTs4D"]], "MoreAbout",
 CellChangeTimes->{{3.703819196363316*^9, 3.70381920279813*^9}},
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<CFTs4D`\>\"", "]"}]], "Input",
 CellLabel->"In[87]:=",
 CellID->668534668],

Cell[BoxData[
 RowBox[{
  RowBox[{"nablaListRule12", "=", 
   RowBox[{
    RowBox[{"operatorListRule", "[", "opN4D", "]"}], "[", 
    RowBox[{"1", ",", "2"}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.703303415374242*^9, 3.703303439766403*^9}, {
  3.7036251269487267`*^9, 3.703625129480983*^9}},
 CellLabel->"In[101]:=",
 CellID->427723972],

Cell["\<\
The rule is essentially a delayed replacement rule, with a list instead of a \
sum in the result\
\>", "ExampleText",
 CellChangeTimes->{{3.703624766731934*^9, 3.703624786350988*^9}, {
  3.70362513270474*^9, 3.703625140903084*^9}},
 CellID->1618778882],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"nablaListRule12", "//", 
  RowBox[{
   RowBox[{"Short", "[", 
    RowBox[{"#", ",", "4"}], "]"}], "&"}]}]], "Input",
 CellChangeTimes->{{3.7033050536994553`*^9, 3.70330505964188*^9}, {
   3.7033051162365427`*^9, 3.703305118346051*^9}, 3.7033051509866037`*^9, {
   3.703624803798276*^9, 3.703624827343766*^9}, 3.7036251437502413`*^9},
 CellLabel->"In[102]:=",
 CellID->1860310408],

Cell[BoxData[
 TagBox[
  RowBox[{"{", 
   RowBox[{
    RowBox[{"CF4pt", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
       "CFTs4D`Private`\[CapitalDelta]1_", ",", 
        "CFTs4D`Private`\[CapitalDelta]2_", ",", 
        "CFTs4D`Private`\[CapitalDelta]3_", ",", 
        "CFTs4D`Private`\[CapitalDelta]4_"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
       "CFTs4D`Private`l1_", ",", "CFTs4D`Private`l2_", ",", 
        "CFTs4D`Private`l3_", ",", "CFTs4D`Private`l4_"}], "}"}], ",", 
      RowBox[{"\[LeftSkeleton]", "3", "\[RightSkeleton]"}], ",", 
      "CFTs4D`Private`g_"}], "]"}], "\[RuleDelayed]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"CF4pt", "[", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"\[LeftSkeleton]", "1", "\[RightSkeleton]"}], "}"}], ",", 
        RowBox[{"\[LeftSkeleton]", "4", "\[RightSkeleton]"}], ",", 
        RowBox[{
         FractionBox["1", "4"], " ", 
         RowBox[{"\[LeftSkeleton]", "3", "\[RightSkeleton]"}], " ", 
         OverscriptBox["z", "\[HorizontalLine]"]}]}], "]"}], ",", 
      RowBox[{"\[LeftSkeleton]", "1", "\[RightSkeleton]"}]}], "}"}]}], "}"}],
  Short[#, 4]& ]], "Output",
 CellChangeTimes->{
  3.703305054947173*^9, 3.703305118879136*^9, {3.703305151297687*^9, 
   3.7033051552234488`*^9}, 3.703624761912959*^9, {3.7036248061411858`*^9, 
   3.703624827638569*^9}, 3.7036251440203533`*^9},
 CellLabel->"Out[102]//Short=",
 CellID->1494194371]
}, Open  ]],

Cell[TextData[{
 "The rule can be applied to the ",
 Cell[BoxData[
  ButtonBox["CF4pt",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/CF4pt"]], "InlineFormula"],
 " structures using ",
 Cell[BoxData[
  ButtonBox["applyListRule",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/applyListRule"]], "InlineFormula"],
 ", producing a list of structures instead of a sum"
}], "ExampleText",
 CellChangeTimes->{{3.703624861118215*^9, 3.703624872717482*^9}, {
  3.703624927959021*^9, 3.703624935162511*^9}, {3.7036251496025743`*^9, 
  3.703625193338482*^9}},
 CellID->1169592302],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"CF4pt", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
      "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3", 
       ",", "\[CapitalDelta]4"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"l1", ",", "l2", ",", "l3", ",", "l4"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"lb1", ",", "lb2", ",", "lb3", ",", "lb4"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"q1", ",", "q2", ",", "q3", ",", "q4"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qb1", ",", "qb2", ",", "qb3", ",", "qb4"}], "}"}], ",", 
     RowBox[{"g", "[", 
      RowBox[{"z", ",", "zb"}], "]"}]}], "]"}], "//", 
   RowBox[{"applyListRule", "[", "nablaListRule12", "]"}]}], "//", 
  "simplifyInCF4pt"}]], "Input",
 CellChangeTimes->{{3.7036248741872063`*^9, 3.703624925049592*^9}, {
  3.703625166698544*^9, 3.7036251733730173`*^9}},
 CellLabel->"In[103]:=",
 CellID->1678297378],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"CF4pt", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{
        RowBox[{"-", 
         FractionBox["1", "2"]}], "+", "\[CapitalDelta]1"}], ",", 
       RowBox[{
        RowBox[{"-", 
         FractionBox["1", "2"]}], "+", "\[CapitalDelta]2"}], ",", 
       "\[CapitalDelta]3", ",", "\[CapitalDelta]4"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{
        RowBox[{"-", "1"}], "+", "l1"}], ",", "l2", ",", "l3", ",", "l4"}], 
      "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"lb1", ",", 
       RowBox[{
        RowBox[{"-", "1"}], "+", "lb2"}], ",", "lb3", ",", "lb4"}], "}"}], 
     ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{
        FractionBox["1", "2"], "+", "q1"}], ",", "q2", ",", "q3", ",", "q4"}],
       "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qb1", ",", 
       RowBox[{
        FractionBox["1", "2"], "+", "qb2"}], ",", "qb3", ",", "qb4"}], "}"}], 
     ",", 
     RowBox[{
      FractionBox["1", "4"], " ", 
      RowBox[{"(", 
       RowBox[{"l1", "-", 
        RowBox[{"2", " ", "q1"}]}], ")"}], " ", 
      RowBox[{"(", 
       RowBox[{"lb2", "-", 
        RowBox[{"2", " ", "qb2"}]}], ")"}], " ", 
      OverscriptBox["z", "\[HorizontalLine]"], " ", 
      RowBox[{"g", "[", 
       RowBox[{"z", ",", 
        OverscriptBox["z", "\[HorizontalLine]"]}], "]"}]}]}], "]"}], ",", 
   RowBox[{"CF4pt", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{
        RowBox[{"-", 
         FractionBox["1", "2"]}], "+", "\[CapitalDelta]1"}], ",", 
       RowBox[{
        RowBox[{"-", 
         FractionBox["1", "2"]}], "+", "\[CapitalDelta]2"}], ",", 
       "\[CapitalDelta]3", ",", "\[CapitalDelta]4"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{
        RowBox[{"-", "1"}], "+", "l1"}], ",", "l2", ",", "l3", ",", "l4"}], 
      "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"lb1", ",", 
       RowBox[{
        RowBox[{"-", "1"}], "+", "lb2"}], ",", "lb3", ",", "lb4"}], "}"}], 
     ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{
        RowBox[{"-", 
         FractionBox["1", "2"]}], "+", "q1"}], ",", "q2", ",", "q3", ",", 
       "q4"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"qb1", ",", 
       RowBox[{
        RowBox[{"-", 
         FractionBox["1", "2"]}], "+", "qb2"}], ",", "qb3", ",", "qb4"}], 
      "}"}], ",", 
     RowBox[{
      RowBox[{"-", 
       FractionBox["1", "4"]}], " ", 
      RowBox[{"(", 
       RowBox[{"l1", "+", 
        RowBox[{"2", " ", "q1"}]}], ")"}], " ", 
      RowBox[{"(", 
       RowBox[{"lb2", "+", 
        RowBox[{"2", " ", "qb2"}]}], ")"}], " ", "z", " ", 
      RowBox[{"g", "[", 
       RowBox[{"z", ",", 
        OverscriptBox["z", "\[HorizontalLine]"]}], "]"}]}]}], "]"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{{3.7036249147993937`*^9, 3.703624925243874*^9}, 
   3.7036251740561028`*^9},
 CellLabel->"Out[103]=",
 CellID->97236187]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{Automatic, -282}, {859, Automatic}},
CellContext->"Global`",
FrontEndVersion->"11.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (July 28, \
2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[14845, 529, 100, 2, 55, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 16780, 600}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 325, 14, 24, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[908, 38, 68, 1, 29, "CategorizationSection",
 CellID->1122911449],
Cell[979, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1061, 45, 79, 2, 70, "Categorization",
 CellID->605800465],
Cell[1143, 49, 76, 2, 70, "Categorization",
 CellID->468444828],
Cell[1222, 53, 72, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1331, 59, 55, 1, 19, "KeywordsSection",
 CellID->477174294],
Cell[1389, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1471, 68, 65, 1, 19, "TemplatesSection",
 CellID->1872225408],
Cell[1539, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1636, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1721, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1805, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1924, 90, 53, 1, 19, "DetailsSection",
 CellID->307771771],
Cell[1980, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2046, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2118, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2185, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2257, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2321, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2385, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2451, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2532, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2636, 132, 59, 1, 63, "ObjectName",
 CellID->1224892054],
Cell[2698, 135, 621, 19, 93, "Usage",
 CellID->982511436],
Cell[3322, 156, 639, 20, 28, "Notes",
 CellID->1067943069],
Cell[3964, 178, 875, 23, 63, "Notes",
 CellID->1257802987],
Cell[4842, 203, 573, 15, 44, "Notes",
 CellID->1129777026]
}, Open  ]],
Cell[CellGroupData[{
Cell[5452, 223, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[5512, 226, 233, 4, 16, "Tutorials",
 CellID->1542474150],
Cell[5748, 232, 277, 5, 16, "Tutorials",
 CellID->1375158345],
Cell[6028, 239, 279, 5, 16, "Tutorials",
 CellID->1009189986]
}, Open  ]],
Cell[CellGroupData[{
Cell[6344, 249, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[6430, 252, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[6525, 258, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[6593, 261, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[6679, 267, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[6737, 270, 243, 6, 22, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[7017, 281, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[7077, 284, 197, 4, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[7311, 293, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[7670, 306, 116, 3, 27, "Input",
 CellID->668534668],
Cell[7789, 311, 346, 9, 27, "Input",
 CellID->427723972],
Cell[8138, 322, 262, 6, 22, "ExampleText",
 CellID->1618778882],
Cell[CellGroupData[{
Cell[8425, 332, 403, 9, 27, "Input",
 CellID->1860310408],
Cell[8831, 343, 1437, 36, 106, "Output",
 CellID->1494194371]
}, Open  ]],
Cell[10283, 382, 589, 16, 41, "ExampleText",
 CellID->1169592302],
Cell[CellGroupData[{
Cell[10897, 402, 932, 24, 66, "Input",
 CellID->1678297378],
Cell[11832, 428, 2964, 95, 222, "Output",
 CellID->97236187]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[14845, 529, 100, 2, 55, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[14948, 533, 125, 3, 33, "ExampleSection",
 CellID->1293636265],
Cell[15076, 538, 148, 3, 21, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[15249, 545, 127, 3, 21, "ExampleSection",
 CellID->2061341341],
Cell[15379, 550, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[15512, 555, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[15657, 561, 131, 3, 21, "ExampleSection",
 CellID->258228157],
Cell[15791, 566, 142, 3, 21, "ExampleSection",
 CellID->2123667759],
Cell[15936, 571, 135, 3, 21, "ExampleSection",
 CellID->1305812373],
Cell[16074, 576, 140, 3, 21, "ExampleSection",
 CellID->1653164318],
Cell[16217, 581, 132, 3, 21, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)


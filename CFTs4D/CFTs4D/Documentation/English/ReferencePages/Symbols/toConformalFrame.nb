(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     16053,        615]
NotebookOptionsPosition[     11223,        443]
NotebookOutlinePosition[     11851,        468]
CellTagsIndexPosition[     11772,        463]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["1.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellChangeTimes->{3.703620769085309*^9},
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["CFTs4D", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["CFTs4D`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["CFTs4D/ref/toConformalFrame", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["toConformalFrame", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"toConformalFrame", "[", "expr", "]"}]], "InlineFormula"],
 " \[LineSeparator]converts the EF expression ",
 Cell[BoxData[
  StyleBox["expr", "TI"]], "InlineFormula"],
 " to a CF expression written in terns of \[Xi] and \[Eta] polarizations."
}], "Usage",
 CellChangeTimes->{{3.702946117471509*^9, 3.702946185583805*^9}, {
  3.702946537261676*^9, 3.7029465379930067`*^9}, {3.703395309482918*^9, 
  3.703395339767466*^9}, {3.703620991194098*^9, 3.703621035812605*^9}},
 CellID->982511436],

Cell[TextData[{
 "To use ",
 Cell[BoxData[
  ButtonBox["toConformalFrame",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/toConformalFrame"]], "InlineFormula"],
 ", you first need to load the ",
 ButtonBox["CFTs4D",
  BaseStyle->"Link",
  ButtonData->"paclet:CFTs4D/guide/CFTs4D"],
 " package using ",
 Cell[BoxData[
  RowBox[{
   ButtonBox["Needs",
    BaseStyle->"Link"], "[", "\"\<CFTs4D`\>\"", "]"}]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{{3.703039193544949*^9, 3.7030392141372538`*^9}, {
  3.7032639561532373`*^9, 3.703263956616253*^9}, {3.7032642341979723`*^9, 
  3.703264240491478*^9}, {3.703264822951807*^9, 3.70326482835677*^9}},
 CellID->195862193],

Cell[TextData[{
 "The output of ",
 Cell[BoxData[
  ButtonBox["toConformalFrame",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/toConformalFrame"]], "InlineFormula"],
 " can be used as input to ",
 Cell[BoxData[
  ButtonBox["collapseCFStructs",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/collapseCFStructs"]], "InlineFormula"],
 " to produce an expression containing the abstract ",
 Cell[BoxData[
  ButtonBox["CF4pt",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/CF4pt"]], "InlineFormula"],
 " structures."
}], "Notes",
 CellChangeTimes->{{3.703621041716004*^9, 3.7036210430956793`*^9}},
 CellID->1757864032]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell[TextData[ButtonBox["Conformal Partial Waves",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConformalPartialWaves"]], "Tutorials",
 CellChangeTimes->{{3.703866584347806*^9, 3.703866596787722*^9}},
 CellID->1542474150],

Cell[TextData[ButtonBox["Conserved Operators",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConservedOperators"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866522555599*^9}, {
  3.703866599411406*^9, 3.703866599610306*^9}},
 CellID->1375158345],

Cell[TextData[ButtonBox["Differential Basis",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/DifferentialBasis"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866525150303*^9}, {
  3.7038666017019033`*^9, 3.7038666018652353`*^9}},
 CellID->1009189986]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["toEmbeddingFormalism",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/toEmbeddingFormalism"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["collapseCFStructs",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/collapseCFStructs"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["expandCFStructs",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/expandCFStructs"]], "InlineFormula"]
}], "SeeAlso",
 CellChangeTimes->{{3.703620968729933*^9, 3.703620977617426*^9}, {
  3.7036210763237143`*^9, 3.7036210993304462`*^9}},
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell["XXXX", "MoreAbout",
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<CFTs4D`\>\"", "]"}]], "Input",
 CellChangeTimes->{{3.703395348652401*^9, 3.7033953530341682`*^9}},
 CellLabel->"In[64]:=",
 CellID->1227615966],

Cell["Example with a non-normalized structure", "ExampleText",
 CellContext->Notebook,
 CellID->1267193550],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   SubsuperscriptBox["I", 
    RowBox[{"{", 
     RowBox[{"3", ",", "4"}], "}"}], 
    RowBox[{"{", 
     RowBox[{"1", ",", "2"}], "}"}]], "//", "toConformalFrame"}], "//", 
  "Simplify"}]], "Input",
 CellChangeTimes->{{3.703395379842643*^9, 3.703395394825288*^9}},
 CellLabel->"In[66]:=",
 CellID->1293441811],

Cell[BoxData[
 RowBox[{
  RowBox[{
   SubscriptBox["\[Eta]", "2"], " ", 
   SubscriptBox[
    OverscriptBox["\[Eta]", "\[HorizontalLine]"], "1"]}], "-", 
  RowBox[{
   SubscriptBox["\[Xi]", "2"], " ", 
   SubscriptBox[
    OverscriptBox["\[Xi]", "\[HorizontalLine]"], "1"]}]}]], "Output",
 CellChangeTimes->{{3.7033953887488956`*^9, 3.703395395284996*^9}},
 CellLabel->"Out[66]=",
 CellID->114218114]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["\t", "ExampleDelimiter"],
  $Line = 0; Null]], "ExampleDelimiter",
 CellContext->Notebook,
 CellID->1652517328],

Cell["Example with a normalized structure", "ExampleText",
 CellContext->Notebook,
 CellID->846360522],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   SubsuperscriptBox[
    OverscriptBox["K", "\[HorizontalLine]"], 
    RowBox[{"{", "3", "}"}], 
    RowBox[{"{", 
     RowBox[{"1", ",", "2"}], "}"}]], "//", "toConformalFrame"}], "//", 
  "Simplify"}]], "Input",
 CellChangeTimes->{{3.703395402756228*^9, 3.7033954172243757`*^9}, 
   3.7036752595451727`*^9},
 CellLabel->"In[33]:=",
 CellID->2030322898],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "+", 
     OverscriptBox["z", "\[HorizontalLine]"]}], ")"}], " ", 
   SubscriptBox[
    OverscriptBox["\[Eta]", "\[HorizontalLine]"], "2"], " ", 
   SubscriptBox[
    OverscriptBox["\[Xi]", "\[HorizontalLine]"], "1"]}], "-", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "+", "z"}], ")"}], " ", 
   SubscriptBox[
    OverscriptBox["\[Eta]", "\[HorizontalLine]"], "1"], " ", 
   SubscriptBox[
    OverscriptBox["\[Xi]", "\[HorizontalLine]"], "2"]}]}]], "Output",
 CellChangeTimes->{{3.703395413459779*^9, 3.7033954175058537`*^9}, 
   3.7036752645260344`*^9},
 CellLabel->"Out[33]=",
 CellID->858183460]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{0, Automatic}, {Automatic, 28}},
CellContext->"Global`",
FrontEndVersion->"11.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (July 28, \
2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[9703, 385, 100, 2, 55, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 11634, 456}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 367, 15, 24, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[950, 39, 68, 1, 29, "CategorizationSection",
 CellID->1122911449],
Cell[1021, 42, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1103, 46, 79, 2, 70, "Categorization",
 CellID->605800465],
Cell[1185, 50, 76, 2, 70, "Categorization",
 CellID->468444828],
Cell[1264, 54, 72, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1373, 60, 55, 1, 19, "KeywordsSection",
 CellID->477174294],
Cell[1431, 63, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1513, 69, 65, 1, 19, "TemplatesSection",
 CellID->1872225408],
Cell[1581, 72, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1678, 76, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1763, 80, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1847, 84, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1966, 91, 53, 1, 19, "DetailsSection",
 CellID->307771771],
Cell[2022, 94, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2088, 98, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2160, 102, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2227, 106, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2299, 110, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2363, 114, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2427, 118, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2493, 122, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2574, 126, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2678, 133, 59, 1, 63, "ObjectName",
 CellID->1224892054],
Cell[2740, 136, 553, 12, 74, "Usage",
 CellID->982511436],
Cell[3296, 150, 685, 20, 28, "Notes",
 CellID->195862193],
Cell[3984, 172, 643, 19, 47, "Notes",
 CellID->1757864032]
}, Open  ]],
Cell[CellGroupData[{
Cell[4664, 196, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[4724, 199, 233, 4, 16, "Tutorials",
 CellID->1542474150],
Cell[4960, 205, 277, 5, 16, "Tutorials",
 CellID->1375158345],
Cell[5240, 212, 279, 5, 16, "Tutorials",
 CellID->1009189986]
}, Open  ]],
Cell[CellGroupData[{
Cell[5556, 222, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[5642, 225, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[5737, 231, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[5805, 234, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[5891, 240, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[5949, 243, 664, 18, 22, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[6650, 266, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[6710, 269, 46, 1, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[6793, 275, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[7152, 288, 185, 4, 27, "Input",
 CellID->1227615966],
Cell[7340, 294, 107, 2, 22, "ExampleText",
 CellID->1267193550],
Cell[CellGroupData[{
Cell[7472, 300, 345, 11, 31, "Input",
 CellID->1293441811],
Cell[7820, 313, 400, 12, 29, "Output",
 CellID->114218114]
}, Open  ]],
Cell[CellGroupData[{
Cell[8257, 330, 150, 4, 17, "ExampleDelimiter",
 CellID->1652517328],
Cell[8410, 336, 102, 2, 22, "ExampleText",
 CellID->846360522],
Cell[CellGroupData[{
Cell[8537, 342, 390, 12, 33, "Input",
 CellID->2030322898],
Cell[8930, 356, 712, 22, 30, "Output",
 CellID->858183460]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[9703, 385, 100, 2, 55, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[9806, 389, 125, 3, 33, "ExampleSection",
 CellID->1293636265],
Cell[9934, 394, 148, 3, 21, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[10107, 401, 127, 3, 21, "ExampleSection",
 CellID->2061341341],
Cell[10237, 406, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[10370, 411, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[10515, 417, 131, 3, 21, "ExampleSection",
 CellID->258228157],
Cell[10649, 422, 142, 3, 21, "ExampleSection",
 CellID->2123667759],
Cell[10794, 427, 135, 3, 21, "ExampleSection",
 CellID->1305812373],
Cell[10932, 432, 140, 3, 21, "ExampleSection",
 CellID->1653164318],
Cell[11075, 437, 132, 3, 21, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)


(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     28608,       1030]
NotebookOptionsPosition[     22791,        822]
NotebookOutlinePosition[     23420,        847]
CellTagsIndexPosition[     23341,        842]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["1.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["CFTs4D", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["CFTs4D`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["CFTs4D/ref/cfEvaluateInPlane", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["cfEvaluateInPlane", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{
   RowBox[{"cfEvaluateInPlane", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       SubscriptBox["z", "1"], ",", 
       SubscriptBox["zb", "1"]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       SubscriptBox["z", "2"], ",", 
       SubscriptBox["zb", "2"]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       SubscriptBox["z", "3"], ",", 
       SubscriptBox["zb", "3"]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       SubscriptBox["z", "4"], ",", 
       SubscriptBox["zb", "4"]}], "}"}]}], "]"}], "[", "expr", "]"}]], 
  "InlineFormula"],
 " \[LineSeparator]evaluates the four-point function represented by the \
conformal frame ",
 Cell[BoxData[
  StyleBox["expr", "TI"]], "InlineFormula"],
 " (given in terms of ",
 Cell[BoxData[
  ButtonBox["CF4pt",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/CF4pt"]], "InlineFormula"],
 " structures) at configuration ",
 Cell[BoxData[
  RowBox[{"{", 
   RowBox[{
    SubscriptBox["z", "i"], ",", 
    SubscriptBox["zb", "i"]}], "}"}]], "InlineFormula",
  FormatType->"StandardForm"],
 " in the conformal frame plane."
}], "Usage",
 CellChangeTimes->{{3.703021258737365*^9, 3.70302136427808*^9}},
 CellID->982511436],

Cell[TextData[{
 "To use ",
 Cell[BoxData[
  ButtonBox["cfEvaluateInPlane",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/cfEvaluateInPlane"]], "InlineFormula"],
 ", you first need to load the ",
 ButtonBox["CFTs4D",
  BaseStyle->"Link",
  ButtonData->"paclet:CFTs4D/guide/CFTs4D"],
 " package using ",
 Cell[BoxData[
  RowBox[{
   ButtonBox["Needs",
    BaseStyle->"Link"], "[", "\"\<CFTs4D`\>\"", "]"}]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{{3.7030213730666533`*^9, 3.703021393420396*^9}, {
  3.703264587061585*^9, 3.703264587365861*^9}},
 CellID->1067943069],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["cfEvaluateInPlane",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/cfEvaluateInPlane"]], "InlineFormula"],
 " returns an expression in terms of ",
 Cell[BoxData[
  ButtonBox["CF4pt",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/CF4pt"]], "InlineFormula"],
 " structures, which now only have the interpretation as products of \[Xi] \
and \[Eta] polarizations, and cannot be used in most of the package functions."
}], "Notes",
 CellChangeTimes->{{3.703021399947927*^9, 3.703021459238385*^9}},
 CellID->703151073],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["cfEvaluateInPlane",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/cfEvaluateInPlane"]], "InlineFormula"],
 " allows one of the coordinates to be ",
 Cell[BoxData[
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     SubscriptBox["z", "i"], ",", 
     SubscriptBox["zb", "i"]}], "}"}], "=", 
   RowBox[{"{", 
    RowBox[{"\[Infinity]", ",", "\[Infinity]"}], "}"}]}]], "InlineFormula",
  FormatType->"StandardForm"],
 "."
}], "Notes",
 CellChangeTimes->{{3.7030214801604767`*^9, 3.7030215055183764`*^9}},
 CellID->2137790401],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["permutePoints",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/permutePoints"]], "InlineFormula"],
 " uses ",
 Cell[BoxData[
  ButtonBox["cfEvaluateInPlane",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/cfEvaluateInPlane"]], "InlineFormula"],
 " to apply permutations to conformal frame expressions. "
}], "Notes",
 CellChangeTimes->{{3.703021509732542*^9, 3.703021548760933*^9}},
 CellID->328570305]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell[TextData[ButtonBox["Conformal Partial Waves",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConformalPartialWaves"]], "Tutorials",
 CellChangeTimes->{{3.703866584347806*^9, 3.703866596787722*^9}},
 CellID->1542474150],

Cell[TextData[ButtonBox["Conserved Operators",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConservedOperators"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866522555599*^9}, {
  3.703866599411406*^9, 3.703866599610306*^9}},
 CellID->1375158345],

Cell[TextData[ButtonBox["Differential Basis",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/DifferentialBasis"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866525150303*^9}, {
  3.7038666017019033`*^9, 3.7038666018652353`*^9}},
 CellID->1009189986]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["permutePoints",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/permutePoints"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["toEmbeddingFormalism",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/toEmbeddingFormalism"]], "InlineFormula"]
}], "SeeAlso",
 CellChangeTimes->{{3.703021558540382*^9, 3.703021578257427*^9}},
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell[TextData[ButtonBox["CFTs4D",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/guide/CFTs4D"]], "MoreAbout",
 CellChangeTimes->{{3.703819196363316*^9, 3.70381920279813*^9}},
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<CFTs4D`\>\"", "]"}]], "Input",
 CellID->631898909],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["\t", "ExampleDelimiter"],
  $Line = 0; Null]], "ExampleDelimiter",
 CellID->801764489],

Cell["\<\
Transform a scalar 4-point function to the \[Rho] coordinate frame\
\>", "ExampleText",
 CellChangeTimes->{{3.7030215906967087`*^9, 3.703021601435733*^9}, {
  3.703021950146914*^9, 3.703021975450918*^9}},
 CellID->1018329148],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"scalar4pt", "=", 
  RowBox[{
   RowBox[{"collapseCFStructs", "[", 
    RowBox[{"{", 
     RowBox[{
     "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3", 
      ",", "\[CapitalDelta]4"}], "}"}], "]"}], "[", 
   RowBox[{"g", "[", 
    RowBox[{"z", ",", "zb"}], "]"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.703021609322534*^9, 3.703021628450679*^9}},
 CellLabel->"In[23]:=",
 CellID->783993992],

Cell[BoxData[
 RowBox[{"CF4pt", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
    "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3", ",",
      "\[CapitalDelta]4"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"g", "[", 
    RowBox[{"z", ",", 
     OverscriptBox["z", "\[HorizontalLine]"]}], "]"}]}], "]"}]], "Output",
 CellChangeTimes->{3.703021628733439*^9},
 CellLabel->"Out[23]=",
 CellID->1245550093]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"cfEvaluateInPlane", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{"-", "\[Rho]"}], ",", 
      RowBox[{"-", "\[Rho]b"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"\[Rho]", ",", "\[Rho]b"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"1", ",", "1"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"-", "1"}], ",", 
      RowBox[{"-", "1"}]}], "}"}]}], "]"}], "[", "scalar4pt", "]"}]], "Input",\

 CellChangeTimes->{{3.7030216322833223`*^9, 3.703021692737412*^9}},
 CellLabel->"In[25]:=",
 CellID->289822095],

Cell[BoxData[
 RowBox[{"CF4pt", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
    "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3", ",",
      "\[CapitalDelta]4"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{
    SuperscriptBox["2", 
     RowBox[{
     "\[CapitalDelta]1", "+", "\[CapitalDelta]2", "-", "\[CapitalDelta]3", 
      "-", "\[CapitalDelta]4"}]], " ", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "1"}], "+", "\[Rho]"}], ")"}], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "1"}], "+", "\[Rho]b"}], ")"}]}], ")"}], 
     RowBox[{
      FractionBox["1", "2"], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", "\[CapitalDelta]1"}], "+", "\[CapitalDelta]2", "+", 
        "\[CapitalDelta]3", "-", "\[CapitalDelta]4"}], ")"}]}]], " ", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"1", "+", "\[Rho]"}], ")"}], " ", 
       RowBox[{"(", 
        RowBox[{"1", "+", "\[Rho]b"}], ")"}]}], ")"}], 
     RowBox[{
      FractionBox["1", "2"], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", "\[CapitalDelta]1"}], "-", 
        RowBox[{"3", " ", "\[CapitalDelta]2"}], "-", "\[CapitalDelta]3", "+", 
        "\[CapitalDelta]4"}], ")"}]}]], " ", 
    RowBox[{"g", "[", 
     RowBox[{
      FractionBox[
       RowBox[{"4", " ", "\[Rho]"}], 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"1", "+", "\[Rho]"}], ")"}], "2"]], ",", 
      FractionBox[
       RowBox[{"4", " ", "\[Rho]b"}], 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"1", "+", "\[Rho]b"}], ")"}], "2"]]}], "]"}]}]}], 
  "]"}]], "Output",
 CellChangeTimes->{3.7030216930378103`*^9},
 CellLabel->"Out[25]=",
 CellID->1552400056]
}, Open  ]],

Cell["Or make a rather trivial transformation", "ExampleText",
 CellChangeTimes->{{3.7030217752925253`*^9, 3.703021792836112*^9}},
 CellID->1408913789],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"cfEvaluateInPlane", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"0", ",", "0"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"2", "z"}], ",", 
      RowBox[{"2", "zb"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"2", ",", "2"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"\[Infinity]", ",", "\[Infinity]"}], "}"}]}], "]"}], "[", 
  "scalar4pt", "]"}]], "Input",
 CellChangeTimes->{{3.7030217960619707`*^9, 3.703021839584424*^9}},
 CellLabel->"In[27]:=",
 CellID->1036810440],

Cell[BoxData[
 RowBox[{"CF4pt", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
    "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3", ",",
      "\[CapitalDelta]4"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{
    SuperscriptBox["2", 
     RowBox[{
      RowBox[{"-", "\[CapitalDelta]1"}], "-", "\[CapitalDelta]2", "-", 
      "\[CapitalDelta]3", "+", "\[CapitalDelta]4"}]], " ", 
    RowBox[{"g", "[", 
     RowBox[{"z", ",", 
      OverscriptBox["z", "\[HorizontalLine]"]}], "]"}]}]}], "]"}]], "Output",
 CellChangeTimes->{3.70302180948497*^9, 3.703021839889398*^9},
 CellLabel->"Out[27]=",
 CellID->1359304246]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["\t", "ExampleDelimiter"],
  $Line = 0; Null]], "ExampleDelimiter",
 CellID->895425736],

Cell["\<\
Transform a fermion-scalar 4-point function to the \[Rho] coordinate frame\
\>", "ExampleText",
 CellChangeTimes->{{3.703021944988718*^9, 3.703021994739427*^9}},
 CellID->923661617],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"spinor4pt", "=", 
  RowBox[{
   RowBox[{"collapseCFStructs", "[", 
    RowBox[{"{", 
     RowBox[{
     "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3", 
      ",", "\[CapitalDelta]4"}], "}"}], "]"}], "[", 
   RowBox[{
    SubscriptBox["\[Xi]", "1"], " ", 
    SubscriptBox["\[Eta]", "2"], 
    RowBox[{"g", "[", 
     RowBox[{"z", ",", "zb"}], "]"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7030219145521*^9, 3.703021929120788*^9}},
 CellLabel->"In[28]:=",
 CellID->2002430423],

Cell[BoxData[
 RowBox[{"CF4pt", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
    "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3", ",",
      "\[CapitalDelta]4"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "1", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"-", 
      FractionBox["1", "2"]}], ",", 
     FractionBox["1", "2"], ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"g", "[", 
    RowBox[{"z", ",", 
     OverscriptBox["z", "\[HorizontalLine]"]}], "]"}]}], "]"}]], "Output",
 CellChangeTimes->{3.703021930216052*^9},
 CellLabel->"Out[28]=",
 CellID->2037014813]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"cfEvaluateInPlane", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{"-", "\[Rho]"}], ",", 
      RowBox[{"-", "\[Rho]b"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"\[Rho]", ",", "\[Rho]b"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"1", ",", "1"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"-", "1"}], ",", 
      RowBox[{"-", "1"}]}], "}"}]}], "]"}], "[", "spinor4pt", "]"}]], "Input",\

 CellChangeTimes->{3.703022003318488*^9},
 CellLabel->"In[29]:=",
 CellID->2051213782],

Cell[BoxData[
 RowBox[{"CF4pt", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
    "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3", ",",
      "\[CapitalDelta]4"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "1", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"-", 
      FractionBox["1", "2"]}], ",", 
     FractionBox["1", "2"], ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"-", 
    RowBox[{
     FractionBox["1", 
      RowBox[{"1", "+", "\[Rho]"}]], 
     RowBox[{
      SuperscriptBox["2", 
       RowBox[{
       "\[CapitalDelta]1", "+", "\[CapitalDelta]2", "-", "\[CapitalDelta]3", 
        "-", "\[CapitalDelta]4"}]], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", "1"}], "+", "\[Rho]"}], ")"}], " ", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{"-", "1"}], "+", "\[Rho]"}], ")"}], " ", 
         RowBox[{"(", 
          RowBox[{
           RowBox[{"-", "1"}], "+", "\[Rho]b"}], ")"}]}], ")"}], 
       RowBox[{
        FractionBox["1", "2"], " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "1"}], "-", "\[CapitalDelta]1", "+", 
          "\[CapitalDelta]2", "+", "\[CapitalDelta]3", "-", 
          "\[CapitalDelta]4"}], ")"}]}]], " ", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"1", "+", "\[Rho]"}], ")"}], " ", 
         RowBox[{"(", 
          RowBox[{"1", "+", "\[Rho]b"}], ")"}]}], ")"}], 
       RowBox[{
        FractionBox["1", "2"], " ", 
        RowBox[{"(", 
         RowBox[{"1", "-", "\[CapitalDelta]1", "-", 
          RowBox[{"3", " ", "\[CapitalDelta]2"}], "-", "\[CapitalDelta]3", 
          "+", "\[CapitalDelta]4"}], ")"}]}]], " ", 
      RowBox[{"g", "[", 
       RowBox[{
        FractionBox[
         RowBox[{"4", " ", "\[Rho]"}], 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"1", "+", "\[Rho]"}], ")"}], "2"]], ",", 
        FractionBox[
         RowBox[{"4", " ", "\[Rho]b"}], 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"1", "+", "\[Rho]b"}], ")"}], "2"]]}], "]"}]}]}]}]}], 
  "]"}]], "Output",
 CellChangeTimes->{3.703022003875724*^9},
 CellLabel->"Out[29]=",
 CellID->1917829137]
}, Open  ]],

Cell["Or make a rather trivial transformation", "ExampleText",
 CellChangeTimes->{{3.7030217752925253`*^9, 3.703021792836112*^9}},
 CellID->1372247736],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"cfEvaluateInPlane", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"0", ",", "0"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"2", "z"}], ",", 
      RowBox[{"2", "zb"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"2", ",", "2"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"\[Infinity]", ",", "\[Infinity]"}], "}"}]}], "]"}], "[", 
  "spinor4pt", "]"}]], "Input",
 CellChangeTimes->{{3.7030217960619707`*^9, 3.703021839584424*^9}, 
   3.703022019768373*^9},
 CellLabel->"In[30]:=",
 CellID->1540813819],

Cell[BoxData[
 RowBox[{"CF4pt", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
    "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3", ",",
      "\[CapitalDelta]4"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "1", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"-", 
      FractionBox["1", "2"]}], ",", 
     FractionBox["1", "2"], ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{
    SuperscriptBox["2", 
     RowBox[{
      RowBox[{"-", "\[CapitalDelta]1"}], "-", "\[CapitalDelta]2", "-", 
      "\[CapitalDelta]3", "+", "\[CapitalDelta]4"}]], " ", 
    RowBox[{"g", "[", 
     RowBox[{"z", ",", 
      OverscriptBox["z", "\[HorizontalLine]"]}], "]"}]}]}], "]"}]], "Output",
 CellChangeTimes->{3.70302180948497*^9, 3.703021839889398*^9, 
  3.703022020124968*^9},
 CellLabel->"Out[30]=",
 CellID->317944853]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{4, Automatic}, {Automatic, 28}},
CellContext->"Global`",
FrontEndVersion->"11.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (July 28, \
2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[21271, 764, 100, 2, 55, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 23202, 835}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 325, 14, 24, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[908, 38, 68, 1, 29, "CategorizationSection",
 CellID->1122911449],
Cell[979, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1061, 45, 79, 2, 70, "Categorization",
 CellID->605800465],
Cell[1143, 49, 76, 2, 70, "Categorization",
 CellID->468444828],
Cell[1222, 53, 73, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1332, 59, 55, 1, 19, "KeywordsSection",
 CellID->477174294],
Cell[1390, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1472, 68, 65, 1, 19, "TemplatesSection",
 CellID->1872225408],
Cell[1540, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1637, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1722, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1806, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1925, 90, 53, 1, 19, "DetailsSection",
 CellID->307771771],
Cell[1981, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2047, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2119, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2186, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2258, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2322, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2386, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2452, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2533, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2637, 132, 60, 1, 63, "ObjectName",
 CellID->1224892054],
Cell[2700, 135, 1263, 42, 96, "Usage",
 CellID->982511436],
Cell[3966, 179, 590, 19, 28, "Notes",
 CellID->1067943069],
Cell[4559, 200, 575, 14, 44, "Notes",
 CellID->703151073],
Cell[5137, 216, 570, 18, 28, "Notes",
 CellID->2137790401],
Cell[5710, 236, 464, 13, 28, "Notes",
 CellID->328570305]
}, Open  ]],
Cell[CellGroupData[{
Cell[6211, 254, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[6271, 257, 233, 4, 16, "Tutorials",
 CellID->1542474150],
Cell[6507, 263, 277, 5, 16, "Tutorials",
 CellID->1375158345],
Cell[6787, 270, 279, 5, 16, "Tutorials",
 CellID->1009189986]
}, Open  ]],
Cell[CellGroupData[{
Cell[7103, 280, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[7189, 283, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[7284, 289, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[7352, 292, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[7438, 298, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[7496, 301, 432, 12, 22, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[7965, 318, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[8025, 321, 197, 4, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[8259, 330, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[8618, 343, 92, 2, 27, "Input",
 CellID->631898909],
Cell[CellGroupData[{
Cell[8735, 349, 125, 3, 17, "ExampleDelimiter",
 CellID->801764489],
Cell[8863, 354, 235, 5, 22, "ExampleText",
 CellID->1018329148],
Cell[CellGroupData[{
Cell[9123, 363, 439, 12, 27, "Input",
 CellID->783993992],
Cell[9565, 377, 721, 20, 49, "Output",
 CellID->1245550093]
}, Open  ]],
Cell[CellGroupData[{
Cell[10323, 402, 578, 19, 27, "Input",
 CellID->289822095],
Cell[10904, 423, 2104, 64, 103, "Output",
 CellID->1552400056]
}, Open  ]],
Cell[13023, 490, 151, 2, 22, "ExampleText",
 CellID->1408913789],
Cell[CellGroupData[{
Cell[13199, 496, 535, 17, 27, "Input",
 CellID->1036810440],
Cell[13737, 515, 930, 25, 49, "Output",
 CellID->1359304246]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[14716, 546, 125, 3, 17, "ExampleDelimiter",
 CellID->895425736],
Cell[14844, 551, 191, 4, 22, "ExampleText",
 CellID->923661617],
Cell[CellGroupData[{
Cell[15060, 559, 526, 15, 27, "Input",
 CellID->2002430423],
Cell[15589, 576, 791, 23, 68, "Output",
 CellID->2037014813]
}, Open  ]],
Cell[CellGroupData[{
Cell[16417, 604, 553, 19, 27, "Input",
 CellID->2051213782],
Cell[16973, 625, 2458, 74, 124, "Output",
 CellID->1917829137]
}, Open  ]],
Cell[19446, 702, 151, 2, 22, "ExampleText",
 CellID->1372247736],
Cell[CellGroupData[{
Cell[19622, 708, 561, 18, 27, "Input",
 CellID->1540813819],
Cell[20186, 728, 1024, 29, 68, "Output",
 CellID->317944853]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[21271, 764, 100, 2, 55, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[21374, 768, 125, 3, 33, "ExampleSection",
 CellID->1293636265],
Cell[21502, 773, 148, 3, 21, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[21675, 780, 127, 3, 21, "ExampleSection",
 CellID->2061341341],
Cell[21805, 785, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[21938, 790, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[22083, 796, 131, 3, 21, "ExampleSection",
 CellID->258228157],
Cell[22217, 801, 142, 3, 21, "ExampleSection",
 CellID->2123667759],
Cell[22362, 806, 135, 3, 21, "ExampleSection",
 CellID->1305812373],
Cell[22500, 811, 140, 3, 21, "ExampleSection",
 CellID->1653164318],
Cell[22643, 816, 132, 3, 21, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)


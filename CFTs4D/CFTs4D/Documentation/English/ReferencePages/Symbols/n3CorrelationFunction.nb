(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     26908,        968]
NotebookOptionsPosition[     21517,        776]
NotebookOutlinePosition[     22146,        801]
CellTagsIndexPosition[     22067,        796]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["1.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["CFTs4D", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["CFTs4D`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["CFTs4D/ref/n3CorrelationFunction", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["n3CorrelationFunction", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"n3CorrelationFunction", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      SubscriptBox["\[CapitalDelta]", "1"], ",", 
      SubscriptBox["\[CapitalDelta]", "2"], ",", 
      SubscriptBox["\[CapitalDelta]", "3"]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
        SubscriptBox["l", "1"], ",", 
        SubscriptBox["lb", "1"]}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
        SubscriptBox["l", "2"], ",", 
        SubscriptBox["lb", "2"]}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
        SubscriptBox["l", "3"], ",", 
        SubscriptBox["lb", "3"]}], "}"}]}], "}"}]}], "]"}]], 
  "InlineFormula"],
 " \[LineSeparator]constructs a generic 3-point correlation functions for \
operators with scaling dimensions ",
 Cell[BoxData[
  SubscriptBox["\[CapitalDelta]", "i"]], "InlineFormula"],
 " and spins ",
 Cell[BoxData[
  RowBox[{"(", 
   RowBox[{
    SubscriptBox["l", "i"], ",", 
    SubscriptBox["lb", "i"]}], ")"}]], "InlineFormula"],
 "."
}], "Usage",
 CellChangeTimes->{{3.7032728242082987`*^9, 3.703272910404387*^9}},
 CellID->982511436],

Cell[TextData[{
 "To use ",
 Cell[BoxData[
  ButtonBox["n3CorrelationFunction",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n3CorrelationFunction"]], "InlineFormula"],
 ", you first need to load the ",
 ButtonBox["CFTs4D",
  BaseStyle->"Link",
  ButtonData->"paclet:CFTs4D/guide/CFTs4D"],
 " package using ",
 Cell[BoxData[
  RowBox[{
   ButtonBox["Needs",
    BaseStyle->"Link"], "[", "\"\<CFTs4D`\>\"", "]"}]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{{3.703039193544949*^9, 3.7030392141372538`*^9}, {
  3.70326326707141*^9, 3.703263281337151*^9}, {3.7032646759544*^9, 
  3.703264676252846*^9}},
 CellID->1067943069],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["n3CorrelationFunction",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n3CorrelationFunction"]], "InlineFormula"],
 " combines the action of ",
 Cell[BoxData[
  ButtonBox["n3KinematicFactor",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n3KinematicFactor"]], "InlineFormula"],
 " together with ",
 Cell[BoxData[
  ButtonBox["n3ListStructures",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n3ListStructures"]], "InlineFormula"],
 " to generate tensor structures for non traceless symmetric operators  and \
with ",
 Cell[BoxData[
  ButtonBox["n3ListStructuresAlternativeTS",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n3ListStructuresAlternativeTS"]], 
  "InlineFormula"],
 " to generate tensor structures for traceless symmetric operators."
}], "Notes",
 CellChangeTimes->{{3.703665366525077*^9, 3.70366551212531*^9}},
 CellID->493872563],

Cell[TextData[{
 "All the independent tensor structures are weighted with the coupling \
constants (OPE-coefficients) ",
 Cell[BoxData[
  SubscriptBox["\[Lambda]", "i"]], "InlineFormula"],
 " for non-traceless symmetric operators and with ",
 Cell[BoxData[
  SubsuperscriptBox["\[Lambda]", "i", "+"]], "InlineFormula"],
 " and ",
 Cell[BoxData[
  SubsuperscriptBox["\[Lambda]", "i", "-"]], "InlineFormula"],
 " for traceless symmetric operators. The superscripts indicate that these \
coupling constants correspond to parity even and parity odd structures \
respectively."
}], "Notes",
 CellChangeTimes->{{3.7036651354069157`*^9, 3.703665340094599*^9}, {
  3.703665522879648*^9, 3.703665573213785*^9}, {3.703747046540695*^9, 
  3.7037470466621304`*^9}, {3.703747079409573*^9, 3.7037471016810102`*^9}},
 CellID->1063818025],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["n3CorrelationFunction",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n3CorrelationFunction"]], "InlineFormula"],
 " returns 0 if conformally-invariant three-point structures do not exist."
}], "Notes",
 CellChangeTimes->{{3.7032729263912983`*^9, 3.703272937895864*^9}, {
  3.703272994094041*^9, 3.703273019109241*^9}, {3.703665158179583*^9, 
  3.703665167758074*^9}},
 CellID->1892310844]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell[TextData[ButtonBox["Conformal Partial Waves",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConformalPartialWaves"]], "Tutorials",
 CellChangeTimes->{{3.703866584347806*^9, 3.703866596787722*^9}},
 CellID->1542474150],

Cell[TextData[ButtonBox["Conserved Operators",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConservedOperators"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866522555599*^9}, {
  3.703866599411406*^9, 3.703866599610306*^9}},
 CellID->1375158345],

Cell[TextData[ButtonBox["Differential Basis",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/DifferentialBasis"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866525150303*^9}, {
  3.7038666017019033`*^9, 3.7038666018652353`*^9}},
 CellID->1009189986]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["n3ListStructures",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n3ListStructures"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["n3ListStructuresAlternativeTS",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n3ListStructuresAlternativeTS"]], 
  "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["n4ListStructures",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n4ListStructures"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["n4ListStructuresEF",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n4ListStructuresEF"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["n3KinematicFactor",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n3KinematicFactor"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["n4KinematicFactor",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/n4KinematicFactor"]], "InlineFormula"]
}], "SeeAlso",
 CellChangeTimes->{{3.703273501415987*^9, 3.703273593581915*^9}},
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell[TextData[ButtonBox["CFTs4D",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/guide/CFTs4D"]], "MoreAbout",
 CellChangeTimes->{{3.703819196363316*^9, 3.70381920279813*^9}},
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<CFTs4D`\>\"", "]"}]], "Input",
 CellID->773525875],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["\t", "ExampleDelimiter"],
  $Line = 0; Null]], "ExampleDelimiter",
 CellContext->Notebook,
 CellID->1652517328],

Cell["Traceless-symmetric case", "ExampleText",
 CellChangeTimes->{{3.703273435651421*^9, 3.7032734418474083`*^9}},
 CellID->843295118],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"n3CorrelationFunction", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
    "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3"}], 
    "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"1", ",", "1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1", ",", "1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1", ",", "1"}], "}"}]}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.703273051612174*^9, 3.7032730527565203`*^9}, {
  3.703273138160183*^9, 3.703273191550091*^9}, {3.703273419941839*^9, 
  3.70327342887304*^9}, {3.703665648304184*^9, 3.703665691051505*^9}},
 CellLabel->"In[54]:=",
 CellID->1930865834],

Cell[BoxData[
 RowBox[{
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"1", ",", "2"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "1"}], "-", "\[CapitalDelta]1", "-", "\[CapitalDelta]2", 
      "+", "\[CapitalDelta]3"}], ")"}]}]], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"1", ",", "3"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "1"}], "-", "\[CapitalDelta]1", "+", "\[CapitalDelta]2", 
      "-", "\[CapitalDelta]3"}], ")"}]}]], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"2", ",", "3"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "1"}], "+", "\[CapitalDelta]1", "-", "\[CapitalDelta]2", 
      "-", "\[CapitalDelta]3"}], ")"}]}]], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        SuperscriptBox[
         OverscriptBox["I", "^"], 
         RowBox[{"{", 
          RowBox[{"1", ",", "2"}], "}"}]], " ", 
        SuperscriptBox[
         OverscriptBox["I", "^"], 
         RowBox[{"{", 
          RowBox[{"2", ",", "3"}], "}"}]], " ", 
        SuperscriptBox[
         OverscriptBox["I", "^"], 
         RowBox[{"{", 
          RowBox[{"3", ",", "1"}], "}"}]]}], "+", 
       RowBox[{
        SuperscriptBox[
         OverscriptBox["I", "^"], 
         RowBox[{"{", 
          RowBox[{"1", ",", "3"}], "}"}]], " ", 
        SuperscriptBox[
         OverscriptBox["I", "^"], 
         RowBox[{"{", 
          RowBox[{"2", ",", "1"}], "}"}]], " ", 
        SuperscriptBox[
         OverscriptBox["I", "^"], 
         RowBox[{"{", 
          RowBox[{"3", ",", "2"}], "}"}]]}]}], ")"}], " ", 
     SubsuperscriptBox["\[Lambda]", "1", "-"]}], "+", 
    RowBox[{
     SubsuperscriptBox[
      OverscriptBox["J", "^"], 
      RowBox[{"{", 
       RowBox[{"2", ",", "3"}], "}"}], 
      RowBox[{"{", "1", "}"}]], " ", 
     SubsuperscriptBox[
      OverscriptBox["J", "^"], 
      RowBox[{"{", 
       RowBox[{"1", ",", "3"}], "}"}], 
      RowBox[{"{", "2", "}"}]], " ", 
     SubsuperscriptBox[
      OverscriptBox["J", "^"], 
      RowBox[{"{", 
       RowBox[{"1", ",", "2"}], "}"}], 
      RowBox[{"{", "3", "}"}]], " ", 
     SubsuperscriptBox["\[Lambda]", "1", "+"]}], "+", 
    RowBox[{
     SubsuperscriptBox[
      OverscriptBox["J", "^"], 
      RowBox[{"{", 
       RowBox[{"2", ",", "3"}], "}"}], 
      RowBox[{"{", "1", "}"}]], " ", 
     SuperscriptBox[
      OverscriptBox["I", "^"], 
      RowBox[{"{", 
       RowBox[{"2", ",", "3"}], "}"}]], " ", 
     SuperscriptBox[
      OverscriptBox["I", "^"], 
      RowBox[{"{", 
       RowBox[{"3", ",", "2"}], "}"}]], " ", 
     SubsuperscriptBox["\[Lambda]", "2", "+"]}], "+", 
    RowBox[{
     SuperscriptBox[
      OverscriptBox["I", "^"], 
      RowBox[{"{", 
       RowBox[{"1", ",", "3"}], "}"}]], " ", 
     SubsuperscriptBox[
      OverscriptBox["J", "^"], 
      RowBox[{"{", 
       RowBox[{"1", ",", "3"}], "}"}], 
      RowBox[{"{", "2", "}"}]], " ", 
     SuperscriptBox[
      OverscriptBox["I", "^"], 
      RowBox[{"{", 
       RowBox[{"3", ",", "1"}], "}"}]], " ", 
     SubsuperscriptBox["\[Lambda]", "3", "+"]}], "+", 
    RowBox[{
     SuperscriptBox[
      OverscriptBox["I", "^"], 
      RowBox[{"{", 
       RowBox[{"1", ",", "2"}], "}"}]], " ", 
     SuperscriptBox[
      OverscriptBox["I", "^"], 
      RowBox[{"{", 
       RowBox[{"2", ",", "1"}], "}"}]], " ", 
     SubsuperscriptBox[
      OverscriptBox["J", "^"], 
      RowBox[{"{", 
       RowBox[{"1", ",", "2"}], "}"}], 
      RowBox[{"{", "3", "}"}]], " ", 
     SubsuperscriptBox["\[Lambda]", "4", "+"]}]}], ")"}]}]], "Output",
 CellChangeTimes->{
  3.7032731920551662`*^9, 3.703273429110758*^9, 3.703665618450225*^9, {
   3.70366565022913*^9, 3.703665691431858*^9}},
 CellLabel->"Out[54]=",
 CellID->750813896]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["\t", "ExampleDelimiter"],
  $Line = 0; Null]], "ExampleDelimiter",
 CellContext->Notebook,
 CellID->542867917],

Cell["Non-traceless-symmetric case", "ExampleText",
 CellChangeTimes->{{3.7032734449383698`*^9, 3.703273450736245*^9}},
 CellID->1296517763],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"n3CorrelationFunction", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
    "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3"}], 
    "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"3", ",", "2"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"4", ",", "5"}], "}"}]}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.703273456172063*^9, 3.7032734649896193`*^9}},
 CellLabel->"In[48]:=",
 CellID->1093551417],

Cell[BoxData[
 RowBox[{
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"1", ",", "2"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
     "2", "-", "\[CapitalDelta]1", "-", "\[CapitalDelta]2", "+", 
      "\[CapitalDelta]3"}], ")"}]}]], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"1", ",", "3"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "2"}], "-", "\[CapitalDelta]1", "+", "\[CapitalDelta]2", 
      "-", "\[CapitalDelta]3"}], ")"}]}]], " ", 
  SubsuperscriptBox["X", 
   RowBox[{"{", 
    RowBox[{"2", ",", "3"}], "}"}], 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "7"}], "+", "\[CapitalDelta]1", "-", "\[CapitalDelta]2", 
      "-", "\[CapitalDelta]3"}], ")"}]}]], " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{
     SuperscriptBox[
      RowBox[{"(", 
       SubsuperscriptBox[
        OverscriptBox["J", "^"], 
        RowBox[{"{", 
         RowBox[{"1", ",", "3"}], "}"}], 
        RowBox[{"{", "2", "}"}]], ")"}], "2"], " ", 
     SuperscriptBox[
      OverscriptBox["I", "^"], 
      RowBox[{"{", 
       RowBox[{"3", ",", "2"}], "}"}]], " ", 
     SuperscriptBox[
      RowBox[{"(", 
       SubsuperscriptBox[
        OverscriptBox["J", "^"], 
        RowBox[{"{", 
         RowBox[{"1", ",", "2"}], "}"}], 
        RowBox[{"{", "3", "}"}]], ")"}], "4"], " ", 
     SubscriptBox["\[Lambda]", "1"]}], "+", 
    RowBox[{
     SubsuperscriptBox[
      OverscriptBox["J", "^"], 
      RowBox[{"{", 
       RowBox[{"1", ",", "3"}], "}"}], 
      RowBox[{"{", "2", "}"}]], " ", 
     SuperscriptBox[
      OverscriptBox["I", "^"], 
      RowBox[{"{", 
       RowBox[{"2", ",", "3"}], "}"}]], " ", 
     SuperscriptBox[
      RowBox[{"(", 
       SuperscriptBox[
        OverscriptBox["I", "^"], 
        RowBox[{"{", 
         RowBox[{"3", ",", "2"}], "}"}]], ")"}], "2"], " ", 
     SuperscriptBox[
      RowBox[{"(", 
       SubsuperscriptBox[
        OverscriptBox["J", "^"], 
        RowBox[{"{", 
         RowBox[{"1", ",", "2"}], "}"}], 
        RowBox[{"{", "3", "}"}]], ")"}], "3"], " ", 
     SubscriptBox["\[Lambda]", "2"]}], "+", 
    RowBox[{
     SuperscriptBox[
      RowBox[{"(", 
       SuperscriptBox[
        OverscriptBox["I", "^"], 
        RowBox[{"{", 
         RowBox[{"2", ",", "3"}], "}"}]], ")"}], "2"], " ", 
     SuperscriptBox[
      RowBox[{"(", 
       SuperscriptBox[
        OverscriptBox["I", "^"], 
        RowBox[{"{", 
         RowBox[{"3", ",", "2"}], "}"}]], ")"}], "3"], " ", 
     SuperscriptBox[
      RowBox[{"(", 
       SubsuperscriptBox[
        OverscriptBox["J", "^"], 
        RowBox[{"{", 
         RowBox[{"1", ",", "2"}], "}"}], 
        RowBox[{"{", "3", "}"}]], ")"}], "2"], " ", 
     SubscriptBox["\[Lambda]", "3"]}]}], ")"}]}]], "Output",
 CellChangeTimes->{{3.703273458175947*^9, 3.7032734652377768`*^9}, 
   3.703665620989541*^9},
 CellLabel->"Out[48]=",
 CellID->1202030276]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["\t", "ExampleDelimiter"],
  $Line = 0; Null]], "ExampleDelimiter",
 CellContext->Notebook,
 CellID->1281914028],

Cell["No non-trivial structures", "ExampleText",
 CellChangeTimes->{{3.7032734449383698`*^9, 3.703273484390645*^9}},
 CellID->1544197007],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"n3CorrelationFunction", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
    "\[CapitalDelta]1", ",", "\[CapitalDelta]2", ",", "\[CapitalDelta]3"}], 
    "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"4", ",", "5"}], "}"}]}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.703273456172063*^9, 3.7032734872986403`*^9}},
 CellLabel->"In[7]:=",
 CellID->979222654],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{{3.703273458175947*^9, 3.703273487544916*^9}},
 CellLabel->"Out[7]=",
 CellID->832008044]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{4, Automatic}, {Automatic, 84}},
CellContext->"Global`",
FrontEndVersion->"11.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (July 28, \
2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[19997, 718, 100, 2, 55, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 21928, 789}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 325, 14, 24, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[908, 38, 68, 1, 29, "CategorizationSection",
 CellID->1122911449],
Cell[979, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1061, 45, 79, 2, 70, "Categorization",
 CellID->605800465],
Cell[1143, 49, 76, 2, 70, "Categorization",
 CellID->468444828],
Cell[1222, 53, 77, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1336, 59, 55, 1, 19, "KeywordsSection",
 CellID->477174294],
Cell[1394, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1476, 68, 65, 1, 19, "TemplatesSection",
 CellID->1872225408],
Cell[1544, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1641, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1726, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1810, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1929, 90, 53, 1, 19, "DetailsSection",
 CellID->307771771],
Cell[1985, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2051, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2123, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2190, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2262, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2326, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2390, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2456, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2537, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2641, 132, 64, 1, 63, "ObjectName",
 CellID->1224892054],
Cell[2708, 135, 1174, 38, 92, "Usage",
 CellID->982511436],
Cell[3885, 175, 644, 20, 47, "Notes",
 CellID->1067943069],
Cell[4532, 197, 926, 25, 66, "Notes",
 CellID->493872563],
Cell[5461, 224, 822, 18, 77, "Notes",
 CellID->1063818025],
Cell[6286, 244, 443, 10, 28, "Notes",
 CellID->1892310844]
}, Open  ]],
Cell[CellGroupData[{
Cell[6766, 259, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[6826, 262, 233, 4, 16, "Tutorials",
 CellID->1542474150],
Cell[7062, 268, 277, 5, 16, "Tutorials",
 CellID->1375158345],
Cell[7342, 275, 279, 5, 16, "Tutorials",
 CellID->1009189986]
}, Open  ]],
Cell[CellGroupData[{
Cell[7658, 285, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[7744, 288, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[7839, 294, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[7907, 297, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[7993, 303, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[8051, 306, 1159, 33, 41, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[9247, 344, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[9307, 347, 197, 4, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[9541, 356, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[9900, 369, 92, 2, 27, "Input",
 CellID->773525875],
Cell[CellGroupData[{
Cell[10017, 375, 150, 4, 17, "ExampleDelimiter",
 CellID->1652517328],
Cell[10170, 381, 135, 2, 22, "ExampleText",
 CellID->843295118],
Cell[CellGroupData[{
Cell[10330, 387, 685, 19, 27, "Input",
 CellID->1930865834],
Cell[11018, 408, 3943, 127, 95, "Output",
 CellID->750813896]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[15010, 541, 149, 4, 17, "ExampleDelimiter",
 CellID->542867917],
Cell[15162, 547, 140, 2, 22, "ExampleText",
 CellID->1296517763],
Cell[CellGroupData[{
Cell[15327, 553, 542, 17, 27, "Input",
 CellID->1093551417],
Cell[15872, 572, 3020, 99, 70, "Output",
 CellID->1202030276]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[18941, 677, 150, 4, 17, "ExampleDelimiter",
 CellID->1281914028],
Cell[19094, 683, 137, 2, 22, "ExampleText",
 CellID->1544197007],
Cell[CellGroupData[{
Cell[19256, 689, 540, 17, 27, "Input",
 CellID->979222654],
Cell[19799, 708, 137, 3, 26, "Output",
 CellID->832008044]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[19997, 718, 100, 2, 55, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[20100, 722, 125, 3, 33, "ExampleSection",
 CellID->1293636265],
Cell[20228, 727, 148, 3, 21, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[20401, 734, 127, 3, 21, "ExampleSection",
 CellID->2061341341],
Cell[20531, 739, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[20664, 744, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[20809, 750, 131, 3, 21, "ExampleSection",
 CellID->258228157],
Cell[20943, 755, 142, 3, 21, "ExampleSection",
 CellID->2123667759],
Cell[21088, 760, 135, 3, 21, "ExampleSection",
 CellID->1305812373],
Cell[21226, 765, 140, 3, 21, "ExampleSection",
 CellID->1653164318],
Cell[21369, 770, 132, 3, 21, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)


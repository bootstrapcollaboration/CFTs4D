(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     16626,        645]
NotebookOptionsPosition[     11725,        469]
NotebookOutlinePosition[     12354,        494]
CellTagsIndexPosition[     12275,        489]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["1.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["CFTs4D", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["CFTs4D`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["CFTs4D/ref/casimirEigenvalue4", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["casimirEigenvalue4", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"casimirEigenvalue4", "[", 
   RowBox[{"\[CapitalDelta]", ",", 
    RowBox[{"{", 
     RowBox[{"l", ",", "lb"}], "}"}]}], "]"}]], "InlineFormula"],
 " \[LineSeparator]gives the eigenvalue of the quartic Casimir on the primary \
with scaling dimension ",
 Cell[BoxData[
  StyleBox["\[CapitalDelta]", "TR"]], "InlineFormula"],
 " and spin ",
 Cell[BoxData[
  RowBox[{"(", 
   RowBox[{
    StyleBox["l", "TI"], ",", 
    StyleBox["lb", "TI"]}], ")"}]], "InlineFormula"],
 ".\n",
 Cell["      ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"casimirEigenvalue4", "[", "p", "]"}]], "InlineFormula"],
 "\[LineSeparator]is equivalent to ",
 Cell[BoxData[
  RowBox[{"casimirEigenvalue4", "[", 
   RowBox[{"\[CapitalDelta]", ",", 
    RowBox[{"{", 
     RowBox[{"l", ",", 
      RowBox[{"l", "+", "p"}]}], "}"}]}], "]"}]], "InlineFormula"],
 " ."
}], "Usage",
 CellChangeTimes->{{3.703286897252418*^9, 3.703286910935603*^9}, {
  3.70361651055539*^9, 3.70361652049627*^9}},
 CellID->982511436],

Cell[TextData[{
 "To use ",
 Cell[BoxData[
  ButtonBox["casimirEigenvalue4",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/casimirEigenvalue4"]], "InlineFormula"],
 ", you first need to load the ",
 ButtonBox["CFTs4D",
  BaseStyle->"Link",
  ButtonData->"paclet:CFTs4D/guide/CFTs4D"],
 " package using ",
 Cell[BoxData[
  RowBox[{
   ButtonBox["Needs",
    BaseStyle->"Link"], "[", "\"\<CFTs4D`\>\"", "]"}]], "InlineFormula"],
 "."
}], "Notes",
 CellChangeTimes->{{3.703039193544949*^9, 3.7030392141372538`*^9}, {
  3.703263520451661*^9, 3.703263525449205*^9}, {3.703264714074502*^9, 
  3.703264714374073*^9}, {3.703286457762343*^9, 3.7032864745518847`*^9}},
 CellID->1067943069]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell[TextData[ButtonBox["Conformal Partial Waves",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConformalPartialWaves"]], "Tutorials",
 CellChangeTimes->{{3.703866584347806*^9, 3.703866596787722*^9}},
 CellID->1542474150],

Cell[TextData[ButtonBox["Conserved Operators",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/ConservedOperators"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866522555599*^9}, {
  3.703866599411406*^9, 3.703866599610306*^9}},
 CellID->1375158345],

Cell[TextData[ButtonBox["Differential Basis",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/tutorial/DifferentialBasis"]], "Tutorials",
 CellChangeTimes->{{3.7038665074093943`*^9, 3.703866525150303*^9}, {
  3.7038666017019033`*^9, 3.7038666018652353`*^9}},
 CellID->1009189986]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell[TextData[{
 Cell[BoxData[
  ButtonBox["opCasimir3EF",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/opCasimir3EF"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["casimirEigenvalue3",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/casimirEigenvalue3"]], "InlineFormula"],
 " \[FilledVerySmallSquare] ",
 Cell[BoxData[
  ButtonBox["casimirEigenvalue2",
   BaseStyle->"Link",
   ButtonData->"paclet:CFTs4D/ref/casimirEigenvalue2"]], "InlineFormula"]
}], "SeeAlso",
 CellChangeTimes->{{3.703287049316579*^9, 3.7032870715881433`*^9}},
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell[TextData[ButtonBox["CFTs4D",
 BaseStyle->"Link",
 ButtonData->"paclet:CFTs4D/guide/CFTs4D"]], "MoreAbout",
 CellChangeTimes->{{3.703819196363316*^9, 3.70381920279813*^9}},
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<CFTs4D`\>\"", "]"}]], "Input",
 CellID->729447638],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["\t", "ExampleDelimiter"],
  $Line = 0; Null]], "ExampleDelimiter",
 CellID->1747103483],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"casimirEigenvalue4", "[", 
  RowBox[{"\[CapitalDelta]", ",", 
   RowBox[{"{", 
    RowBox[{"l", ",", "lb"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.703286976963076*^9, 3.7032869818794537`*^9}},
 CellLabel->"In[44]:=",
 CellID->1989794790],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"-", 
    FractionBox["1", "8"]}], " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      RowBox[{"l", " ", 
       RowBox[{"(", 
        RowBox[{"2", "+", "l"}], ")"}]}], "-", 
      RowBox[{"lb", " ", 
       RowBox[{"(", 
        RowBox[{"2", "+", "lb"}], ")"}]}]}], ")"}], "2"]}], "+", 
  RowBox[{
   FractionBox["1", "4"], " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      RowBox[{"l", " ", 
       RowBox[{"(", 
        RowBox[{"2", "+", "l"}], ")"}]}], "+", 
      RowBox[{"lb", " ", 
       RowBox[{"(", 
        RowBox[{"2", "+", "lb"}], ")"}]}]}], ")"}], "2"]}], "+", 
  RowBox[{"6", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "4"}], "+", "\[CapitalDelta]"}], ")"}], " ", 
   "\[CapitalDelta]"}], "+", 
  RowBox[{
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "4"}], "+", "\[CapitalDelta]"}], ")"}], "2"], " ", 
   SuperscriptBox["\[CapitalDelta]", "2"]}]}]], "Output",
 CellChangeTimes->{3.703286982638633*^9},
 CellLabel->"Out[44]=",
 CellID->1596651155]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["\t", "ExampleDelimiter"],
  $Line = 0; Null]], "ExampleDelimiter",
 CellID->283620115],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"casimirEigenvalue4", "[", "p", "]"}], "-", 
   RowBox[{"casimirEigenvalue4", "[", 
    RowBox[{"\[CapitalDelta]", ",", 
     RowBox[{"{", 
      RowBox[{"l", ",", 
       RowBox[{"l", "+", "p"}]}], "}"}]}], "]"}]}], "//", 
  "Simplify"}]], "Input",
 CellChangeTimes->{{3.70366386042437*^9, 3.7036638968813667`*^9}, {
  3.70366394683888*^9, 3.7036639494498463`*^9}},
 CellLabel->"In[26]:=",
 CellID->1852853774],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{{3.70366386410959*^9, 3.703663897380731*^9}, 
   3.703663949803096*^9},
 CellLabel->"Out[26]=",
 CellID->1698242818]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"casimirEigenvalue4", "[", "p", "]"}], "-", 
   RowBox[{"casimirEigenvalue4", "[", 
    RowBox[{"\[CapitalDelta]", ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"l", "+", "p"}], ",", "l"}], "}"}]}], "]"}]}], "//", 
  "Simplify"}]], "Input",
 CellChangeTimes->{{3.70366390606641*^9, 3.703663912278326*^9}, {
  3.703663951966405*^9, 3.703663959341999*^9}},
 CellLabel->"In[28]:=",
 CellID->237564543],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{
  3.703663912718539*^9, {3.703663957349719*^9, 3.7036639598618193`*^9}},
 CellLabel->"Out[28]=",
 CellID->2096226998]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{4, Automatic}, {Automatic, 28}},
CellContext->"Global`",
FrontEndVersion->"11.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (July 28, \
2016)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[10205, 411, 100, 2, 55, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 12136, 482}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 325, 14, 24, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[908, 38, 68, 1, 29, "CategorizationSection",
 CellID->1122911449],
Cell[979, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1061, 45, 79, 2, 70, "Categorization",
 CellID->605800465],
Cell[1143, 49, 76, 2, 70, "Categorization",
 CellID->468444828],
Cell[1222, 53, 74, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1333, 59, 55, 1, 19, "KeywordsSection",
 CellID->477174294],
Cell[1391, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1473, 68, 65, 1, 19, "TemplatesSection",
 CellID->1872225408],
Cell[1541, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1638, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1723, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1807, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1926, 90, 53, 1, 19, "DetailsSection",
 CellID->307771771],
Cell[1982, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2048, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2120, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2187, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2259, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2323, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2387, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2453, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2534, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2638, 132, 61, 1, 63, "ObjectName",
 CellID->1224892054],
Cell[2702, 135, 1041, 32, 130, "Usage",
 CellID->982511436],
Cell[3746, 169, 689, 20, 28, "Notes",
 CellID->1067943069]
}, Open  ]],
Cell[CellGroupData[{
Cell[4472, 194, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[4532, 197, 233, 4, 16, "Tutorials",
 CellID->1542474150],
Cell[4768, 203, 277, 5, 16, "Tutorials",
 CellID->1375158345],
Cell[5048, 210, 279, 5, 16, "Tutorials",
 CellID->1009189986]
}, Open  ]],
Cell[CellGroupData[{
Cell[5364, 220, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[5450, 223, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[5545, 229, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[5613, 232, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[5699, 238, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[5757, 241, 605, 17, 22, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[6399, 263, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[6459, 266, 197, 4, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[6693, 275, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[7052, 288, 92, 2, 27, "Input",
 CellID->729447638],
Cell[CellGroupData[{
Cell[7169, 294, 126, 3, 17, "ExampleDelimiter",
 CellID->1747103483],
Cell[CellGroupData[{
Cell[7320, 301, 271, 7, 27, "Input",
 CellID->1989794790],
Cell[7594, 310, 1068, 38, 42, "Output",
 CellID->1596651155]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[8711, 354, 125, 3, 17, "ExampleDelimiter",
 CellID->283620115],
Cell[CellGroupData[{
Cell[8861, 361, 457, 13, 27, "Input",
 CellID->1852853774],
Cell[9321, 376, 164, 4, 26, "Output",
 CellID->1698242818]
}, Open  ]],
Cell[CellGroupData[{
Cell[9522, 385, 453, 13, 27, "Input",
 CellID->237564543],
Cell[9978, 400, 166, 4, 26, "Output",
 CellID->2096226998]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[10205, 411, 100, 2, 55, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[10308, 415, 125, 3, 33, "ExampleSection",
 CellID->1293636265],
Cell[10436, 420, 148, 3, 21, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[10609, 427, 127, 3, 21, "ExampleSection",
 CellID->2061341341],
Cell[10739, 432, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[10872, 437, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[11017, 443, 131, 3, 21, "ExampleSection",
 CellID->258228157],
Cell[11151, 448, 142, 3, 21, "ExampleSection",
 CellID->2123667759],
Cell[11296, 453, 135, 3, 21, "ExampleSection",
 CellID->1305812373],
Cell[11434, 458, 140, 3, 21, "ExampleSection",
 CellID->1653164318],
Cell[11577, 463, 132, 3, 21, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)


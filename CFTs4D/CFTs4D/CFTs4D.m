(* ::Package:: *)

(*

CFTs4D package for Wolfram Mathematica.

CFTs4D implements various algorithms for kinematics of
4D Conformal Field Theories. 

Install by placing the directory CFTs4D containing this 
file under $BaseDirectory/Applications/

This package is based on the paper
  
  - Gabriel Francisco Cuomo, Denis Karateev and Petr Kravchuk,
    'General Bootstrap Equations in 4D CFTs,' arXiv:1705.05401

Authors: Denis Karateev and Petr Kravchuk

Copyright 2017, Denis Karateev and Petr Kravchuk.
Distributed under the MIT license. (See http://opensource.org/licenses/MIT)

*)


BeginPackage["CFTs4D`"];
Unprotect["CFTs4D`*","CFTs4D`Private`*"];
ClearAll["CFTs4D`*","CFTs4D`Private`*"];

timeStart=AbsoluteTime[];
timeStamp:=ToString[AbsoluteTime[]-timeStart];
logTime[flagSTAF_]:=If[(ValueQ[Global`CFTs4DDebugTime]&&Global`CFTs4DDebugTime),
	Print[timeStamp<>" : "<>flagSTAF];
];
Remove[flagSTAF,flagSTAF$];(* Cleaning up the mess *)


(* ::Section::Closed:: *)
(*Definitions of Global Functions*)


(* ::Subsection::Closed:: *)
(*1. Tensor Structures of n-point Functions*)


logTime["Start Usage"];
(*--------------------Invariants--------------------*)

sp::usage			="sp[i,j] represents the scalar product (Xi,Xj)";
spM::usage			="spM[i,j] represents the modified scalar product -2(Xi,Xj)";


invSbS::usage		="invSbS[{i,j},klist] encodes non-normalized invariants of the form Sb[i].X[k1].Xb[k2]...S[j]";
invSS::usage		="invSS[{i,j},klist] encodes non-normalized invariants of the form S[i].Xb[k1].X[k2]...S[j]";
invSbSb::usage		="invSbSb[{i,j},klist] encodes non-normalized invariants of the form Sb[i].X[k1].Xb[k2]...Sb[j] ";
invSbSnorm::usage	="invSbSnorm[{i,j},klist] encodes normalized invariants of the form Sb[i].X[k1].Xb[k2]...S[j] ";
invSSnorm::usage	="invSSnorm[{i,j},klist] encodes normalized invariants of the form S[i].Xb[k1].X[k2]...S[j] ";
invSbSbnorm::usage	="invSbSbnorm[{i,j},klist] encodes normalized invariants of the form Sb[i].X[k1].Xb[k2]...Sb[j] ";


(*--------------------Auxiliary Spinor Components--------------------*)

\[Xi]::usage		="component of the auxiliary spinor s";
\[Xi]b::usage		="component of the auxiliary spinor s";
\[Eta]::usage		="component of the auxiliary spinor sb";
\[Eta]b::usage		="a component of the auxiliary spinor sb";

(*--------------------Construction of Tensor Structures--------------------*)

n2CorrelationFunction::usage		="n2CorrelationFunction[\[CapitalDelta],{{l1,lb1},{l2,lb2}}] constructs a generic 2-point function in the Embedding Formalism";

n3KinematicFactor::usage			="n3KinematicFactor[{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3},{{l1,lb1},{l2,lb2},{l3,lb3}}] constructs the kinematic factor of a given 3-point function in the Embedding Formalism";
n3ListStructures::usage				="n3ListStructures[{{l1,lb1},{l2,lb2},{l3,lb3}}] constructs a list of normilized tensor structures of a given 3-point function in the Embedding Formalism";
n3ListStructuresAlternativeTS::usage="n3ListStructuresAlternativeTS[{{l1,lb1},{l2,lb2},{l3,lb3}}] constructs an alternative list of normilized tensor structures of a given 3-point function of trace-less symmetric fields in the Embedding Formalism. These structures match nicely the notation of the Spinning Conformal Correlator paper.";
n3CorrelationFunction::usage		="n3CorrelationFunction[{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3},{{l1,lb1},{l2,lb2},{l3,lb3}}] constructs a given 3-point function in the Embedding Formalism";

n4KinematicFactor::usage			="n4KinematicFactor[{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3,\[CapitalDelta]4},{{l1,lb1},{l2,lb2},{l3,lb3},{l4,lb4}}] constructs the kinematic factor of a given 4-point function in the Embedding Formalism";
n4ListStructuresEF::usage			="n4ListStructuresEF[{{l1,lb1},{l2,lb2},{l3,lb3},{l4,lb4}}] constructs a list of tensor structures of a given 4-point function in the Embedding Formalism";

n4ListStructures::usage				="n4ListStructures[{{l1,lb1},{l2,lb2},{l3,lb3},{l4,lb4}}] constructs tensor structures of a given 4-point function in the Conformal Frame";

countStructures::usage				="countStructures[spinslist] provides a number of tensor structures for a given n-point function. It automatically makes splitting into parity-even and parity-odd structures when only traceless-symmetric tensors are present";



(* ::Subsection::Closed:: *)
(*2. Conformal Partial Waves*)


(*--------------------Cross-ratios and functions--------------------*)

U::usage="u-variable";
V::usage="v-variable";
z::usage="z-variable";
zb::usage="zb-variable";

(*--------------------CPWs and CBs--------------------*)

seedCPW::usage  ="seedCPW[p] writes the Seed Conformal Partial Wave for exchange of |l-lb|=p in terms of the Seed Conformal Blocks";

G::usage		="G[{p,e},{\[CapitalDelta],l},{\[CapitalDelta][1],\[CapitalDelta][2],\[CapitalDelta][3],\[CapitalDelta][4]},{n,m}] represents the (n,m) derivative of the seed or the dual seed conformal block written in (U,V)-variables";
H::usage		="H[{p,e},{\[CapitalDelta],l},{\[CapitalDelta][1],\[CapitalDelta][2],\[CapitalDelta][3],\[CapitalDelta][4]},{n,m}][z,zb] represents the (n,m) derivative of the seed or the dual seed conformal block in (z,zb)-variables";

(*--------------------Casimir Eigenvalue--------------------*)

casimirEigenvalue2::usage="casimirEigenvalue2[\[CapitalDelta],{l,lb}] returnes the quadratic Casimir eigenvalue for given \[CapitalDelta], l, and lb.\ncasimirEigenvalue2[p] is equivalent to casimirEigenvalue2[\[CapitalDelta],{l,l+p}]"; (* take \[CapitalDelta],l as arguments, or better make l,lb version? *)
casimirEigenvalue3::usage="casimirEigenvalue3[\[CapitalDelta],{l,lb}] returnes the cubic Casimir eigenvalue for given \[CapitalDelta], l, and lb.\ncasimirEigenvalue3[p] is equivalent to casimirEigenvalue3[\[CapitalDelta],{l,l+p}]"; (* take \[CapitalDelta],l as arguments, or better make l,lb version? *)
casimirEigenvalue4::usage="casimirEigenvalue4[\[CapitalDelta],{l,lb}] returnes the quartic Casimir eigenvalue for given \[CapitalDelta], l, and lb.\ncasimirEigenvalue4[p] is equivalent to casimirEigenvalue4[\[CapitalDelta],{l,l+p}]"; (* take \[CapitalDelta],l as arguments, or better make l,lb version? *)

(*--------------------Functions--------------------*)

changeVariables::usage		="changeVariables[expr] cnahnges (U,V)-variables into (z,zb)-variables and converts (U,V)-derivatives of G functions into (z,zb)-derivatives of H-functions. The derivative order with respect to (U,V) or (z,zb)-variables is denoted by (m,n). The convertion rule is precomputed for m+n<=10";
specialSimplifyG::usage		="specialSimplifyG[expr] collects terms witht the same G and simplifies the coefficients";
specialSimplifyH::usage		="specialSimplifyH[expr] collects terms witht the same H and simplifies the coefficients";

plugSeedBlocks::usage		="plugSeedBlocks[t][expr] plugs explicit expressions of the Seed Conformal Blocks H in expr. t=1 gives the full blocks, t=0 gives half of each block.";
plugDualSeedBlocks::usage	="plugSeedBlocks[t][expr] plugs explicit expressions of the Dual Seed Conformal Blocks H in expr. t=1 gives the full blocks, t=0 gives half of each block.";

(*-------------------------------New Functions-------------------------------*)

plugSeedRecursion::usage            ="plugSeedRecursion[expr] rewrites all the functions H[{p,e},__][z,zb] in terms of the functions H[{p-1,e'},__][z,zb] assuming that H[{p,e},__][z,zb] represents the seed block";
plugDualSeedRecursion::usage        ="plugDualSeedRecursion[expr] rewrites all the functions H[{p,e},__][z,zb] in terms of the functions H[{p-1,e'},__][z,zb] assuming that H[{p,e},__][z,zb] represents the dual seed block";

reduceKFunctionDerivatives::usage	="reduceKFunctionDerivatives[expr] rewrites derivatives of the k-functions in terms of the k-functions and polynomials P and Q";
plugPolynomialsPQ::usage			="plugPolynomialsPQ[expr] plugs explicit expressions of the polynomials P[n] and Q[n]. Their values are precomputed for n<=10";
plugCoefficients::usage				="plugCoefficients[p][expr] plugs the coefficients of the seed and dual seed conformal blocks. These coefficients are determined up to an overal constant (for seeds and dual seeds separately) which are set to +1 for now. Correct constants reflecting the 2-point function normalization to be found";
plugKFunctions::usage				="plugKFunctions[expr] plugs exact expression of the k-functions";
plugKFunctionsApproximated::usage	="plugKFunctionsApproximated[ord][expr] plugs k-functions approximated to order ord. These approximations are precomputed for ord\[LessEqual]12";

cs::usage	="cs[{\[CapitalDelta], l},{\[CapitalDelta][1],\[CapitalDelta][2],\[CapitalDelta][3],\[CapitalDelta][4]}][p,e,m,n] are the coefficients entering the seed conformal blocks";
csd::usage	="csd[{\[CapitalDelta], l},{\[CapitalDelta][1],\[CapitalDelta][2],\[CapitalDelta][3],\[CapitalDelta][4]}][p,e,m,n] are the coefficients entering the sual seed conformal blocks";

P::usage	="P[n] represents the polynomial in front of k";
Q::usage	="Q[n] represnts the polynomial in front of k'";

k::usage	="k[\[Rho],a,b,c][x] represents the k-function x^\[Rho]Hypergeometric2F1[a+\[Rho],b+\[Rho],c+2\[Rho],x]";



(* ::Subsection::Closed:: *)
(*3. Embedding Formalism Algebra*)


(*--------------------Differential Operators--------------------*)

opConservationEF::usage="opConservationEF[i][expr] applies the conservation operator in EF at position i";

opDEF::usage	="opDEF[i,j][expr] applies the spinning operator D in EF at positions (i,j)";
opDtEF::usage	="opDtEF[i,j][expr] applies the spinning operator Dt in EF at positions (i,j)";
opIEF::usage	="opIEF[i,j][expr] applies the spinningn operator I in EF at positions (i,j)";
opdEF::usage	="opdEF[i,j][expr] applies the spinning operator d in EF at positions (i,j)";
opdbEF::usage	="opdbEF[i,j][expr] applies the spinning operator db in EF at positions (i,j)";
opNEF::usage	="opNEF[i,j][expr] applies the spinning operator nabla in EF at positions (i,j)";

\[CapitalXi]::usage ="\[CapitalXi][op] represents a differential operator supplemented by the action of dimension shift operator \[CapitalXi]  which together result in no change in scaling dimension";
	
(*--------------------Basic Derivatives---------------------*)

DS::usage		="Derivative with respect to S"; (* need help with description of arguments here and below *)
DSb::usage		="Derivative with respect to Sb";
DXSigma::usage	="Derivative with respect to X contructed with the Sigma-matrix";
DXSigmab::usage	="Derivative with respect to X contructed with the Sigmab-matrix";

opL::usage			="opL[i,a,c][expr_] applies the lorentz differential generator at the position i with the SU(2,2) lower index a and upper index c to the expression expr";
opCasimir2EF::usage	="opCasimir2EF[i][expr] applies the quadratic casimir differential operator at the position i to the expression expr";
opCasimir3EF::usage	="opCasimir3EF[i][expr] applies the cubic casimir differential operator at the position i to the expression expr";
opCasimir4EF::usage	="opCasimir4EF[i][expr] applies the quartic casimir differential operator at the position i to the expression expr";

(*--------------------Objects with Indices--------------------*)

DEL::usage	="delta-symbol";

EPS::usage	="epsilon-symbol";
EPSb::usage	="epsilonb-symbol";

X::usage	="X-symbol";
Xb::usage	="Xb-symbol";

XXb::usage	="XXb-symbol";
XbX::usage	="XXb-symbol";

S::usage	="S-symbol";
Sb::usage	="Sb-symbol";

XbS::usage	="XbS-symbol";
SXb::usage	="SXb-symbol";

SbX::usage	="SbX-symbol";
XSb::usage	="XSb-symbol";

XXbS::usage	="XXbS-symbol";
SXbX::usage	="SXbX-symbol";

SbXXb::usage="SbXXb-symbol";
XbXSb::usage="XbXSb-symbol";

(*--------------------Constants--------------------*)

(* 
listConstants::usage="a list of objects treated as constants by all EF differential operators";
listFunctions::usage="a list of names of funtion which should be differentiated using the chain rule for all their arguments by EF differential operators";
*)

\[Lambda]::usage	="coupling constant";
\[Lambda]P::usage	="coupling constant, parity-even";
\[Lambda]M::usage	="coupling constant, parity-odd";

\[CapitalDelta]::usage="4D scaling dimension of the exchaned field";
l::usage="spin index of the exchaned field";


(*--------------------Functions--------------------*)

contractIndices::usage		="contracts all the indices in the expression forming invariant structures";

applyEFProperties::usage	="applyEFProperties[expr] applies the properties of the Embedding Formalism to the expression";

applyJacobiRelations::usage	="applyJacobiRelations[expr] applies the n=3 Jacobi relations";

normalizeInvariants::usage	="normalizeInvariants[expr] replaces non-normalized invariants by normalized ones";
denormalizeInvariants::usage="denormalizeInvariants[expr] replaces normalized invariants by non-normalized ones";

formCrossRatios::usage		="formCrossRatios[expr] forms cross-ratios out of 6D scalar products when possible";
permutePoints::usage		="permutePoints[i,j][expr] permutes points i and j inside tensor structures and scalar products.\npermutePoints[perm][expr] permutes the points in expr accroding to the permutation function perm.";
applyPParity::usage			="applyPParity[expr] applies P-parity transformation to tensor structures in expr";
applyTParity::usage			="applyTParity[expr] applies T-parity transformation to tensor structures in expr. It does not conjugate the complex numbers in expr";
applyConjugation::usage		="applyConjugation[expr] applies complex conjugation to tensor structures in expr. It does not conjugate the complex numbers in expr";

(*Elementary Differential Operators*)

spinorD::usage ="spinorD[\[CapitalDelta],l,lb][i,a][expr] is a covariant differential operator with an upper open index a which acts at the position i. The are 4 different sets of allowed parameters {\[CapitalDelta],l,lb}: {-1/2,0,+1/2}, {-1/2,0,-1/2}, {+1/2,+1/2,0} and {+1/2,-1/2,0}";
spinorDb::usage ="spinorDb[\[CapitalDelta],l,lb][i,a][expr] is a covariant differential operator with a lower open index a which acts at the position i. The are 4 different sets of allowed parameters {\[CapitalDelta],l,lb}: {-1/2,+1/2,0}, {-1/2,-1/2,0}, {+1/2,0,+1/2} and {+1/2,0,-1/2}";


(* old notation for weight-shifting operators: [deprecated]
R::usage	="R[i_,a_][expr_] is an operator in fundamental representation which rises spin l by 1. It is simply given by S[i][a].";
Rp::usage	="Rp[i_,a_][expr_] is a differential operator in fundamental representation which rises spin lb by 1";
L::usage	="L[i_,a_][expr_] is a differential operator in fundamental representation which lowers spin l by 1";
Lp::usage	="Lp[i_,a_][expr_] is a differential operator in fundamental representation which lowers spin lb by 1";

Rpb::usage	="R[i_,a_][expr_] is an operator in anti-fundamental representation which rises spin lb by 1. It is simply given by Sb[i][a].";
Rb::usage	="Rb[i_,a_][expr_] is a differential operator in anti-fundamental representation which rises spin l by 1";
Lpb::usage	="Lpb[i_,a_][expr_] is a differential operator in anti-fundamental representation which lowers spin lb by 1";
Lb::usage	="Lb[i_,a_][expr_] is a differential operator in anti-fundamental representation which lowers spin l by 1";
*)


(* ::Subsection::Closed:: *)
(*4. Relation Between EF and CF*)


toConformalFrame::usage		="toConformalFrame[expr] restricts all the objects to Conformal Frame";
toEmbeddingFormalism::usage	="toEmbeddingFormalism[flag_][expr] transofrms all the Conformal Frame exressions into Emedding Formalism. If flag==True the resulting tensor structures will be normalized, if flag==False the resulting tensor structures will be non-normalized";



(* ::Subsection::Closed:: *)
(*5. Conformal frame *)


(*  Abstract CF structure  *)

CF4pt::usage			="CF4pt[{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3,\[CapitalDelta]4},{l1,l2,l3,l4},{lb1,lb2,lb3,lb4},{q1,q2,q3,q4},{qb1,qb2,qb3,qb4},g] Represents the general conformal frame 4-point tensor structure with parameters qi and the coefficient function g[z,zb] for the primary operators with scaling dimensions \[CapitalDelta]i in irreps (li,lbi)";

expandCFStructs::usage	="expandCFStructs[expr] replaces the CF4pt structures in expr with their expressions in terms of polarizations \[Xi] and \[Eta].";
collapseCFStructs::usage="collapseCFStructs[{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3,\[CapitalDelta]4}][expr] re-expresses expr written as combinations of \[Xi] and \[Eta] polarization in terms of CF4pt structures, assuming the scaling dimensions \[CapitalDelta]i.";

collectInCF4pt::usage	="collectCF4pt[params][expr] applies Collect[g,params] to every coefficient function g inside a CF4pt in expr (params can be a sequence of arguments).";
collectGInCF4pt::usage	="collectGInCF4pt[g][f][expr] in every coefficient function inside a CF4pt collects g and its derivatives, applying the optional f to coefficients.";
simplifyInCF4pt::usage	="simplifyInCF4pt[expr] applies Simplify to every coefficient function  g inside a CF4pt in expr.";
mapInCF4pt::usage		="mapInCF4pt[f][expr] applies the function f to every coefficient function  g inside a CF4pt in expr.";

(* differential operators aux functions *)

genericFunction::usage			="genericFunction represents quite literally a generic function.";
generic4ptCorrelator::usage		="generic4ptCorrelator is a genericFunction with arguments representing the positions of the 4 operators and their spinor polarizations.";

resolve4DDerivatives::usage		="resolve4DDerivatives[expr] resolves the derivatives (up to 2nd order) of the generic4ptCorrelator in terms of the ones which can be computed explicitly in the conformal frame.";
generic4DtoCFExpression::usage	="generic4DtoCFExpression[expr] takes an expr expressed in terms of (resolved, see resolve4DDerivatives) derivatives of generic4ptCorrelator and expresses it terms of CF4pt structs.";

operatorRule::usage				="operatorRule[op][params] produces a CF rule for the action of operator op[params], assuming that op[params] can act on generic4ptCorrelator and that the dimension shifts dShift[op][params] are defined.";
operatorListRule::usage			="operatorListRule[op][params] is the same as operatorRule[op][params] except that it produces a List instead of a sum, which sometimes allows for more efficient application of the rule.";
dShift::usage					="dShift[op][rule] returns a list of 4 numbers which give the scaling dimension shifts induced by the operator op[params].";
applyListRule::usage			="applyListRule[listrule][expr] applies the listrule produced by operatorListRule to a list expression expr."

(* pre-defined differential operators *)

opD4D::usage	="opD4D[i,j][expr] gives the 4D diff. operator D which can be applied to a generic4ptCorrelator, ready to be used with operatorRule.";
opDt4D::usage	="opDt4D[i,j][expr] gives the 4D diff. operator Dt which can be applied to a generic4ptCorrelator, ready to be used with operatorRule.";
opd4D::usage	="opd4D[i,j][expr] gives the 4D diff. operator d which can be applied to a generic4ptCorrelator, ready to be used with operatorRule.";
opdb4D::usage	="opdb4D[i,j][expr] gives the 4D diff. operator db which can be applied to a generic4ptCorrelator, ready to be used with operatorRule.";
opN4D::usage	="opN4D[i,j][expr] gives the 4D diff. operator nabla (N) which can be applied to a generic4ptCorrelator, ready to be used with operatorRule.";
opI4D::usage	="opI4D[i,j][expr] gives the 4D diff. operator I which can be applied to a generic4ptCorrelator, ready to be used with operatorRule.";

opCasimir24D::usage ="opCasimir24D[pts][expr] gives the 4D Casimir operator on points pts (a sequence) which can be applied to a generic4ptCorrelator, ready to be used with operatorRule.";

opConservation4D::usage="opConservation4D[i][expr] applies the conservation operator in 4D at position i";

spinorD4D::usage ="spinorD4D[\[CapitalDelta],l,lb][i,a][expr] is a 4D version of covariant differential operator with an upper open index a which acts at the position i. The are 4 different sets of allowed parameters {\[CapitalDelta],l,lb}: {-1/2,0,+1/2}, {-1/2,0,-1/2}, {+1/2,+1/2,0} and {+1/2,-1/2,0}";
spinorDb4D::usage ="spinorDb4D[\[CapitalDelta],l,lb][i,a][expr] is a 4D version of covariant differential operator with a lower open index a which acts at the position i. The are 4 different sets of allowed parameters {\[CapitalDelta],l,lb}: {-1/2,+1/2,0}, {-1/2,-1/2,0}, {+1/2,0,+1/2} and {+1/2,0,-1/2}";

opContracted4D::usage ="opContracted4D[op1,op2][expr]";

(* permutations and semi-covariant stuff *)

cfEvaluateInPlane::usage ="cfEvaluateInPlane[{z1,zb1},{z2,zb2},{z3,zb3},{z4,zb4}][expr] evaluates expr, given in terms of CF4pt structures, at a generic configuration in CF plane.";



(* ::Section::Closed:: *)
(*Setting up notation*)


logTime["Start Notation"];
Notation`AutoLoadNotationPalette=False;
Needs["Notation`"];
logTime["Start NotationDefinitions"];


Begin["`NotationPrivacy`"]; (* Otherwise the Notation below populates CFTs4D` with i,j,k,m (I guess these come from the patterns..) *)
If[!(ValueQ[Global`CFTs4DDisableFancy]&&Global`CFTs4DDisableFancy),

	(*--------------------Invariants in the EF--------------------*)
	
	(*Scalar Products*)
	Notation[ParsedBoxWrapper[SubscriptBox["X", RowBox[{"{",RowBox[{"i_", ",", "j_"}], "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"spM", "[", RowBox[{"i_", ",", "j_"}], "]"}]]];
	
	(*Class I*)
	Notation[ParsedBoxWrapper[SuperscriptBox["I", RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSbS", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}], ",", RowBox[{"{", "}"}]}], "]"}]]];
	Notation[ParsedBoxWrapper[SubsuperscriptBox["I", RowBox[{"{", RowBox[{"k_", ",", "l_"}], "}"}], RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSbS", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}], ",", RowBox[{"{", RowBox[{"k_", ",", "l_"}], "}"}]}], "]"}]]];
	Notation[ParsedBoxWrapper[SuperscriptBox[OverscriptBox["I", "^"], RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSbSnorm", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}], ",", RowBox[{"{", "}"}]}], "]"}]]];
	Notation[ParsedBoxWrapper[SubsuperscriptBox[OverscriptBox["I", "^"], RowBox[{"{", RowBox[{"k_", ",", "l_"}], "}"}], RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSbSnorm", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}], ",", RowBox[{"{", RowBox[{"k_", ",", "l_"}], "}"}]}], "]"}]]];
	
	(*Class II*)
	Notation[ParsedBoxWrapper[SubsuperscriptBox["J", RowBox[{"{", RowBox[{"j_", ",", "k_"}], "}"}], RowBox[{"{", "i_", "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSbS", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "i_"}], "}"}], ",", RowBox[{"{", RowBox[{"j_", ",", "k_"}], "}"}]}], "]"}]]];
	Notation[ParsedBoxWrapper[SubsuperscriptBox["J", RowBox[{"{", RowBox[{"j_", ",", "k_", ",", "l_", ",", "m_"}], "}"}], RowBox[{"{", "i_", "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSbS", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "i_"}], "}"}], ",", RowBox[{"{", RowBox[{"j_", ",", "k_", ",", "l_", ",", "m_"}], "}"}]}], "]"}]]];
	Notation[ParsedBoxWrapper[SubsuperscriptBox[OverscriptBox["J", "^"], RowBox[{"{", RowBox[{"j_", ",", "k_"}], "}"}], RowBox[{"{", "i_", "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSbSnorm", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "i_"}], "}"}], ",", RowBox[{"{", RowBox[{"j_", ",", "k_"}], "}"}]}], "]"}]]];
	Notation[ParsedBoxWrapper[SubsuperscriptBox[OverscriptBox["J", "^"], RowBox[{"{", RowBox[{"j_", ",", "k_", ",", "l_", ",", "m_"}], "}"}], RowBox[{"{", "i_", "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSbSnorm", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "i_"}], "}"}], ",", RowBox[{"{", RowBox[{"j_", ",", "k_", ",", "l_", ",", "m_"}], "}"}]}], "]"}]]];
	
	(*Class III*)
	(*---non-bar invariants---*)
	Notation[ParsedBoxWrapper[SubsuperscriptBox["K", RowBox[{"{", "k_", "}"}], RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSS", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}], ",", RowBox[{"{", "k_", "}"}]}], "]"}]]];
	Notation[ParsedBoxWrapper[SubsuperscriptBox["K", RowBox[{"{", RowBox[{"k_", ",", "l_", ",", "m_"}], "}"}], RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSS", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}], ",", RowBox[{"{", RowBox[{"k_", ",", "l_", ",", "m_"}], "}"}]}], "]"}]]];
	Notation[ParsedBoxWrapper[SubsuperscriptBox[OverscriptBox["K", "^"], RowBox[{"{", "k_", "}"}], RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSSnorm", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}], ",", RowBox[{"{", "k_", "}"}]}], "]"}]]];
	Notation[ParsedBoxWrapper[SubsuperscriptBox[OverscriptBox["K", "^"], RowBox[{"{", RowBox[{"k_", ",", "l_", ",", "m_"}], "}"}], RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSSnorm", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}], ",", RowBox[{"{", RowBox[{"k_", ",", "l_", ",", "m_"}], "}"}]}], "]"}]]];
	
	(*---bar invariants---*)
	Notation[ParsedBoxWrapper[SubsuperscriptBox[OverscriptBox["K", "\[HorizontalLine]"], RowBox[{"{", "k_", "}"}], RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSbSb", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}], ",", RowBox[{"{", "k_", "}"}]}], "]"}]]];
	Notation[ParsedBoxWrapper[SubsuperscriptBox[OverscriptBox["K", "\[HorizontalLine]"], RowBox[{"{", RowBox[{"k_", ",", "l_", ",", "m_"}], "}"}], RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSbSb", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}], ",", RowBox[{"{", RowBox[{"k_", ",", "l_", ",", "m_"}], "}"}]}], "]"}]]];
	Notation[ParsedBoxWrapper[SubsuperscriptBox[OverscriptBox[OverscriptBox["K","\[HorizontalLine]"], "^"], RowBox[{"{", "k_", "}"}], RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSbSbnorm", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}], ",", RowBox[{"{", "k_", "}"}]}], "]"}]]];
	Notation[ParsedBoxWrapper[SubsuperscriptBox[OverscriptBox[OverscriptBox["K","\[HorizontalLine]"], "^"], RowBox[{"{", RowBox[{"k_", ",", "l_", ",", "m_"}], "}"}], RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSbSbnorm", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "j_"}], "}"}], ",", RowBox[{"{", RowBox[{"k_", ",", "l_", ",", "m_"}], "}"}]}], "]"}]]];
	
	(*Class IV*)
	Notation[ParsedBoxWrapper[SubsuperscriptBox["L", RowBox[{"{", RowBox[{"j_", ",", "k_", ",", "l_"}], "}"}], RowBox[{"{", "i_", "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSS", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "i_"}], "}"}], ",", RowBox[{"{", RowBox[{"j_", ",", "k_", ",", "l_"}], "}"}]}], "]"}]]];
	Notation[ParsedBoxWrapper[SubsuperscriptBox[OverscriptBox["L", "\[HorizontalLine]"], RowBox[{"{", RowBox[{"j_", ",", "k_", ",", "l_"}], "}"}], RowBox[{"{", "i_", "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSbSb", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "i_"}], "}"}], ",", RowBox[{"{", RowBox[{"j_", ",", "k_", ",", "l_"}], "}"}]}], "]"}]]];
	Notation[ParsedBoxWrapper[SubsuperscriptBox[OverscriptBox["L", "^"], RowBox[{"{", RowBox[{"j_", ",", "k_", ",", "l_"}], "}"}], RowBox[{"{", "i_", "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSSnorm", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "i_"}], "}"}], ",", RowBox[{"{", RowBox[{"j_", ",", "k_", ",", "l_"}], "}"}]}], "]"}]]];
	Notation[ParsedBoxWrapper[SubsuperscriptBox[OverscriptBox[OverscriptBox["L","\[HorizontalLine]"], "^"], RowBox[{"{", RowBox[{"j_", ",", "k_", ",", "l_"}], "}"}], RowBox[{"{", "i_", "}"}]]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"invSbSbnorm", "[", RowBox[{RowBox[{"{", RowBox[{"i_", ",", "i_"}], "}"}], ",", RowBox[{"{", RowBox[{"j_", ",", "k_", ",", "l_"}], "}"}]}], "]"}]]];
	
	(*--------------------Miscellaneous--------------------*)
	
	(*coupling constants*)
	Notation[ParsedBoxWrapper[SubsuperscriptBox["\[Lambda]", "i_", "+"]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"\[Lambda]P", "[", "i_", "]"}]]];
	Notation[ParsedBoxWrapper[SubsuperscriptBox["\[Lambda]", "i_", "-"]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"\[Lambda]M", "[", "i_", "]"}]]];
	Notation[ParsedBoxWrapper[SubscriptBox["\[Lambda]", "i_"]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"\[Lambda]", "[", "i_", "]"}]]];
	(*variables*)
	Notation[ParsedBoxWrapper[OverscriptBox["z", "\[HorizontalLine]"]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper["zb"]];
	(*conformal frames notation*)
	Notation[ParsedBoxWrapper[SubscriptBox["\[Xi]", "i_"]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"\[Xi]", "[", "i_", "]"}]]];
	Notation[ParsedBoxWrapper[SubscriptBox["\[Eta]", "i_"]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"\[Eta]", "[", "i_", "]"}]]];
	Notation[ParsedBoxWrapper[SubscriptBox[OverscriptBox["\[Xi]", "\[HorizontalLine]"], "i_"]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"\[Xi]b", "[", "i_", "]"}]]];
	Notation[ParsedBoxWrapper[SubscriptBox[OverscriptBox["\[Eta]", "\[HorizontalLine]"], "i_"]] \[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"\[Eta]b", "[", "i_", "]"}]]];

];
End[];



(* ::Section:: *)
(*Begin*)


logTime["Start Private"]

Begin["`Private`"];



(* ::Section::Closed:: *)
(*1. Tensor Structure of n-point Functions*)


(*--------------------n=2--------------------*)
n2CorrelationFunction[\[CapitalDelta]1_,{{l1_,lb1_},{l2_,lb2_}}]:=
	If[l1==lb2&&l2==lb1
		,(I^(lb1-l1) invSbSnorm[{1,2},{}]^lb1 invSbSnorm[{2,1},{}]^l1)/spM[1,2]^(\[CapitalDelta]1+(l1+lb1)/2)
		,0
	];

(*--------------------n=3: generic case--------------------*)

ExponentGenerator[{{l1_,lb1_},{l2_,lb2_},{l3_,lb3_}}]:=Module[
{
	m12,m21,m13,m31,m23,m32,
	k1,k2,k3,
	j1,j2,j3,
	\[CapitalDelta]l=l1+l2+l3-lb1-lb2-lb3,
	
	solution,ListPowers,ListInequalities,ListInequalitiesRelations,
	
	out={}
},

	(*We consider here only the case \[CapitalDelta]l\[GreaterEqual]0, the dual case \[CapitalDelta]l<0 is easily obtained from this one by parity conjugation*)
	(*We solve the system for 6 unknowns. The rest of variables can be varied freely. There is also a requirement that all the exponents are non-negative*)
	If[(\[CapitalDelta]l>=0) && (IntegerQ[\[CapitalDelta]l/2]==True),(*it is enough to assecure that all exponents are integers*)
	
		(*We define the solution of the systems and all the constraints*)
		solution={
			k1->-l1+lb1+\[CapitalDelta]l/2- m12- m13+ m21+m31,
			k2->-l2+lb2+\[CapitalDelta]l/2+m12-m21- m23+ m32,
			k3->-l3+lb3+\[CapitalDelta]l/2+m13+ m23- m31-m32,
			j1->lb1-m12-m13,
			j2->lb2-m21-m23,
			j3->lb3-m31-m32
		};
		ListPowers={m12,m21,m13,m31,m23,m32,k1,k2,k3,0,0,0,j1,j2,j3}/.solution;
		ListInequalities={k1>=0,k2>=0,k3>=0,j1>=0,j2>=0,j3>=0}/.solution;
		ListInequalitiesRelations={
			j1!=0&&j2!=0&&j3!=0,
			k1!=0&&j1!=0,
			k2!=0&&j2!=0,
			k3!=0&&j3!=0
		}/.solution;
		(*we iterate free exponents and pick only the cases satisfying the constraints*)
		Do[
			If[(Simplify[ListInequalities]//Union)=={True}&&(Simplify[ListInequalitiesRelations]//Union)=={False}
				,out=Append[out,ListPowers];
			];
			,{m12,0,Simplify@Min[lb1,l2]}
			,{m21,0,Simplify@Min[lb2,l1]}
			,{m13,0,Simplify@Min[lb1-m12,l3]}
			,{m31,Simplify@Max[0,-(-l1+lb1+\[CapitalDelta]l/2- m12- m13+ m21)],Simplify@Min[l1-m21,lb3]}
			,{m23,0,Simplify@Min[l3-m13,lb2-m21]}
			,{m32,Simplify@Max[0,-(-l2+lb2+\[CapitalDelta]l/2+m12-m21- m23)],Simplify@Min[l2-m12,lb3-m31,-l3+lb3+\[CapitalDelta]l/2+m13+ m23- m31]}
		];
	];

	Return[out]

];



n3ListStructures[{{l1_,lb1_},{l2_,lb2_},{l3_,lb3_}}]:=Module[
{
	i,
	temp,
	out,
	\[CapitalDelta]l=l1+l2+l3-lb1-lb2-lb3
},

	If[Simplify[\[CapitalDelta]l>=0],
	
		(*This case is straightforward*)
		temp=ExponentGenerator[{{l1,lb1},{l2,lb2},{l3,lb3}}];
		out=Table[
			Times[
				invSbSnorm[{1,2},{}]^temp[[i,1]],
				invSbSnorm[{2,1},{}]^temp[[i,2]],
				invSbSnorm[{1,3},{}]^temp[[i,3]],
				invSbSnorm[{3,1},{}]^temp[[i,4]],
				invSbSnorm[{2,3},{}]^temp[[i,5]],
				invSbSnorm[{3,2},{}]^temp[[i,6]],
				invSSnorm[{2,3},{1}]^temp[[i,7]],
				invSSnorm[{1,3},{2}]^temp[[i,8]],
				invSSnorm[{1,2},{3}]^temp[[i,9]],
				invSbSbnorm[{2,3},{1}]^temp[[i,10]],
				invSbSbnorm[{1,3},{2}]^temp[[i,11]],
				invSbSbnorm[{1,2},{3}]^temp[[i,12]],
				invSbSnorm[{1,1},{2,3}]^temp[[i,13]],
				invSbSnorm[{2,2},{1,3}]^temp[[i,14]],
				invSbSnorm[{3,3},{1,2}]^temp[[i,15]]
			]
		,{i,1,Length[temp]}
		]
	,
	
		(*This case is related by parity to the \[CapitalDelta]l\[LessEqual]0. Thus we just need to do the following fipping: l \[UndirectedEdge] lb; Subscript[I, ij] \[UndirectedEdge] Subscript[I, ji], K \[UndirectedEdge] Kb*)
		temp=ExponentGenerator[{{lb1,l1},{lb2,l2},{lb3,l3}}];
		out=Table[
			Times[
				invSbSnorm[{2,1},{}]^temp[[i,1]],
				invSbSnorm[{1,2},{}]^temp[[i,2]],
				invSbSnorm[{3,1},{}]^temp[[i,3]],
				invSbSnorm[{1,3},{}]^temp[[i,4]],
				invSbSnorm[{3,2},{}]^temp[[i,5]],
				invSbSnorm[{2,3},{}]^temp[[i,6]],
				invSbSbnorm[{2,3},{1}]^temp[[i,7]],
				invSbSbnorm[{1,3},{2}]^temp[[i,8]],
				invSbSbnorm[{1,2},{3}]^temp[[i,9]],
				invSSnorm[{2,3},{1}]^temp[[i,10]],
				invSSnorm[{1,3},{2}]^temp[[i,11]],
				invSSnorm[{1,2},{3}]^temp[[i,12]],
				invSbSnorm[{1,1},{2,3}]^temp[[i,13]],
				invSbSnorm[{2,2},{1,3}]^temp[[i,14]],
				invSbSnorm[{3,3},{1,2}]^temp[[i,15]]
			]
			,{i,1,Length[temp]}
		];
	];
	
	Return[out]

];

n3KinematicFactor[{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_},{{l1_,lb1_},{l2_,lb2_},{l3_,lb3_}}]:=
	Times[
		spM[1,2]^(-(1/2)(\[CapitalDelta]1+\[CapitalDelta]2-\[CapitalDelta]3+((l1+lb1)+(l2+lb2)-(l3+lb3))/2)),
		spM[1,3]^(-(1/2)(\[CapitalDelta]1-\[CapitalDelta]2+\[CapitalDelta]3+((l1+lb1)-(l2+lb2)+(l3+lb3))/2)),
		spM[2,3]^(-(1/2)(-\[CapitalDelta]1+\[CapitalDelta]2+\[CapitalDelta]3+(-(l1+lb1)+(l2+lb2)+(l3+lb3))/2))
	]//PowerExpand;


(*--------------------n=3: trace-less symmetric case (alternative treat)--------------------*)
Cut[x_]:=If[x>=0,x,0];

n3ListStructuresAlternativeTS::nonstt="The spin `1` is not a symmetric-traceless tensor.";
n3ListStructuresAlternativeTS[{{l1_,lb1_},{l2_,lb2_},{l3_,lb3_}}]:=Module[
{
	dl,
	h12,h13,h23,
	listTSeven={},listTSodd={},list={}
},
	
	 dl=((l1+l2+l3)-(lb1+lb2+lb3))/2;
	
	(*--------------------Traceless-Symmetric Operators-------------------- *)
	(*No K invariants are allowed*)
	If[l1==lb1&&l2==lb2&&l3==lb3,
		
		(*----------Parity even structures only----------*)
		Do[
			AppendTo[listTSeven,Times[
				(invSbSnorm[{1,2},{}]invSbSnorm[{2,1},{}])^h12,
				(invSbSnorm[{1,3},{}]invSbSnorm[{3,1},{}])^h13,
				(invSbSnorm[{2,3},{}]invSbSnorm[{3,2},{}])^h23,
				invSbSnorm[{1,1},{2,3}]^(l1-h12-h13),
				invSbSnorm[{2,2},{1,3}]^(l2-h12-h23),
				invSbSnorm[{3,3},{1,2}]^(l3-h13-h23)
				]];
			,{h12,0,Simplify@Min[l1,l2]}
			,{h13,0,Simplify@Min[l1-h12,l3]}
			,{h23,0,Simplify@Min[l2-h12,l3-h13]}
		  ];
		
		(*---------------------------------------------------*)
		
		
		(*----------Parity odd structures only----------*)
		Do[	
			AppendTo[listTSodd,Times[
				(invSbSnorm[{1,2},{}]invSbSnorm[{2,3},{}]invSbSnorm[{3,1},{}]+invSbSnorm[{2,1},{}]invSbSnorm[{3,2},{}]invSbSnorm[{1,3},{}]),
				(invSbSnorm[{1,2},{}]invSbSnorm[{2,1},{}])^h12,
				(invSbSnorm[{1,3},{}]invSbSnorm[{3,1},{}])^h13,
				(invSbSnorm[{2,3},{}]invSbSnorm[{3,2},{}])^h23,
				invSbSnorm[{1,1},{2,3}]^(l1-h12-h13-1),
				invSbSnorm[{2,2},{1,3}]^(l2-h12-h23-1),
				invSbSnorm[{3,3},{1,2}]^(l3-h13-h23-1)
				]];
			,{h12,0,Simplify@Min[l1-1,l2-1]}
			,{h13,0,Simplify@Min[l1-h12-1,l3-1]}
			,{h23,0,Simplify@Min[l2-h12-1,l3-h13-1]}
		];
		(*-------------------------------------------------*)
		
		
		list={listTSeven,listTSodd};
	,
		Message[n3ListStructuresAlternativeTS::nonstt,
			Which[
				l1!=lb1, {l1,lb1},
				l2!=lb2, {l2,lb2},
				l3!=lb3, {l3,lb3}
			];
		];
		
	];
	
	Return[list]

];


n3CorrelationFunction[{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_},{{l1_,lb1_},{l2_,lb2_},{l3_,lb3_}}]:=Module[
{
	str,temp,n,
	\[CapitalDelta]l=l1+l2+l3-lb1-lb2-lb3
},

	If[l1==lb1&&l2==lb2&&l3==lb3,
		
		(*---If we deal with traceless-symmetric operators only---*)
		str=n3ListStructuresAlternativeTS[{{l1,lb1},{l2,lb2},{l3,lb3}}];
		temp=Sum[\[Lambda]P[n]str[[1,n]],{n,1,Length[str[[1]]]}]+Sum[\[Lambda]M[n]str[[2,n]],{n,1,Length[str[[2]]]}]
	,
	
		(*---If we deal with non traceless-symmetric operators--*)
		str=n3ListStructures[{{l1,lb1},{l2,lb2},{l3,lb3}}];
		temp=Sum[\[Lambda][n]str[[n]],{n,1,Length[str]}]
	];

	Return[Times[n3KinematicFactor[{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3},{{l1,lb1},{l2,lb2},{l3,lb3}}],temp]]

];


(*--------------------n=4--------------------*)
ToOrdinaryOutput[expr_]:=expr/.myPower->Power;
myPower[A_,n_]:=1/;n==0;


(*myPower=Power; this definition screws things!!!*)


(*----------Generates tensor structures. The exponents are represented my MyPower----------*)
n4ListStructuresAuxiliary[{{l1_,lb1_},{l2_,lb2_},{l3_,lb3_},{l4_,lb4_}}]:=Module[
{
	q1,qb1,q2,qb2,q3,qb3,q4,qb4,
	list={},
	i,
	
	listA={},listB={},listC={},
	
	out={}
},

	(*--------------Trace-less symmetric operators--------------*)
	If[l1==lb1&&l2==lb2&&l3==lb3&&l4==lb4,
		Do[
			If[(q1-qb1)+(q2-qb2)+(q3-qb3)+(q4-qb4)==((l1+l2+l3+l4)-(lb1+lb2+lb3+lb4))/2,
				(*Parity-definite structures*)
				If[{q1,q2,q3,q4}=={qb1,qb2,qb3,qb4},
					listA=Append[listA,myPower[\[Xi][1],q1] myPower[\[Xi][2],q2] myPower[\[Xi][3],q3] myPower[\[Xi][4],q4] myPower[\[Xi]b[1],qb1] myPower[\[Xi]b[2],qb2] myPower[\[Xi]b[3],qb3] myPower[\[Xi]b[4],qb4]myPower[\[Eta][1],l1-q1] myPower[\[Eta][2],l2-q2] myPower[\[Eta][3],l3-q3] myPower[\[Eta][4],l4-q4] myPower[\[Eta]b[1],lb1-qb1] myPower[\[Eta]b[2],lb2-qb2] myPower[\[Eta]b[3],lb3-qb3] myPower[\[Eta]b[4],lb4-qb4]]
				];
				(*Parity-indefinite structures*)
				If[{q1,q2,q3,q4}!={qb1,qb2,qb3,qb4}&&MemberQ[list,{qb1,q1,qb2,q2,qb3,q3,qb4,q4}]==False,
					list=Append[list,{q1,qb1,q2,qb2,q3,qb3,q4,qb4}]
				];
			];
	
			,{q1,0,l1},{qb1,0,lb1}
			,{q2,0,l2},{qb2,0,lb2}
			,{q3,0,l3},{qb3,0,lb3}
			,{q4,0,l4},{qb4,0,lb4}
		];
		
		Do[
			{q1,qb1,q2,qb2,q3,qb3,q4,qb4}=list[[i]];
			listB=Append[listB,
				myPower[\[Xi][1],q1] myPower[\[Xi][2],q2] myPower[\[Xi][3],q3] myPower[\[Xi][4],q4] myPower[\[Xi]b[1],qb1] myPower[\[Xi]b[2],qb2] myPower[\[Xi]b[3],qb3] myPower[\[Xi]b[4],qb4]myPower[\[Eta][1],l1-q1] myPower[\[Eta][2],l2-q2] myPower[\[Eta][3],l3-q3] myPower[\[Eta][4],l4-q4] myPower[\[Eta]b[1],lb1-qb1] myPower[\[Eta]b[2],lb2-qb2] myPower[\[Eta]b[3],lb3-qb3] myPower[\[Eta]b[4],lb4-qb4]
			];
			listC=Append[listC,
				myPower[\[Xi][1],qb1] myPower[\[Xi][2],qb2] myPower[\[Xi][3],qb3] myPower[\[Xi][4],qb4] myPower[\[Xi]b[1],q1] myPower[\[Xi]b[2],q2] myPower[\[Xi]b[3],q3] myPower[\[Xi]b[4],q4]myPower[\[Eta][1],l1-qb1] myPower[\[Eta][2],l2-qb2] myPower[\[Eta][3],l3-qb3] myPower[\[Eta][4],l4-qb4] myPower[\[Eta]b[1],lb1-q1] myPower[\[Eta]b[2],lb2-q2] myPower[\[Eta]b[3],lb3-q3] myPower[\[Eta]b[4],lb4-q4]
			];
	
			,{i,1,Length[list]}
		];
		
		out={listA,listB,listC};
	];
	
	
	(*--------------Non trace-less symmetric operators--------------*)
	If[l1!=lb1||l2!=lb2||l3!=lb3||l4!=lb4,
		Do[
			If[(q1-qb1)+(q2-qb2)+(q3-qb3)+(q4-qb4)==((l1+l2+l3+l4)-(lb1+lb2+lb3+lb4))/2,
				out=Append[out,
					myPower[\[Xi][1],q1] myPower[\[Xi][2],q2] myPower[\[Xi][3],q3] myPower[\[Xi][4],q4] myPower[\[Xi]b[1],qb1] myPower[\[Xi]b[2],qb2] myPower[\[Xi]b[3],qb3] myPower[\[Xi]b[4],qb4]myPower[\[Eta][1],l1-q1] myPower[\[Eta][2],l2-q2] myPower[\[Eta][3],l3-q3] myPower[\[Eta][4],l4-q4] myPower[\[Eta]b[1],lb1-qb1] myPower[\[Eta]b[2],lb2-qb2] myPower[\[Eta]b[3],lb3-qb3] myPower[\[Eta]b[4],lb4-qb4]
				];
			];
		
		,{q1,0,l1},{qb1,0,lb1}
		,{q2,0,l2},{qb2,0,lb2}
		,{q3,0,l3},{qb3,0,lb3}
		,{q4,0,l4},{qb4,0,lb4}
		];
	];
	
	Return[out]

];

(*----------Generates tensor structures in an ordinary form----------*)
n4ListStructures[{{l1_,lb1_},{l2_,lb2_},{l3_,lb3_},{l4_,lb4_}}]:=n4ListStructuresAuxiliary[{{l1,lb1},{l2,lb2},{l3,lb3},{l4,lb4}}]//ToOrdinaryOutput;

n4KinematicFactor[{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_,\[CapitalDelta]4_},{{l1_,lb1_},{l2_,lb2_},{l3_,lb3_},{l4_,lb4_}}]:=
	Times[
		(spM[2,4]/spM[1,4])^(1/2 (\[CapitalDelta]1-\[CapitalDelta]2+1/2 (l1+lb1-l2-lb2))),
		(spM[1,4]/spM[1,3])^(1/2 (\[CapitalDelta]3-\[CapitalDelta]4+1/2 (l3+lb3-l4-lb4))),
		spM[1,2]^(-(1/2)(\[CapitalDelta]1+\[CapitalDelta]2+1/2 (l1+lb1+l2+lb2))) ,
		spM[3,4]^(-(1/2)(\[CapitalDelta]3+\[CapitalDelta]4+1/2 (+l3+lb3+l4+lb4)))
	]//PowerExpand;


countStructures[SpinList_]:=Module[
{
	out,
	i,
	temp
},
	
	(*1-point functions*)
	If[Length[SpinList]==1,
		out=0;
	];
	
	(*2-point functions*)
	If[Length[SpinList]==2, 
		If[(SpinList[[1,1]]==SpinList[[2,2]])&&(SpinList[[1,2]]==SpinList[[2,1]]),
			If[SpinList[[1,1]]==SpinList[[1,2]],
				out=SuperPlus[1];
				,
				out=1;
			];
			,
			out=0;
		];
	];
	
	(*3-point functions*)
	If[Length[SpinList]==3, 
		If[(SpinList[[1,1]]==SpinList[[1,2]])&&(SpinList[[2,1]]==SpinList[[2,2]])&&(SpinList[[3,1]]==SpinList[[3,2]]),
			temp=n3ListStructuresAlternativeTS[SpinList];
			out=SuperPlus[temp[[1]]//Length]+SuperMinus[temp[[2]]//Length];
			,
			out=n3ListStructures[SpinList]//Length;
		];
	];
	
	(*4-point functions*)
	If[Length[SpinList]==4,
		temp=n4ListStructures[SpinList];
		If[(SpinList[[1,1]]==SpinList[[1,2]])&&(SpinList[[2,1]]==SpinList[[2,2]])&&(SpinList[[3,1]]==SpinList[[3,2]])&&(SpinList[[4,1]]==SpinList[[4,2]]),
			out=SuperPlus[Length[temp[[1]]]+Length[temp[[2]]]]+SuperMinus[temp[[2]]//Length];
			,
			out=temp//Length;
		];
	];
	
	(*5-point functions*)
	If[Length[SpinList]==5,
		If[(SpinList[[1,1]]==SpinList[[1,2]])&&(SpinList[[2,1]]==SpinList[[2,2]])&&(SpinList[[3,1]]==SpinList[[3,2]])&&(SpinList[[4,1]]==SpinList[[4,2]])&&(SpinList[[5,1]]==SpinList[[5,2]]),
			temp=Product[(1+SpinList[[i,1]]),{i,1,5}];
			out=SuperPlus[1/2temp(temp+1)]+SuperMinus[1/2temp(temp-1)];
			,
			temp=Sum[SpinList[[i,1]]+SpinList[[i,2]],{i,1,5}];
			If[EvenQ[temp]==True,
				out=Product[(1+SpinList[[i,1]])(1+SpinList[[i,2]]),{i,1,5}];
				,
				out=0;
			];
		];
	];

	If[Length[SpinList]>=6,
		temp=Sum[SpinList[[i,1]]+SpinList[[i,2]],{i,1,Length[SpinList]}];
		If[EvenQ[temp]==True,
			out=Product[(1+SpinList[[i,1]])(1+SpinList[[i,2]]),{i,1,Length[SpinList]}];
			,
			out=0;
		];
	];

	Return[out]

];



(* ::Section::Closed:: *)
(*2. Conformal Partial Waves*)


(* ::Subsubsubsection::Closed:: *)
(*Start*)


basePath=$InputFileName//DirectoryName;
safeImport[filename_String,errormsg_String:"Some functionality may be unavailable."]:=Module[
{
	path
},
	path=FileNameJoin[{basePath,"Data",filename}];
	If[FileExistsQ[path],
		Get[path]
		,
		Print["Warning: Couldn't load \""<>filename<>"\". "<>errormsg];
		Null
	]
];

safeImportPrivateSymbol[symbol_String,errormsg_String:"Some functionality may be unavailable.", dir_: ""] := Module[{},
   BeginPackage["CFTs4D`"];
   Begin["`Private`"];
   
   Inactive[Unprotect]["CFTs4D`Private`" <> symbol] // Activate;
   Inactive[ClearAll]["CFTs4D`Private`" <> symbol] // Activate;
   Inactive[Set][Symbol["CFTs4D`Private`" <> symbol], safeImport[
      If[dir == "", symbol, FileNameJoin[{dir, symbol}]]
      ,errormsg
      ]] // Activate;
   Inactive[Protect]["CFTs4D`Private`" <> symbol] // Activate;
   
   End[];
   EndPackage[];
   
   Symbol["CFTs4D`Private`" <> symbol]
   ];

safeImportPrivateSymbolNoSet[symbol_String,errormsg_String:"Some functionality may be unavailable.", dir_: ""] := Module[{},
   BeginPackage["CFTs4D`"];
   Begin["`Private`"];
   
   Inactive[Unprotect]["CFTs4D`Private`" <> symbol] // Activate;
   Inactive[ClearAll]["CFTs4D`Private`" <> symbol] // Activate;
   safeImport[
      If[dir == "", symbol, FileNameJoin[{dir, symbol}]]
      ,errormsg
      ];
   Inactive[Protect]["CFTs4D`Private`" <> symbol] // Activate;
   
   End[];
   EndPackage[];
   
   Symbol["CFTs4D`Private`" <> symbol]
   ];

safeImportPrivate[file_,errormsg_String:"Some functionality may be unavailable."] := Module[{tmp},
   BeginPackage["CFTs4D`"];
   Begin["`Private`"];
   
   tmp = safeImport[file,errormsg];
   
   End[];
   EndPackage[];
   
   tmp
   ];


seedCPW[p_]:=n4KinematicFactor[{\[CapitalDelta][1],\[CapitalDelta][2],\[CapitalDelta][3],\[CapitalDelta][4]},{{0,0},{p,0},{0,0},{0,p}}]*Sum[
	invSbS[{4,2},{}]^e (invSbS[{4,2},{3,1}]/spM[1,3])^(p-e) (-2)^(p-e)G[{p,e},{\[CapitalDelta],l},{\[CapitalDelta][1],\[CapitalDelta][2],\[CapitalDelta][3],\[CapitalDelta][4]},{0,0}]
	,{e,0,p}
	];


(*--------------------Change of Variables: (u,v) to (z,zb)--------------------*)
ListReplacementDerivatives:=safeImportPrivateSymbol["ListReplacementDerivatives","Function changeVariables will be unavailable."];
(*Module[{},
	Unprotect[ListReplacementDerivatives];
	ClearAll[ListReplacementDerivatives];
	ListReplacementDerivatives=safeImport["ListReplacementDerivatives","Function changeVariables will be unavailable."];
	Protect[ListReplacementDerivatives];
	ListReplacementDerivatives
];*)

changeVariables[expr_]:=(expr/.{U->z zb,V->(1-z)(1-zb)}/.ListReplacementDerivatives);

Derivative[n_,m_][H[a__,{n0_,m0_}]][args__]:=H[a,{n+n0,m+m0}][args];


(*--------------------Special Simplify--------------------*)
specialSimplifyG[expr_]:=Collect[expr,G[__],Simplify];
specialSimplifyH[expr_]:=Collect[expr,H[__][__],Simplify];



(* ::Subsubsubsection::Closed:: *)
(*Plugging the Structure of the Seed CPWs and Duals Seed CPWs*)


(*--------------------Constructing the Octagons--------------------*)
(*defining the octogon: -(2p-e)\[LessEqual]m\[LessEqual]p*)
nMin[m_,p_,e_]:=If[-(2p-e)<=m&&m<-(p-e),-m-(2p-e),0]+If[-(p-e)<=m&&m<=0,-p,0]+If[0<m&&m<=p,m-p,0];
nMax[m_,p_,e_]:=If[-(2p-e)<=m&&m<-(p-e),m+2p,0]+If[-(p-e)<=m&&m<=0,p+e,0]+If[0<m&&m<=p,-m+p+e,0];
(*Checking the shape of the octogons
Octogon[p_,e_]:=Table[{m,n},{m,-2p+e,p},{n,nMin[m,p,e],nMax[m,p,e]}];
DualOctogon[p_,e_]:=Table[{m,n},{m,-p-e,p},{n,nMin[m,p,p-e],nMax[m,p,p-e]}];
*)


(*--------------------Constructing the Structural Form of the Seed and Dual Seed Conformal Blocks--------------------*)
(*Eigenfunctions*)
F[\[Rho]1_,\[Rho]2_][a_,b_,c_]:=k[\[Rho]1,a,b,c][z]k[\[Rho]2,a,b,c][zb] - tt k[\[Rho]1,a,b,c][zb]k[\[Rho]2,a,b,c][z]; (*tt - term tracker*)

(*Expresses all the derivatives of the prefactor in terms of the prefactor and polynomials in z and zb*)
PrefactorDerivativesRule[m_,n_]:=Module[
{
	flag=True
	,temp
},
	If[m==0&&n==0, temp=pref[z,zb];flag=False];
	If[m==1&&n==0,temp=-1/z^2 pref[z,zb]^2;flag=False];
	If[m==0&&n==1,temp=1/zb^2 pref[z,zb]^2;flag=False];
	If[flag==True, temp=(-1)^m (m+n-2)! (z zb)^(-m-n-1) pref[z,zb]^(m+n+1)(n(n-1)z^2+2m n z zb+m(m-1)zb^2)];
	Return[temp]
];

reducePrefactorDerivatives[expr_]:=expr/.Derivative[m_,n_][pref][z,zb]:>PrefactorDerivativesRule[m,n];

plugPrefactor[expr_]:=expr/.{pref:>Function[{z,zb},(z zb)/(z-zb)]};

(*
dE=\[CapitalDelta]: dimension of the exchanged operator;
ell=l: spin of the exchanged operator;
*)

(*Blocks*)
Core[p_,e_,m_,n_]:=pref[z,zb]^(2p+1) F[(dE+ell+p/2)/2+m,(dE-ell+p/2)/2-(p+1)+n][a,b+p-e,p-e];
CoreDerivatives[p_,e_,m_,n_][M_,N_]:=D[D[Core[p,e,m,n],{z,M}],{zb,N}]//reducePrefactorDerivatives//plugPrefactor;
Hex[p_,e_][M_,N_]:=Sum[cs[{dE,ell},dims][p,e,m,n]CoreDerivatives[p,e,m,n][M,N],{m,-2p+e,p},{n,nMin[m,p,e],nMax[m,p,e]}];

(*Dual Blocks*)
DualCore[p_,e_,m_,n_]:=pref[z,zb]^(2p+1) F[(dE+ell-p/2)/2+e+m,(dE-ell-p/2)/2+e-(p+1)+n][a,b+p-e,p-e];
DualCoreDerivatives[p_,e_,m_,n_][M_,N_]:=D[D[DualCore[p,e,m,n],{z,M}],{zb,N}]//reducePrefactorDerivatives//plugPrefactor;
DualHex[p_,e_][M_,N_]:=Sum[csd[{dE,ell},dims][p,e,m,n]DualCoreDerivatives[p,e,m,n][M,N],{m,-p-e,p},{n,nMin[m,p,p-e],nMax[m,p,p-e]}];


(*Transforming the Coefficients*)
abToDeltas[p_,{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_,\[CapitalDelta]4_}][expr_]:=expr/.{a->-((\[CapitalDelta]1-\[CapitalDelta]2)/2)+p/4,b->(\[CapitalDelta]3-\[CapitalDelta]4)/2-p/4};

(*list_={\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_,\[CapitalDelta]4_}*)
Hfinal[{p_,e_},{\[CapitalDelta]_,l_},list_,{M_,N_}]:=(Hex[p,e][M,N]/.dims->list/.{dE->\[CapitalDelta],ell->l})//abToDeltas[p,list];
DualHfinal[{p_,e_},{\[CapitalDelta]_,l_},list_,{M_,N_}]:=(DualHex[p,e][M,N]/.dims->list/.{dE->\[CapitalDelta],ell->l})//abToDeltas[p,list];


(*Substituting H's*)
plugSeedBlocks[val_][expr_]:=expr/.H[{p_,e_},{\[CapitalDelta]_,l_},list_,{M_,N_}][zv_,zbv_]:>(Hfinal[{p,e},{\[CapitalDelta],l},list,{M,N}]/.tt->val/.{z->zv,zb->zbv});
plugDualSeedBlocks[val_][expr_]:=expr/.H[{p_,e_},{\[CapitalDelta]_,l_},list_,{M_,N_}][zv_,zbv_]:>(DualHfinal[{p,e},{\[CapitalDelta],l},list,{M,N}]/.tt->val/.{z->zv,zb->zbv});


(*Expresses all the derivatives of k-functions in terms of k-functions*)
reduceKFunctionDerivatives[expr_]:=expr/.Derivative[n_][k[R_,A_,B_,C_]][X_]:>P[R,A,B,C,X][n]k[R,A,B,C][X]+Q[R,A,B,C,X][n](R/X k[R,A,B,C][X]+((A+R) (B+R))/(C+2R) k[R,A+1,B+1,C+1][X]);


(* ::Subsubsubsection::Closed:: *)
(*Plugging Explicit Expression of Structural Components*)


ListPolynomialsFull:=safeImportPrivateSymbolNoSet["ListPolynomialsFull","The function plugPolynomialsPQ will be anavailable."];
(*Module[{},
	Unprotect[ListPolynomialsFull];
	ClearAll[ListPolynomialsFull];
	ListPolynomialsFull=safeImport["ListPolynomialsFull","The function plugPolynomialsPQ will be anavailable."];
	Protect[ListPolynomialsFull];
	ListPolynomialsFull
];*)

plugPolynomialsPQ[expr_]:=expr/.{
	P[R_,A_,B_,C_,X_][n_]:>((P[n]/.ListPolynomialsFull)/.{\[Rho]->R,a->A,b->B,c->C,z->X}),
	Q[R_,A_,B_,C_,X_][n_]:>((Q[n]/.ListPolynomialsFull)/.{\[Rho]->R,a->A,b->B,c->C,z->X})
};



(*Plugs explicit expressions of the k-functions*)
plugKFunctions[expr_]:=expr/.{
	k[\[Rho]_,a_,b_,c_]:>Function[X,X^\[Rho] Hypergeometric2F1[a+\[Rho],b+\[Rho],c+2\[Rho],X]]
(*	Derivative[m_][k[\[Rho]_,a_,b_,c_]][X_]:>D[X^\[Rho] Hypergeometric2F1[a+\[Rho],b+\[Rho],c+2\[Rho],X],{X,m}]*)
};

(*Plugs a polynomial approxiamtion of the k-functions at z=1/2*)
kApproximation:=safeImportPrivateSymbolNoSet["kApproximation","The function plugKFunctionsApproximated will be unavailable."];
(*(safeImport["kApproximation","The function plugKFunctionsApproximated will be unavailable."];kApproximation);*)
plugKFunctionsApproximated[order_][expr_]:=expr/.kApproximation[[order]];

plugCoefficients[p_][expr_]:=Module[{name,temp,tempDual},

	name=FileNameJoin[{"seeds","blocks","coefficientsP"<>ToString[p]}];
	temp=safeImportPrivate[name,"The functionality of plugCoefficients is anavailable for the given value of p."];
	
	name=FileNameJoin[{"seeds","dual_blocks","coefficientsP"<>ToString[p]<>"dual"}];
	tempDual=safeImportPrivate[name,"The functionality of plugCoefficients is anavailable for the given value of p."];
	
	Return[
		expr/.{
			cs[{dE_,ell_},dims_][p,e_,m_,n_]:>(c[e][m,n]/.temp/.c[p][0,-p]->(-1)^l I^p/.{\[CapitalDelta]->dE,l->ell}//abToDeltas[p,dims])
			,csd[{dE_,ell_},dims_][p,e_,m_,n_]:>(c[e][m,n]/.tempDual/.c[0][0,-p]->(-1)^l I^p (1/2)^p/.{\[CapitalDelta]->dE,l->ell}//abToDeltas[p,dims])
			}
(*The replacement \[CapitalDelta]\[Rule]"something" is dangerous if there are exntries {\[CapitalDelta][1],\[CapitalDelta][2],\[CapitalDelta][3],\[CapitalDelta][4]}. Luckily in the above expressions
 all the dependencies on {\[CapitalDelta][1],\[CapitalDelta][2],\[CapitalDelta][3],\[CapitalDelta][4]} are hidden in "a" and "b"*)

	](*we set the un-fixed normalization to +1. In principle correct normalization should be chosen.*)

];


(* ::Subsubsubsection::Closed:: *)
(*Recursion Relations*)


seedRecursionRelation:=safeImportPrivateSymbol["seedRecursionRelation","Seed recursion relations will be unavailable.","seeds/blocks"];
dualSeedRecursionRelation:=safeImportPrivateSymbol["dualSeedRecursionRelation","Dual seed recursion relations will be unavailable.","seeds/dual_blocks"];


plugSeedRecursion[expr_]:=expr/.seedRecursionRelation/. H[{p_, e_}, stuff__][z_, zb_] :> 
   If[e < 0 || e > p, 0, H[{p, e}, stuff][z, zb]];

plugDualSeedRecursion[expr_]:=expr/.dualSeedRecursionRelation/. H[{p_, e_}, stuff__][z_, zb_] :> 
   If[e < 0 || e > p, 0, H[{p, e}, stuff][z, zb]];





(* ::Section::Closed:: *)
(*3. Embedding Formalism Algebra*)


(* ::Subsection::Closed:: *)
(*Differentiation*)


(* ::Subsubsubsection::Closed:: *)
(*Main Rules*)


(*--------------------Elementary Properties of Derivatives--------------------*)
(*---we define a list of elements which should be treated as constants by all the differential operators---*)
(*listConstants={tt,\[CapitalDelta],l,lb,cs,csd};*)
(*---we define a list of elements which should be treated as functions by all the differential operators---*)
(*listFunctions={Gamma};*)


(*----------DS----------*)
DS[A_][X__]:=0/;NumberQ[A];(*action on numbers*)
DS[A_ B_][X__]:=A DS[B][X]+B DS[A][X];(*action on products*)
DS[A_^n_][X__]:=n A^(n-1)DS[A][X];(*action on powers*)
DS[A_+B_][X__]:=DS[A][X]+DS[B][X];(*action on sums*)

(*----------DSb----------*)
DSb[A_][X__]:=0/;NumberQ[A];(*action on numbers*)
DSb[A_ B_][X__]:=A DSb[B][X]+B DSb[A][X];(*action on products*)
DSb[A_^n_][X__]:=n A^(n-1)DSb[A][X];(*action on powers*)
DSb[A_+B_][X__]:=DSb[A][X]+DSb[B][X];(*action on sums*)

(*----------DXSigma----------*)
DXSigma[A_][X__]:=0/;NumberQ[A];(*action on numbers*)
DXSigma[A_ B_][X__]:=A DXSigma[B][X]+B DXSigma[A][X];(*action on products*)
DXSigma[A_^n_][X__]:=n A^(n-1) DXSigma[A][X];(*action on powers*)
DXSigma[A_+B_][X__]:=DXSigma[A][X]+DXSigma[B][X];(*action on sums*)

(*----------DXSigmab----------*)
DXSigmab[A_][X__]:=0/;NumberQ[A];(*action on numbers*)
DXSigmab[A_ B_][X__]:=A DXSigmab[B][X]+B DXSigmab[A][X];(*action on products*)
DXSigmab[A_^n_][X__]:=n A^(n-1) DXSigmab[A][X];(*action on powers*)
DXSigmab[A_+B_][X__]:=DXSigmab[A][X]+DXSigmab[B][X];(*action on sums*)


(*--------------------Contractions of Sigma-matrices--------------------*)
SigmaDotSigma[a_,b_,c_,d_]:=2EPS[a,b,c,d];
SigmaDotSigmab[a_,b_,c_,d_]:=2(DEL[d,a]DEL[c,b]-DEL[c,a]DEL[d,b]);
SigmabDotSigma[a_,b_,c_,d_]:=SigmaDotSigmab[c,d,a,b];
SigmabDotSigmab[a_,b_,c_,d_]:=2EPSb[a,b,c,d];


(*------------------------------------------------------------Part I: differentiation------------------------------------------------------------*)


(*--------------------Differentiation I: objects with indices--------------------*)

(*----------DS----------*)
DS[DEL[a_,b_]][X__]:=0;
DS[EPS[a_,b_,c_,d_]][X__]:=0;
DS[EPSb[a_,b_,c_,d_]][X__]:=0;
DS[X[list_][a_,b_]][X__]:=0;
DS[Xb[list_][a_,b_]][X__]:=0;
DS[XXb[list_][a_,b_]][X__]:=0;
DS[S[i_][a_]][j_,b_]:=If[i==j,DEL[b,a],0];
DS[Sb[i_][a_]][X__]:=0;
DS[XbS[i_,list_][a_]][j_,b_]:=If[j==i,Xb[list][a,b],0];
DS[SbX[i_,list_][a_]][X__]:=0;
DS[XXbS[i_,list_][a_]][j_,b_]:=If[j==i,XXb[list][a,b],0];
DS[SbXXb[i_,list_][a_]][X__]:=0;

(*----------DSb----------*)
DSb[DEL[a_,b_]][X__]:=0;
DSb[EPS[a_,b_,c_,d_]][X__]:=0;
DSb[EPSb[a_,b_,c_,d_]][X__]:=0;
DSb[X[list_][a_,b_]][X__]:=0;
DSb[Xb[list_][a_,b_]][X__]:=0;
DSb[XXb[list_][a_,b_]][X__]:=0;
DSb[S[i_][a_]][X__]:=0;
DSb[Sb[i_][a_]][j_,b_]:=If[i==j,DEL[a,b],0];
DSb[XbS[i_,list_][a_]][X__]:=0;
DSb[SbX[i_,list_][a_]][j_,b_]:=If[j==i,X[list][b,a],0];
DSb[XXbS[i_,list_][a_]][X__]:=0;
DSb[SbXXb[i_,list_][a_]][j_,b_]:=If[j==i,XXb[list][b,a],0];

(*----------DXSigma----------*)
DXSigma[DEL[a_,b_]][X__]:=0;
DXSigma[EPS[a_,b_,c_,d_]][X__]:=0;
DXSigma[EPSb[a_,b_,c_,d_]][X__]:=0;

DXSigma[X[list_][a_,b_]][i_,c_,d_]:=Module[{A,B,g,out=0},(*---e.g. list={i,j,k,l,m}: \[InvisiblePrefixScriptBase]Subscript[X, i] Subscript[Xb, j ]Subscript[X, k] Subscript[Xb, l] Subscript[X, m]---*)
If[MemberQ[list,i]==True,
(*---only if there are points to be differentiated---*)
 
 Do[
(*---we check every entry of the list if it should be differentiated---*)
	 If[i==list[[g]],

(*---if the entry is X---*)
If[OddQ[g]==True,
out=out+contractIndices[XXb[Take[list,g-1]][a,A]SigmaDotSigma[c,d,A,B]] XbX[Take[list,g-Length[list]]][B,b]
   ];
(*---if the entry is Xb---*)
If[EvenQ[g]==True,
out=out+contractIndices[X[Take[list,g-1]][a,A]SigmaDotSigmab[c,d,A,B]] X[Take[list,g-Length[list]]][B,b]
  ]
(*---ending the "Do" cycle---*)

	  ],
{g,1,Length[list]}]

  ];

Return[out//contractIndices]
];

DXSigma[Xb[list_][a_,b_]][i_,c_,d_]:=Module[{A,B,g,out=0},(*---e.g. list={i,j,k,l,m}: \[InvisiblePrefixScriptBase]Subscript[Xb, i] Subscript[X, j ]Subscript[Xb, k] Subscript[X, l] Subscript[Xb, m]---*)
If[MemberQ[list,i]==True,
(*---only if there are points to be differentiated---*)
 
 Do[
(*---we check every entry of the list if it should be differentiated---*)
	 If[i==list[[g]],

(*---if the entry is Xb---*)
If[OddQ[g]==True,
out=out+contractIndices[XbX[Take[list,g-1]][a,A]SigmaDotSigmab[c,d,A,B]] XXb[Take[list,g-Length[list]]][B,b]
  ];
(*---if the entry is X---*)
If[EvenQ[g]==True,
out=out+contractIndices[Xb[Take[list,g-1]][a,A]SigmaDotSigma[c,d,A,B]] Xb[Take[list,g-Length[list]]][B,b]
  ]
(*---ending the "Do" cycle---*)

	  ],
{g,1,Length[list]}]

  ];

Return[out//contractIndices]
];

DXSigma[XXb[list_][a_,b_]][i_,c_,d_]:=Module[{A,B,g,out=0},(*---e.g. list={i,j,k,l,m}: \[InvisiblePrefixScriptBase]Subscript[X, i] Subscript[Xb, j ]Subscript[X, k] Subscript[Xb, l]---*)
If[MemberQ[list,i]==True,
(*---only if there are points to be differentiated---*)
 
 Do[
(*---we check every entry of the list if it should be differentiated---*)
	 If[i==list[[g]],

(*---if the entry is X---*)
If[OddQ[g]==True,
out=out+contractIndices[XXb[Take[list,g-1]][a,A]SigmaDotSigma[c,d,A,B]] Xb[Take[list,g-Length[list]]][B,b]
  ];
(*---if the entry is Xb---*)
If[EvenQ[g]==True,
out=out+contractIndices[X[Take[list,g-1]][a,A]SigmaDotSigmab[c,d,A,B]] XXb[Take[list,g-Length[list]]][B,b]
  ]
(*---ending the "Do" cycle---*)

	  ],
{g,1,Length[list]}]

  ];

Return[out//contractIndices]
];

DXSigma[S[i_][a_]][X__]:=0;
DXSigma[Sb[i_][a_]][X__]:=0;
DXSigma[XbS[i_,list_][a_]][j_,b_,c_]:=Module[{A},DXSigma[Xb[list][a,A]][j,b,c]S[i][A]]//contractIndices;
DXSigma[SbX[i_,list_][a_]][j_,b_,c_]:=Module[{A},Sb[i][A]DXSigma[X[list][A,a]][j,b,c]]//contractIndices;
DXSigma[XXbS[i_,list_][a_]][j_,b_,c_]:=Module[{A},DXSigma[XXb[list][a,A]][j,b,c]S[i][A]]//contractIndices;
DXSigma[SbXXb[i_,list_][a_]][j_,b_,c_]:=Module[{A},Sb[i][A]DXSigma[XXb[list][A,a]][j,b,c]]//contractIndices;

(*----------DXSigmab----------*)
DXSigmab[DEL[a_,b_]][X__]:=0;
DXSigmab[EPS[a_,b_,c_,d_]][X__]:=0;
DXSigmab[EPSb[a_,b_,c_,d_]][X__]:=0;

DXSigmab[X[list_][a_,b_]][i_,c_,d_]:=Module[{A,B,g,out=0},(*---e.g. list={i,j,k,l,m}: \[InvisiblePrefixScriptBase]Subscript[X, i] Subscript[Xb, j ]Subscript[X, k] Subscript[Xb, l] Subscript[X, m]---*)
If[MemberQ[list,i]==True,
(*---only if there are points to be differentiated---*)
 
 Do[
(*---we check every entry of the list if it should be differentiated---*)
	 If[i==list[[g]],

(*---if the entry is X---*)
If[OddQ[g]==True,
out=out+contractIndices[XXb[Take[list,g-1]][a,A]SigmabDotSigma[c,d,A,B]] XbX[Take[list,g-Length[list]]][B,b]
   ];
(*---if the entry is Xb---*)
If[EvenQ[g]==True,
out=out+contractIndices[X[Take[list,g-1]][a,A]SigmabDotSigmab[c,d,A,B]] X[Take[list,g-Length[list]]][B,b]
  ]
(*---ending the "Do" cycle---*)

	  ],
{g,1,Length[list]}]

  ];

Return[out//contractIndices]
];

DXSigmab[Xb[list_][a_,b_]][i_,c_,d_]:=Module[{A,B,g,out=0},(*---e.g. list={i,j,k,l,m}: \[InvisiblePrefixScriptBase]Subscript[Xb, i] Subscript[X, j ]Subscript[Xb, k] Subscript[X, l] Subscript[Xb, m]---*)
If[MemberQ[list,i]==True,
(*---only if there are points to be differentiated---*)
 
 Do[
(*---we check every entry of the list if it should be differentiated---*)
	 If[i==list[[g]],

(*---if the entry is Xb---*)
If[OddQ[g]==True,
out=out+contractIndices[XbX[Take[list,g-1]][a,A]SigmabDotSigmab[c,d,A,B]] XXb[Take[list,g-Length[list]]][B,b]
  ];
(*---if the entry is X---*)
If[EvenQ[g]==True,
out=out+contractIndices[Xb[Take[list,g-1]][a,A]SigmabDotSigma[c,d,A,B]] Xb[Take[list,g-Length[list]]][B,b]
  ]
(*---ending the "Do" cycle---*)

	  ],
{g,1,Length[list]}]

  ];

Return[out//contractIndices]
];

DXSigmab[XXb[list_][a_,b_]][i_,c_,d_]:=Module[{A,B,g,out=0},(*---e.g. list={i,j,k,l,m}: \[InvisiblePrefixScriptBase]Subscript[X, i] Subscript[Xb, j ]Subscript[X, k] Subscript[Xb, l]---*)
If[MemberQ[list,i]==True,
(*---only if there are points to be differentiated---*)
 
 Do[
(*---we check every entry of the list if it should be differentiated---*)
	 If[i==list[[g]],

(*---if the entry is X---*)
If[OddQ[g]==True,
out=out+contractIndices[XXb[Take[list,g-1]][a,A]SigmabDotSigma[c,d,A,B]] Xb[Take[list,g-Length[list]]][B,b]
  ];
(*---if the entry is Xb---*)
If[EvenQ[g]==True,
out=out+contractIndices[X[Take[list,g-1]][a,A]SigmabDotSigmab[c,d,A,B]] XXb[Take[list,g-Length[list]]][B,b]
  ]
(*---ending the "Do" cycle---*)

	  ],
{g,1,Length[list]}]

  ];

Return[out//contractIndices]
];

DXSigmab[S[i_][a_]][X__]:=0;
DXSigmab[Sb[i_][a_]][X__]:=0;
DXSigmab[XbS[i_,list_][a_]][j_,b_,c_]:=Module[{A},DXSigmab[Xb[list][a,A]][j,b,c]S[i][A]]//contractIndices;
DXSigmab[SbX[i_,list_][a_]][j_,b_,c_]:=Module[{A},Sb[i][A]DXSigmab[X[list][A,a]][j,b,c]]//contractIndices;
DXSigmab[XXbS[i_,list_][a_]][j_,b_,c_]:=Module[{A},DXSigmab[XXb[list][a,A]][j,b,c]S[i][A]]//contractIndices;
DXSigmab[SbXXb[i_,list_][a_]][j_,b_,c_]:=Module[{A},Sb[i][A]DXSigmab[XXb[list][A,a]][j,b,c]]//contractIndices;


(*--------------------Differentiation II: invariants--------------------*)

(*----------DS----------*)
DS[sp[i_,j_]][X__]:=0;
DS[spM[i_,j_]][X__]:=0;
DS[invSbS[{i_,j_},list_]][k_,a_]:=If[k==j,SbXXb[i,list][a],0];
DS[invSS[{i_,j_},list_]][k_,a_]:=Module[{A},DS[S[i][A]][k,a]XbS[j,list][A]+SXb[i,list][A]DS[S[j][A]][k,a]//contractIndices];
DS[invSbSb[{i_,j_},list_]][X__]:=0;

(*----------DSb----------*)
DSb[sp[i_,j_]][X__]:=0;
DSb[spM[i_,j_]][X__]:=0;
DSb[invSbS[{i_,j_},list_]][k_,a_]:=If[k==i,XXbS[j,list][a],0];
DSb[invSS[{i_,j_},list_]][k_,a_]:=0;
DSb[invSbSb[{i_,j_},list_]][k_,a_]:=Module[{A},DSb[Sb[i][A]][k,a]XSb[j,list][A]+SbX[i,list][A]DSb[Sb[j][A]][k,a]//contractIndices];

(*----------DXSigma----------*)
DXSigma[sp[i_,j_]][k_,a_,b_]:=If[k==i,X[{j}][a,b],0]+If[k==j,X[{i}][a,b],0];
DXSigma[spM[i_,j_]][k_,a_,b_]:=-2(If[k==i,X[{j}][a,b],0]+If[k==j,X[{i}][a,b],0]);
DXSigma[invSbS[{i_,j_},list_]][k_,a_,b_]:=Module[{A,B},Sb[i][A]DXSigma[XXb[list][A,B]][k,a,b]S[j][B]//contractIndices];
DXSigma[invSS[{i_,j_},list_]][k_,a_,b_]:=Module[{A,B},S[i][A]DXSigma[Xb[list][A,B]][k,a,b]S[j][B]//contractIndices];
DXSigma[invSbSb[{i_,j_},list_]][k_,a_,b_]:=Module[{A,B},Sb[i][A]DXSigma[X[list][A,B]][k,a,b]Sb[j][B]//contractIndices];

(*----------DXSigmab----------*)
DXSigmab[sp[i_,j_]][k_,a_,b_]:=If[k==i,Xb[{j}][a,b],0]+If[k==j,Xb[{i}][a,b],0];
DXSigmab[spM[i_,j_]][k_,a_,b_]:=-2(If[k==i,Xb[{j}][a,b],0]+If[k==j,Xb[{i}][a,b],0]);
DXSigmab[invSbS[{i_,j_},list_]][k_,a_,b_]:=Module[{A,B},Sb[i][A]DXSigmab[XXb[list][A,B]][k,a,b]S[j][B]//contractIndices];
DXSigmab[invSS[{i_,j_},list_]][k_,a_,b_]:=Module[{A,B},S[i][A]DXSigmab[Xb[list][A,B]][k,a,b]S[j][B]//contractIndices];
DXSigmab[invSbSb[{i_,j_},list_]][k_,a_,b_]:=Module[{A,B},Sb[i][A]DXSigmab[X[list][A,B]][k,a,b]Sb[j][B]//contractIndices];


(*--------------------Differentiation III: cross-ratios and functions--------------------*)

(*----------DS----------*)
DS[U][X__]:=0;
DS[V][X__]:=0;
DS[G[__]][X__]:=0;

(*----------DSb----------*)
DSb[U][X__]:=0;
DSb[V][X__]:=0;
DSb[G[__]][X__]:=0;

(*----------DXSigma----------*)
DXSigma[U][i_,a_,b_]:=DXSigma[(spM[1,2]spM[3,4])/(spM[1,3]spM[2,4])][i,a,b];
DXSigma[V][i_,a_,b_]:=DXSigma[(spM[1,4]spM[2,3])/(spM[1,3]spM[2,4])][i,a,b];
DXSigma[G[{p_,e_},{\[CapitalDelta]_,l_},{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_,\[CapitalDelta]4_},{m_,n_}]][i_,a_,b_]:=G[{p,e},{\[CapitalDelta],l},{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3,\[CapitalDelta]4},{m+1,n}]DXSigma[U][i,a,b]+G[{p,e},{\[CapitalDelta],l},{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3,\[CapitalDelta]4},{m,n+1}]DXSigma[V][i,a,b];

(*----------DXSigma----------*)
DXSigmab[U][i_,a_,b_]:=DXSigmab[(spM[1,2]spM[3,4])/(spM[1,3]spM[2,4])][i,a,b];
DXSigmab[V][i_,a_,b_]:=DXSigmab[(spM[1,4]spM[2,3])/(spM[1,3]spM[2,4])][i,a,b];
DXSigmab[G[{p_,e_},{\[CapitalDelta]_,l_},{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_,\[CapitalDelta]4_},{m_,n_}]][i_,a_,b_]:=G[{p,e},{\[CapitalDelta],l},{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3,\[CapitalDelta]4},{m+1,n}]DXSigmab[U][i,a,b]+G[{p,e},{\[CapitalDelta],l},{\[CapitalDelta]1,\[CapitalDelta]2,\[CapitalDelta]3,\[CapitalDelta]4},{m,n+1}]DXSigmab[V][i,a,b];

(* ----- Nested functions ------ *)

(*This is just to not waste time on going in*)
DS[\[CapitalDelta][n_]][X__]:=0;
DS[\[Lambda][n__]][X__]:=0;
DS[\[Lambda]P[n_]][X__]:=0;
DS[\[Lambda]M[n_]][X__]:=0;

DSb[\[CapitalDelta][n_]][X__]:=0;
DSb[\[Lambda][n__]][X__]:=0;
DSb[\[Lambda]P[n_]][X__]:=0;
DSb[\[Lambda]M[n_]][X__]:=0;

DXSigma[\[CapitalDelta][n_]][X__]:=0;
DXSigma[\[Lambda][n__]][X__]:=0;
DXSigma[\[Lambda]P[n_]][X__]:=0;
DXSigma[\[Lambda]M[n_]][X__]:=0;

DXSigmab[\[CapitalDelta][n_]][X__]:=0;
DXSigmab[\[Lambda][n__]][X__]:=0;
DXSigmab[\[Lambda]P[n_]][X__]:=0;
DXSigmab[\[Lambda]M[n_]][X__]:=0;

(* Chain rules *)

DS[fn_[args__]][X__] := Module[
{
	ln = Length[{args}]
},
   Sum[(Derivative @@ UnitVector[ln, i])[fn][args] DS[{args}[[i]]][X], {i,
      1, ln}]
];

DS[Derivative[ns_][fn_][args__]][X__] := Module[
{
	ln = Length[{args}]
	,nl = {ns}
},
   Sum[(Derivative @@ (UnitVector[ln, i] + nl))[fn][
      args] DS[{args}[[i]]][X], {i, 1, ln}]
];

DSb[fn_[args__]][X__] := Module[
{
	ln = Length[{args}]
},
   Sum[(Derivative @@ UnitVector[ln, i])[fn][args] DSb[{args}[[i]]][X], {i,
      1, ln}]
];

DSb[Derivative[ns_][fn_][args__]][X__] := Module[
{
	ln = Length[{args}]
	,nl = {ns}
},
   Sum[(Derivative @@ (UnitVector[ln, i] + nl))[fn][
      args] DSb[{args}[[i]]][X], {i, 1, ln}]
];

DXSigma[fn_[args__]][X__] := Module[
{
	ln = Length[{args}]
},
   Sum[(Derivative @@ UnitVector[ln, i])[fn][args] DXSigma[{args}[[i]]][X], {i,
      1, ln}]
];

DXSigma[Derivative[ns_][fn_][args__]][X__] := Module[
{
	ln = Length[{args}]
	,nl = {ns}
},
   Sum[(Derivative @@ (UnitVector[ln, i] + nl))[fn][
      args] DXSigma[{args}[[i]]][X], {i, 1, ln}]
];

DXSigmab[fn_[args__]][X__] := Module[
{
	ln = Length[{args}]
},
   Sum[(Derivative @@ UnitVector[ln, i])[fn][args] DXSigmab[{args}[[i]]][X], {i,
      1, ln}]
];

DXSigmab[Derivative[ns_][fn_][args__]][X__] := Module[
{
	ln = Length[{args}]
	,nl = {ns}
},
   Sum[(Derivative @@ (UnitVector[ln, i] + nl))[fn][
      args] DXSigmab[{args}[[i]]][X], {i, 1, ln}]
];

(* -------- The rest is trivial -------- *)

(* These have to be the very last definitions *)
DS[A_][X__]:=0;
DSb[A_][X__]:=0;
DXSigma[A_][X__]:=0;
DXSigmab[A_][X__]:=0;




(* ::Subsubsubsection::Closed:: *)
(*Differential Operators*)


(*----------Conservation----------*)
difSbS[expr_][i_,a_,c_]:=Module[{b},4DS[DSb[expr][i,c]][i,a]+S[i][b]DS[DS[DSb[expr][i,c]][i,a]][i,b]+Sb[i][b]DSb[DS[DSb[expr][i,c]][i,a]][i,b]-S[i][c]DS[DS[DSb[expr][i,b]][i,b]][i,a]-Sb[i][a]DSb[DS[DSb[expr][i,b]][i,b]][i,c]//Expand];
difX[expr_][i_,a_,c_]:=Module[{b},1/4 (X[{i}][a,b]DXSigmab[expr][i,b,c]-Xb[{i}][b,c]DXSigma[expr][i,a,b])//Expand];
opConservationEF[i_][expr_]:=Module[{A,C},difX[difSbS[expr][i,A,C]][i,A,C]//Expand//contractIndices];


(*----------Spinning Differential Operators----------*)
opDEF[i_,j_][expr_]:=Module[{A,B,C},1/2 Sb[i][A]X[{j}][A,B]DXSigmab[expr][i,B,C]S[i][C]-1/2 Sb[i][A]DXSigma[expr][i,A,B]Xb[{j}][B,C]S[i][C](*/.{\[CapitalDelta][i]:>\[CapitalDelta][i],\[CapitalDelta][j]:>\[CapitalDelta][j]+1}*)//contractIndices];
opDtEF[i_,j_][expr_]:=Module[{A,B,C},Sb[i][A]X[{j}][A,B]DXSigmab[expr][j,B,C]S[i][C]+2invSbS[{i,j},{}]S[i][A]DS[expr][j,A]-2invSbS[{j,i},{}]Sb[i][A]DSb[expr][j,A](*/.{\[CapitalDelta][i]:>\[CapitalDelta][i]+1,\[CapitalDelta][j]:>\[CapitalDelta][j]}*)//contractIndices];
opIEF[i_,j_][expr_]:=(*spM[i,j]^(-(1/2))*) invSbS[{i,j},{}]expr;
opdEF[i_,j_][expr_]:=Module[{A,B},(*spM[i,j]^(-(1/2))*) S[j][A]Xb[{i}][A,B]DSb[expr][i,B]//contractIndices];
opdbEF[i_,j_][expr_]:=Module[{A,B},(*spM[i,j]^(-(1/2))*) Sb[j][A]X[{i}][A,B]DS[expr][i,B]//contractIndices];
opNEF[i_,j_][expr_]:=Module[{A,B},(*spM[i,j]^(-(1/2))*) XXb[{i,j}][A,B]DSb[DS[expr][i,A]][j,B]//contractIndices];

dShift[opDEF][i_,j_]:=-UnitVector[4,j];
dShift[opDtEF][i_,j_]:=-UnitVector[4,i];
dShift[opIEF][i_,j_]:=-1/2 (UnitVector[4,i]+UnitVector[4,j]);
dShift[opdEF][i_,j_]:=-1/2 (UnitVector[4,i]+UnitVector[4,j]);
dShift[opdbEF][i_,j_]:=-1/2 (UnitVector[4,i]+UnitVector[4,j]);
dShift[opNEF][i_,j_]:=-1/2 (UnitVector[4,i]+UnitVector[4,j]);

dShift[opCasimir2EF][__]:={0,0,0,0};
dShift[opCasimir3EF][__]:={0,0,0,0};
dShift[opCasimir4EF][__]:={0,0,0,0};

\[CapitalXi][op_][arg__][expr_]:=op[arg][expr]/.{\[CapitalDelta][i_]:>\[CapitalDelta][i]-dShift[op][arg][[i]]};



(* ::Subsubsubsection::Closed:: *)
(*Casimir Differential Operators*)


e2pSO4[l_,lb_]:=(l(l+2)+lb(lb+2))/2;
e2mSO4[l_,lb_]:=(l(l+2)-lb(lb+2))/2;

casimirEigenvalue2[DD_,{l_,lb_}]:=DD*(DD-4)+e2pSO4[l,lb];
casimirEigenvalue3[DD_,{l_,lb_}]:=(DD-2)*e2mSO4[l,lb];
casimirEigenvalue4[DD_,{l_,lb_}]:=DD^2*(DD-4)^2+6*DD*(DD-4)+e2pSO4[l,lb]^2-e2mSO4[l,lb]^2/2;

casimirEigenvalue2[p_]:=casimirEigenvalue2[\[CapitalDelta],{l,l+p}];
casimirEigenvalue3[p_]:=casimirEigenvalue3[\[CapitalDelta],{l,l+p}];
casimirEigenvalue4[p_]:=casimirEigenvalue4[\[CapitalDelta],{l,l+p}];

(*----------Lorentz Generators: alternative----------*)
L[Arg_][i_,a_,b_,c_,d_]:=Module[{e,f,R,S1,S2},
	R=1/4 (X[{i}][a,b]DXSigma[Arg][i,c,d]-X[{i}][c,d]DXSigma[Arg][i,a,b]);
	S1=1/4 (-S[i][a]EPS[b,c,d,f]+S[i][b]EPS[c,d,a,f]+S[i][c]EPS[d,a,b,f]-S[i][d]EPS[a,b,c,f])DS[Arg][i,f];
	S2=1/4 Sb[i][e](-EPS[e,a,b,c]DSb[Arg][i,d]-EPS[e,b,c,d]DSb[Arg][i,a]+EPS[e,c,d,a]DSb[Arg][i,b]+EPS[e,d,a,b]DSb[Arg][i,c]);
	Return[R+S1+S2]
];

Lb[Arg_][i_,a_,b_,c_,d_]:=Module[{e,f,R,S1,S2},
	R=1/4 (Xb[{i}][a,b]DXSigmab[Arg][i,c,d]-Xb[{i}][c,d]DXSigmab[Arg][i,a,b]);
	S1=1/4 S[i][e](-EPSb[e,a,b,c]DS[Arg][i,d]-EPSb[e,b,c,d]DS[Arg][i,a]+EPSb[e,c,d,a]DS[Arg][i,b]+EPSb[e,d,a,b]DS[Arg][i,c]);
	S2=1/4 (-Sb[i][a]EPSb[b,c,d,f]+Sb[i][b]EPSb[c,d,a,f]+Sb[i][c]EPSb[d,a,b,f]-Sb[i][d]EPSb[a,b,c,f])DSb[Arg][i,f];
	Return[R+S1+S2]
];

(*----------Quadratic Casimir Operator: alternative, works faster----------*)
opCasimir2EF[pts__][Arg_]:=Module[{a,b,c,d,step},
	step=Sum[L[Arg][i,a,b,c,d],{i,{pts}}];
	Return[-1/2 (Sum[Lb[step][i,a,b,c,d],{i,{pts}}]//contractIndices//applyEFProperties)]
];


(*----------The 6D Lorentz Generators----------*)
(*a - lower index and c - upper index*)
opL[i_,a_,c_][expr_]:=Module[{b},

	-(1/2(X[{i}][a,b]DXSigmab[expr][i,b,c]-Xb[{i}][c,b]DXSigma[expr][i,b,a])-1/2DEL[c,a](S[i][b]DS[expr][i,b]-Sb[i][b]DSb[expr][i,b])+2(S[i][a]DS[expr][i,c]-Sb[i][c]DSb[expr][i,a]))//Expand//contractIndices//applyEFProperties

];

(*sum of lorentz generators*)
opLcollective[list_,a_,c_][expr_]:=Module[{i},
	Sum[expr//opL[list[[i]],a,c],{i,1,list//Length}]
];


(*notice in what follows: A[B[C[expr]]]=expr//C//B//A *)

(*
(*----------Quadratic Casimir Operator----------*)
opCasimir2EF[A__][expr_]:=Module[{a,b,list},
list=A//List;

1/4 expr//opLcollective[list,a,b]//opLcollective[list,b,a]//Expand//contractIndices//applyEFProperties
(*ordering of hilbert-space operators: L_a^b L_b^a, the order of differential operators is reversed*)

];
*)

(*----------Cubic Casimir Operator----------*)
opCasimir3EF[A__][expr_]:=Module[{a,b,c,temp1,temp2,list},
	list=A//List;
	
	temp1=expr//opLcollective[list,a,b]//opLcollective[list,b,c]//opLcollective[list,c,a]//Expand//contractIndices//applyEFProperties;
	(*ordering of hilbert-space operators: L_a^b L_b^c L_c^a, the order of differential operators is reversed*)
	
	temp2=expr//(opCasimir2EF@@list);
	
	Return[1/12(temp1-16temp2)]

];

(*----------Quartic Casimir Operator----------*)
opCasimir4EF[A__][expr_]:=Module[{a,b,c,d,temp1,temp2,temp3,temp4,list},
	list=A//List;
	
	temp1=-(1/4)expr//opLcollective[list,a,b]//opLcollective[list,b,c]//opLcollective[list,c,d]//opLcollective[list,d,a]//Expand//contractIndices//applyEFProperties;
	(*ordering of hilbert-space operators: L_a^b L_b^c L_c^d L_d^a, the order of differential operators is reversed*)
	
	temp2=2 expr//opLcollective[list,a,b]//opLcollective[list,b,c]//opLcollective[list,c,a]//Expand//contractIndices//applyEFProperties;
	(*ordering of hilbert-space operators: L_a^b L_b^c L_c^a, the order of differential operators is reversed*)
	
	temp3=-4expr//(opCasimir2EF@@list);
	temp4=3expr//(opCasimir2EF@@list)//(opCasimir2EF@@list);
	
	Return[1/2 (temp1+temp2+temp3+temp4)]

];


(* ::Subsection::Closed:: *)
(*Contractions*)


(*Warning: during contractions sometimes we use gauge conditions. Thus contract only when acting with gauge invariant operators*)


(*Ordering:
1:   DEL[a,b] (*a - index up and b - index down*)
2:   EPS[a,b,c,d]
3:   EPSb[a,b,c,d]
4:   X[list][a,b]
5:   Xb[list][a,b]
6:   XXb[list][a,b]
7:   S[i][a]
8:   Sb[i][a]
9:   XbS[m,list][a]
10: SbX[m,list][a]
11: XXbS[m,list][a]
12: SbXXb[m,list][a]
*)


(*----------Auxiliary Functions----------*)
(*Reverse objects*)
XbX[list_][a_,b_]:=XXb[list//Reverse][b,a];
SXb[i_,list_][a_]:=-XbS[i,list//Reverse][a];
XSb[i_,list_][a_]:=-SbX[i,list//Reverse][a];
SXbX[i_,list_][a_]:=XXbS[i,list//Reverse][a];
XbXSb[i_,list_][a_]:=SbXXb[i,list//Reverse][a];

(*Degenerate cases*)
XXb[{}][a_,b_]:=DEL[b,a];
XXbS[i_,{}][a_]:=S[i][a];
SbXXb[i_,{}][a_]:=Sb[i][a];

(*ZeroIfTrue*)
ZeroIfTrue[expr_]:=If[expr==True,0,1];

(*----------Contractions 1----------*)
(*Contraction: 1-1*)
contraction[1,1]={
	DEL[a_,a_]:>4,
	DEL[a_,b_]DEL[b_,c_]:>DEL[a,c],
	DEL[a_,b_]DEL[c_,a_]:>DEL[c,b]
};

(*Contraction: 1-2*)
contraction[1,2]=
{
	DEL[a_,e_]EPS[a_,b_,c_,d_]:>EPS[e,b,c,d],
	DEL[a_,e_]EPS[b_,a_,c_,d_]:>EPS[b,e,c,d],
	DEL[a_,e_]EPS[b_,c_,a_,d_]:>EPS[b,c,e,d],
	DEL[a_,e_]EPS[b_,c_,d_,a_]:>EPS[b,c,d,e]
};

(*Contraction: 1-3*)
contraction[1,3]=
{
	DEL[e_,a_]EPSb[a_,b_,c_,d_]:>EPSb[e,b,c,d],
	DEL[e_,a_]EPSb[b_,a_,c_,d_]:>EPSb[b,e,c,d],
	DEL[e_,a_]EPSb[b_,c_,a_,d_]:>EPSb[b,c,e,d],
	DEL[e_,a_]EPSb[b_,c_,d_,a_]:>EPSb[b,c,d,e]
};

(*Contraction: 1-4*)
contraction[1,4]=
{
	DEL[a_,b_]X[list_][a_,c_]:>X[list][b,c],
	DEL[a_,b_]X[list_][c_,a_]:>X[list][c,b]
};

(*Contraction: 1-5*)
contraction[1,5]=
{
	DEL[a_,b_]Xb[list_][b_,c_]:>Xb[list][a,c],
	DEL[a_,b_]Xb[list_][c_,b_]:>Xb[list][c,a]
};

(*Contraction: 1-6*)
contraction[1,6]=
{
	DEL[a_,b_]XXb[list_][a_,c_]:>XXb[list][b,c],
	DEL[a_,b_]XXb[list_][c_,b_]:>XXb[list][c,a]
};

(*Contraction: 1-7*)
contraction[1,7]=
{
	DEL[a_,b_]S[i_][a_]:>S[i][b]
};

(*Contraction: 1-8*)
contraction[1,8]=
{
	DEL[a_,b_]Sb[i_][b_]:>Sb[i][a]
};

(*Contraction: 1-9*)
contraction[1,9]=
{
	DEL[a_,b_]XbS[i_,list_][b_]:>XbS[i,list][a]
};

(*Contraction: 1-10*)
contraction[1,10]=
{
	DEL[a_,b_]SbX[i_,list_][a_]:>SbX[i,list][b]
};

(*Contraction: 1-11*)
contraction[1,11]=
{
	DEL[a_,b_]XXbS[i_,list_][a_]:>XXbS[i,list][b]
};

(*Contraction: 1-12*)
contraction[1,12]=
{
	DEL[a_,b_]SbXXb[i_,list_][b_]:>SbXXb[i,list][a]
};

(*----------Contractions 2----------*)
	contraction[1]=Table[contraction[1,i],{i,1,12}]//Flatten;

(*Contraction: 2-2*)
contraction[2,2]=
{
};

(*Contraction: 2-3*)
contraction[2,3]=
{
	(*---The Rule---*)
	EPSb[a_,b_,c_,d_]EPS[a_,e_,f_,g_]:>DEL[b,e]DEL[c,f]DEL[d,g]-DEL[b,e]DEL[c,g]DEL[d,f]-DEL[b,f]DEL[c,e]DEL[d,g]+DEL[b,f]DEL[c,g]DEL[d,e]+DEL[b,g]DEL[c,e]DEL[d,f]-DEL[b,g]DEL[c,f]DEL[d,e],
	
	EPSb[a_,b_,c_,d_]EPS[e_,a_,f_,g_]:>-EPSb[a,b,c,d]EPS[a,e,f,g],(*---permutations 1---*)
	EPSb[a_,b_,c_,d_]EPS[e_,f_,a_,g_]:>EPSb[a,b,c,d]EPS[a,e,f,g],
	EPSb[a_,b_,c_,d_]EPS[e_,f_,g_,a_]:>-EPSb[a,b,c,d]EPS[a,e,f,g],
	
	EPSb[b_,a_,c_,d_]EPS[a_,e_,f_,g_]:>-EPSb[a,b,c,d]EPS[a,e,f,g],(*---permutations 2---*)
	EPSb[b_,a_,c_,d_]EPS[e_,a_,f_,g_]:>EPSb[a,b,c,d]EPS[a,e,f,g],
	EPSb[b_,a_,c_,d_]EPS[e_,f_,a_,g_]:>-EPSb[a,b,c,d]EPS[a,e,f,g],
	EPSb[b_,a_,c_,d_]EPS[e_,f_,g_,a_]:>EPSb[a,b,c,d]EPS[a,e,f,g],
	
	EPSb[b_,c_,a_,d_]EPS[a_,e_,f_,g_]:>EPSb[a,b,c,d]EPS[a,e,f,g],(*---permutations 3---*)
	EPSb[b_,c_,a_,d_]EPS[e_,a_,f_,g_]:>-EPSb[a,b,c,d]EPS[a,e,f,g],
	EPSb[b_,c_,a_,d_]EPS[e_,f_,a_,g_]:>EPSb[a,b,c,d]EPS[a,e,f,g],
	EPSb[b_,c_,a_,d_]EPS[e_,f_,g_,a_]:>-EPSb[a,b,c,d]EPS[a,e,f,g],
	
	EPSb[b_,c_,d_,a_]EPS[a_,e_,f_,g_]:>-EPSb[a,b,c,d]EPS[a,e,f,g],(*---permutations 4---*)
	EPSb[b_,c_,d_,a_]EPS[e_,a_,f_,g_]:>EPSb[a,b,c,d]EPS[a,e,f,g],
	EPSb[b_,c_,d_,a_]EPS[e_,f_,a_,g_]:>-EPSb[a,b,c,d]EPS[a,e,f,g],
	EPSb[b_,c_,d_,a_]EPS[e_,f_,g_,a_]:>EPSb[a,b,c,d]EPS[a,e,f,g]
};

(*Contraction: 2-4*)
contraction[2,4]=
{
};

(*Contraction: 2-5*)
contraction[2,5]=
{
	EPS[a_,b_,c_,d_]Xb[list_][a_,f_]:>Module[{e},
		Expand[
		-(DEL[e,b]X[Take[list,1]][c,d]+DEL[e,c]X[Take[list,1]][d,b]+DEL[e,d]X[Take[list,1]][b,c])XXb[Drop[list,1]][e,f]
		]/.contraction[1]
	],
	
	EPS[b_,a_,c_,d_]Xb[list_][a_,f_]:>-EPS[a,b,c,d]Xb[list][a,f],(*---permutations 1---*)
	EPS[b_,c_,a_,d_]Xb[list_][a_,f_]:>EPS[a,b,c,d]Xb[list][a,f],
	EPS[b_,c_,d_,a_]Xb[list_][a_,f_]:>-EPS[a,b,c,d]Xb[list][a,f],
	
	EPS[a_,b_,c_,d_]Xb[list_][f_,a_]:>-EPS[a,b,c,d]Xb[list//Reverse][a,f],(*---permutations 2---*)
	EPS[b_,a_,c_,d_]Xb[list_][f_,a_]:>EPS[a,b,c,d]Xb[list//Reverse][a,f],
	EPS[b_,c_,a_,d_]Xb[list_][f_,a_]:>-EPS[a,b,c,d]Xb[list//Reverse][a,f],
	EPS[b_,c_,d_,a_]Xb[list_][f_,a_]:>EPS[a,b,c,d]Xb[list//Reverse][a,f]
};

(*Contraction: 2-6*)
contraction[2,6]=
{
	EPS[a_,b_,c_,d_]XXb[list_][f_,a_]:>Module[{e},
		Expand[
		(DEL[e,b]X[Take[list,-1]][c,d]+DEL[e,c]X[Take[list,-1]][d,b]+DEL[e,d]X[Take[list,-1]][b,c])X[Drop[list,-1]][f,e]
		]/.contraction[1]
	],

	EPS[b_,a_,c_,d_]XXb[list_][f_,a_]:>-EPS[a,b,c,d]XXb[list][f,a],(*---permutations---*)
	EPS[b_,c_,a_,d_]XXb[list_][f_,a_]:>EPS[a,b,c,d]XXb[list][f,a],
	EPS[b_,c_,d_,a_]XXb[list_][f_,a_]:>-EPS[a,b,c,d]XXb[list][f,a]
};

(*Contraction: 2-7*)
contraction[2,7]=
{
};

(*Contraction: 2-8*)
contraction[2,8]=
{
	EPS[a_,b_,c_,d_]Sb[i_][a_]Sb[j_][b_]Sb[k_][c_]Sb[l_][d_]:>If[i==j||i==k||i==l||j==k||j==l||k==l,0,Mb[{i,j,k,l}]],
	
	EPS[a_,b_,c_,d_]Sb[i_][a_]Sb[i_][b_]->0,
	EPS[a_,b_,c_,d_]Sb[i_][a_]Sb[i_][c_]->0,
	EPS[a_,b_,c_,d_]Sb[i_][a_]Sb[i_][d_]->0,
	EPS[a_,b_,c_,d_]Sb[i_][b_]Sb[i_][c_]->0,
	EPS[a_,b_,c_,d_]Sb[i_][b_]Sb[i_][d_]->0,
	EPS[a_,b_,c_,d_]Sb[i_][c_]Sb[i_][d_]->0
};

(*Contraction: 2-9*)
contraction[2,9]=
{
	EPS[a_,b_,c_,d_]XbS[i_,list_][a_]:>Module[{A},
		EPS[a,b,c,d]Xb[list][a,A]S[i][A]/.contraction[2,5]
	],
	
	EPS[b_,a_,c_,d_]XbS[i_,list_][a_]:>-EPS[a,b,c,d]XbS[i,list][a],(*---permutations---*)
	EPS[b_,c_,a_,d_]XbS[i_,list_][a_]:>EPS[a,b,c,d]XbS[i,list][a],
	EPS[b_,c_,d_,a_]XbS[i_,list_][a_]:>-EPS[a,b,c,d]XbS[i,list][a]
};

(*Contraction: 2-10*)
contraction[2,10]=
{
};

(*Contraction: 2-11*)
contraction[2,11]=
{
};

(*Contraction: 2-12*)
contraction[2,12]=
{
	EPS[a_,b_,c_,d_]SbXXb[i_,list_][a_]:>Module[{A},
		EPS[a,b,c,d]Sb[i][A]XXb[list][A,a]/.contraction[2,6]//Expand
	],
	
	EPS[b_,a_,c_,d_]SbXXb[i_,list_][a_]:>-EPS[a,b,c,d]SbXXb[i,list][a],(*---permutations---*)
	EPS[b_,c_,a_,d_]SbXXb[i_,list_][a_]:>EPS[a,b,c,d]SbXXb[i,list][a],
	EPS[b_,c_,d_,a_]SbXXb[i_,list_][a_]:>-EPS[a,b,c,d]SbXXb[i,list][a]
};

(*----------Contractions 3----------*)
(*Contraction: 3-3*)
contraction[3,3]=
{
};

(*Contraction: 3-4*)
contraction[3,4]=
{
	EPSb[a_,b_,c_,d_]X[list_][a_,f_]:>Module[{e},
		Expand[
			-(DEL[b,e]Xb[Take[list,1]][c,d]+DEL[c,e]Xb[Take[list,1]][d,b]+DEL[d,e]Xb[Take[list,1]][b,c])XbX[Drop[list,1]][e,f]
		]/.contraction[1]
	],
	
	EPSb[b_,a_,c_,d_]X[list_][a_,f_]:>-EPSb[a,b,c,d]X[list][a,f],(*---permutations 1---*)
	EPSb[b_,c_,a_,d_]X[list_][a_,f_]:>EPSb[a,b,c,d]X[list][a,f],
	EPSb[b_,c_,d_,a_]X[list_][a_,f_]:>-EPSb[a,b,c,d]X[list][a,f],
	
	EPSb[a_,b_,c_,d_]X[list_][f_,a_]:>-EPSb[a,b,c,d]X[list//Reverse][a,f],(*---permutations 2---*)
	EPSb[b_,a_,c_,d_]X[list_][f_,a_]:>EPSb[a,b,c,d]X[list//Reverse][a,f],
	EPSb[b_,c_,a_,d_]X[list_][f_,a_]:>-EPSb[a,b,c,d]X[list//Reverse][a,f],
	EPSb[b_,c_,d_,a_]X[list_][f_,a_]:>EPSb[a,b,c,d]X[list//Reverse][a,f]
};

(*Contraction: 3-5*)
contraction[3,5]=
{
};

(*Contraction: 3-6*)
contraction[3,6]=
{
	EPSb[a_,b_,c_,d_]XXb[list_][a_,f_]:>Module[{e},
		Expand[
			-(DEL[b,e]Xb[Take[list,1]][c,d]+DEL[c,e]Xb[Take[list,1]][d,b]+DEL[d,e]Xb[Take[list,1]][b,c])Xb[Drop[list,1]][e,f]
		]/.contraction[1]
	],
	
	EPSb[b_,a_,c_,d_]XXb[list_][a_,f_]:>-EPSb[a,b,c,d]XXb[list][a,f],(*---permutations---*)
	EPSb[b_,c_,a_,d_]XXb[list_][a_,f_]:>EPSb[a,b,c,d]XXb[list][a,f],
	EPSb[b_,c_,d_,a_]XXb[list_][a_,f_]:>-EPSb[a,b,c,d]XXb[list][a,f]
};

(*Contraction: 3-7*)
contraction[3,7]=
{
	EPSb[a_,b_,c_,d_]S[i_][a_]S[j_][b_]S[k_][c_]S[l_][d_]:>If[i==j||i==k||i==l||j==k||j==l||k==l,0,M[{i,j,k,l}]],
	
	EPSb[a_,b_,c_,d_]S[i_][a_]S[i_][b_]->0,
	EPSb[a_,b_,c_,d_]S[i_][a_]S[i_][c_]->0,
	EPSb[a_,b_,c_,d_]S[i_][a_]S[i_][d_]->0,
	EPSb[a_,b_,c_,d_]S[i_][b_]S[i_][c_]->0,
	EPSb[a_,b_,c_,d_]S[i_][b_]S[i_][d_]->0,
	EPSb[a_,b_,c_,d_]S[i_][c_]S[i_][d_]->0
};

(*Contraction: 3-8*)
contraction[3,8]=
{
};

(*Contraction: 3-9*)
contraction[3,9]=
{
};

(*Contraction: 3-10*)
contraction[3,10]=
{
	EPSb[a_,b_,c_,d_]SbX[i_,list_][a_]:>Module[{A},
		EPSb[a,b,c,d]Sb[i][A]X[list][A,a]/.contraction[3,4]
	],

	EPSb[b_,a_,c_,d_]SbX[i_,list_][a_]:>-EPSb[a,b,c,d]SbX[i,list][a],(*---permutations---*)
	EPSb[b_,c_,a_,d_]SbX[i_,list_][a_]:>EPSb[a,b,c,d]SbX[i,list][a],
	EPSb[b_,c_,d_,a_]SbX[i_,list_][a_]:>-EPSb[a,b,c,d]SbX[i,list][a]
};

(*Contraction: 3-11*)
contraction[3,11]=
{
	EPSb[a_,b_,c_,d_]XXbS[i_,list_][a_]:>Module[{e},
		Expand[
			-(DEL[b,e]Xb[Take[list,1]][c,d]+DEL[c,e]Xb[Take[list,1]][d,b]+DEL[d,e]Xb[Take[list,1]][b,c])XbS[i,Drop[list,1]][e]
		]/.contraction[1,9]
	],
	
	EPSb[b_,a_,c_,d_]XXbS[i_,list_][a_]:>-EPSb[a,b,c,d]XXbS[i,list][a],(*---permutations---*)
	EPSb[b_,c_,a_,d_]XXbS[i_,list_][a_]:>EPSb[a,b,c,d]XXbS[i,list][a],
	EPSb[b_,c_,d_,a_]XXbS[i_,list_][a_]:>-EPSb[a,b,c,d]XXbS[i,list][a]
};

(*Contraction: 3-12*)
contraction[3,12]=
{
};

(*----------Contractions 4----------*)
(*Contraction: 4-4*)
contraction[4,4]=
{
};

(*Contraction: 4-5*)
contraction[4,5]=
{
	X[list1_][a_,b_]Xb[list2_][b_,c_]:>XXb[Join[list1,list2]][a,c]ZeroIfTrue[Extract[list1,-1]==Extract[list2,1]],
	X[list1_][a_,b_]Xb[list2_][c_,b_]:>-XXb[Join[list1,list2//Reverse]][a,c]ZeroIfTrue[Extract[list1,-1]==Extract[list2,-1]],
	X[list1_][b_,a_]Xb[list2_][b_,c_]:>-XXb[Join[list1//Reverse,list2]][a,c]ZeroIfTrue[Extract[list1,1]==Extract[list2,1]],
	X[list1_][b_,a_]Xb[list2_][c_,b_]:>XXb[Join[list1//Reverse,list2//Reverse]][a,c]ZeroIfTrue[Extract[list1,1]==Extract[list2,-1]]
};

(*Contraction: 4-6*)
contraction[4,6]=
{
	X[list1_][a_,b_]XXb[list2_][c_,a_]:>X[Join[list2,list1]][c,b]ZeroIfTrue[Extract[list2,-1]==Extract[list1,1]],
	X[list1_][a_,b_]XXb[list2_][c_,b_]:>-X[Join[list2,list1//Reverse]][c,a]ZeroIfTrue[Extract[list2,-1]==Extract[list1,-1]]
};

(*Contraction: 4-7*)
contraction[4,7]=
{
};

(*Contraction: 4-8*)
contraction[4,8]=
{
	X[list_][a_,b_]Sb[i_][a_]:>SbX[i,list][b]ZeroIfTrue[i==Extract[list,1]],
	X[list_][a_,b_]Sb[i_][b_]:>-SbX[i,list//Reverse][a]ZeroIfTrue[i==Extract[list,-1]]
};

(*Contraction: 4-9*)
contraction[4,9]=
{
	X[list1_][a_,b_]XbS[i_,list2_][b_]:>XXbS[i,Join[list1,list2]][a]ZeroIfTrue[Extract[list1,-1]==Extract[list2,1]],
	X[list1_][a_,b_]XbS[i_,list2_][a_]:>-XXbS[i,Join[list1//Reverse,list2]][b]ZeroIfTrue[Extract[list1,1]==Extract[list2,1]]
};

(*Contraction: 4-10*)
contraction[4,10]=
{
};

(*Contraction: 4-11*)
contraction[4,11]=
{
};

(*Contraction: 4-12*)
contraction[4,12]=
{
	X[list1_][a_,b_]SbXXb[i_,list2_][a_]:>SbX[i,Join[list2,list1]][b]ZeroIfTrue[Extract[list2,-1]==Extract[list1,1]],
	X[list1_][a_,b_]SbXXb[i_,list2_][b_]:>-SbX[i,Join[list2,list1//Reverse]][a]ZeroIfTrue[Extract[list2,-1]==Extract[list1,-1]]
};

(*----------Contractions 5----------*)
(*Contraction: 5-5*)
contraction[5,5]=
{
};

(*Contraction: 5-6*)
contraction[5,6]=
{
	Xb[list1_][a_,b_]XXb[list2_][b_,c_]:>Xb[Join[list1,list2]][a,c]ZeroIfTrue[Extract[list1,-1]==Extract[list2,1]],
	Xb[list1_][b_,a_]XXb[list2_][b_,c_]:>-Xb[Join[list1//Reverse,list2]][a,c]ZeroIfTrue[Extract[list1,1]==Extract[list2,1]]
};

(*Contraction: 5-7*)
contraction[5,7]=
{
	Xb[list_][a_,b_]S[i_][b_]:>XbS[i,list][a]ZeroIfTrue[i==Extract[list,-1]],
	Xb[list_][a_,b_]S[i_][a_]:>-XbS[i,list//Reverse][b]ZeroIfTrue[i==Extract[list,1]]
};

(*Contraction: 5-8*)
contraction[5,8]=
{
};

(*Contraction: 5-9*)
contraction[5,9]=
{
};

(*Contraction: 5-10*)
contraction[5,10]=
{
	Xb[list1_][a_,b_]SbX[i_,list2_][a_]:>SbXXb[i,Join[list2,list1]][b]ZeroIfTrue[Extract[list2,-1]==Extract[list1,1]],
	Xb[list1_][a_,b_]SbX[i_,list2_][b_]:>-SbXXb[i,Join[list2,list1//Reverse]][a]ZeroIfTrue[Extract[list2,-1]==Extract[list1,-1]]
};

(*Contraction: 5-11*)
contraction[5,11]=
{
	Xb[list1_][a_,b_]XXbS[i_,list2_][b_]:>XbS[i,Join[list1,list2]][a]ZeroIfTrue[Extract[list1,-1]==Extract[list2,1]],
	Xb[list1_][a_,b_]XXbS[i_,list2_][a_]:>-XbS[i,Join[list1//Reverse,list2]][b]ZeroIfTrue[Extract[list1,1]==Extract[list2,1]]
};

(*Contraction: 5-12*)
contraction[5,12]=
{
};

(*----------Contractions 6----------*)
(*Contraction: 6-6*)
contraction[6,6]=
{
	XXb[list1_][a_,b_]XXb[list2_][b_,c_]:>XXb[Join[list1,list2]][a,c]ZeroIfTrue[Extract[list1,-1]==Extract[list2,1]],
	XXb[list_][a_,a_]:>tr[list]
};

(*Contraction: 6-7*)
contraction[6,7]=
{
	XXb[list_][a_,b_]S[i_][b_]:>XXbS[i,list][a]ZeroIfTrue[i==Extract[list,-1]]
};

(*Contraction: 6-8*)
contraction[6,8]=
{
	XXb[list_][a_,b_]Sb[i_][a_]:>SbXXb[i,list][b]ZeroIfTrue[i==Extract[list,1]]
};

(*Contraction: 6-9*)
contraction[6,9]=
{
	XXb[list1_][a_,b_]XbS[i_,list2_][a_]:>XbS[i,Join[list1//Reverse,list2]][b]ZeroIfTrue[Extract[list1,1]==Extract[list2,1]]
};

(*Contraction: 6-10*)
contraction[6,10]=
{
	XXb[list1_][a_,b_]SbX[i_,list2_][b_]:>SbX[i,Join[list2,list1//Reverse]][a]ZeroIfTrue[Extract[list2,-1]==Extract[list1,-1]]
};

(*Contraction: 6-11*)
contraction[6,11]=
{
	XXb[list1_][a_,b_]XXbS[i_,list2_][b_]:>XXbS[i,Join[list1,list2]][a]ZeroIfTrue[Extract[list1,-1]==Extract[list2,1]]
};

(*Contraction: 6-12*)
contraction[6,12]=
{
	XXb[list1_][a_,b_]SbXXb[i_,list2_][a_]:>SbXXb[i,Join[list2,list1]][b]ZeroIfTrue[Extract[list2,-1]==Extract[list1,1]]
};

(*----------Contractions 7----------*)
(*Contraction: 7-7*)
contraction[7,7]=
{
};

(*Contraction: 7-8*)
contraction[7,8]=
{
	S[i_][a_]Sb[j_][a_]:>If[i==j,0,invSbS[{j,i},{}]]
};

(*Contraction: 7-9*)
contraction[7,9]=
{
	S[i_][a_]XbS[j_,list_][a_]:>invSS[{i,j},list]ZeroIfTrue[i==Extract[list,1]]
};

(*Contraction: 7-10*)
contraction[7,10]=
{
};

(*Contraction: 7-11*)
contraction[7,11]=
{
};

(*Contraction: 7-12*)
contraction[7,12]=
{
	S[i_][a_]SbXXb[j_,list_][a_]:>invSbS[{j,i},list]ZeroIfTrue[i==Extract[list,-1]]
};

(*----------Contractions 8----------*)
(*Contraction: 8-8*)
contraction[8,8]=
{
};

(*Contraction: 8-9*)
contraction[8,9]=
{
};

(*Contraction: 8-10*)
contraction[8,10]=
{
	Sb[i_][a_]SbX[j_,list_][a_]:>invSbSb[{j,i},list]ZeroIfTrue[i==Extract[list,-1]]
};

(*Contraction: 8-11*)
contraction[8,11]=
{
	Sb[i_][a_]XXbS[j_,list_][a_]:>invSbS[{i,j},list]ZeroIfTrue[i==Extract[list,1]]
};

(*Contraction: 8-12*)
contraction[8,12]=
{
};

(*----------Contractions 9----------*)
(*Contraction: 9-9*)
contraction[9,9]=
{
};

(*Contraction: 9-10*)
contraction[9,10]=
{
	XbS[i_,list1_][a_]SbX[j_,list2_][a_]:>invSbS[{j,i},Join[list2,list1]]ZeroIfTrue[Extract[list2,-1]==Extract[list1,1]]
};

(*Contraction: 9-11*)
contraction[9,11]=
{
	XbS[i_,list1_][a_]XXbS[j_,list2_][a_]:>-invSS[{i,j},Join[list1//Reverse,list2]]ZeroIfTrue[Extract[list1,1]==Extract[list2,1]]
};

(*Contraction: 9-12*)
contraction[9,12]=
{
};

(*----------Contractions 10----------*)
(*Contraction: 10-10*)
contraction[10,10]=
{
};

(*Contraction: 10-11*)
contraction[10,11]=
{
};

(*Contraction: 10-12*)
contraction[10,12]=
{
	SbX[i_,list1_][a_]SbXXb[j_,list2_][a_]:>-invSbSb[{j,i},Join[list2,list1//Reverse]]ZeroIfTrue[Extract[list2,-1]==Extract[list1,-1]]
};

(*----------Contractions 11----------*)
(*Contraction: 11-11*)
contraction[11,11]=
{
};

(*Contraction: 11-12*)
contraction[11,12]=
{
	XXbS[i_,list1_][a_]SbXXb[j_,list2_][a_]:>invSbS[{j,i},Join[list2,list1]]ZeroIfTrue[Extract[list2,-1]==Extract[list1,1]]
};

(*----------Contractions 12----------*)
contraction[12,12]=
{
};


(*----------Contraction Function----------*)
FullContraction=Table[contraction[i,j],{i,1,12},{j,i,12}]//Flatten;
contractIndices[expr_]:=FixedPoint[Expand[#/.FullContraction/.{sp[i_,j_]:>-spM[i,j]/2}]&,Expand[expr]];(*This function takes an expression and expands it, then it repeatedly performs contractions + expansions until the expression does not change anymore*)


(* ::Subsection::Closed:: *)
(*Applying Properties*)


(* ::Subsubsection::Closed:: *)
(*Properties: light cone + gauge*)


(*Specia Rules*)

specialRule1={
	X[{i_}][a_,b_]+X[{i_}][b_,a_]->0,
	Xb[{i_}][a_,b_]+Xb[{i_}][b_,a_]->0
};

(* TODO: write a general function for longer traces *)
traceRules={
	tr[{}]->4,
	tr[{i_,j_}]:>4sp[i,j],
	tr[{i_,j_,k_,l_}]:>4sp[i,j]sp[k,l]-4sp[i,k]sp[j,l]+4sp[i,l]sp[j,k]
};


(*----------Properties of scalar product----------*)
Attributes[sp]={Orderless};
Attributes[spM]={Orderless};

(*----------applying the Properties on a single element----------*)
ApplyProperties[{factor_,{i_,list_,j_}}]:=Module[
{
	n,m
	,res
	,flag=True
	,pos
},

	res={{factor,{i,list,j}}}; (*If no manipulations are done below return the input*)
	
	
	(*ignore the below if we work with traces*)
	If[i>0&&j>0,
		(*---begin the "zero" check. We check weather the expression is zero or not---*)
		If[Length[list]!=0,
			If[(i==Extract[list,1]||j==Extract[list,-1]),
				res={};
				flag=False;
			];
		];(*Due to SXb=0 and SbX=0*)
		
		If[i==j&&(Length[list]<=1)&&flag==True,
			res={};
			flag=False;
		];(*Due to antysimmetry of X and Xb*)
		
		Do[
			If[list[[n]]==list[[n+1]]&&flag==True,
				res={};
				flag=False;
			];
			
			,{n,1,Length[list]-1}
		]; (*Due to XXb=0 and XbX=0*)
		(*---end of the "zero" check---*)
		
		
		(*---Applying symmetry properties of the invariants I, J, K and Kb---*)
		(*I*)If[Length[list]==2&&flag==True,
			If[i>j&&(list[[1]]<list[[2]]),
				res={{2factor sp[list[[1]],list[[2]]],{i,{},j}},{-factor,{i,list//Reverse,j}}};
			];
			If[i<j&&(list[[1]]>list[[2]]),
				res={{2factor sp[list[[1]],list[[2]]],{i,{},j}},{-factor,{i,list//Reverse,j}}};
			];
		];
		
		(*J*)If[Length[list]==2&&flag==True,
			If[i==j&&(list[[1]]>list[[2]]),
				res={{-factor,{i,list//Reverse,j}}};
				flag=False;
			];
		];
		
		(*K and Kb*)If[Length[list]==1&&flag==True,
			If[i>j,
				res={{-factor,{j,list,i}}};
				flag=False;
			];
		];
		(*---end of symmetry Subsubsection---*)
	];
	
	
	(*---begin the "decomposition" Subsubsection. We use the property Subscript[X, i]Subscript[Xb, j]=-Subscript[X, j]Subscript[Xb, i]+2Subscript[X, ij]to reshuffle the structures and decompose them---*)
	If[Length[list]!=0&&flag==True,(*only if the list has a non-zero length*)
		
		(*---Decomposition I---*)
		If[MemberQ[list,i]==True&&flag==True,(*we start reshuffling to utilize the property SXb=0 or SbX=0 for the point "i"*)
			pos=First[Position[list,i]][[1]];(*we pick the nearest position*)
			res=Table[{(-1)^(pos-1+n) 2sp[i,list[[n]]]factor,{i,Delete[list,{{n},{pos}}],j}},{n,1,pos-1}];
			flag=False
		];
		(*-----------------------*)
		
		
		(*---Decomposition II---*)
		If[MemberQ[list,j]==True&&flag==True,(*we start reshuffling to utilize the property SXb=0 or SbX=0 for the point "j"*)
			pos=Last[Position[list,j]][[1]];(*we pick the nearest position*)
			res=Table[{(-1)^(pos+n) 2sp[j,list[[n+1]]]factor,{i,Delete[list,{{pos},{n+1}}],j}},{n,pos,Length[list]-1}];
			flag=False;
		];
		(*-----------------------*)
		
		
		
		(*---Decomposition III---*)
		If[Length[list]!=Length[list//Union]&&flag==True,
			
			Do[
				listCut=Drop[list,n];(*we create a sub-list of the list*)
				el=Extract[list,n];(*we select the BASE ELEMENT of the list*)
				pos=Position[listCut,el]; (*we determine the position of the "repeated" element inside the listCut*)
				If[pos=={},Null,
					pos=n+pos[[1,1]];(*gives a position within of the "repeated" element within the list*)
					res=Table[{(-1)^(n-1-m) 2sp[el,list[[m]]]factor,{i,Delete[list,{{n},{m}}],j}},{m,n+1,pos-1}];(*we go through the listCut*)
					Break[]
				];
			
			,{n,1,Length[list]-2}
			];
		
		];
		(*------------------------*)
		
		
	
	];
	(*---end the "decomposition" Subsubsection*)

	Return[res]
];

(*----------Applying the properties to the list of elements----------*)
ApplyPropertiesOnList[list_]:=Module[
{
	t,temp,full={}
},
	Do[
		temp=ApplyProperties[list[[t]]];
		full=Join[full,temp]
	
	,{t,1,Length[list]}
	];

	Return[full]
];

(*----------Applying the properties on a single element repeatedly----------*)
ApplyPropertiesIteratively[list_]:=FixedPoint[ApplyPropertiesOnList[#]&,{list}];

(*----------Converting the Auxiliary Notation To Invariants----------*)
AuxiliaryToInvariants[expr_,name_]:=Module[
{
	p,
	factor,
	i,j,list,
	res=0
},

	Do[
		{factor,{i,list,j}}=expr[[p]];
		res=res+factor name[{i,j},list];
		
		,{p,1,Length[expr]}
	];

Return[res]
];

(*----------Converting the Auxiliary Notation To Traces----------*)
AuxiliaryToTraces[expr_]:=Module[
{
	p,
	factor,
	i,j,list,
	res=0
},

	Do[
		{factor,{i,list,j}}=expr[[p]];
		res=res+factor tr[list];
	,{p,1,Length[expr]}
	];
	
	Return[res]
];


(*----------Applying Properties on Invariants----------*)
applyEFProperties[expr_]:=
	expr/.{spM[i_,j_]:>-2sp[i,j]}/.{
		sp[i_,i_]:>0,
		invSbS[{i_,j_},list_]:>AuxiliaryToInvariants[{1,{i,list,j}}//ApplyPropertiesIteratively,invSbS],
		invSS[{i_,j_},list_]:>AuxiliaryToInvariants[{1,{i,list,j}}//ApplyPropertiesIteratively,invSS],
		invSbSb[{i_,j_},list_]:>AuxiliaryToInvariants[{1,{i,list,j}}//ApplyPropertiesIteratively,invSbSb],
		tr[list_]:>(AuxiliaryToTraces[{1,{-7,list,-13}}//ApplyPropertiesIteratively]/.traceRules)
	}/.{sp[i_,j_]:>-spM[i,j]/2}/.specialRule1;


(* ::Subsubsection::Closed:: *)
(*Jacobi Relations: n=3*)


n3RelationKKb1=
{
	invSS[{j_,k_},{i_}]invSbSb[{i_,k_},{j_}]:>2 sp[i,j]invSbS[{i,k},{}]invSbS[{k,j},{}]+invSbS[{i,j},{}]invSbS[{k,k},{i,j}],
	
	(*---permutations---*)
	invSS[{j_,k_},{i_}]invSbSb[{k_,i_},{j_}]:>-invSS[{j,k},{i}]invSbSb[{i,k},{j}],
	invSS[{k_,j_},{i_}]invSbSb[{i_,k_},{j_}]:>-invSS[{j,k},{i}]invSbSb[{i,k},{j}],
	invSS[{k_,j_},{i_}]invSbSb[{k_,i_},{j_}]:>invSS[{j,k},{i}]invSbSb[{i,k},{j}]
};

n3RelationKKb2=
{
	invSS[{i_,j_},{k_}]invSbSb[{i_,j_},{k_}]:>1/2 1/sp[i,j] invSbS[{i,i},{j,k}]invSbS[{j,j},{i,k}]-2 sp[i,k]sp[j,k]/sp[i,j] invSbS[{i,j},{}]invSbS[{j,i},{}],
	
	(*---permutations---*)
	invSS[{i_,j_},{k_}]invSbSb[{j_,i_},{k_}]:>-invSS[{i,j},{k}]invSbSb[{i,j},{k}],
	invSS[{j_,i_},{k_}]invSbSb[{i_,j_},{k_}]:>-invSS[{i,j},{k}]invSbSb[{i,j},{k}]
};

n3RelationJK=
{
	invSbS[{i_,i_},{j_,k_}]invSS[{j_,k_},{i_}]:>-2(sp[i,k]invSbS[{i,j},{}]invSS[{i,k},{j}]+sp[i,j]invSbS[{i,k},{}]invSS[{i,j},{k}]),
	
	(*---permutations---*)
	invSbS[{i_,i_},{j_,k_}]invSS[{k_,j_},{i_}]:>-invSbS[{i,i},{j,k}]invSS[{j,k},{i}],
	invSbS[{i_,i_},{k_,j_}]invSS[{j_,k_},{i_}]:>-invSbS[{i,i},{j,k}]invSS[{j,k},{i}]
};

n3RelationJKb=
{
	invSbS[{i_,i_},{j_,k_}]invSbSb[{j_,k_},{i_}]:>2(sp[i,k]invSbS[{j,i},{}]invSbSb[{i,k},{j}]+sp[i,j]invSbS[{k,i},{}]invSbSb[{i,j},{k}]),
	
	(*---permutations---*)
	invSbS[{i_,i_},{j_,k_}]invSS[{k_,j_},{i_}]:>-invSbS[{i,i},{j,k}]invSS[{j,k},{i}],
	invSbS[{i_,i_},{k_,j_}]invSS[{j_,k_},{i_}]:>-invSbS[{i,i},{j,k}]invSS[{j,k},{i}]
};


n3RelationJJJ=
{
	invSbS[{i_,i_},{j_,k_}]invSbS[{j_,j_},{i_,k_}]invSbS[{k_,k_},{i_,j_}]:>8sp[j,k]sp[i,k]sp[i,j](invSbS[{j,i},{}]invSbS[{i,k},{}]invSbS[{k,j},{}]-invSbS[{i,j},{}]invSbS[{k,i},{}]invSbS[{j,k},{}])+4(sp[i,k]sp[i,j]invSbS[{j,k},{}]invSbS[{k,j},{}]invSbS[{i,i},{j,k}]-sp[j,k]sp[i,j]invSbS[{i,k},{}]invSbS[{k,i},{}]invSbS[{j,j},{i,k}]+sp[j,k]sp[i,k]invSbS[{i,j},{}]invSbS[{j,i},{}]invSbS[{k,k},{i,j}]),
	
	(*---permutations---*)
	invSbS[{i_,i_},{k_,j_}]invSbS[{j_,j_},{i_,k_}]invSbS[{k_,k_},{i_,j_}]:>-invSbS[{i,i},{j,k}]invSbS[{j,j},{i,k}]invSbS[{k,k},{i,j}],
	invSbS[{i_,i_},{j_,k_}]invSbS[{j_,j_},{k_,i_}]invSbS[{k_,k_},{i_,j_}]:>-invSbS[{i,i},{j,k}]invSbS[{j,j},{i,k}]invSbS[{k,k},{i,j}],
	invSbS[{i_,i_},{j_,k_}]invSbS[{j_,j_},{i_,k_}]invSbS[{k_,k_},{j_,i_}]:>-invSbS[{i,i},{j,k}]invSbS[{j,j},{i,k}]invSbS[{k,k},{i,j}],
	
	invSbS[{i_,i_},{k_,j_}]invSbS[{j_,j_},{k_,i_}]invSbS[{k_,k_},{i_,j_}]:>invSbS[{i,i},{j,k}]invSbS[{j,j},{i,k}]invSbS[{k,k},{i,j}],
	invSbS[{i_,i_},{k_,j_}]invSbS[{j_,j_},{i_,k_}]invSbS[{k_,k_},{j_,i_}]:>invSbS[{i,i},{j,k}]invSbS[{j,j},{i,k}]invSbS[{k,k},{i,j}],
	invSbS[{i_,i_},{j_,k_}]invSbS[{j_,j_},{k_,i_}]invSbS[{k_,k_},{j_,i_}]:>invSbS[{i,i},{j,k}]invSbS[{j,j},{i,k}]invSbS[{k,k},{i,j}],
	
	invSbS[{i_,i_},{k_,j_}]invSbS[{j_,j_},{k_,i_}]invSbS[{k_,k_},{j_,i_}]:>invSbS[{i,i},{j,k}]invSbS[{j,j},{i,k}]invSbS[{k,k},{i,j}]
};


n3RelationKKb=Union[n3RelationKKb1,n3RelationKKb2];

n3RelationKKbExtended={

	invSS[x__]^m_ invSbSb[y__]:>(invSS[x]invSbSb[y]/.n3RelationKKb)invSS[x]^(m-1),
	invSS[x__] invSbSb[y__]^m_:>(invSS[x]invSbSb[y]/.n3RelationKKb)invSbSb[y]^(m-1),
	invSS[x__]^m_ invSbSb[y__]^n_:>(invSS[x]invSbSb[y]/.n3RelationKKb)^Min[m,n]invSS[x]^(m-Min[m,n]) invSbSb[y]^(n-Min[m,n])

};

n3RelationJKExtednded={

	invSbS[{i_,i_},{x__}]^m_ invSS[{y__},{i_}]:>(invSbS[{i,i},{x}] invSS[{y},{i}]/.n3RelationJK)invSbS[{i,i},{x}]^(m-1),
	invSbS[{i_,i_},{x__}] invSS[{y__},{i_}]^m_:>(invSbS[{i,i},{x}] invSS[{y},{i}]/.n3RelationJK)invSS[{y},{i}]^(m-1),
	invSbS[{i_,i_},{x__}]^m_ invSS[{y__},{i_}]^n_:>(invSbS[{i,i},{x}] invSS[{y},{i}]/.n3RelationJK)^Min[m,n]invSbS[{i,i},{x}]^(m-Min[m,n])invSS[{y},{i}]^(n-Min[m,n])

};

n3RelationJKbExtednded={

	invSbS[{i_,i_},{x__}]^m_ invSbSb[{y__},{i_}]:>(invSbS[{i,i},{x}] invSbSb[{y},{i}]/.n3RelationJKb)invSbS[{i,i},{x}]^(m-1),
	invSbS[{i_,i_},{x__}] invSbSb[{y__},{i_}]^m_:>(invSbS[{i,i},{x}] invSbSb[{y},{i}]/.n3RelationJKb)invSbSb[{y},{i}]^(m-1),
	invSbS[{i_,i_},{x__}]^m_ invSbSb[{y__},{i_}]^n_:>(invSbS[{i,i},{x}] invSbSb[{y},{i}]/.n3RelationJKb)^Min[m,n]invSbS[{i,i},{x}]^(m-Min[m,n])invSbSb[{y},{i}]^(n-Min[m,n])

};

n3RelationJJJExtended=
{

	invSbS[{i_,i_},{x__}]^m_ invSbS[{j_,j_},{y__}]invSbS[{k_,k_},{z__}]:>(invSbS[{i,i},{x}]invSbS[{j,j},{y}]invSbS[{k,k},{z}]/.n3RelationJJJ)invSbS[{i,i},{x}]^(m-1),
	invSbS[{i_,i_},{x__}]invSbS[{j_,j_},{y__}]^m_ invSbS[{k_,k_},{z__}]:>(invSbS[{i,i},{x}]invSbS[{j,j},{y}]invSbS[{k,k},{z}]/.n3RelationJJJ)invSbS[{j,j},{y}]^(m-1),
	invSbS[{i_,i_},{x__}]invSbS[{j_,j_},{y__}]invSbS[{k_,k_},{z__}]^m_:>(invSbS[{i,i},{x}]invSbS[{j,j},{y}]invSbS[{k,k},{z}]/.n3RelationJJJ)invSbS[{k,k},{z}]^(m-1),
	
	invSbS[{i_,i_},{x__}]^m_ invSbS[{j_,j_},{y__}]^n_ invSbS[{k_,k_},{z__}]:>(invSbS[{i,i},{x}]invSbS[{j,j},{y}]invSbS[{k,k},{z}]/.n3RelationJJJ)invSbS[{i,i},{x}]^(m-1)invSbS[{j,j},{y}]^(n-1),
	invSbS[{i_,i_},{x__}]^m_ invSbS[{j_,j_},{y__}]invSbS[{k_,k_},{z__}]^n_:>(invSbS[{i,i},{x}]invSbS[{j,j},{y}]invSbS[{k,k},{z}]/.n3RelationJJJ)invSbS[{i,i},{x}]^(m-1)invSbS[{k,k},{z}]^(n-1),
	invSbS[{i_,i_},{x__}]invSbS[{j_,j_},{y__}]^m_ invSbS[{k_,k_},{z__}]^n_:>(invSbS[{i,i},{x}]invSbS[{j,j},{y}]invSbS[{k,k},{z}]/.n3RelationJJJ)invSbS[{j,j},{y}]^(m-1)invSbS[{k,k},{z}]^(n-1),
	
	invSbS[{i_,i_},{x__}]^m_ invSbS[{j_,j_},{y__}]^n_ invSbS[{k_,k_},{z__}]^p_:>(invSbS[{i,i},{x}]invSbS[{j,j},{y}]invSbS[{k,k},{z}]/.n3RelationJJJ)^Min[m,n,p]invSbS[{i,i},{x}]^(m-Min[m,n,p])invSbS[{j,j},{y}]^(n-Min[m,n,p])invSbS[{k,k},{z}]^(p-Min[m,n,p])

}


(*----------Applying Relations----------*)
n3Relations={
	n3RelationKKb,n3RelationKKbExtended,
	n3RelationJK,n3RelationJKExtednded,
	n3RelationJKb,n3RelationJKbExtednded,
	n3RelationJJJ,n3RelationJJJExtended
}//Flatten;

ReplaceRelations[x_]:=applyEFProperties[Expand[x//.n3Relations]];

applyJacobiRelations[exprM_]:=Module[{expr},
	expr=exprM/.{spM[i_,j_]:>-2sp[i,j]}//Factor;
	FixedPoint[ReplaceRelations[#]/.{spM[i_,j_]:>-2sp[i,j]}&,Expand[expr]]/.{sp[i_,j_]:>-spM[i,j]/2}
];

(*An issue which is fixed a bit in an ugly way.
Applying //applyJacobiRelations to the expression
spM[1,2]^((2-\[CapitalDelta][1]-\[CapitalDelta][2]+\[CapitalDelta][3])/2)
gives an infinite loop due to appereance of factors 2 which do not sum up with the original exponent ((2-\[CapitalDelta][1]-\[CapitalDelta][2]+\[CapitalDelta][3])/2) due to its complicated form.
The remedy is to add //Factor before applying the loop. It splits the exponent ((2-\[CapitalDelta][1]-\[CapitalDelta][2]+\[CapitalDelta][3])/2) into pieces
*)


(* ::Subsection::Closed:: *)
(*Auxiliary Functions*)


(* ::Subsubsection::Closed:: *)
(*normalizeInvariants and denormalizeInvariants*)


(*----------normalizeInvariants factors----------*)
NormalizationFactorSbS[{i_,j_},list_]:=Product[spM[list[[g]],list[[g+1]]],{g,1,Length[list],2}]^-1;

NormalizationFactorSS[{i_,j_},list_]:=
	If[Length[list]==1,
		(*for the simplest K and Kb invariants*)
		Sqrt[spM[i,list[[1]]]spM[list[[1]],j]/spM[i,j]]
		
		,
		
		(*for the rest of K and Kb invariants*)
		Sqrt[
			spM[Extract[list,1],Extract[list,2]]*
			Product[spM[list[[g]],list[[g+1]]],{g,2,Length[list]-1}]*
			spM[Extract[list,-1],Extract[list,1]]
		]
	]^-1;

(*----------"normalizeInvariants" function converts non-normilezed invariants into normalized ones----------*)
normalizeInvariants[expr_]:=expr/.{
	(*normalizeInvariants of the "SbS"-structures*)
	invSbS[{i_,j_},list_]:>NormalizationFactorSbS[{i,j},list]^-1 invSbSnorm[{i,j},list],
	
	(*normalizeInvariants of the "SS"-structures*)
	invSS[{i_,j_},list_]:>NormalizationFactorSS[{i,j},list]^-1 invSSnorm[{i,j},list],
	
	(*normalizeInvariants of the "SbSb"-structures*)
	invSbSb[{i_,j_},list_]:>NormalizationFactorSS[{i,j},list]^-1 invSbSbnorm[{i,j},list]
}//PowerExpand;

(*----------"denormalizeInvariants" function converts normilezed invariants into non-normalized ones----------*)
denormalizeInvariants[expr_]:=expr/.{
	(*normalizeInvariants of the "SbS"-structures*)
	invSbSnorm[{i_,j_},list_]:>NormalizationFactorSbS[{i,j},list]invSbS[{i,j},list],
	
	(*normalizeInvariants of the "SS"-structures*)
	invSSnorm[{i_,j_},list_]:>NormalizationFactorSS[{i,j},list]invSS[{i,j},list],
	
	(*normalizeInvariants of the "SbSb"-structures*)
	invSbSbnorm[{i_,j_},list_]:>NormalizationFactorSS[{i,j},list]invSbSb[{i,j},list]
}//PowerExpand;


(* ::Subsubsection::Closed:: *)
(*Cross-Ratios*)


formCrossRatiosLP[expr_]:=ReplaceAll[ReplaceAll[ReplaceAll[ReplaceAll[expr,spM[1,2]spM[3,4]->U spM[1,3]spM[2,4]],spM[1,4]spM[2,3]->V spM[1,3]spM[2,4]],spM[1,3]spM[2,4]->1/V spM[1,4]spM[2,3]],spM[1,3]spM[2,4]->1/U spM[1,2]spM[3,4]];
formCrossRatiosHP[expr_]:=ReplaceAll[ReplaceAll[ReplaceAll[ReplaceAll[expr,spM[1,2]^p_ spM[3,4]^p_:>U^p spM[1,3]^p spM[2,4]^p],spM[1,4]^p_ spM[2,3]^p_:>V^p spM[1,3]^p spM[2,4]^p],spM[1,3]^p_ spM[2,4]^p_:>(1/V^p) spM[1,4]^p spM[2,3]^p],spM[1,3]^p_ spM[2,4]^p_:>(1/U^p) spM[1,2]^p spM[3,4]^p];
formCrossRatios[expr_]:=expr//formCrossRatiosLP//formCrossRatiosHP;


(* ::Subsubsection::Closed:: *)
(*Permutations*)


permutePoints[m_,n_][expr_]:=Module[
{
	rule
	,perm
},
	
	perm[m]=n;
	perm[n]=m;
	perm[i_]:=i;
	
	rule=
	{
		sp[i_,j_]:>Hold[sp[i,j]/.{m->n,n->m}],
		spM[i_,j_]:>Hold[spM[i,j]/.{m->n,n->m}],
		
		(*non-normalized invariants*)
		invSbS[{i_,j_},list_]:>Hold[invSbS[{i,j},list]/.{m->n,n->m}],
		invSS[{i_,j_},list_]:>Hold[invSS[{i,j},list]/.{m->n,n->m}],
		invSbSb[{i_,j_},list_]:>Hold[invSbSb[{i,j},list]/.{m->n,n->m}],
		
		(*normalized invariants*)
		invSbSnorm[{i_,j_},list_]:>Hold[invSbSnorm[{i,j},list]/.{m->n,n->m}],
		invSSnorm[{i_,j_},list_]:>Hold[invSSnorm[{i,j},list]/.{m->n,n->m}],
		invSbSbnorm[{i_,j_},list_]:>Hold[invSbSbnorm[{i,j},list]/.{m->n,n->m}]
	};
	
	ReleaseHold[expr/.rule]//cfApplyPermutation[perm]
];


permutePoints[perm_][expr_]:=Module[
{
	rule
},
	
	rule=
	{
		sp[i_,j_]:>Hold[sp[perm[i],perm[j]]],
		spM[i_,j_]:>Hold[spM[perm[i],perm[j]]],
		
		(*non-normalized invariants*)
		invSbS[{i_,j_},list_]:>Hold[invSbS[{perm[i],perm[j]},perm/@list]],
		invSS[{i_,j_},list_]:>Hold[invSS[{perm[i],perm[j]},perm/@list]],
		invSbSb[{i_,j_},list_]:>Hold[invSbSb[{perm[i],perm[j]},perm/@list]],
		
		(*normalized invariants*)
		invSbSnorm[{i_,j_},list_]:>Hold[invSbSnorm[{perm[i],perm[j]},perm/@list]],
		invSSnorm[{i_,j_},list_]:>Hold[invSSnorm[{perm[i],perm[j]},perm/@list]],
		invSbSbnorm[{i_,j_},list_]:>Hold[invSbSbnorm[{perm[i],perm[j]},perm/@list]]
	};
	
	ReleaseHold[expr/.rule]//cfApplyPermutation[perm]
];


applyConjugation[expr_]:=expr/.{
	(* non-normalized : *)
	invSbS[{i_,j_},list_]:>(-1)^(1+Length@list) invSbS[{j,i},Reverse@list],
	invSS[{i_,j_},list_]:>-invSbSb[{i,j},list],
	invSbSb[{i_,j_},list_]:>-invSS[{i,j},list],
	(* normalized : *)
	invSbSnorm[{i_,j_},list_]:>(-1)^(1+Length@list) invSbSnorm[{j,i},Reverse@list],
	invSSnorm[{i_,j_},list_]:>-invSbSbnorm[{i,j},list],
	invSbSbnorm[{i_,j_},list_]:>-invSSnorm[{i,j},list],
	(* expanded CF *)
	\[Xi][i_]:>\[Xi]b[i],\[Xi]b[i_]:>\[Xi][i],
	\[Eta][i_]:>\[Eta]b[i],\[Eta]b[i_]:>\[Eta][i],
	(* collapsed CF *)
	CF4pt[ds_,ls_,lbs_,qs_,qbs_,g_]:>CF4pt[ds,lbs,ls,qbs,qs,g]
};


applyPParity[expr_]:=expr/.{
	(* non-normalized : *)
	invSbS[{i_,j_},list_]:>(-1)^(1+Length@list) invSbS[{j,i},Reverse@list],
	invSS[{i_,j_},list_]:>invSbSb[{i,j},list],
	invSbSb[{i_,j_},list_]:>invSS[{i,j},list],
	(* normalized : *)
	invSbSnorm[{i_,j_},list_]:>(-1)^(1+Length@list) invSbSnorm[{j,i},Reverse@list],
	invSSnorm[{i_,j_},list_]:>invSbSbnorm[{i,j},list],
	invSbSbnorm[{i_,j_},list_]:>invSSnorm[{i,j},list],
	(* expanded CF *)
	\[Xi][i_]:>-I*\[Xi]b[i],\[Xi]b[i_]:>I*\[Xi][i],
	\[Eta][i_]:>-I*\[Eta]b[i],\[Eta]b[i_]:>I*\[Eta][i],
	(* collapsed CF *)
	CF4pt[ds_,ls_,lbs_,qs_,qbs_,g_]:>I^(-Total[ls-lbs])CF4pt[ds,lbs,ls,qbs,qs,g]
};


applyTParity[expr_]:=expr/.{
	(* non-normalized : *)
	invSbS[{i_,j_},list_]:>invSbS[{i,j},list],
	invSS[{i_,j_},list_]:>-invSS[{i,j},list],
	invSbSb[{i_,j_},list_]:>-invSbSb[{i,j},list],
	(* normalized : *)
	invSbSnorm[{i_,j_},list_]:>invSbSnorm[{i,j},list],
	invSSnorm[{i_,j_},list_]:>-invSSnorm[{i,j},list],
	invSbSbnorm[{i_,j_},list_]:>-invSbSbnorm[{i,j},list],
	(* expanded CF *)
	\[Xi][i_]:>I*\[Xi][i],\[Xi]b[i_]:>-I*\[Xi]b[i],
	\[Eta][i_]:>I*\[Eta][i],\[Eta]b[i_]:>-I*\[Eta]b[i],
	(* collapsed CF *)
	CF4pt[ds_,ls_,lbs_,qs_,qbs_,g_]:>I^{Total[ls-lbs]}CF4pt[ds,ls,lbs,qs,qbs,g]
};


(* ::Subsection::Closed:: *)
(*Elementary Differential Operators (experimental)*)


(*Operators Probing Parameters*)
measureDegree[i_][expr_]:=Module[{a,b},
	-(1/4) Xb[{i}][a,b]DXSigma[expr][i,b,a]
];

measurel[i_][expr_]:=Module[{a},
	S[i][a]DS[expr][i,a]
];

measurelb[i_][expr_]:=Module[{a},
	Sb[i][a]DSb[expr][i,a]
];


(*Operator Coefficients*)
aCoef[i_][expr_]:= expr- measureDegree[i][expr]+measurel[i][expr];
aCoefb[i_][expr_]:= expr- measureDegree[i][expr]+measurelb[i][expr];

bCoef[i_][expr_]:=2(expr+measurelb[i][expr]);
bCoefb[i_][expr_]:=2(expr+measurel[i][expr]);

cCoef[i_][expr_]:=-2 expr+measureDegree[i][expr]-( measurel[i][expr]+ measurelb[i][expr]);


(*Rising and lowering operators in fundamental representation*)
R[i_,a_][expr_]:=expr S[i][a];

Rp[i_,a_][expr_]:=Module[{b,c}, 
	Sb[i][b]DXSigma[expr//aCoef[i]][i,a,b]+ S[i][a]Sb[i][b]DS[DXSigma[expr][i,b,c]][i,c]//contractIndices//applyEFProperties
];

L[i_,a_][expr_]:=Module[{b},
	X[{i}][a,b]DS[expr][i,b]//contractIndices//applyEFProperties
];

Lp[i_,a_][expr_]:=Module[{b,c,d,
term1,term2,term3,term4
},
	term1=DSb[expr//bCoef[i]//cCoef[i]][i,a];
	term2=S[i][a]DSb[DS[expr//bCoef[i]][i,b]][i,b];
	term3=Xb[{i}][b,c]DSb[DXSigma[expr//cCoef[i]][i,a,b]][i,c];
	term4=-S[i][a]Xb[{i}][b,c]DS[DSb[DXSigma[expr][i,b,d]][i,c]][i,d];
	
	Return[term1+term2+term3+term4//contractIndices//applyEFProperties]
];


(*Rising and lowering operators in anti-fundamental representation*)
Rpb[i_,a_][expr_]:=expr Sb[i][a];

Rb[i_,a_][expr_]:=Module[{b,c}, 
	S[i][b]DXSigmab[expr//aCoefb[i]][i,a,b]+ Sb[i][a]S[i][b]DSb[DXSigmab[expr][i,b,c]][i,c]//contractIndices//applyEFProperties
];

Lpb[i_,a_][expr_]:=Module[{b},
	Xb[{i}][a,b]DSb[expr][i,b]//contractIndices//applyEFProperties
];

Lb[i_,a_][expr_]:=Module[
{
	b,c,d,
	term1,term2,term3,term4
},
	term1=DS[expr//bCoefb[i]//cCoef[i]][i,a];
	term2=Sb[i][a]DS[DSb[expr//bCoefb[i]][i,b]][i,b];
	term3=X[{i}][b,c]DS[DXSigmab[expr//cCoef[i]][i,a,b]][i,c];
	term4=-Sb[i][a]X[{i}][b,c]DSb[DS[DXSigmab[expr][i,b,d]][i,c]][i,d];
	
	Return[term1+term2+term3+term4//contractIndices//applyEFProperties]
];


(*official notation*)
spinorD[-(1/2), 0, +1][i_, a_][expr_] := Rpb[i, a][expr];
spinorD[+(1/2), +1, 0][i_, a_][expr_] := Rb[i, a][expr];
spinorD[-(1/2), 0, -1][i_, a_][expr_] := Lpb[i, a][expr];
spinorD[+(1/2), -1, 0][i_, a_][expr_] := Lb[i, a][expr];

spinorDb[-(1/2), +1, 0][i_, a_][expr_] := R[i, a][expr];
spinorDb[+(1/2), 0, +1][i_, a_][expr_] := Rp[i, a][expr];
spinorDb[-(1/2), -1, 0][i_, a_][expr_] := L[i, a][expr];
spinorDb[+(1/2), 0, -1][i_, a_][expr_] := Lp[i, a][expr];










(*Auxiliary function: remove*)


(* ::Section::Closed:: *)
(*4. Relation Between EF and CF*)


(* ::Subsection::Closed:: *)
(*EF -> CF*)


(* ::Subsubsection::Closed:: *)
(*Defining the Conformal Frame*)


(*---We use the notation of the Appendix A of Wess and Bagger and of the paper ArXiv:1412.1796---*)


(*----------Defining matrices in 4D----------*)
spaceDimension=4;
(*---4D metric: {-,+,+,+}. It is the same for upper and lower indices.---*)
metric4D={
	{-1,0,0,0},
	{0,1,0,0},
	{0,0,1,0},
	{0,0,0,1}
};
epsilonU={{0,1},{-1,0}};(*convention for the 4D epsilon symbol with upper-indices*)
epsilonD={{0,-1},{1,0}};(*convention for the 4D epsilon symbol with lower-indices*)

(*---In the definition of the sigma-matrices we use the upper-coordinate index (mu-index)---*)
sigma={-{{1,0},{0,1}},PauliMatrix[1],PauliMatrix[2],PauliMatrix[3]}; (*convention for the sigma-matrix*)
sigmab=Table[-epsilonU.Transpose[sigma[[i]]].epsilonU,{i,1,4}]; (*convention for the sigmab-matrix*)


lowerIndex[y_[\[Mu]_]]/;NumberQ[\[Mu]]:=metric4D[[\[Mu]]].(y/@Range[1,spaceDimension]);


lorentzSquare[x_]:=#.metric4D.#&@(x/@Range[1,spaceDimension]);


(*----------Defining matrices in 6D----------*)
(*---In the definition of the Sigma-matrices we use the upper-coordinate index (M-index)---*)
matrixZero={{0,0},{0,0}}; (*creates a 2x2 zero matrix*)

Sigma={
	{{matrixZero,-sigma[[1]].epsilonU},{sigmab[[1]].epsilonD,matrixZero}}//ArrayFlatten,
	{{matrixZero,-sigma[[2]].epsilonU},{sigmab[[2]].epsilonD,matrixZero}}//ArrayFlatten,
	{{matrixZero,-sigma[[3]].epsilonU},{sigmab[[3]].epsilonD,matrixZero}}//ArrayFlatten,
	{{matrixZero,-sigma[[4]].epsilonU},{sigmab[[4]].epsilonD,matrixZero}}//ArrayFlatten,
	{{matrixZero,matrixZero},{matrixZero,2epsilonU}}//ArrayFlatten,
	{{-2epsilonD,matrixZero},{matrixZero,matrixZero}}//ArrayFlatten
};(*convention for the Sigma-matrix*)

Sigmab={
	{{matrixZero,-epsilonU.sigma[[1]]},{epsilonD.sigmab[[1]],matrixZero}}//ArrayFlatten,
	{{matrixZero,-epsilonU.sigma[[2]]},{epsilonD.sigmab[[2]],matrixZero}}//ArrayFlatten,
	{{matrixZero,-epsilonU.sigma[[3]]},{epsilonD.sigmab[[3]],matrixZero}}//ArrayFlatten,
	{{matrixZero,-epsilonU.sigma[[4]]},{epsilonD.sigmab[[4]],matrixZero}}//ArrayFlatten,
	{{-2epsilonU,matrixZero},{matrixZero,matrixZero}}//ArrayFlatten,
	{{matrixZero,matrixZero},{matrixZero,2epsilonD}}//ArrayFlatten
};(*convention for the Sigmab-matrix*)

(*---Defining the 6D metric. It depends on the 4D to 6D embedding, which is chosen to be: x^\[Mu] \[Rule] X^M={x^\[Mu],X^5,X^6} \[Rule] X^M={x^\[Mu],X^+,X^-} with X^+=(X^5+X^6)/2 and X^-=(X^5-X^6)/2 ---*)
metricU={
	{-1,0,0,0,0,0},
	{0,1,0,0,0,0},
	{0,0,1,0,0,0},
	{0,0,0,1,0,0},
	{0,0,0,0,0,2},
	{0,0,0,0,2,0}
};

metricD={
	{-1,0,0,0,0,0},
	{0,1,0,0,0,0},
	{0,0,1,0,0,0},
	{0,0,0,1,0,0},
	{0,0,0,0,0,1/2},
	{0,0,0,0,1/2,0}
};


(*----------Defining the Conformal Frame Configuration of Points in 4D----------*)
x[1]={0,0,0,0};
x[2]={-((z-zb)/2),0,0,(z+zb)/2}; (* Should go to x[2]={(z-zb)/(2i),0,0,(z+zb)/2} in Euclidean under tE=i*tL *)
x[3]={0,0,0,1};
x[4]={0,0,0,L};InfinitePoint=4;
x[5]={x1[5],x2[5],0,x4[5]};
x[n_/;n>=6]:={x1[n],x2[n],x3[n],x4[n]};

xpre[1]={0,0,0,0};
xpre[2]={y[2][1],0,0,y[2][4]};zPoint=2;
xpre[3]={0,0,0,1};
xpre[4]={0,0,0,L};


(*----------6D objects in terms of 4D objects---------*)

(*6D vectors: X^M. The embedding rule is X+=1 and X-=-x^2*)
XvectorPre0[n_]:={(y[n][#]&/@Range[1,4]),{1,y[n][1]^2-y[n][2]^2-y[n][3]^2-y[n][4]^2}}//Flatten;

XvectorPre[n_]:={x[n],{1,x[n][[1]]^2-x[n][[2]]^2-x[n][[3]]^2-x[n][[4]]^2}}//Flatten;
Xvector[n_]:=XvectorPre[n];
Xvector[n_/;n==InfinitePoint]:=Limit[1/L^2 XvectorPre[n],L->\[Infinity]];
(*6D matrices: X*)
Xmatrix[n_]:=metricD.Xvector[n].Sigma;
XmatrixPre[n_]:=metricD.XvectorPre[n].Sigma;

(*6D matrices: Xb*)
Xbmatrix[n_]:=metricD.Xvector[n].Sigmab;
XbmatrixPre[n_]:=metricD.XvectorPre[n].Sigmab;

(*---Defining the auxiliary twistors S={s,tb}, where s={\[Xi],\[Eta]}---*)

SvectorPre0[i_]:=Module[{temp},

	temp=
	{
		{\[Xi][i],\[Eta][i]},
		-((metric4D.(y[i][#]&/@Range[1,4])).sigmab).{\[Xi][i],\[Eta][i]}
	}//Flatten;
	temp=Simplify[temp];
	
	Return[temp]
];

SvectorPre[i_]:=Module[{temp},

	temp=
	{
		{\[Xi][i],\[Eta][i]},
		-((metric4D.x[i]).sigmab).{\[Xi][i],\[Eta][i]}
	}//Flatten;
	temp=Simplify[temp];
	
	Return[temp]
];



(*---Defining the auxiliary twistors Sb={t,sb}, where Overscript[s, _]b={\[Xi]b,\[Eta]b}---*)
SbvectorPre[i_]:=Module[{temp},

	temp={
		{\[Xi]b[i],\[Eta]b[i]}.((metric4D.x[i]).sigmab),
		{\[Xi]b[i],\[Eta]b[i]}
	}//Flatten;
	temp=Simplify[temp];
	
	Return[temp]
];

SbvectorPre0[i_]:=Module[{temp},

	temp={
		{\[Xi]b[i],\[Eta]b[i]}.((metric4D.(y[i][#]&/@Range[1,4])).sigmab),
		{\[Xi]b[i],\[Eta]b[i]}
	}//Flatten;
	temp=Simplify[temp];
	
	Return[temp]
];


Svector[i_]:=If[i==InfinitePoint,Limit[1/L SvectorPre[i],L->\[Infinity]],SvectorPre[i]];
Sbvector[i_]:=If[i==InfinitePoint,Limit[1/L SbvectorPre[i],L->\[Infinity]],SbvectorPre[i]];


(* ::Subsubsection::Closed:: *)
(*Converting to the Conformal Frame*)


(*----------Auxiliary Functions----------*)
ListToXList[ex_]:=Module[
{i},
	Table[
		If[OddQ[i]==True,Xmatrix[ex[[i]]],Xbmatrix[ex[[i]]]]
	,{i,1,Length[ex]}
	]
];

ListToXbList[ex_]:=Module[
{i},
	Table[
		If[OddQ[i]==True,Xbmatrix[ex[[i]]],Xmatrix[ex[[i]]]]
	,{i,1,Length[ex]}
	]
];

(*----------Rules----------*)
(*Scalar Product*)
ruleSPM={spM[i_,j_]:>-2metricD.Xvector[i].Xvector[j]//Simplify};

(*invariants 1*)
ruleI={invSbS[{i_,j_},list_]:>FullSimplify[Sbvector[i].(Dot@@ListToXList[list]).Svector[j]]};
ruleInorm={invSbSnorm[{i_,j_},list_]:>FullSimplify[(NormalizationFactorSbS[{i,j},list]/.ruleSPM)Sbvector[i].(Dot@@ListToXList[list]).Svector[j]]};

(*invariants 2*)
ruleII={invSS[{i_,j_},list_]:>FullSimplify[Svector[i].(Dot@@ListToXbList[list]).Svector[j]]};
ruleIIb={invSbSb[{i_,j_},list_]:>FullSimplify[Sbvector[i].(Dot@@ListToXList[list]).Sbvector[j]]};
ruleIInorm={invSSnorm[{i_,j_},list_]:>FullSimplify[(NormalizationFactorSS[{i,j},list]/.ruleSPM)Svector[i].(Dot@@ListToXbList[list]).Svector[j]]};
ruleIIbnorm={invSbSbnorm[{i_,j_},list_]:>FullSimplify[(NormalizationFactorSS[{i,j},list]/.ruleSPM)Sbvector[i].(Dot@@ListToXList[list]).Sbvector[j]]};

(*invariants 3*)
ruleIII=M[{i_,j_,k_,l_}]:>Simplify[Sum[LeviCivitaTensor[4][[a,b,c,d]]Svector[i][[a]]Svector[j][[b]]Svector[k][[c]]Svector[l][[d]],{a,1,4},{b,1,4},{c,1,4},{d,1,4}]];
ruleIIIb=Mb[{i_,j_,k_,l_}]:>FullSimplify[Sum[LeviCivitaTensor[4][[a,b,c,d]]Sbvector[i][[a]]Sbvector[j][[b]]Sbvector[k][[c]]Sbvector[l][[d]],{a,1,4},{b,1,4},{c,1,4},{d,1,4}]];

(*----------toConformalFrame function----------*)
rule={ruleSPM,ruleI,ruleInorm,ruleII,ruleIIb,ruleIInorm,ruleIIbnorm,ruleIII,ruleIIIb}//Flatten;
ruleINV={ruleI,ruleII,ruleIIb,ruleIII,ruleIIIb}//Flatten;
toConformalFrame[ex_]:=ex/.rule
InvariantsToConformalFrame[ex_]:=ex/.ruleINV


(* ::Subsection::Closed:: *)
(*EF <- CF*)


(* ::Subsubsection::Closed:: *)
(*toEmbeddingFormalism*)


(*We form pairs of the type A*)
listPairsABasic={\[Xi][i_] \[Xi]b[j_]:>P\[Xi]\[Xi]b[i,j],\[Eta][i_] \[Eta]b[j_]:>P\[Eta]\[Eta]b[i,j]};

listPairsAExtended={
	\[Xi][i_]^m_ \[Xi]b[j_]:>(\[Xi][i]\[Xi]b[j]/.listPairsABasic)\[Xi][i]^(m-1),
	\[Xi][i_]\[Xi]b[j_]^m_:>(\[Xi][i]\[Xi]b[j]/.listPairsABasic)\[Xi]b[j]^(m-1),
	\[Xi][i_]^m_ \[Xi]b[j_]^n_:>(\[Xi][i]\[Xi]b[j]/.listPairsABasic)^Min[m,n]\[Xi][i]^(m-Min[m,n])\[Xi]b[j]^(n-Min[m,n]),
	
	\[Eta][i_]^m_ \[Eta]b[j_]:>(\[Eta][i]\[Eta]b[j]/.listPairsABasic)\[Eta][i]^(m-1),
	\[Eta][i_]\[Eta]b[j_]^m_:>(\[Eta][i]\[Eta]b[j]/.listPairsABasic)\[Eta]b[j]^(m-1),
	\[Eta][i_]^m_ \[Eta]b[j_]^n_:>(\[Eta][i]\[Eta]b[j]/.listPairsABasic)^Min[m,n]\[Eta][i]^(m-Min[m,n])\[Eta]b[j]^(n-Min[m,n])
};

listPairsA=Union[listPairsABasic,listPairsAExtended];

(*We form pairs of the type B*)
listPairsBBasic={\[Xi][i_] \[Eta][j_]:>P\[Xi]\[Eta][i,j],\[Xi]b[i_] \[Eta]b[j_]:>P\[Xi]b\[Eta]b[i,j]};

listPairsBExtended={
	\[Xi][i_]^m_ \[Eta][j_]:>(\[Xi][i]\[Eta][j]/.listPairsBBasic)\[Xi][i]^(m-1),
	\[Xi][i_]\[Eta][j_]^m_:>(\[Xi][i]\[Eta][j]/.listPairsBBasic)\[Eta][j]^(m-1),
	\[Xi][i_]^m_ \[Eta][j_]^n_:>(\[Xi][i]\[Eta][j]/.listPairsBBasic)^Min[m,n]\[Xi][i]^(m-Min[m,n])\[Eta][j]^(n-Min[m,n]),
	
	\[Xi]b[i_]^m_ \[Eta]b[j_]:>(\[Xi]b[i]\[Eta]b[j]/.listPairsBBasic)\[Xi]b[i]^(m-1),
	\[Xi]b[i_]\[Eta]b[j_]^m_:>(\[Xi]b[i]\[Eta]b[j]/.listPairsBBasic)\[Eta]b[j]^(m-1),
	\[Xi]b[i_]^m_ \[Eta]b[j_]^n_:>(\[Xi]b[i]\[Eta]b[j]/.listPairsBBasic)^Min[m,n]\[Xi]b[i]^(m-Min[m,n])\[Eta]b[j]^(n-Min[m,n])
};

listPairsB=Union[listPairsBBasic,listPairsBExtended];

(*Replace products with pairs*)
ToFormPairs[expr_]:=(expr//.listPairsA)//.listPairsB;

(*<<"ConnectionCFEF";*)
(*<<"ConnectionCFEFnorm";*)
ConnectionCFEFnorm:=(safeImportPrivateSymbolNoSet["ConnectionCFEFnorm","The function toEmbeddingFormalism will be unavailable."](*;ConnectionCFEFnorm*));

toEmbeddingFormalism[expr_]:=ToFormPairs[expr]/.ConnectionCFEFnorm;


(* ::Subsubsection::Closed:: *)
(*Forming the Basis1*)


(*Auxiliary Functions*)

(*I-invariants*)
myComplementI[i_,j_]:=Module[
{
	list={1,2,3,4},
	temp
},
	temp=Complement[list,{i,j}];
	If[i<j,
		Complement[list,{i,j}],
		Complement[list,{i,j}]//Reverse
	]
];

(*J-invariants*)
myComplementJ[i_][flag_]:=
	If[flag==True,
		Delete[Complement[{1,2,3,4},{i}],2]
		,
		Delete[Complement[{1,2,3,4},{i}],3]
	];

(*K-invariants*)
myComplementK[i_,j_]:=
If[i>j,
	Delete[Complement[{1,2,3,4},{i,j}],1]
	,
	Delete[Complement[{1,2,3,4},{i,j}],2]
];

(*L-invariants*)
myComplementL[i_]:=Complement[{1,2,3,4},{i}];


(*The Function*)
n4ListStructuresEF[list_]:=Module[
{out,expr},

	expr=n4ListStructuresAuxiliary[list];
	
	(*Reconstructing J-invariants*)
	out=expr//.{
		myPower[\[Xi][i_],q_] myPower[\[Xi]b[i_],qb_]:>invSbSnorm[{i,i},myComplementJ[i][True]]^Min[q,qb]myPower[\[Xi][i],q-Min[q,qb]] myPower[\[Xi]b[i],qb-Min[q,qb]],
		myPower[\[Eta][i_],q_] myPower[\[Eta]b[i_],qb_]:>invSbSnorm[{i,i},myComplementJ[i][False]]^Min[q,qb]myPower[\[Eta][i],q-Min[q,qb]] myPower[\[Eta]b[i],qb-Min[q,qb]]
	};
	
	(*Reconstructing I-invariants*)
	out=out//.{
		myPower[\[Xi]b[i_],qb_] myPower[\[Xi][j_],q_]:>invSbSnorm[{i,j},{}]^Min[qb,q]myPower[\[Xi]b[i],qb-Min[qb,q]] myPower[\[Xi][j],q-Min[qb,q]],
		myPower[\[Eta]b[i_],qb_] myPower[\[Eta][j_],q_]:>invSbSnorm[{i,j},myComplementI[i,j]]^Min[qb,q]myPower[\[Eta]b[i],qb-Min[qb,q]] myPower[\[Eta][j],q-Min[qb,q]]
	};
	
	(*Reconstructing L- and Lb- invariants*)
	out=out//.{
		myPower[\[Xi][i_],q\[Xi]_] myPower[\[Eta][i_],q\[Eta]_]:>invSSnorm[{i,i},myComplementL[i]]^Min[q\[Xi],q\[Eta]]myPower[\[Xi][i],q\[Xi]-Min[q\[Xi],q\[Eta]]] myPower[\[Eta][i],q\[Eta]-Min[q\[Xi],q\[Eta]]],
		myPower[\[Xi]b[i_],q\[Xi]_] myPower[\[Eta]b[i_],q\[Eta]_]:>invSbSbnorm[{i,i},myComplementL[i]]^Min[q\[Xi],q\[Eta]]myPower[\[Xi]b[i],q\[Xi]-Min[q\[Xi],q\[Eta]]] myPower[\[Eta]b[i],q\[Eta]-Min[q\[Xi],q\[Eta]]]
	};
	
	(*Reconstructing K- and Kb- invariants*)
	out=out//.{
		myPower[\[Xi][i_],q\[Xi]_] myPower[\[Eta][j_],q\[Eta]_]:>invSSnorm[Sort[{i,j}],myComplementK[i,j]]^Min[q\[Xi],q\[Eta]]myPower[\[Xi][i],q\[Xi]-Min[q\[Xi],q\[Eta]]] myPower[\[Eta][j],q\[Eta]-Min[q\[Xi],q\[Eta]]],
		myPower[\[Xi]b[i_],q\[Xi]_] myPower[\[Eta]b[j_],q\[Eta]_]:>invSbSbnorm[Sort[{i,j}],myComplementK[i,j]]^Min[q\[Xi],q\[Eta]]myPower[\[Xi]b[i],q\[Xi]-Min[q\[Xi],q\[Eta]]] myPower[\[Eta]b[j],q\[Eta]-Min[q\[Xi],q\[Eta]]]
	};
	
	Return[out]

];

logTime["Start Conformal Frame"];


(* ::Section::Closed:: *)
(*5. Conformal Frame*)


(* ::Subsection::Closed:: *)
(*CF structures*)


(* ::Subsubsection::Closed:: *)
(*CF abstract structure *)


(*ClearAll[CF4pt];*)
CF4pt/: CF4pt[ds_,ls_,lbs_,qs_,qbs_,g1_]+CF4pt[ds_,ls_,lbs_,qs_,qbs_,g2_]:=CF4pt[ds,ls,lbs,qs,qbs,g1+g2];
CF4pt/: a_*CF4pt[ds_,ls_,lbs_,qs_,qbs_,g_]:=CF4pt[ds,ls,lbs,qs,qbs,a g]
CF4pt[ds_,ls_,lbs_,qs_,qbs_,0]=0;


expandCFStructs[expr_]:=expr/.{CF4pt[\[CapitalDelta]s_,ls_,lbs_,qs_,qbs_,g_]:>g Module[{i},Product[\[Xi][i]^(-qs[[i]]+ls[[i]]/2) \[Eta][i]^(qs[[i]]+ls[[i]]/2) \[Xi]b[i]^(-qbs[[i]]+lbs[[i]]/2) \[Eta]b[i]^(qbs[[i]]+lbs[[i]]/2),{i,1,4}]]};

collapseCFStructs[_][0]:=0;
collapseCFStructs[\[CapitalDelta]s_][expr_]:=Module[
{
	tmp=Expand[expr,\[Xi][_]|\[Xi]b[_]|\[Eta][_]|\[Eta]b[_]]
	,\[Xi]s,\[Xi]bs
	,\[Eta]s,\[Eta]bs
	,qs,qbs
	,ls,lbs
	,i
},
	If[tmp===0,Return[0]];
	tmp=If[Head[tmp]=!=Plus,{tmp},List@@tmp];
	\[Xi]s=Expand@Table[Exponent[#,\[Xi][i]],{i,1,4}]&/@tmp;
	\[Xi]bs=Expand@Table[Exponent[#,\[Xi]b[i]],{i,1,4}]&/@tmp;
	\[Eta]s=Expand@Table[Exponent[#,\[Eta][i]],{i,1,4}]&/@tmp;
	\[Eta]bs=Expand@Table[Exponent[#,\[Eta]b[i]],{i,1,4}]&/@tmp;
	tmp=tmp/.{\[Xi][_]->1,\[Xi]b[_]->1,\[Eta][_]->1,\[Eta]b[_]->1};
	
	qs=Expand[(\[Eta]s-\[Xi]s)/2];
	qbs=Expand[(\[Eta]bs-\[Xi]bs)/2];
	ls=Expand[\[Eta]s+\[Xi]s];
	lbs=Expand[\[Eta]bs+\[Xi]bs];
	
	tmp=Plus@@(Table[CF4pt[\[CapitalDelta]s,ls[[i]],lbs[[i]],qs[[i]],qbs[[i]],tmp[[i]]],{i,1,Length@qs}]);
	Collect[tmp,CF4pt[___],Simplify]
]


genericCFstruct=CF4pt[{\[CapitalDelta][1],\[CapitalDelta][2],\[CapitalDelta][3],\[CapitalDelta][4]},{l[1],l[2],l[3],l[4]},{lb[1],lb[2],lb[3],lb[4]},{q[1],q[2],q[3],q[4]},{qb[1],qb[2],qb[3],qb[4]},g[z,zb]]


collectInCF4pt[param___][expr_]:=expr/.{CF4pt[\[CapitalDelta]s_,ls_,lbs_,qs_,qbs_,g_]:>CF4pt[\[CapitalDelta]s,ls,lbs,qs,qbs,Collect[g,param]]};
simplifyInCF4pt[expr_]:=expr/.{CF4pt[\[CapitalDelta]s_,ls_,lbs_,qs_,qbs_,g_]:>CF4pt[\[CapitalDelta]s,ls,lbs,qs,qbs,Simplify@g]};


HoldPattern[collectGInCF4pt[fn_][param___][expr_]]:=expr/.{CF4pt[\[CapitalDelta]s_,ls_,lbs_,qs_,qbs_,g1_]:>CF4pt[\[CapitalDelta]s,ls,lbs,qs,qbs,Collect[g1,fn[___]|Derivative[___][fn][___],param]]};


mapInCF4pt[f_][expr_]:=expr/.{CF4pt[\[CapitalDelta]s_,ls_,lbs_,qs_,qbs_,g_]:>CF4pt[\[CapitalDelta]s,ls,lbs,qs,qbs,f@g]};


(* ::Subsection::Closed:: *)
(*Derivatives*)


(* ::Subsubsection::Closed:: *)
(*Spin representation generators*)


sigmaRotation[\[Mu]_,\[Nu]_]:=1/4 (sigma[[\[Mu]]].sigmab[[\[Nu]]]-sigma[[\[Nu]]].sigmab[[\[Mu]]]);
sigmaRotationb[\[Mu]_,\[Nu]_]:=1/4 (sigmab[[\[Mu]]].sigma[[\[Nu]]]-sigmab[[\[Nu]]].sigma[[\[Mu]]]);

vectorRotation[\[Lambda]_,\[Sigma]_]:=Table[metric4D[[\[Mu],\[Sigma]]]KroneckerDelta[\[Lambda],\[Nu]]-metric4D[[\[Mu],\[Lambda]]]KroneckerDelta[\[Sigma],\[Nu]],{\[Mu],1,4},{\[Nu],1,4}];

RandomRotation[]:=#-Transpose[#]&@Table[RandomReal[{-1,1},WorkingPrecision->30],{4},{4}]//Chop;

vectorFiniteRotation[omega_]:=MatrixExp[1/2 Sum[omega[[\[Mu],\[Nu]]]vectorRotation[\[Mu],\[Nu]],{\[Mu],1,4},{\[Nu],1,4}]];

sigmaFiniteRotation[omega_]:=MatrixExp[1/2 Sum[omega[[\[Mu],\[Nu]]]sigmaRotation[\[Mu],\[Nu]],{\[Mu],1,4},{\[Nu],1,4}]];

sigmaFiniteRotationb[omega_]:=MatrixExp[1/2 Sum[omega[[\[Mu],\[Nu]]]sigmaRotationb[\[Mu],\[Nu]],{\[Mu],1,4},{\[Nu],1,4}]];


(*
This was supposed to check some consistensy of rotation matrices
*)

(*Block[{R,om=RandomRotation[],s,sb,V,Vb,Vn,Vnb},
s=Table[RandomReal[{-1,1},WorkingPrecision\[Rule]30],{2}];
sb=Table[RandomReal[{-1,1},WorkingPrecision\[Rule]30],{2}];
V=(epsilonU.s).#.sb&/@sigma;
Vb=(epsilonD.sb).#.s&/@sigmab;

R=vectorFiniteRotation[om];

s=sigmaFiniteRotation[om].s;
sb=sigmaFiniteRotationb[om].sb;
Vn=(epsilonU.s).#.sb&/@sigma;
Vnb=(epsilonD.sb).#.s&/@sigmab;
Print[R.V-Vn];
Print[R.Vb-Vnb];
]*)


(* ::Subsubsection::Closed:: *)
(*4D derivatives in conformal frame*)


delta=KroneckerDelta;


(* General dimension generators [notation of 1509.07475] *)

generatorD[][i_][a_]:=Sum[y[i][\[Mu]]D[a,y[i][\[Mu]]],{\[Mu],1,spaceDimension}]+\[CapitalDelta][i]*a;

generatorP[\[Mu]_][i_][a_]:=D[a,y[i][\[Mu]]];

generatorK[\[Mu]_][i_][a_]:=
	Sum[
	   (2lowerIndex[y[i][\[Mu]]]y[i][\[Sigma]]-lorentzSquare[y[i]]delta[\[Mu],\[Sigma]])D[a,y[i][\[Sigma]]]
	,{\[Sigma],1,spaceDimension}]+
	2\[CapitalDelta][i]*lowerIndex[y[i][\[Mu]]]*a-
	Sum[
		2y[i][\[Sigma]]generatorSpin[\[Mu],\[Sigma]][i][a]
	,{\[Sigma],1,spaceDimension}];

generatorM[\[Mu]_,\[Nu]_][i_][a_]:=
	lowerIndex[y[i][\[Nu]]]D[a,y[i][\[Mu]]]-
	lowerIndex[y[i][\[Mu]]]D[a,y[i][\[Nu]]]+
	generatorSpin[\[Mu],\[Nu]][i][a];
	
(* 4d specific *)

(*

h[\[Mu]_,\[Nu]_]:=metric4D[[\[Mu],\[Nu]]];
commutationLHS[M_][\[Mu]_,\[Nu]_,\[Rho]_,\[Sigma]_]:=M[\[Mu],\[Nu]].M[\[Rho],\[Sigma]]-M[\[Rho],\[Sigma]].M[\[Mu],\[Nu]];
commutationRHS[M_][\[Mu]_,\[Nu]_,\[Rho]_,\[Sigma]_]:=
	h[\[Nu],\[Rho]]M[\[Mu],\[Sigma]]-h[\[Mu],\[Rho]]M[\[Nu],\[Sigma]]-h[\[Nu],\[Sigma]]M[\[Mu],\[Rho]]+h[\[Mu],\[Sigma]]M[\[Nu],\[Rho]];
	
(* This gives True, thus sigmaRotation(b) is appropriate matrix for generatorSpin *)
(* since generatorSpin should have commutation relation the opposite from M *)
commutationLHS[sigmaRotation][2,3,1,3]+commutationRHS[sigmaRotation][2,3,1,3]\[Equal]{{0,0},{0,0}}
commutationLHS[sigmaRotationb][2,3,1,3]+commutationRHS[sigmaRotationb][2,3,1,3]\[Equal]{{0,0},{0,0}}

*)

(* s is by default with upper index, and sb is with lower index *)
generatorSpinUP[\[Mu]_,\[Nu]_][i_][a_]:=
	Sum[s[i][\[Alpha]]sigmaRotation[\[Mu],\[Nu]][[\[Alpha],\[Beta]]]D[a,s[i][\[Beta]]],{\[Alpha],1,2},{\[Beta],1,2}]+
	Sum[sb[i][\[Alpha]]sigmaRotationb[\[Mu],\[Nu]][[\[Alpha],\[Beta]]]D[a,sb[i][\[Beta]]],{\[Alpha],1,2},{\[Beta],1,2}];

generatorSpin[\[Mu]_,\[Nu]_][i_][a_]:=Sum[
	metric4D[[\[Mu],\[Mu]p]]metric4D[[\[Nu],\[Nu]p]]generatorSpinUP[\[Mu]p,\[Nu]p][i][a]
	,{\[Mu]p,1,spaceDimension},{\[Nu]p,1,spaceDimension}];
	
(*

Block[
   {L=generatorSpin[2,3][1],
	R=generatorSpin[1,3][1],
	U=generatorSpin[1,2][1],
	f=g[s[1][1],s[1][2],sb[1][1],sb[1][2]]},
	Expand[L[R[f]]-R[L[f]]+U[f]]\[Equal]0
] (* Returns True *)

*)


generator4pt[gen_][args___][a_]:=Sum[gen[args][i][a],{i,1,4}];
generatorNpt[gen_][pt__][args___][a_]:=Sum[gen[args][i][a],{i,{pt}}];

generators4ptList:=generators4ptList=
	Flatten[
		{generator4pt[generatorD][]}~Join~
		Table[generator4pt[generatorP][\[Mu]],{\[Mu],1,spaceDimension}]~Join~
		Table[generator4pt[generatorK][\[Mu]],{\[Mu],1,spaceDimension}]~Join~
		Table[generator4pt[generatorM][\[Mu],\[Nu]],{\[Mu],1,spaceDimension},{\[Nu],\[Mu]+1,spaceDimension}]
	];
	
(* Conformal frame definition sensitive *)
On[Assert];
Assert[DeleteDuplicates@Flatten[x[#][[2;;3]]&/@Range[1,4]]=={0}];
generators4ptNonStabilizer:=generators4ptNonStabilizer=DeleteCases[generators4ptList,generator4pt[generatorM][2,3]];


generic4ptCorrelator=genericFunction@@(
		Flatten[
		   Table[y[i][\[Mu]],{i,1,4},{\[Mu],1,spaceDimension}]~Join~
		   Table[s[i][\[Alpha]],{i,1,4},{\[Alpha],1,2}]~Join~ 
		   Table[sb[i][\[Alpha]],{i,1,4},{\[Alpha],1,2}]
		]
	);

CollectGenerics[a_,func___]:=Collect[a,genericFunction[__]|Derivative[__][genericFunction][__],func];

(* Conformal frame definition sensitive *)
On[Assert];
Assert[DeleteDuplicates@Flatten[x[#][[2;;3]]&/@Range[1,4]]=={0}];
Assert[And@@NumberQ/@Flatten[x[#]&/@{1,3,4}/.L->1]];
all4ptDerivatives:=all4ptDerivatives=Flatten@Table[
		D[generic4ptCorrelator,y[i][\[Mu]]],
		{i,1,4},{\[Mu],1,spaceDimension}];
unknown4ptDerivatives:=unknown4ptDerivatives=DeleteCases[
		all4ptDerivatives,
		D[generic4ptCorrelator,y[2][1]]|D[generic4ptCorrelator,y[2][4]]
	];


invariance4ptEquations:=invariance4ptEquations=Collect[
	#[generic4ptCorrelator]&/@generators4ptNonStabilizer,
	generic4ptCorrelator|Derivative[___][_][___],
	Simplify
];

logTime["Start {invariance4ptB,invariance4ptM}"];

(* invariance4ptM.unknown4ptDerivatives\[Equal]invariance4ptB *)
invariance4ptB:=First[{invariance4ptB,invariance4ptM}={-1,1}*(Normal/@CoefficientArrays[invariance4ptEquations,unknown4ptDerivatives])];
invariance4ptM:=Last[{invariance4ptB,invariance4ptM}={-1,1}*(Normal/@CoefficientArrays[invariance4ptEquations,unknown4ptDerivatives])];

(* Compute the first-order derivatives *)

cfRule = {y[i_][\[Mu]_]:>xpre[i][[\[Mu]]]};
invariance4ptB1stOrder:=First[{invariance4ptB1stOrder,invariance4ptM1stOrder}={invariance4ptB,invariance4ptM}/.cfRule//Simplify];
invariance4ptM1stOrder:=Last[{invariance4ptB1stOrder,invariance4ptM1stOrder}={invariance4ptB,invariance4ptM}/.cfRule//Simplify];

invariance4ptM1stOrderInverse:=invariance4ptM1stOrderInverse=Simplify@Inverse[invariance4ptM1stOrder];

unknown4ptDerivatives1stOrder:=unknown4ptDerivatives1stOrder=
	Collect[
		invariance4ptM1stOrderInverse.invariance4ptB1stOrder,
		generic4ptCorrelator|Derivative[___][_][___],
		Simplify
	];
	
all4ptDerivatives1stOrder:=all4ptDerivatives1stOrder=Module[{pos},
	Table[
		pos=Position[unknown4ptDerivatives,der];
		If[Length@pos==0,der,unknown4ptDerivatives1stOrder[[pos[[1,1]]]]]
	,{der,all4ptDerivatives}]
];


(* Compute the second-order derivatives *)

(* unknownDerivatives = invariance4ptM^-1.invariance4ptB *)
(* \!\(
\*SubscriptBox[\(\[PartialD]\), \(i\)]unknownDerivatives\) = (\!\(
\*SubscriptBox[\(\[PartialD]\), \(i\)]
\*SuperscriptBox[\(invariance4ptM\), \(-1\)]\)).invariance4ptB + invariance4ptM^-1.(\!\(
\*SubscriptBox[\(\[PartialD]\), \(i\)]invariance4ptB\)) *)
(* (\!\(
\*SubscriptBox[\(\[PartialD]\), \(i\)]
\*SuperscriptBox[\(invariance4ptM\), \(-1\)]\)) = - invariance4ptM^-1.(\!\(
\*SubscriptBox[\(\[PartialD]\), \(i\)]invariance4ptM\)).invariance4ptM^-1 *)
(* (\!\(
\*SubscriptBox[\(\[PartialD]\), \(i\)]invariance4ptB\)) needs to be evaluated using first order derivatives *)

derivativeVariable[Derivative[ns___][f_][vars___]]:={ns}.{vars};

ClearAll[invariance4ptMInverseDerivative];
invariance4ptMInverseDerivative[i_]:=invariance4ptMInverseDerivative[i]=
	 - invariance4ptM1stOrderInverse.(
		D[
		   invariance4ptM,derivativeVariable[all4ptDerivatives[[i]]]
		]/.cfRule).invariance4ptM1stOrderInverse//Simplify;

On[Assert];
Assert[DeleteDuplicates@Flatten[x[#][[2;;3]]&/@Range[1,4]]=={0}];
Assert[And@@NumberQ/@Flatten[x[#]&/@{1,3,4}/.L->1]];
known4ptDerivativeVars=
	Flatten[
		Table[s[i][\[Alpha]],{i,1,4},{\[Alpha],1,2}]~Join~
		Table[sb[i][\[Alpha]],{i,1,4},{\[Alpha],1,2}]~Join~
		{y[2][1],y[2][4]}
	];
	
logTime["Start ruleUse1stOrder"];
	
ruleUse1stOrder:=ruleUse1stOrder=Dispatch@Module[
	{d,i},
	Flatten@Table[
		d=derivativeVariable[unknown4ptDerivatives[[i]]];
		D[generic4ptCorrelator,k,d]->D[unknown4ptDerivatives1stOrder[[i]],k]
	,{k,known4ptDerivativeVars}
	,{i,1,Length@unknown4ptDerivatives}]
	~Join~
	Flatten@Table[
		unknown4ptDerivatives[[i]]->unknown4ptDerivatives1stOrder[[i]]
	,{i,1,Length@unknown4ptDerivatives}]
];

ClearAll[invariance4ptBDerivative];
invariance4ptBDerivative[i_]:=invariance4ptBDerivative[i]=
		Collect[D[
			invariance4ptB,
			derivativeVariable[all4ptDerivatives[[i]]]
		],genericFunction[___]|Derivative[___][_][___],Factor]/.ruleUse1stOrder/.cfRule //
		  Collect[#,genericFunction[___]|Derivative[___][_][___],Factor]&;
	

(* i is in all4ptDerivatives, j is in unknown4ptDerivatives *)
ClearAll[secondOrderDerivative];
secondOrderDerivative[i_,j_] := secondOrderDerivative[i,j] =
	(invariance4ptMInverseDerivative[i][[j]].invariance4ptB1stOrder)+
	(invariance4ptM1stOrderInverse[[j]].invariance4ptBDerivative[i]) //Collect[#,genericFunction[___]|Derivative[___][_][___],Simplify]&;


composeSimpleDerivatives[Derivative[ns__][f_][args__],Derivative[ms__][f_][args__]]:=Derivative[Sequence@@({ns}+{ms})][f][args];
spliceSecondOrderDerivative[Derivative[ns__][f_][args__]]:=Module[{v={ns}},
	If[Length@Position[v,1]==0,
		{Derivative[Sequence@@(v/2)][f][args],Derivative[Sequence@@(v/2)][f][args]},
		Derivative[Sequence@@#][f][args]&/@(UnitVector[Length@v,#[[1]]]&/@Position[v,1])
	]
]


secondOrderDerivativeRule={
	d:(Derivative[ns__][genericFunction][args__]/;Total[{ns}[[1;;16]]]==2):>Module[{a,b,i,j,res},
		{a,b}=spliceSecondOrderDerivative[d];
		(*Print[{a,b}];*)
		{i,j}=SortBy[{Position[unknown4ptDerivatives,#],#}&/@{a,b},Length@*First];
		(*Print[{i,j}];*)
		If[Length@(j[[1]])==0,
			res=d/.cfRule,
			j=j[[1,1,1]];
			i=Position[all4ptDerivatives,i[[2]]][[1,1]];
			(*Print[{i,j}];*)
			res=secondOrderDerivative[i,j];
		];
		res
	],
	d:(Derivative[ns__][genericFunction][args__]/;Total[{ns}[[1;;16]]]==1):>Module[{ypart,spart,i,svar},
		 ypart=PadRight[{ns}[[1;;16]],Length@{ns}];
		 spart={ns}-ypart;
		 i=Position[all4ptDerivatives,Derivative[Sequence@@ypart][genericFunction][args]][[1,1]];
		 svar=Times@@((List@@generic4ptCorrelator)^spart);
		 If[svar===1,all4ptDerivatives1stOrder[[i]]/.cfRule,
			 D[all4ptDerivatives1stOrder[[i]]/.cfRule,svar]
		 ]
	],
	d:genericFunction[args__]:>(d/.cfRule)
};


(*resolve4DDerivatives[expr_]:=expr/.secondOrderDerivativeRule;*)
takeWithMultiplicities[list_,mults_]:=Flatten[MapThread[ConstantArray,{list,mults},1],1];
resolve4DDerivatives[expr_]:=expr/.{
	d:Derivative[ns__][genericFunction][args__]:>Module[{v={ns},vsfree,dwithouts,sders},
		vsfree=PadRight[v[[1;;4*spaceDimension]],Length@v];
		dwithouts = (Derivative[Sequence@@vsfree][genericFunction][args])/.secondOrderDerivativeRule;
		sders = takeWithMultiplicities[{args}[[4 spaceDimension+1;;-1]],v[[4 spaceDimension+1;;-1]]];
		D[dwithouts,Sequence@@sders]
	],
	d:genericFunction[args__]:>(d/.cfRule)
};



(* ::Subsubsubsection::Closed:: *)
(*Transformation into CF rules*)


logTime["End CF derivatives"];

toGreek={s[i_][1]:>\[Eta][i],s[i_][2]:>-\[Xi][i],sb[i_][1]:>\[Xi]b[i],sb[i_][2]:>\[Eta]b[i],y[2][1]:>1/2(zb-z),y[2][4]:>1/2 (z+zb)};
toLatin={\[Eta][i_]:>s[i][1],\[Xi][i_]:>-s[i][2],\[Xi]b[i_]:>sb[i][1],\[Eta]b[i_]:>sb[i][2],z:>-y[2][1]+y[2][4],zb:>y[2][1]+y[2][4]};


ClearAll[derivativeList];
derivativeList[genericFunction[args__]]={};
derivativeList[Derivative[ns__][genericFunction][args__]]:=Module[{nsl={ns}},
	DeleteCases[Inner[List,List@@generic4ptCorrelator,nsl,List],{_,0}]
]


genericFunctionSubstitute[expr_,f_]:=Module[{},
	expr/.{
		genericFunction[args__]->f,
		d:Derivative[ns__][genericFunction][args__]:>D[f,Sequence@@derivativeList[d]]
	}
]


generic4DToCFExpression[genericFunctionResult_]:=Module[{tmp},
	tmp=genericFunctionSubstitute[genericFunctionResult/.cfRule,(genericCFstruct//expandCFStructs)/.toLatin];
	tmp=collapseCFStructs[{\[CapitalDelta][1],\[CapitalDelta][2],\[CapitalDelta][3],\[CapitalDelta][4]}][tmp/.toGreek];
	tmp(*//simplifyInCF4pt*)
]


CF4ptPattern=CF4pt[{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_,\[CapitalDelta]4_},{l1_,l2_,l3_,l4_},{lb1_,lb2_,lb3_,lb4_},{q1_,q2_,q3_,q4_},{qb1_,qb2_,qb3_,qb4_},g_];
cfExpressionToRuleCF[expression_,dimshift_:{0,0,0,0}]:=ReleaseHold@Module[{temp},
{
CF4ptPattern:>temp
}/.{temp->(expression/.{CF4pt[ds_,rest__]:>CF4pt[ds+dimshift,rest]}/.{
	g[z,zb]->g,
	Derivative[1,0][g][z,zb]->Hold[D[g,z]],
	Derivative[0,1][g][z,zb]->Hold[D[g,zb]],
	Derivative[1,1][g][z,zb]->Hold[D[g,z,zb]],
	Derivative[2,0][g][z,zb]->Hold[D[g,{z,2}]],
	Derivative[0,2][g][z,zb]->Hold[D[g,{zb,2}]],
	l[1]->l1,l[2]->l2,l[3]->l3,l[4]->l4,
	\[CapitalDelta][1]->\[CapitalDelta]1,\[CapitalDelta][2]->\[CapitalDelta]2,\[CapitalDelta][3]->\[CapitalDelta]3,\[CapitalDelta][4]->\[CapitalDelta]4,
	lb[1]->lb1,lb[2]->lb2,lb[3]->lb3,lb[4]->lb4,
	q[1]->q1,q[2]->q2,q[3]->q3,q[4]->q4,
	qb[1]->qb1,qb[2]->qb2,qb[3]->qb3,qb[4]->qb4
	})}
];


generic4DToCFRule[expr_]:=cfExpressionToRuleCF@generic4DToCFExpression@expr;


(* ::Subsubsubsection::Closed:: *)
(*Casimir operator*)


opCasimir24D[pts__][f_]:=-1/2 (-2 generatorNpt[generatorD][pts][][generatorNpt[generatorD][pts][][f]] +
					   Sum[generatorNpt[generatorP][pts][\[Mu]][generatorNpt[generatorK][pts][\[Nu]][f]] metric4D[[\[Mu],\[Nu]]],{\[Mu],1,spaceDimension},{\[Nu],1,spaceDimension}]+
					   Sum[generatorNpt[generatorK][pts][\[Mu]][generatorNpt[generatorP][pts][\[Nu]][f]] metric4D[[\[Mu],\[Nu]]],{\[Mu],1,spaceDimension},{\[Nu],1,spaceDimension}]+
					   Sum[generatorNpt[generatorM][pts][\[Mu],\[Nu]][generatorNpt[generatorM][pts][\[Mu]p,\[Nu]p][f]] metric4D[[\[Mu],\[Mu]p]]metric4D[[\[Nu],\[Nu]p]],{\[Mu],1,spaceDimension},{\[Nu],1,spaceDimension},{\[Mu]p,1,spaceDimension},{\[Nu]p,1,spaceDimension}]
					   );


(* ::Subsubsection::Closed:: *)
(*6D derivatives to 4D derivatives*)


(* We don't need more than 1st order in X, so for simlicity we don't do 2nd or higher (although it is not hard to generilize) *)


ClearAll[D6D]; (* Eh... never forget to put this before the definition you are debugging >_< *)

D6D[a_,X6D[i_][mu_]]/;1<=mu<=spaceDimension := D[a,y[i][mu]];
D6D[a_,X6D[i_][6]]:=0; (* "-" component *)
D6D[a_,X6D[i_][5]]:=(-\[CapitalDelta][i]-(l[i]+lb[i])/2)a-Sum[y[i][\[Nu]]D[a,y[i][\[Nu]]],{\[Nu],1,spaceDimension}];

D6D[a_,S6D[i_][1]]:=-D[a,s[i][2]];
D6D[a_,S6D[i_][2]]:=D[a,s[i][1]];
D6D[a_,S6D[i_][3]]:=0;
D6D[a_,S6D[i_][4]]:=0;

D6D[a_,Sb6D[i_][3]]:=D[a,sb[i][1]];
D6D[a_,Sb6D[i_][4]]:=D[a,sb[i][2]];
D6D[a_,Sb6D[i_][1]]:=0;
D6D[a_,Sb6D[i_][2]]:=0;

(* Notation more consistent with 6D formalism, not used everywhere below *)

DS4d[expr_][i_,a_]:=D6D[expr,S6D[i][a]];
DSb4d[expr_][i_,a_]:=D6D[expr,Sb6D[i][a]];

DX4d[expr_][i_,a_,b_]:=Module[{m},Sum[D6D[expr,X6D[i][m]]Sigma[[m,a,b]],{m,1,6}]];
DXb4d[expr_][i_,a_,b_]:=Module[{m},Sum[D6D[expr,X6D[i][m]]Sigmab[[m,a,b]],{m,1,6}]];

(* ::Subsubsubsection:: *)
(*The 6D operators*)


pairFor[1]=2;
pairFor[2]=1;
pairFor[3]=4;
pairFor[4]=3;

ClearAll[opD4D]
opD4D[i_,j_][a_]:=Module[{M,N},
	Sum[1/2 (SbvectorPre[i].Sigma[[M]].Sigmab[[N]].SvectorPre[i])(
			(metricD.XvectorPre[j])[[M]]D6D[a,X6D[i][N]]-(metricD.XvectorPre[j])[[N]]D6D[a,X6D[i][M]]
		),{M,1,6},{N,1,6}]
	];
	
opI4D[i_,j_][a_]:=(*(-2XvectorPre[i].metricD.XvectorPre[j])^(-1/2)*) SbvectorPre[i].SvectorPre[j]a;

opDt4D[i_,j_][expr_]:=Module[{M,a},
	Sum[SbvectorPre[i].(metricD.XvectorPre[j].Sigma).Sigmab[[M]].SvectorPre[i]D6D[expr,X6D[j][M]],{M,1,6}]+
	2*opI4D[i,j][Sum[SvectorPre[i][[a]]D6D[expr,S6D[j][a]],{a,1,4}]]-
	2*opI4D[j,i][Sum[SbvectorPre[i][[a]]D6D[expr,Sb6D[j][a]],{a,1,4}]]
	];
	
opd4D[i_,j_][expr_]:=Module[{a},
	(*(-2XvectorPre[i].metricD.XvectorPre[j])^(-1/2)*) Sum[(SvectorPre[j].(metricD.XvectorPre[i].Sigmab))[[a]]D6D[expr,Sb6D[i][a]],{a,1,4}]
	];
	
opdb4D[i_,j_][expr_]:=Module[{a},
	(*(-2XvectorPre[i].metricD.XvectorPre[j])^(-1/2)*) Sum[(SbvectorPre[j].(metricD.XvectorPre[i].Sigma))[[a]]D6D[expr,S6D[i][a]],{a,1,4}]
	];

(* the order of arguments is intentional -- fixing a bug *)
opN4D[j_,i_][expr_]:=Module[{a,b},
	(*(-2XvectorPre[i].metricD.XvectorPre[j])^(-1/2)*) Sum[((metricD.XvectorPre[i].Sigmab).(metricD.XvectorPre[j].Sigma))[[a,b]](*/XvectorPre[i].metricD.XvectorPre[j]*) D6D[D6D[expr,Sb6D[i][a]],S6D[j][b]],{a,1,4},{b,1,4}]
	];
	
difSbS4D[expr_][i_,a_,c_]:=Module[
{
	b
},
	4D6D[D6D[expr,Sb6D[i][c]],S6D[i][a]]+
	Sum[SvectorPre[i][[b]]D6D[D6D[D6D[expr,Sb6D[i][c]],S6D[i][a]],S6D[i][b]],{b,1,4}]+
	Sum[SbvectorPre[i][[b]]D6D[D6D[D6D[expr,Sb6D[i][c]],S6D[i][a]],Sb6D[i][b]],{b,1,4}]-
	Sum[SvectorPre[i][[c]]D6D[D6D[D6D[expr,Sb6D[i][b]],S6D[i][b]],S6D[i][a]],{b,1,4}]-
	Sum[SbvectorPre[i][[a]]D6D[D6D[D6D[expr,Sb6D[i][b]],S6D[i][b]],Sb6D[i][c]],{b,1,4}]
];

difX4D[expr_][i_,a_,c_]:=Module[
{
	M
},
	1/4 Sum[
		(((metricD.XvectorPre[i].Sigma).Sigmab[[M]])[[a,c]]
		-(Sigma[[M]].(metricD.XvectorPre[i].Sigmab))[[a,c]])D6D[expr,X6D[i][M]]
		,{M,1,6}]
];



opConservation4D[i_][expr_]:=Module[
{
	a,c
},
	Sum[
		difX4D[difSbS4D[expr][i,a,c]][i,a,c]
		,{a,1,4}
		,{c,1,4}
	]
]


aCoef4d[i_]:= 1-\[CapitalDelta][i]+l[i]/2-lb[i]/2;
aCoefb4d[i_]:= 1-\[CapitalDelta][i]-l[i]/2+lb[i]/2;

bCoef4d[i_]:=2(lb[i]+1);
bCoefb4d[i_]:=2(l[i]+1);

cCoef4d[i_]:=-2+\[CapitalDelta][i]-(l[i]+lb[i])/2;

spinorDb4D[-(1/2), +1, 0][i_][a_][expr_]:=expr SvectorPre[i][[a]];

spinorDb4D[+(1/2), 0, +1][i_][a_][expr_]:=Module[{b,c}, 
	Sum[
		aCoef4d[i] SbvectorPre[i][[b]]DX4d[expr][i,a,b],
		{b,1,4}
	]+
	Sum[
	 SvectorPre[i][[a]]SbvectorPre[i][[b]]D6D[DX4d[expr][i,b,c],S6D[i][c]]
	,{b,1,4}
	,{c,1,4}
	]
];

spinorDb4D[-(1/2), -1, 0][i_][a_][expr_]:=Module[{b},
	Sum[
	XmatrixPre[i][[a,b]]D6D[expr,S6D[i][b]]
	,{b,1,4}]
];

spinorDb4D[+(1/2), 0, -1][i_][a_][expr_]:=Module[{b,c,d,
term1,term2,term3,term4
},
	term1=bCoef4d[i]cCoef4d[i] D6D[expr,Sb6D[i][a]];
	term2=bCoef4d[i] SvectorPre[i][[a]] Sum[D6D[D6D[expr,S6D[i][b]],Sb6D[i][b]],{b,1,4}];
	term3=cCoef4d[i] Sum[XbmatrixPre[i][[b,c]]D6D[DX4d[expr][i,a,b],Sb6D[i][c]],{b,1,4},{c,1,4}];
	term4=-SvectorPre[i][[a]]Sum [XbmatrixPre[i][[b,c]]D6D[D6D[DX4d[expr][i,b,d],Sb6D[i][c]],S6D[i][d]],{b,1,4},{c,1,4},{d,1,4}];
	
	term1+term2+term3+term4
];

(*Rising and lowering operators in anti-fundamental representation*)
spinorD4D[-(1/2), 0, +1][i_][a_][expr_]:=expr SbvectorPre[i][[a]];

spinorD4D[+(1/2), +1, 0][i_][a_][expr_]:=Module[{b,c}, 
	Sum[
		aCoefb4d[i] SvectorPre[i][[b]]DXb4d[expr][i,a,b]
	,{b,1,4}]
	+Sum[
		SbvectorPre[i][[a]]SvectorPre[i][[b]]DSb4d[DXb4d[expr][i,b,c]][i,c]
	,{b,1,4},{c,1,4}]
];

spinorD4D[-(1/2), 0, -1][i_][a_][expr_]:=Module[{b},
	Sum[XbmatrixPre[i][[a,b]]DSb4d[expr][i,b],{b,1,4}]
];

spinorD4D[+(1/2), -1, 0][i_][a_][expr_]:=Module[
{
	b,c,d,
	term1,term2,term3,term4
},
	term1=bCoefb4d[i]*cCoef4d[i]DS4d[expr][i,a];
	term2=bCoefb4d[i]SbvectorPre[i][[a]]Sum[DS4d[DSb4d[expr][i,b]][i,b],{b,1,4}];
	term3=cCoef4d[i] Sum[XmatrixPre[i][[b,c]]DS4d[DXb4d[expr][i,a,b]][i,c],{b,1,4},{c,1,4}];
	term4=-SbvectorPre[i][[a]]Sum[XmatrixPre[i][[b,c]]DSb4d[DS4d[DXb4d[expr][i,b,d]][i,c]][i,d],{b,1,4},{c,1,4},{d,1,4}];
	
	term1+term2+term3+term4
];

opContracted4D[op1_,op2_][expr_]:=Module[{a},
	Sum[op1[a]@op2[a]@expr,{a,1,4}]
];

dShift[opD4D][i_,j_]:=-UnitVector[4,j];
dShift[opDt4D][i_,j_]:=-UnitVector[4,i];
dShift[opI4D][i_,j_]:=-1/2 (UnitVector[4,i]+UnitVector[4,j]);
dShift[opd4D][i_,j_]:=-1/2 (UnitVector[4,i]+UnitVector[4,j]);
dShift[opdb4D][i_,j_]:=-1/2 (UnitVector[4,i]+UnitVector[4,j]);
dShift[opN4D][i_,j_]:=-1/2 (UnitVector[4,i]+UnitVector[4,j]);
dShift[opCasimir24D][pts__]:={0,0,0,0};
dShift[opConservation4D][i_]:=UnitVector[4,i];

dShift[opContracted4D][spinorD4D[deltai_,_,_][i_],spinorDb4D[deltaj_,_,_][j_]]:=UnitVector[4,i]deltai+UnitVector[4,j]deltaj;
dShift[opContracted4D][spinorDb4D[deltai_,_,_][i_],spinorD4D[deltaj_,_,_][j_]]:=UnitVector[4,i]deltai+UnitVector[4,j]deltaj;

specialDimensionRule[opConservation4D][i_]:={\[CapitalDelta][i]->2+(l[i]+lb[i])/2};
specialDimensionRule[_][__]:={};

operatorRule[\[CapitalXi][op_]][params___]:=(operatorRule[op][params]/.{a:(lhs_:>rhs_):>(
MapAt[#/.{\[CapitalDelta][i_]:>\[CapitalDelta][i]-dShift[op][params][[i]]}&,a,2
])});


operatorListRule[\[CapitalXi][op_]][params___]:=(operatorListRule[op][params]/.{a:(lhs_:>rhs_):>(
MapAt[#/.{\[CapitalDelta][i_]:>\[CapitalDelta][i]-dShift[op][params][[i]]}&,a,2
])});


ClearAll[RationalLLimit];
RationalLLimit[expr_]:=Module[{tmp,den,num,enum,eden},
	tmp=Expand[expr];
	(*Print["expanded"];*)
	tmp=If[Head[tmp]===Plus,List@@tmp,{tmp}];
	den=PolynomialLCM@@(Denominator/@tmp);
	(*Print["lcm done"];*)
	tmp=Factor[den #]&/@tmp;
	(*Print["cancelled"];*)
	num=Total@Expand[tmp];
	
	If[PolynomialQ[num,L]&&PolynomialQ[den,L],
		enum=Exponent[num,L];
		eden=Exponent[den,L];
		
		Which[
			enum==eden, Last@CoefficientList[num,L]/Last@CoefficientList[den,L]//Factor,
			enum>eden,  ComplexInfinity,
			eden>enum,  0
		]
		
		,Limit[num/den,L->\[Infinity]]
	]
]

(* MonitorSafe = Monitor *)
MonitorSafe[expr_,args__]:=expr;

operatorRulePre[operator_][params___]:=Module[{dshift=dShift[operator][params],tmp,i},
	(*Print["Resolving derivatives.."];*)
	tmp=CollectGenerics[CollectGenerics[operator[params][generic4ptCorrelator],Simplify]//resolve4DDerivatives];
	tmp=If[Head[tmp]===Plus,List@@tmp,{tmp}];
	(*Print["Assembling rule"];*)
	tmp=MonitorSafe[Table[generic4DToCFExpression[tmp[[i]]],{i,1,Length@tmp}],{i,Length@tmp}];
	tmp=Flatten[If[Head[#]===Plus,List@@#,{#}]&/@tmp];
	tmp=DeleteCases[tmp,0];
	tmp=Plus@@@GatherBy[tmp,#[[1;;-2]]&];
	(*tmp=MonitorSafe[Table[tmp[[i]]//collectGInCFS4pt[Factor],{i,1,Length@tmp}],{i,Length@tmp}];*)
	(*Print["Taking limit"];*)
	tmp=MonitorSafe[
			Table[tmp[[i]]/.specialDimensionRule[operator][params]
				//collectGInCF4pt[g][RationalLLimit[L^(2dshift[[InfinitePoint]]) #]&]
			,{i,1,Length@tmp}]
		,{i,Length@tmp}];
	(*Print["Simplifying and getting the rule"];*)
	tmp/.{d:Derivative[__][_][__]:>Simplify[d],d:g[__]:>Simplify[d]}
];

operatorRule[operator_][params___]:=Module[{dshift=dShift[operator][params],tmp},
	tmp=Plus@@operatorRulePre[operator][params];
	cfExpressionToRuleCF[tmp,dshift]
];

operatorListRule[operator_][params___]:=Module[{dshift=dShift[operator][params],tmp},
	tmp=operatorRulePre[operator][params];
	cfExpressionToRuleCF[tmp,dshift]
];

applyListRule[listrule_][expr_]:=Module[{tmp},
	tmp=Flatten[expr/.listrule];
	tmp=DeleteCases[tmp,0];
	Plus@@@GatherBy[tmp,#[[1;;-2]]&]
];



(* ::Subsection::Closed:: *)
(*Permutations*)


(* ::Subsubsection::Closed:: *)
(*Semi-Covariant form*)


kinematics[{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_,\[CapitalDelta]4_},{p1_,p2_,p3_,p4_}][{z1_,zb1_},{z2_,zb2_},{z3_,zb3_},{z4_,zb4_}]:=Module[{expr,scale,spin,z13,z14,z24,z34,h1,h2,h3,h4},
	expr=z13^(-h1-h2-h3+h4) z14^(-h1+h2+h3-h4) z24^(-2h2) z34^(h1+h2-h3-h4);
	scale=expr/.{h1->\[CapitalDelta]1,h2->\[CapitalDelta]2,h3->\[CapitalDelta]3,h4->\[CapitalDelta]4,z13->Sqrt[(z1-z3)(zb1-zb3)],z14->Sqrt[(z1-z4)(zb1-zb4)],z24->Sqrt[(z2-z4)(zb2-zb4)],z34->Sqrt[(z3-z4)(zb3-zb4)]};
	spin=expr/.{h1->p1,h2->p2,h3->p3,h4->p4,z13->-(z1-z3)/Sqrt[(z1-z3)(zb1-zb3)],z14->-(z1-z4)/Sqrt[(z1-z4)(zb1-zb4)],z24->-(z2-z4)/Sqrt[(z2-z4)(zb2-zb4)],z34->-(z3-z4)/Sqrt[(z3-z4)(zb3-zb4)]};
	Simplify[scale*spin]
]

kinematics[{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_,\[CapitalDelta]4_},{p1_,p2_,p3_,p4_}][{z1_,zb1_},{z2_,zb2_},{z3_,zb3_},{\[Infinity],\[Infinity]}]:=Module[{expr,scale,spin,z13,z14,z24,z34,h1,h2,h3,h4},
	expr=z13^(-h1-h2-h3+h4) (*z14^(-h1+h2+h3-h4) z24^(-2h2) z34^(h1+h2-h3-h4)*);
	scale=expr/.{h1->\[CapitalDelta]1,h2->\[CapitalDelta]2,h3->\[CapitalDelta]3,h4->\[CapitalDelta]4,z13->Sqrt[(z1-z3)(zb1-zb3)],z14->1,z24->1,z34->1};
	spin=expr/.{h1->p1,h2->p2,h3->p3,h4->p4,z13->-(z1-z3)/Sqrt[(z1-z3)(zb1-zb3)],z14->1,z24->1,z34->1};
	Simplify[scale*spin]
]

kinematics[{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_,\[CapitalDelta]4_},{p1_,p2_,p3_,p4_}][{\[Infinity],\[Infinity]},{z2_,zb2_},{z3_,zb3_},{z4_,zb4_}]:=Module[{expr,scale,spin,z13,z14,z24,z34,h1,h2,h3,h4},
	expr=z13^(-h1-h2-h3+h4) z14^(-h1+h2+h3-h4) z24^(-2h2) z34^(h1+h2-h3-h4);
	scale=expr/.{h1->\[CapitalDelta]1,h2->\[CapitalDelta]2,h3->\[CapitalDelta]3,h4->\[CapitalDelta]4,z13->1,z14->1,z24->Sqrt[(z2-z4)(zb2-zb4)],z34->Sqrt[(z3-z4)(zb3-zb4)]};
	spin=expr/.{h1->p1,h2->p2,h3->p3,h4->p4,z13->-1,z14->-1,z24->-(z2-z4)/Sqrt[(z2-z4)(zb2-zb4)],z34->-(z3-z4)/Sqrt[(z3-z4)(zb3-zb4)]};
	Simplify[scale*spin]
]

kinematics[{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_,\[CapitalDelta]4_},{p1_,p2_,p3_,p4_}][{z1_,zb1_},{\[Infinity],\[Infinity]},{z3_,zb3_},{z4_,zb4_}]:=Module[{expr,scale,spin,z13,z14,z24,z34,h1,h2,h3,h4},
	expr=z13^(-h1-h2-h3+h4) z14^(-h1+h2+h3-h4) z24^(-2h2) z34^(h1+h2-h3-h4);
	scale=expr/.{h1->\[CapitalDelta]1,h2->\[CapitalDelta]2,h3->\[CapitalDelta]3,h4->\[CapitalDelta]4,z13->Sqrt[(z1-z3)(zb1-zb3)],z14->Sqrt[(z1-z4)(zb1-zb4)],z24->1,z34->Sqrt[(z3-z4)(zb3-zb4)]};
	spin=expr/.{h1->p1,h2->p2,h3->p3,h4->p4,z13->-(z1-z3)/Sqrt[(z1-z3)(zb1-zb3)],z14->-(z1-z4)/Sqrt[(z1-z4)(zb1-zb4)],z24->-1,z34->-(z3-z4)/Sqrt[(z3-z4)(zb3-zb4)]};
	Simplify[scale*spin]
]

kinematics[{\[CapitalDelta]1_,\[CapitalDelta]2_,\[CapitalDelta]3_,\[CapitalDelta]4_},{p1_,p2_,p3_,p4_}][{z1_,zb1_},{z2_,zb2_},{\[Infinity],\[Infinity]},{z4_,zb4_}]:=Module[{expr,scale,spin,z13,z14,z24,z34,h1,h2,h3,h4},
	expr=z13^(-h1-h2-h3+h4) z14^(-h1+h2+h3-h4) z24^(-2h2) z34^(h1+h2-h3-h4);
	scale=expr/.{h1->\[CapitalDelta]1,h2->\[CapitalDelta]2,h3->\[CapitalDelta]3,h4->\[CapitalDelta]4,z13->1,z14->Sqrt[(z1-z4)(zb1-zb4)],z24->Sqrt[(z2-z4)(zb2-zb4)],z34->1};
	spin=expr/.{h1->p1,h2->p2,h3->p3,h4->p4,z13->1,z14->-(z1-z4)/Sqrt[(z1-z4)(zb1-zb4)],z24->-(z2-z4)/Sqrt[(z2-z4)(zb2-zb4)],z34->-1};
	Simplify[scale*spin]
]


cfEvaluateInPlane[{z1_,zb1_},{z2_,zb2_},{z3_,zb3_},{z4_,zb4_}][expr_]:=Module[
{
	znew
	,zbnew
	,Lnew
	,safeRule
	,z1safe,zb1safe
	,z2safe,zb2safe
	,z3safe,zb3safe
	,z4safe,zb4safe
},
	safeRule={\[Infinity]->Lnew};
	{z1safe,z2safe,z3safe,z4safe}={z1,z2,z3,z4}/.safeRule;
	{zb1safe,zb2safe,zb3safe,zb4safe}={zb1,zb2,zb3,zb4}/.safeRule;
	znew=Limit[((z1safe-z2safe)(z3safe-z4safe))/((z1safe-z3safe)(z2safe-z4safe)),Lnew->\[Infinity]];
	zbnew=Limit[((zb1safe-zb2safe)(zb3safe-zb4safe))/((zb1safe-zb3safe)(zb2safe-zb4safe)),Lnew->\[Infinity]];
	expr/.{
		CF4pt[\[CapitalDelta]s_,ls_,lbs_,qs_,qbs_,g_]:>Module[{gnew},
			gnew=g/.{z->znew,zb->zbnew};
			gnew=gnew*kinematics[\[CapitalDelta]s,qs+qbs][{z1,zb1},{z2,zb2},{z3,zb3},{z4,zb4}];
			CF4pt[\[CapitalDelta]s,ls,lbs,qs,qbs,gnew]
		]
	}
];


inversePermList4[perm_][list_]:=Module[{tmp={0,0,0,0}},
	tmp[[perm[1]]]=list[[1]];
	tmp[[perm[2]]]=list[[2]];
	tmp[[perm[3]]]=list[[3]];
	tmp[[perm[4]]]=list[[4]];
	tmp
]


cfApplyPermutation[perm_][expr_]:=Module[
	{zs={0,z,1,\[Infinity]}
	,zbs={0,zb,1,\[Infinity]}
	,temp},
	temp=cfEvaluateInPlane[
			{zs[[perm[1]]],zbs[[perm[1]]]}
			,{zs[[perm[2]]],zbs[[perm[2]]]}
			,{zs[[perm[3]]],zbs[[perm[3]]]}
			,{zs[[perm[4]]],zbs[[perm[4]]]}
		][expr];
	temp/.{
		CF4pt[\[CapitalDelta]s_,ls_,lbs_,qs_,qbs_,g_]:>
			CF4pt[
				inversePermList4[perm][\[CapitalDelta]s]
				,inversePermList4[perm][ls]
				,inversePermList4[perm][lbs]
				,inversePermList4[perm][qs]
				,inversePermList4[perm][qbs]
				,g
			]
	}
]


(* ::Section::Closed:: *)
(*End*)


logTime["Done"];
Remove[timeStart,timeStamp,logTime];

End[]

Protect["CFTs4D`*"];
(* If we also protect the private functions, then we need to explcitly unprotect the memoized ones below.
   The delayed imports work OK with protecting the private functions.
 *)
(*Unprotect/@{generators4ptList, generators4ptNonStabilizer, all4ptDerivatives,unknown4ptDerivatives, invariance4ptEquations,invariance4ptM1stOrderInverse, unknown4ptDerivatives1stOrder,all4ptDerivatives1stOrder, invariance4ptMInverseDerivative,ruleUse1stOrder, invariance4ptBDerivative, secondOrderDerivative};
*)


EndPackage[]

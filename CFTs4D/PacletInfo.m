(* Paclet Info File *)

(* created 2018/09/11*)

Paclet[
    Name -> "CFTs4D",
    Version -> "1.1.0",
    MathematicaVersion -> "10+",
    Description -> "This package provides tools for conformal bootstrap calculations in 4D Conformal Fields Theories. Theoretical description of these tools is provided in arXiv:1705.05401",
    Creator -> "Denis Karateev, Petr Kravchuk",
    Extensions -> 
        {
            {"Documentation", Language -> "English", MainPage -> "Guides/CFTs4D"}
        }
]


